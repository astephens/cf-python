#!/usr/bin/python2
#-*-python-*-

import tempfile
import os
import sys
import numpy
import cf

import atexit

'''
Units tests for the cf package.

'''

tmpfile  = tempfile.mktemp('.nc')
tmpfile2 = tempfile.mktemp('.nca')
tmpfiles = [tmpfile, tmpfile2]
def _remove_tmpfiles():
    '''
'''
    for f in tmpfiles:
        try:
            os.remove(f)
        except OSError:
            pass
#--- End: def
atexit.register(_remove_tmpfiles)

print '\n--------------------------------------------------------------------'
print 'TEST: Set chunk size:'

# Save original chunksize
original_chunksize = cf.CHUNKSIZE()

cf.CHUNKSIZE(60)
print 'CHUNKSIZE reset to',cf.CHUNKSIZE()

print '\n--------------------------------------------------------------------'
print "TEST: Create a field:"

# Dimension coordinates
dim0 = cf.Coordinate(data=cf.Data(numpy.arange(10.), 'degrees'))
dim0.standard_name = 'grid_latitude'
 
dim1 = cf.Coordinate(data=cf.Data(numpy.arange(9.) + 20, 'degrees'))
dim1.standard_name = 'grid_longitude'
dim1.Data[-1] += 5
bounds = cf.Data(numpy.array([dim1.Data.array-0.5, dim1.Data.array+0.5]).transpose((1,0)))
bounds[-2,1] = 30
bounds[-1,:] = [30, 36]
dim1.insert_bounds(cf.CoordinateBounds(data=bounds))

dim2 = cf.Coordinate(data=cf.Data(1.5), bounds=cf.Data([1, 2.]))
dim2.standard_name = 'atmosphere_hybrid_height_coordinate'

# Auxiliary coordinates
aux0 = cf.Coordinate(data=cf.Data(10., 'm'))
aux0.id = 'atmosphere_hybrid_height_coordinate_ak'
aux0.insert_bounds(cf.Data([5, 15.], aux0.Units))

aux1 = cf.Coordinate(data=cf.Data(20.))
aux1.id = 'atmosphere_hybrid_height_coordinate_bk'
aux1.insert_bounds(cf.Data([14, 26.]))

aux2 = cf.Coordinate(data=cf.Data(numpy.arange(-45, 45, dtype='int32').reshape(10, 9),
                                  units='degree_N'))
aux2.standard_name = 'latitude'

aux3 = cf.Coordinate(data=cf.Data(numpy.arange(60, 150, dtype='int32').reshape(9, 10),
                                  units='degreesE'))
aux3.standard_name = 'longitude'

# Cell measures
cm0 = cf.CellMeasure(data=cf.Data(numpy.arange(90.).reshape(9, 10)*1234, 'km 2'))
cm0.measure = 'area'

# Transforms
trans1 = cf.Transform(grid_mapping_name='rotated_latitude_longitude',
                      grid_north_pole_latitude=38.0,
                      grid_north_pole_longitude=190.0)
            
# Data          
data = cf.Data(numpy.arange(90.).reshape(10, 9), 'm s-1')

# Domain
domain = cf.Domain(dim0=dim0, dim1=dim1, dim2=dim2,
                   auxs=[aux0, aux1, aux2, aux3],
                   trans1=trans1,
                   cms=cm0,
                   dimensions={'aux0': ['dim2'],
                               'aux1': ['dim2'],
                               'aux3': ['dim1', 'dim0'],
                               'cm0' : ['dim1', 'dim0']},
                   transform_map={'trans1': ['dim1', 'dim0']}
                   )

properties = {'standard_name': 'eastward_wind'}

f = cf.Field(properties=properties, domain=domain, data=data) 

orog = f.copy()
orog.standard_name = 'surface_altitude'
orog.Data = cf.Data(f.array*2, 'm')
orog.squeeze()
orog.domain.squeeze('dim2')
orog.transpose([1, 0])
orog.finalize()
t = cf.Transform(a='aux0', b='aux1', orog=orog)
t.name = 'atmosphere_hybrid_height_coordinate'

f.domain.insert_transform(t, coords=('dim2',))

print  f.Data.dumpd()

# Ancillary variables
tmp = f.copy()
del tmp.coord('atmosphere_hybrid_height_coordinate', exact=True).transforms
tmp.domain.transforms = cf.CfDict()
tmp.domain.remove_coordinate('aux0')
tmp.domain.remove_coordinate('aux1')

f.ancillary_variables = cf.AncillaryVariables()

g = tmp.copy()
g.transpose([1,0])
g.standard_name = 'ancillary0'
g *= 0.01
f.ancillary_variables.append(g) 

g = tmp.copy()
g.domain.squeeze('dim2')
g.standard_name = 'ancillary1'
g *= 0.01
f.ancillary_variables.append(g) 

g = tmp.copy()
g = g.subspace[0]
g.squeeze(domain=True)
g.standard_name = 'ancillary2'
g *= 0.001
f.ancillary_variables.append(g)

g = tmp.copy()
g = g.subspace[..., 0]
g.squeeze(domain=True)
g.domain.squeeze('dim2')
g.standard_name = 'ancillary3'
g *= 0.001
f.ancillary_variables.append(g)

#ff.ancillary_variables = f.ancillary_variables.copy()

#f.cell_methods = cf.CellMethods('height: mean area: mean')
#f.cell_methods[0].dim  = ['dim2']
#
#f.cell_methods[1].dim  = ['dim0', 'dim1']
#f.cell_methods[1]['name']     = ['grid_latitude'               , 'grid_longitude']
#f.cell_methods[1]['interval'] = [cf.Data(0.1, 'degrees_N'), cf.Data(0.2, 'degrees_E')]


f.flag_values = [1,2,4]
f.flag_meanings = ['a', 'bb', 'ccc']

print f.dump(complete=1)

print '\n--------------------------------------------------------------------'
print 'TEST: Print a dump of the field:'
print repr(f)

print f.dump()

print '\n--------------------------------------------------------------------'
print 'TEST: Print CF properties:'
print f.properties

print '\n--------------------------------------------------------------------'
print "TEST: Shape of the partition array:"
print '(pndim, psize, pshape) =', (f.Data.partitions.ndim,
                                   f.Data.partitions.size,
                                   f.Data.partitions.shape)

print '\n--------------------------------------------------------------------'
print "TEST: Non-weighted collapse:"

axes='grid_latitude'
c = cf.collapse(f, 'mean', axes=axes, weights=None)
print c
expected = numpy.array([numpy.ma.average(f.array[:,i]) for i in range(9)])
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Unweighted mean over %s didn't work" % repr(axes))
print "Unweighted mean over %s OK" % repr(axes)
#sys.exit(0)
axes='grid_longitude'
c = cf.collapse(f, 'mean', axes=axes, weights='equal')
print c
expected = numpy.array([numpy.ma.average(f.array[i,:]) for i in range(10)])
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected
    raise RuntimeError("Unweighted mean over %s didn't work" % repr(axes))
print "Unweighted mean over %s OK" % repr(axes)

axes=['grid_longitude', 'grid_latitude']
c = cf.collapse(f, 'mean', axes=axes, weights=None)
print c
expected = numpy.ma.average(f.array)
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Unweighted mean over %s didn't work" % repr(axes))
print "Unweighted mean over %s OK" % repr(axes)

axes=None
c = cf.collapse(f, 'variance', weights=None)
print c
mean = numpy.ma.average(f.array)
expected = numpy.ma.sum((f.array-mean)**2)/(1.0*f.array.size)
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Unweighted biased variance over %s didn't work" % repr(axes))
print "Unweighted biased variance over %s OK" % repr(axes)

axes=None
c = cf.collapse(f, 'variance', weights='equal', unbiased=True)
print c
mean = numpy.ma.average(f.array)
expected = numpy.ma.sum((f.array-mean)**2)/(f.array.size-1)
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Unweighted unbiased variance over %s didn't work" % repr(axes))
print "Unweighted unbiased variance over %s OK" % repr(axes)

print '\n--------------------------------------------------------------------'
print "TEST: Weighted collapse:"

axes='grid_latitude'
c = cf.collapse(f, 'mean', axes=axes)
print c
w = cf.tools.collapse.calc_weights(f.coord(axes), infer_bounds=True).array
expected = numpy.array([numpy.ma.average(f.array[:,i], weights=w) for i in range(9)])
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected
    raise RuntimeError("Weighted mean over %s didn't work" % repr(axes))
print "Weighted mean over %s OK" % repr(axes)

axes='grid_longitude'
c = cf.collapse(f, 'mean', axes=axes)
print c
w = cf.tools.collapse.calc_weights(f.coord(axes), infer_bounds=True).array
expected = numpy.array([numpy.ma.average(f.array[i,:], weights=w) for i in range(10)])
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Weighted mean over %s didn't work" % repr(axes))
print "Weighted mean over %s OK" % repr(axes)

cm = f.domain.remove_cell_measure('cm0')

axes=['grid_longitude', 'grid_latitude']
c = cf.collapse(f, 'mean', axes=axes)
print c
w0 = cf.tools.collapse.calc_weights(f.coord('grid_latitude'), infer_bounds=True).array.reshape(10,1)
w1 = cf.tools.collapse.calc_weights(f.coord('grid_longitude'), infer_bounds=True).array.reshape(1,9)
w = w0*w1
print w
expected = numpy.ma.average(f.array, weights=w)
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Weighted mean over %s didn't work" % repr(axes))
print "Weighted mean over %s (no cell measure) OK" % repr(axes)

cell_methods='grid_longitude: mean grid_latitude: max'
c = cf.collapse(f, cell_methods, weights='infer')
print c
d = cf.collapse(f, 'mean', axes='grid_longitude')
expected = cf.collapse(d, 'max', axes='grid_latitude')
ok = numpy.ma.allclose(c.array.flatten(), expected.array.flatten())
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Unweighted %s didn't work" % repr(cell_methods))
print "Weighted mean, max over %s (no cell measure) OK" % repr(cell_methods)

f.domain.insert_cell_measure(cm0, dimensions=['dim1', 'dim0'])

axes=['grid_longitude', 'grid_latitude']
c = cf.collapse(f, 'mean', axes=axes)
print c
w = f.domain['cm0'].copy()
w.transpose()
w = w.array
expected = numpy.ma.average(f.array, weights=w)
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Weighted mean over %s didn't work" % repr(axes))
print "Weighted mean over %s (with cell measure) OK" % repr(axes)

axes=None
c = cf.collapse(f, 'variance', weights='infer')
print c
mean = numpy.ma.average(f.array, weights=w)
expected = numpy.ma.sum(w*(f.array-mean)**2)/numpy.ma.sum(w)
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Weighted biased variance over %s didn't work" % repr(axes))
print "Weighted biased variance over %s OK" % repr(axes)

axes=None
c = cf.collapse(f, 'variance', weights='infer', unbiased=True)
print c
mean = numpy.ma.average(f.array, weights=w)
sow = numpy.ma.sum(w)
sow2 = numpy.ma.sum(w**2)
expected = numpy.ma.sum(w*(f.array-mean)**2)*(sow/(sow**2-sow2))
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Weighted unbiased variance over %s didn't work" % repr(axes))
print "Weighted unbiased variance over %s OK" % repr(axes)
#sys.exit(0)
print '\n--------------------------------------------------------------------'
print "TEST: Cell measures weighted collapse:"

axes=['grid_longitude', 'grid_latitude']
c = cf.collapse(f, 'mean', axes=axes)
print c
w = f.domain['cm0'].copy()
w.transpose()
w = w.array
expected = numpy.ma.average(f.array, weights=w)
ok = numpy.ma.allclose(c.array.flatten(), expected)
if not ok:    
    print '\nGot     :', c.array.flatten(), '\nExpected:',expected, '\nDiff    :',c.array.flatten()-expected   
    raise RuntimeError("Weighted mean over %s didn't work" % repr(axes))
print "Cell measures weighted mean over %s OK" % repr(axes)

f.cell_methods = cf.CellMethods('grid_longitude: mean grid_latitude: max')

print '\n--------------------------------------------------------------------'
print 'TEST: Write the field to disk:'
print 'tmpfile=', tmpfile
print
print f.dump()
print
cf.write(f, tmpfile)

print '\n--------------------------------------------------------------------'
print 'TEST: Read the field from disk:'
g = cf.read(tmpfile, squeeze=True)[0]
try:
    del g.history
except AttributeError:
    pass

print g.dump()
#sys.exit(0)
print '\n--------------------------------------------------------------------'
print '\nComparison (set)'
c = cf.Comparison('set', [0,3,4,5])
print c
a = (f == c)
print repr(a)
print a.array
#sys.exit(0)

print '\n--------------------------------------------------------------------'
print "TEST: Check the equality function:"
if not cf.equals(g, g.copy(), traceback=True):
    raise RuntimeError("Field is not equal to itself")

if not cf.equals(f, g, traceback=True):
    raise RuntimeError("Field is not equal to itself read back in")

print 'OK'

print '\n--------------------------------------------------------------------'
print "TEST: +, -, *, /, **:"
h = g.copy()
h **= 2
h **= 0.5
h *= 10
h /= 10.
h += 100
h -= 100
h = h ** 3
h = h ** (1/3.)
h = h * 1000
h = h / 1000.
h = h + 10000
h = h - 10000
if not cf.equals(g, h, traceback=True):
    raise RuntimeError("+, -, *, / or ** didn't work")

print '\n--------------------------------------------------------------------'
print "TEST: tranpose:"
h.transpose((1, 0))
h.transpose((1, 0))
h.transpose(('grid_longitude', 'grid_latitude'))
h.transpose(('grid_latitude', 'grid_longitude'))
if not cf.equals(g, h, traceback=True):
    raise RuntimeError("Tranpose didn't work")

print '\n--------------------------------------------------------------------'
print "TEST: flip:"
print h.Data.pmndim
print 'h.flip((1, 0))'
h.flip((1, 0))
print 'h.flip((1, 0))'
h.flip((1, 0))
print 'h.flip(0)'
h.flip(0)
print 'h.flip(1)'
h.flip(1)
print 'h.flip([0, 1])'
h.flip([0, 1])
if not cf.equals(g, h, traceback=True):
    raise RuntimeError("Flipping dimensions' directions didn't work")

print '\n--------------------------------------------------------------------'
print "TEST: expand_dims and squeeze:"
h.expand_dims()
h.expand_dims()
new_dims = h.Data.dimensions[:2]
h.squeeze()
h.domain.squeeze(new_dims[0])
h.domain.squeeze(new_dims[1])
if not cf.equals(g, h, traceback=True):
    raise RuntimeError("expand_dims or squeeze didn't work")

print '\n--------------------------------------------------------------------'
print "TEST: Access the field's data as a numpy array:"
print g.array

print '\n--------------------------------------------------------------------'
print "TEST: Access the field's coordinates' data arrays:"
print 'grid_latitude :', g.coord('lat').array
print 'grid_longitude:', g.coord('lon').array

print '\n--------------------------------------------------------------------'
print 'TEST: Indices for a subspace defined by coordinates:'
print f.indices()
print f.indices(grid_latitude=cf.lt(5), grid_longitude=27)
print f.indices(grid_latitude=cf.lt(5), grid_longitude=27, atmosphere_hybrid_height_coordinate=1.5)

print '\n--------------------------------------------------------------------'
print 'TEST: Subspace the field:'
print g.subspace(grid_latitude=cf.lt(5), grid_longitude=27, atmosphere_hybrid_height_coordinate=1.5)

print '\n--------------------------------------------------------------------'
print 'TEST: Subspace the field:'
print g.subspace[..., 2:5].array

print '\n--------------------------------------------------------------------'
print 'TEST: Subspace the field:'
print g.subspace[9::-4, ...].array

print '\n--------------------------------------------------------------------'
print 'TEST: Create list of fields:'
fl = cf.FieldList([g, g, g, g])

print '\n--------------------------------------------------------------------'
print 'TEST: Write a list of fields to disk:'
cf.write((f, fl), tmpfile)
cf.write(fl, tmpfile)

print '\n--------------------------------------------------------------------'
print 'TEST: Read a list of fields from disk:'
fl = cf.read(tmpfile, squeeze=True)
try:
    fl.delattr('history')
except AttributeError:
    pass

print repr(fl)

print '\n--------------------------------------------------------------------'
print 'TEST: Print all fields in the list:'
print fl

print '\n--------------------------------------------------------------------'
print 'TEST: Print the last field in the list:'
print fl[-1]

print '\n--------------------------------------------------------------------'
print 'TEST: Print the data of the last field in the list:'
print fl[-1].array

print '\n--------------------------------------------------------------------'
print 'TEST: Modify the last field in the list:'
fl[-1] *= -1
print fl[-1].array

print '\n--------------------------------------------------------------------'
print 'TEST: Changing units\n:'
fl[-1].units = 'mm.s-1'
print fl[-1].array

print '\n--------------------------------------------------------------------'
print 'TEST: Combine fields not in place:'
g = fl[-1] - fl[-1]
print g.array

print '\n--------------------------------------------------------------------'
print 'TEST: Combine field with a size 1 Data object:'
g += cf.Data([[[[[1.5]]]]], 'cm.s-1')
print g.array
print g.dump()
print '\n--------------------------------------------------------------------'
print "TEST: Setting data array elements to a scalar with subspace[]:"
g.subspace[...] = 0
print g
g.subspace[3:7, 2:5] = -1
print g.array,'\n'
g.subspace[6:2:-1, 4:1:-1] = numpy.array(-1)
print g.array,'\n'
g.subspace[[0, 3, 8], [1, 7, 8]] = numpy.array([[[[-2]]]])
print g.array,'\n'
g.subspace[[8, 3, 0], [8, 7, 1]] = cf.Data(-3, None)
print g.array,'\n'
g.subspace[[7, 4, 1], slice(6, 8)] = [-4]
print g.array

print '\n--------------------------------------------------------------------'
print "TEST: Setting of (un)masked elements with setitem():"
g.subspace[::2, 1::2] = numpy.ma.masked
print g.array,'\n'
g.Data.to_memory(1)
print g.Data.partitions[0][1].subarray
g.setitem(99)
print g.array,'\n'
g.Data.to_memory(1)
print g.Data.partitions[0][1].subarray
g.setitem(2, masked=True)
print g.array,'\n'
g.Data.to_memory(1)
print g.Data.partitions[0][1].subarray
print '\n--------------'

g.setitem(numpy.ma.masked, indices=(slice(None, None, 2), slice(1, None, 2)))
print g.array,'\n'
g.Data.to_memory(1)
print g.Data.partitions[0][1].subarray
g.setitem([[-1]], masked=False)
print g.array,'\n'
g.Data.to_memory(1)
print g.Data.partitions[0][1].subarray
g.setitem(cf.Data(0, None))
print g.array,'\n'
g.Data.to_memory(1)
print g.Data.partitions[0][1].subarray

h = g.subspace[:3, :4]
h.setitem(-1)
h.setitem(2, (0, 2))
print h.dump()
print h.array
h.transpose([1, 0])
print h.array

h.flip([1, 0])
print h.array

g.setitem(h, (slice(None, 3), slice(None, 4)))

h = g.subspace[:3, :4]
h.setitem(-1)
h.setitem(2, (0, 2))
g.setitem(h, (slice(None, 3), slice(None, 4)))
print g.array

print '\n--------------------------------------------------------------------'
print "TEST: Make sure all partitions' data are in temporary files:"
g.Data.to_disk()
print g.Data.partitions

print '\n--------------------------------------------------------------------'
print "TEST: Push partitions' data from temporary files into memory:"
g.Data.to_memory(regardless=True)
print g.Data.partitions

print '\n--------------------------------------------------------------------'
print g.Data.partitions
print "TEST: Push partitions' data from memory to temporary files:"
g.Data.to_disk()
print g.Data.partitions

print '\n--------------------------------------------------------------------'
print "TEST: Iterate through array values:"
for x in f.Data.flat():
    print x,
print

print '\n--------------------------------------------------------------------'
print "TEST: Data any() and all():"
print f.Data.any()
print f.Data.all()

print '\n--------------------------------------------------------------------'
print 'TEST: Reset chunk size:'
cf.CHUNKSIZE(original_chunksize)
print 'CHUNKSIZE reset to',cf.CHUNKSIZE()

print '\n--------------------------------------------------------------------'
print 'TEST: Move Data partitions to disk:'
f.Data.to_disk()
print f.Data.dumpd()

print '\n--------------------------------------------------------------------'
print 'TEST: Create a CFA file ('+tmpfile2+'):'
print f.Data.directions
#f.Data.directions = {'dim0': False, 'dim1': False}
cf.write(f, tmpfile2, format='CFA')
print 'OK'

print '\n--------------------------------------------------------------------'
print 'TEST: Read the CFA file ('+tmpfile2+'):'
n = cf.read(tmpfile2, squeeze=True)[0]
print repr(n)

if not cf.equals(f, n, traceback=True):
    raise RuntimeError("Field is not equal to itself read back in from CFA file")
print 'OK'

#print '\n--------------------------------------------------------------------'
#print 'TEST: flip(0):'

#f.flip(0)
#print 'OK'
#print '\n--------------------------------------------------------------------'
#print 'TEST: flip(1):'
#f.flip(1)
#print 'OK'

cf.CHUNKSIZE(original_chunksize)
print f

f.transpose()
f.flip()

print  f.Data.dumpd()
cf.write(f, 'delme.nc')
f = cf.read('delme.nc')[0]

print f
print  f.Data.dumpd()
cf.write(f, 'delme.nca', format='CFA')
g = cf.read('delme.nca')[0]
print g

f.domain['aux0'].id = 'atmosphere_hybrid_height_coordinate_ak'
f.domain['aux1'].id = 'atmosphere_hybrid_height_coordinate_bk'


b = f.subspace[:,0:6,:]
c = f.subspace[:,6:,:]
print '-----------------------'
print f
print b
print c

d = cf.aggregate([b, c], messages=1, debug=0)[0]

print d



print '\n--------------------------------------------------------------------'
print "TEST: Remove temporary files:"
cf.data.partition._remove_temporary_files()

cf.CHUNKSIZE(original_chunksize)

print f.dump(complete=1)

print
print '--------------------------------------------------------------------'
print 'Hooray! All tests passed for cf version', cf.__version__
print '--------------------------------------------------------------------'
