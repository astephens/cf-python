from distutils.core import setup
import os, fnmatch
import imp

def find_package_data_files(directory):
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, '*'):
                filename = os.path.join(root, basename)
#                if filename.find('/.svn') > -1:
#                    continue
                yield filename.replace('cf/', '', 1)

# Check that the dependencies are met
for _module in ('netCDF4', 'numpy'):
    try:
        imp.find_module(_module)
    except ImportError as error:
        raise ImportError, "Missing dependency. %s" % error
#--- End: for

version            = '0.9.8.1'
packages           = ['cf']
etc_files          = [f for f in find_package_data_files('cf/etc')]
#doc_files          = [f for f in find_package_data_files('docs/build')] + [f for f in find_package_data_files('docs/source')] 
#test_files         = ['test/test1.py']
#man_files          = [f for f in find_package_data_files('scripts/man1')]

setup(name = "cf",
      version      = version,
      description  = "Python interface to the CF data model",
      author       = "David Hassell",
      author_email = "d.c.hassell at reading.ac.uk",
      url          = "http://cfpython.bitbucket.org/",
      download_url = "https://bitbucket.org/cfpython/cf-python/downloads",
      platforms    = ["any"],
      license      = ["OSI Approved"],
      keywords     = ['cf', 'numpy','netcdf','data','science','network','oceanography','meteorology','climate'],
      classifiers  = ["Development Status :: 3 - Alpha",
                      "Intended Audience :: Science/Research", 
                      "License :: OSI Approved", 
                      "Topic :: Software Development :: Libraries :: Python Modules",
                      "Topic :: System :: Archiving :: Compression",
                      "Operating System :: OS Independent"],
      packages     = ['cf', 'cf.pp', 'cf.data', 'cf.netcdf', 'cf.tools'],
      package_data = {'cf': etc_files},
      scripts      = ['scripts/cfa',
                      'scripts/cfdump'],
      requires     = ['netCDF4 (>=0.9.7)', 'numpy (>=1.6)']
  )
