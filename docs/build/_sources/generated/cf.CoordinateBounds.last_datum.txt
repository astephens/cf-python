cf.CoordinateBounds.last_datum
==============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.last_datum