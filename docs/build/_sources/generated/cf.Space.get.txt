cf.Space.get
============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Space.get