cf.Data.iterindices
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.iterindices