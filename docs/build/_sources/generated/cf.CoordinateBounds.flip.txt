cf.CoordinateBounds.flip
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.flip