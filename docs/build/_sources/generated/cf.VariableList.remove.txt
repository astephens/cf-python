cf.VariableList.remove
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.remove