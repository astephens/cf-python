cf.CoordinateBounds.Data
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.Data