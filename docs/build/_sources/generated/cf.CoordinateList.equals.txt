cf.CoordinateList.equals
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.equals