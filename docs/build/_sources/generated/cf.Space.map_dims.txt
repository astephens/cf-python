cf.Space.map_dims
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Space.map_dims