cf.Variable.mask
================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.mask