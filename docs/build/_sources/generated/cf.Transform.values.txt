cf.Transform.values
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.values