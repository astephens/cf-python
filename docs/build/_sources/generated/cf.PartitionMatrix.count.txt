cf.PartitionMatrix.count
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.count