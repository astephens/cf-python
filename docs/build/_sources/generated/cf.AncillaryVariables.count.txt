cf.AncillaryVariables.count
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.count