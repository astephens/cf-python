cf.Space.pop
============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Space.pop