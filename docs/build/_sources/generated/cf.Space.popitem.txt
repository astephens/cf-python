cf.Space.popitem
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Space.popitem