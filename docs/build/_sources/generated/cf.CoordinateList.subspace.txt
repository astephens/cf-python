cf.CoordinateList.subspace
==========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateList.subspace