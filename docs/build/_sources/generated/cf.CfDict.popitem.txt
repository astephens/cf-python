cf.CfDict.popitem
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.popitem