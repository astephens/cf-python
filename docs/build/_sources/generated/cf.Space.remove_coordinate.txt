cf.Space.remove_coordinate
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Space.remove_coordinate