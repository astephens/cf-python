cf.Comparison.operator
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Comparison.operator