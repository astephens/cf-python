cf.Domain.squeeze
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.squeeze