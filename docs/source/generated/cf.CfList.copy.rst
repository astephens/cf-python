cf.CfList.copy
==============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfList.copy