cf.CellMethods.copy
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.copy