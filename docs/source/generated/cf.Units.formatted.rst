cf.Units.formatted
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Units.formatted