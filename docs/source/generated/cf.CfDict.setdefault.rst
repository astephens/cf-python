cf.CfDict.setdefault
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.setdefault