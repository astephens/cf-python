cf.Coordinate.ndim
==================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.ndim