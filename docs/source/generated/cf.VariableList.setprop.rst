cf.VariableList.setprop
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.setprop