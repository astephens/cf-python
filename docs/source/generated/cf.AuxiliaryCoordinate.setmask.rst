cf.AuxiliaryCoordinate.setmask
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.setmask