cf.CoordinateList.setprop
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.setprop