cf.Units.isreftime
==================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Units.isreftime