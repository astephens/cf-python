cf.AncillaryVariables.remove
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.remove