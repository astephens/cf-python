cf.Coordinate.Units
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.Units