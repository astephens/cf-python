cf.VariableList.dump
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.dump