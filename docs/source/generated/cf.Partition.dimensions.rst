cf.Partition.dimensions
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Partition.dimensions