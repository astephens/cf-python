cf.Domain.iteritems
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.iteritems