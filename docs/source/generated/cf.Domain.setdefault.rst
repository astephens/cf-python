cf.Domain.setdefault
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.setdefault