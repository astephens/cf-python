cf.CfDict.copy
==============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.copy