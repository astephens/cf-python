cf.PartitionMatrix.shape
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.PartitionMatrix.shape