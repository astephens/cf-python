cf.Domain.popitem
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.popitem