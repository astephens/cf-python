cf.CellMethods.insert
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.insert