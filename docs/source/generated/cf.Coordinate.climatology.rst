cf.Coordinate.climatology
=========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.climatology