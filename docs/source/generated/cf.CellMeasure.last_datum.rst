cf.CellMeasure.last_datum
=========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CellMeasure.last_datum