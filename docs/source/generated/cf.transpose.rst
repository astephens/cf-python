cf.transpose
============

.. currentmodule:: cf
.. default-role:: obj

.. autofunction:: cf.transpose