cf.VariableList.override_units
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.override_units