cf.VariableList.hasprop
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.hasprop