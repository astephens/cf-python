cf.Transform.has_key
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.has_key