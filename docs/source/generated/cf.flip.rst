cf.flip
=======

.. currentmodule:: cf
.. default-role:: obj

.. autofunction:: cf.flip