cf.Coordinate.insert_data
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.insert_data