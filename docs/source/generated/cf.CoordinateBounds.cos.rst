cf.CoordinateBounds.cos
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.cos