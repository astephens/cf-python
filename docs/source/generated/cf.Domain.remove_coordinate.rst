cf.Domain.remove_coordinate
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.remove_coordinate