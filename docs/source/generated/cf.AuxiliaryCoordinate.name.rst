cf.AuxiliaryCoordinate.name
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.name