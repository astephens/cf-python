cf.Partition.size
=================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Partition.size