cf.Domain.clear
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.clear