cf.Transform.isgrid_mapping
===========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Transform.isgrid_mapping