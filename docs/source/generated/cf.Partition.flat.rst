cf.Partition.flat
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.flat