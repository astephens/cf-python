cf.Domain.remove_dimension
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.remove_dimension