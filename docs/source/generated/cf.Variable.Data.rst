cf.Variable.Data
================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.Data