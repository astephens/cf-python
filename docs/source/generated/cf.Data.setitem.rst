cf.Data.setitem
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.setitem