cf.Partition.location
=====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Partition.location