cf.CoordinateList.getprop
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.getprop