cf.AuxiliaryCoordinate.setitem
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.setitem