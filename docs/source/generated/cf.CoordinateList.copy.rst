cf.CoordinateList.copy
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.copy