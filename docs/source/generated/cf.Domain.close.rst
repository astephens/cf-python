cf.Domain.close
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.close