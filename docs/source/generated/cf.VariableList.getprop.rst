cf.VariableList.getprop
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.getprop