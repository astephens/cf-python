cf.PartitionMatrix.transpose
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.transpose