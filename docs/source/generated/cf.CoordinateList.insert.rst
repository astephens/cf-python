cf.CoordinateList.insert
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.insert