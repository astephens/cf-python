cf.Variable.first_datum
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.first_datum