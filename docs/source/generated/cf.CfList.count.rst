cf.CfList.count
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfList.count