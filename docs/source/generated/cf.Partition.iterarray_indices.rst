cf.Partition.iterarray_indices
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.iterarray_indices