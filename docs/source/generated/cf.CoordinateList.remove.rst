cf.CoordinateList.remove
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.remove