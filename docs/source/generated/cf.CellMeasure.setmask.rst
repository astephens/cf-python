cf.CellMeasure.setmask
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.setmask