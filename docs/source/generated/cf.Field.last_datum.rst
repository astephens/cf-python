cf.Field.last_datum
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Field.last_datum