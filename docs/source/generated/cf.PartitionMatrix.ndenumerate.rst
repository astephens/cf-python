cf.PartitionMatrix.ndenumerate
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.ndenumerate