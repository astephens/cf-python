cf.CoordinateBounds.missing_value
=================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.missing_value