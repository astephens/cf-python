cf.PartitionMatrix.change_dimension_names
=========================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.change_dimension_names