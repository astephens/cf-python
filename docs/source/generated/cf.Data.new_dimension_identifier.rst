cf.Data.new_dimension_identifier
================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.new_dimension_identifier