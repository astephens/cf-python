cf.Units.islatitude
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Units.islatitude