cf.Field.setmask
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.setmask