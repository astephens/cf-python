cf.PartitionMatrix.ndim
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.PartitionMatrix.ndim