cf.Partition.itermaster_indices
===============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.itermaster_indices