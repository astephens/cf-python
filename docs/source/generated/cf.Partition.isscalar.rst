cf.Partition.isscalar
=====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Partition.isscalar