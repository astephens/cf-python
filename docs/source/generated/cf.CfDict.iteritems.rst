cf.CfDict.iteritems
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.iteritems