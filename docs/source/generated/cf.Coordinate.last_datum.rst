cf.Coordinate.last_datum
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.last_datum