cf.CellMethods.count
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.count