cf.Data.last_datum
==================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Data.last_datum