cf.CoordinateBounds.valid_min
=============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.valid_min