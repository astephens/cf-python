cf.Domain.pop
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.pop