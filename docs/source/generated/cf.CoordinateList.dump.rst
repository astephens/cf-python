cf.CoordinateList.dump
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.dump