cf.Domain.itercoordinates
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.itercoordinates