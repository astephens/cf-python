cf.Partition.subarray
=====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Partition.subarray