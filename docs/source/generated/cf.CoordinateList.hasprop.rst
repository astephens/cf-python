cf.CoordinateList.hasprop
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.hasprop