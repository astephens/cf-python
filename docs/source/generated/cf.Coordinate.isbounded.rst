cf.Coordinate.isbounded
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.isbounded