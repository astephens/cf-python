cf.Partition.to_disk
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.to_disk