cf.CoordinateBounds.first_datum
===============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.first_datum