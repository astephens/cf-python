cf.Domain.dimension_name
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.dimension_name