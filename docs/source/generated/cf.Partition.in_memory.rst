cf.Partition.in_memory
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Partition.in_memory