cf.VariableList.match
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.match