cf.CellMeasure.setitem
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.setitem