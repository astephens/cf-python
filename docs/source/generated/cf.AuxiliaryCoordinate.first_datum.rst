cf.AuxiliaryCoordinate.first_datum
==================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.AuxiliaryCoordinate.first_datum