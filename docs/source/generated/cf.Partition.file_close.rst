cf.Partition.file_close
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.file_close