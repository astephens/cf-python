cf.CfDict.equals
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.equals