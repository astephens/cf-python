cf.CoordinateBounds.subspace
============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.subspace