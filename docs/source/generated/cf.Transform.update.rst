cf.Transform.update
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.update