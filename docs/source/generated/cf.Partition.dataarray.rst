cf.Partition.dataarray
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.dataarray