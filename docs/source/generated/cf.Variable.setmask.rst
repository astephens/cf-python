cf.Variable.setmask
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.setmask