cf.CoordinateList.match
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.match