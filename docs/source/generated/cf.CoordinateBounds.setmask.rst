cf.CoordinateBounds.setmask
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.setmask