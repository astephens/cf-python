cf.PartitionMatrix.flat
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.flat