cf.AuxiliaryCoordinate.islatitude
=================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.AuxiliaryCoordinate.islatitude