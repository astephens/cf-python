cf.VariableList.sort
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.sort