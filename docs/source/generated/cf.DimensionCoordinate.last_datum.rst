cf.DimensionCoordinate.last_datum
=================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.DimensionCoordinate.last_datum