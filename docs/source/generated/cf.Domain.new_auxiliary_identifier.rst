cf.Domain.new_auxiliary_identifier
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.new_auxiliary_identifier