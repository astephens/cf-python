cf.Coordinate.setmask
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.setmask