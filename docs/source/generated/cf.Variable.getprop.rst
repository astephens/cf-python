cf.Variable.getprop
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.getprop