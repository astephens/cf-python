cf.AncillaryVariables.extend
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.extend