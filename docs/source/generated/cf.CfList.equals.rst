cf.CfList.equals
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfList.equals