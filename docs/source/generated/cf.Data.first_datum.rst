cf.Data.first_datum
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Data.first_datum