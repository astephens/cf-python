cf.CfDict.has_key
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.has_key