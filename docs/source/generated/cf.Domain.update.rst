cf.Domain.update
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.update