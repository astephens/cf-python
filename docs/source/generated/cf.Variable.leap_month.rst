cf.Variable.leap_month
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.leap_month