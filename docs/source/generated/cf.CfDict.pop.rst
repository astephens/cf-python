cf.CfDict.pop
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.pop