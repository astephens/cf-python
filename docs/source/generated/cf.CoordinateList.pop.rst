cf.CoordinateList.pop
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.pop