cf.CfDict.clear
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.clear