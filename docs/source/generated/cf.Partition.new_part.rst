cf.Partition.new_part
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.new_part