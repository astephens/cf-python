cf.VariableList.reverse
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.reverse