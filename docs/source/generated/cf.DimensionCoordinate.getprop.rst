cf.DimensionCoordinate.getprop
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.DimensionCoordinate.getprop