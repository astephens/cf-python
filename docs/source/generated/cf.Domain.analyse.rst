cf.Domain.analyse
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.analyse