cf.AuxiliaryCoordinate.select
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.select