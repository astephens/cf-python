cf.Units.islongitude
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Units.islongitude