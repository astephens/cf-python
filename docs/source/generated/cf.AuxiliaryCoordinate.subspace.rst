cf.AuxiliaryCoordinate.subspace
===============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.AuxiliaryCoordinate.subspace