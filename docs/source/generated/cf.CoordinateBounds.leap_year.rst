cf.CoordinateBounds.leap_year
=============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.leap_year