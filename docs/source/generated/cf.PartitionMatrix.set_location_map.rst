cf.PartitionMatrix.set_location_map
===================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.set_location_map