cf.CfDict.values
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.values