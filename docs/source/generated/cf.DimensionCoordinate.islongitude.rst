cf.DimensionCoordinate.islongitude
==================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.DimensionCoordinate.islongitude