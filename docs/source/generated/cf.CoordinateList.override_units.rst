cf.CoordinateList.override_units
================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.override_units