cf.Domain.new_dimension_identifier
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.new_dimension_identifier