cf.CoordinateBounds.standard_name
=================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.standard_name