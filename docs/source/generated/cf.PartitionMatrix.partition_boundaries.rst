cf.PartitionMatrix.partition_boundaries
=======================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.partition_boundaries