cf.DimensionCoordinate.first_datum
==================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.DimensionCoordinate.first_datum