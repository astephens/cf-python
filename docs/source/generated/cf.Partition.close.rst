cf.Partition.close
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.close