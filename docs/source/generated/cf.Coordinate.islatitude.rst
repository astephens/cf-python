cf.Coordinate.islatitude
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.islatitude