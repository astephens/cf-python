cf.Coordinate.setitem
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.setitem