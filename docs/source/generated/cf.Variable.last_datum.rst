cf.Variable.last_datum
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.last_datum