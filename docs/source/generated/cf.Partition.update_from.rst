cf.Partition.update_from
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.update_from