cf.CellMethods.reverse
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.reverse