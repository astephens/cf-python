cf.PartitionMatrix.expand_dims
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.expand_dims