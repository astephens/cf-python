cf.Coordinate.subspace
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.subspace