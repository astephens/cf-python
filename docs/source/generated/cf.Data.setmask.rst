cf.Data.setmask
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.setmask