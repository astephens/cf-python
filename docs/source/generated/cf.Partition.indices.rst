cf.Partition.indices
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Partition.indices