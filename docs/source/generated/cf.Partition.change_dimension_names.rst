cf.Partition.change_dimension_names
===================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.change_dimension_names