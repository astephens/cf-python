cf.CoordinateList.sort
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.sort