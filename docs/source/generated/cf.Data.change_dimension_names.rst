cf.Data.change_dimension_names
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.change_dimension_names