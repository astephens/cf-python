cf.DimensionCoordinate.islatitude
=================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.DimensionCoordinate.islatitude