cf.VariableList.count
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.count