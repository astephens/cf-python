cf.PartitionMatrix.add_partitions
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.add_partitions