cf.Domain.remove_cell_measure
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.remove_cell_measure