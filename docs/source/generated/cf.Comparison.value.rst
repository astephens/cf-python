cf.Comparison.value
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Comparison.value
