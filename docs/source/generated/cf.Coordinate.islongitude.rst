cf.Coordinate.islongitude
=========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.islongitude