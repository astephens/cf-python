cf.CfDict.itervalues
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.itervalues