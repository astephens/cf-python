cf.CellMethods.netCDF_translation
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.netCDF_translation