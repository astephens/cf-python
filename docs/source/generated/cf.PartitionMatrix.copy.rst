cf.PartitionMatrix.copy
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.copy