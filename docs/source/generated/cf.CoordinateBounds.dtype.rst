cf.CoordinateBounds.dtype
=========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.dtype