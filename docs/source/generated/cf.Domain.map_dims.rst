cf.Domain.map_dims
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.map_dims