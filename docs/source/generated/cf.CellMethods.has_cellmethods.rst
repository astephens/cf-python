cf.CellMethods.has_cellmethods
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.has_cellmethods