cf.CfDict.update
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.update