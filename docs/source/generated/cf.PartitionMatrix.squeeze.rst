cf.PartitionMatrix.squeeze
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.PartitionMatrix.squeeze