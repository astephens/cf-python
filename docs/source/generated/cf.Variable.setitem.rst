cf.Variable.setitem
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.setitem