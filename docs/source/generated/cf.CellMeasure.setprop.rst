cf.CellMeasure.setprop
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.setprop