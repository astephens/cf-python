cf.equivalent
=============

.. currentmodule:: cf
.. default-role:: obj

.. autofunction:: cf.equivalent