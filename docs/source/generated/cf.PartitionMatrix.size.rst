cf.PartitionMatrix.size
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.PartitionMatrix.size