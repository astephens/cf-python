cf.Partition.copy
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Partition.copy