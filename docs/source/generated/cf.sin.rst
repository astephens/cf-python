cf.sin
======

.. currentmodule:: cf
.. default-role:: obj

.. autofunction:: cf.sin