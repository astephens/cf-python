cf.Coordinate.get_data
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.get_data