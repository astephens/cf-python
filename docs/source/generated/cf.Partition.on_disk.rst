cf.Partition.on_disk
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Partition.on_disk