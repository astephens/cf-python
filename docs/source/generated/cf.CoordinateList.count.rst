cf.CoordinateList.count
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.count