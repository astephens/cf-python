cf.CoordinateList.index
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.index