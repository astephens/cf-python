cf.Field.setitem
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.setitem