cf.Field.first_datum
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Field.first_datum