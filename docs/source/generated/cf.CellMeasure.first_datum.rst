cf.CellMeasure.first_datum
==========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CellMeasure.first_datum