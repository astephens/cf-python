cf.CfDict.items
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.items