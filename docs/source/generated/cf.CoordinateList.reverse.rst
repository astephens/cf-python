cf.CoordinateList.reverse
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateList.reverse