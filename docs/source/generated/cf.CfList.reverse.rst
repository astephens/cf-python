cf.CfList.reverse
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfList.reverse