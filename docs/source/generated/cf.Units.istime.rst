cf.Units.istime
===============

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Units.istime