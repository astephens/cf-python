cf.Units.equivalent
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Units.equivalent