cf.AuxiliaryCoordinate.last_datum
=================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.AuxiliaryCoordinate.last_datum