cf.Coordinate.first_datum
=========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.first_datum