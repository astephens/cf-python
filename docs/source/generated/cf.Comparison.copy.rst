cf.Comparison.copy
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Comparison.copy