cf.CfDict.get
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfDict.get