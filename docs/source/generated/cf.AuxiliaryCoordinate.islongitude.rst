cf.AuxiliaryCoordinate.islongitude
==================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.AuxiliaryCoordinate.islongitude