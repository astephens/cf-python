cf.CfList.remove
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CfList.remove