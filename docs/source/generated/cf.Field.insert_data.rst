cf.Field.insert_data
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.insert_data