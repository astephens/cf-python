cf.VariableList.copy
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.copy