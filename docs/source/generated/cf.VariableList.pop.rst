cf.VariableList.pop
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.VariableList.pop