.. currentmodule:: cf

Constants of the :mod:`cf` module
=================================

.. data:: cf.masked

    The :attr:`cf.masked` constant allows data array values to be
    masked by direct assignment. This is consistent with the
    :ref:`behaviour of numpy masked arrays
    <numpy:maskedarray.generic.constructing>`.

    For example, one way of masking every element of an array is:

    >>> f.setitem(cf.mask)
