.. currentmodule:: cf
.. default-role:: obj

cf.DimensionCoordinate
======================

.. autoclass:: cf.DimensionCoordinate
   :no-members:
   :no-inherited-members:

DimensionCoordinate CF properties
---------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.DimensionCoordinate.add_offset
   ~cf.DimensionCoordinate.axis
   ~cf.DimensionCoordinate.calendar
   ~cf.DimensionCoordinate.comment
   ~cf.DimensionCoordinate._FillValue
   ~cf.DimensionCoordinate.history
   ~cf.DimensionCoordinate.leap_month
   ~cf.DimensionCoordinate.leap_year
   ~cf.DimensionCoordinate.long_name
   ~cf.DimensionCoordinate.missing_value
   ~cf.DimensionCoordinate.month_lengths
   ~cf.DimensionCoordinate.positive
   ~cf.DimensionCoordinate.scale_factor
   ~cf.DimensionCoordinate.standard_name
   ~cf.DimensionCoordinate.units
   ~cf.DimensionCoordinate.valid_max
   ~cf.DimensionCoordinate.valid_min
   ~cf.DimensionCoordinate.valid_range

DimensionCoordinate data attributes
-----------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.DimensionCoordinate.array
   ~cf.DimensionCoordinate.Data
   ~cf.DimensionCoordinate.dtype
   ~cf.DimensionCoordinate._FillValue
   ~cf.DimensionCoordinate.first_datum
   ~cf.DimensionCoordinate.hardmask
   ~cf.DimensionCoordinate.isscalar
   ~cf.DimensionCoordinate.last_datum
   ~cf.DimensionCoordinate.mask
   ~cf.DimensionCoordinate.ndim
   ~cf.DimensionCoordinate.shape
   ~cf.DimensionCoordinate.size
   ~cf.DimensionCoordinate.Units
   ~cf.DimensionCoordinate.varray

DimensionCoordinate attributes
------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.DimensionCoordinate.attributes
   ~cf.DimensionCoordinate.bounds
   ~cf.DimensionCoordinate.climatology
   ~cf.DimensionCoordinate.id
   ~cf.DimensionCoordinate.isbounded
   ~cf.DimensionCoordinate.islatitude
   ~cf.DimensionCoordinate.islongitude
   ~cf.DimensionCoordinate.isscalar
   ~cf.DimensionCoordinate.properties
   ~cf.DimensionCoordinate.subspace
   ~cf.DimensionCoordinate.transforms

DimensionCoordinate methods
---------------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.DimensionCoordinate.asauxiliary
   ~cf.DimensionCoordinate.asdimension
   ~cf.DimensionCoordinate.binary_mask
   ~cf.DimensionCoordinate.chunk
   ~cf.DimensionCoordinate.close
   ~cf.DimensionCoordinate.contiguous
   ~cf.DimensionCoordinate.copy
   ~cf.DimensionCoordinate.cos
   ~cf.DimensionCoordinate.delprop
   ~cf.DimensionCoordinate.direction
   ~cf.DimensionCoordinate.dump
   ~cf.DimensionCoordinate.equals
   ~cf.DimensionCoordinate.expand_dims
   ~cf.DimensionCoordinate.fill_value
   ~cf.DimensionCoordinate.find_bounds
   ~cf.DimensionCoordinate.flip
   ~cf.DimensionCoordinate.get_data
   ~cf.DimensionCoordinate.getprop
   ~cf.DimensionCoordinate.hasprop
   ~cf.DimensionCoordinate.identity
   ~cf.DimensionCoordinate.insert_bounds
   ~cf.DimensionCoordinate.insert_data
   ~cf.DimensionCoordinate.insert_transform
   ~cf.DimensionCoordinate.match
   ~cf.DimensionCoordinate.name
   ~cf.DimensionCoordinate.override_units
   ~cf.DimensionCoordinate.select
   ~cf.DimensionCoordinate.setitem
   ~cf.DimensionCoordinate.setmask
   ~cf.DimensionCoordinate.setprop
   ~cf.DimensionCoordinate.sin
   ~cf.DimensionCoordinate.squeeze
   ~cf.DimensionCoordinate.transpose
