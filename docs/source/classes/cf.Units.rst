.. currentmodule:: cf
.. default-role:: obj

cf.Units
========

.. autoclass:: cf.Units
   :no-members:
   :no-inherited-members:

Units attributes
----------------

.. autosummary::
   :toctree: ../generated/	
   :template: attribute.rst

   ~Units.calendar
   ~Units.islatitude
   ~Units.islongitude
   ~Units.ispressure
   ~Units.isreftime
   ~Units.istime
   ~Units.units

Units methods
-------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~Units.change_reftime_units
   ~Units.copy
   ~Units.dump
   ~Units.equals
   ~Units.equivalent
   ~Units.formatted
   ~Units.log

Units static methods
--------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~Units.conform