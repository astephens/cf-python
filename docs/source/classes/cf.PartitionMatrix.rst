.. currentmodule:: cf
.. default-role:: obj

cf.PartitionMatrix
==================

.. autoclass:: cf.PartitionMatrix
   :no-members:
   :no-inherited-members:

PartitionMatrix attributes
--------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~PartitionMatrix.matrix
   ~PartitionMatrix.ndim
   ~PartitionMatrix.shape
   ~PartitionMatrix.size
   
PartitionMatrix methods
-----------------------

Undocumented methods behave exactly as their counterparts in a
built-in list.
     
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~PartitionMatrix.add_partitions
   ~PartitionMatrix.change_dimension_names
   ~PartitionMatrix.copy
   ~PartitionMatrix.expand_dims
   ~PartitionMatrix.flat
   ~PartitionMatrix.ndenumerate
   ~PartitionMatrix.partition_boundaries
   ~PartitionMatrix.rollaxis
   ~PartitionMatrix.set_location_map
   ~PartitionMatrix.squeeze
   ~PartitionMatrix.transpose
