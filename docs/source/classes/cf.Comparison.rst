.. currentmodule:: cf
.. default-role:: obj

cf.Comparison
=============

.. autoclass:: cf.Comparison
   :no-members:
   :no-inherited-members:

Comparison attributes
---------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: attribute.rst

   ~Comparison.operator
   ~Comparison.value

Comparison methods
------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~Comparison.copy
   ~Comparison.dump
   ~Comparison.evaluate
