.. currentmodule:: cf
.. default-role:: obj

cf.VariableList
===============

.. autoclass:: cf.VariableList
   :no-members:
   :no-inherited-members:

VariableList attributes
-----------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.VariableList.subspace

VariableList methods
--------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~cf.VariableList.append
   ~cf.VariableList.copy
   ~cf.VariableList.count
   ~cf.VariableList.delprop
   ~cf.VariableList.dump
   ~cf.VariableList.equals
   ~cf.VariableList.extend
   ~cf.VariableList.getprop
   ~cf.VariableList.hasprop
   ~cf.VariableList.index
   ~cf.VariableList.insert
   ~cf.VariableList.match
   ~cf.VariableList.name
   ~cf.VariableList.override_units
   ~cf.VariableList.pop
   ~cf.VariableList.remove
   ~cf.VariableList.reverse
   ~cf.VariableList.select
   ~cf.VariableList.set_equals
   ~cf.VariableList.setprop
   ~cf.VariableList.sort
   ~cf.VariableList.subspace
