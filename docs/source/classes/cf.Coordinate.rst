.. currentmodule:: cf
.. default-role:: obj

cf.Coordinate
=============

.. autoclass:: cf.Coordinate
   :no-members:
   :no-inherited-members:

Coordinate CF properties
------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Coordinate.add_offset
   ~cf.Coordinate.axis
   ~cf.Coordinate.calendar
   ~cf.Coordinate.comment
   ~cf.Coordinate._FillValue
   ~cf.Coordinate.history
   ~cf.Coordinate.leap_month
   ~cf.Coordinate.leap_year
   ~cf.Coordinate.long_name
   ~cf.Coordinate.missing_value
   ~cf.Coordinate.month_lengths
   ~cf.Coordinate.positive
   ~cf.Coordinate.scale_factor
   ~cf.Coordinate.standard_name
   ~cf.Coordinate.units
   ~cf.Coordinate.valid_max
   ~cf.Coordinate.valid_min
   ~cf.Coordinate.valid_range

Coordinate data attributes
--------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Coordinate.array
   ~cf.Coordinate.Data
   ~cf.Coordinate.dtype
   ~cf.Coordinate._FillValue
   ~cf.Coordinate.first_datum
   ~cf.Coordinate.hardmask
   ~cf.Coordinate.isscalar
   ~cf.Coordinate.last_datum
   ~cf.Coordinate.mask
   ~cf.Coordinate.ndim
   ~cf.Coordinate.shape
   ~cf.Coordinate.size
   ~cf.Coordinate.Units
   ~cf.Coordinate.varray

Coordinate attributes
---------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Coordinate.attributes
   ~cf.Coordinate.bounds
   ~cf.Coordinate.climatology
   ~cf.Coordinate.id
   ~cf.Coordinate.isbounded
   ~cf.Coordinate.islatitude
   ~cf.Coordinate.islongitude
   ~cf.Coordinate.isscalar
   ~cf.Coordinate.properties
   ~cf.Coordinate.subspace
   ~cf.Coordinate.transforms

Coordinate methods
------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Coordinate.asauxiliary
   ~cf.Coordinate.asdimension
   ~cf.Coordinate.binary_mask
   ~cf.Coordinate.chunk
   ~cf.Coordinate.close
   ~cf.Coordinate.contiguous
   ~cf.Coordinate.copy
   ~cf.Coordinate.cos
   ~cf.Coordinate.delprop
   ~cf.Coordinate.dump
   ~cf.Coordinate.equals
   ~cf.Coordinate.expand_dims
   ~cf.Coordinate.fill_value
   ~cf.Coordinate.flip
   ~cf.Coordinate.get_data
   ~cf.Coordinate.getprop
   ~cf.Coordinate.hasprop
   ~cf.Coordinate.identity
   ~cf.Coordinate.insert_bounds
   ~cf.Coordinate.insert_data
   ~cf.Coordinate.insert_transform
   ~cf.Coordinate.match
   ~cf.Coordinate.name
   ~cf.Coordinate.override_units
   ~cf.Coordinate.select
   ~cf.Coordinate.setitem
   ~cf.Coordinate.setmask
   ~cf.Coordinate.setprop
   ~cf.Coordinate.sin
   ~cf.Coordinate.squeeze
   ~cf.Coordinate.transpose
