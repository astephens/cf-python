.. currentmodule:: cf
.. default-role:: obj

cf.Field
========

.. autoclass:: cf.Field
   :no-members:
   :no-inherited-members:

Field CF Properties
-------------------
 
.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Field.add_offset
   ~cf.Field.calendar
   ~cf.Field.cell_methods
   ~cf.Field.comment
   ~cf.Field.Conventions
   ~cf.Field._FillValue
   ~cf.Field.flag_masks
   ~cf.Field.flag_meanings
   ~cf.Field.flag_values
   ~cf.Field.history
   ~cf.Field.institution
   ~cf.Field.leap_month
   ~cf.Field.leap_year
   ~cf.Field.long_name
   ~cf.Field.missing_value
   ~cf.Field.month_lengths
   ~cf.Field.references
   ~cf.Field.scale_factor
   ~cf.Field.source
   ~cf.Field.standard_error_multiplier
   ~cf.Field.standard_name
   ~cf.Field.title
   ~cf.Field.units
   ~cf.Field.valid_max
   ~cf.Field.valid_min
   ~cf.Field.valid_range

Field data attributes
---------------------
   
.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Field.array
   ~cf.Field.Data
   ~cf.Field.dtype
   ~cf.Field._FillValue
   ~cf.Field.first_datum
   ~cf.Field.hardmask
   ~cf.Field.isscalar
   ~cf.Field.last_datum
   ~cf.Field.mask
   ~cf.Field.ndim
   ~cf.Field.shape
   ~cf.Field.size
   ~cf.Field.Units
   ~cf.Field.varray

Field attributes
----------------
   
.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Field.ancillary_variables
   ~cf.Field.domain
   ~cf.Field.Flags
   ~cf.Field.id
   ~cf.Field.properties
   ~cf.Field.subspace

Field methods
-------------
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Field.binary_mask
   ~cf.Field.chunk
   ~cf.Field.clip
   ~cf.Field.close
   ~cf.Field.coord
   ~cf.Field.copy
   ~cf.Field.cos
   ~cf.Field.delprop
   ~cf.Field.dump
   ~cf.Field.equals
   ~cf.Field.equivalent
   ~cf.Field.equivalent_data
   ~cf.Field.expand_dims
   ~cf.Field.finalize
   ~cf.Field.flip
   ~cf.Field.getprop
   ~cf.Field.hasprop
   ~cf.Field.identity
   ~cf.Field.indices
   ~cf.Field.insert_data
   ~cf.Field.match
   ~cf.Field.name
   ~cf.Field.override_units
   ~cf.Field.remove_data
   ~cf.Field.select
   ~cf.Field.setitem
   ~cf.Field.setmask
   ~cf.Field.setprop
   ~cf.Field.sin
   ~cf.Field.squeeze
   ~cf.Field.transpose
   ~cf.Field.unsqueeze

.. Flags? dump_simple_properties equivalent equivalent_domain file? ncvar? spans?

Field arithmetic and comparison operations
------------------------------------------

See the section on :ref:`arithmetic and comparison operations
<Arithmetic-and-comparison>`.
