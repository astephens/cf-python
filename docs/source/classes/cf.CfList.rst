.. currentmodule:: cf
.. default-role:: obj

cf.CfList
=========

.. autoclass:: cf.CfList
   :no-members:
   :no-inherited-members:

CfList methods
--------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.CfList.append
   ~cf.CfList.copy
   ~cf.CfList.count
   ~cf.CfList.equals
   ~cf.CfList.extend
   ~cf.CfList.index
   ~cf.CfList.insert
   ~cf.CfList.pop
   ~cf.CfList.remove
   ~cf.CfList.reverse
