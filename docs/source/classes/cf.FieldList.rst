.. currentmodule:: cf
.. default-role:: obj

cf.FieldList
============

.. autoclass:: cf.FieldList
   :no-members:
   :no-inherited-members:

FieldList attributes
--------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.FieldList.subspace

FieldList methods
-----------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.FieldList.append
   ~cf.FieldList.coord
   ~cf.FieldList.copy
   ~cf.FieldList.count
   ~cf.FieldList.delprop
   ~cf.FieldList.dump
   ~cf.FieldList.equals
   ~cf.FieldList.extend
   ~cf.FieldList.getprop
   ~cf.FieldList.hasprop
   ~cf.FieldList.index
   ~cf.FieldList.insert
   ~cf.FieldList.match
   ~cf.FieldList.name
   ~cf.FieldList.override_units
   ~cf.FieldList.pop
   ~cf.FieldList.remove
   ~cf.FieldList.reverse
   ~cf.FieldList.select
   ~cf.FieldList.set_equals
   ~cf.FieldList.setprop
   ~cf.FieldList.sort
   ~cf.FieldList.squeeze
   ~cf.FieldList.unsqueeze
