.. currentmodule:: cf
.. default-role:: obj

cf.Partition
============

.. autoclass:: cf.Partition
   :no-members:
   :no-inherited-members:

Partition attributes
--------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Partition._original
   ~cf.Partition._save
   ~cf.Partition.dimensions
   ~cf.Partition.directions
   ~cf.Partition.in_memory
   ~cf.Partition.indices
   ~cf.Partition.isscalar
   ~cf.Partition.location
   ~cf.Partition.on_disk
   ~cf.Partition.part
   ~cf.Partition.shape
   ~cf.Partition.size
   ~cf.Partition.subarray

Partition methods
-----------------
     
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Partition.change_dimension_names
   ~cf.Partition.close
   ~cf.Partition.copy
   ~cf.Partition.dataarray
   ~cf.Partition.file_close
   ~cf.Partition.flat
   ~cf.Partition.iterarray_indices
   ~cf.Partition.itermaster_indices
   ~cf.Partition.new_part
   ~cf.Partition.to_disk
   ~cf.Partition.update_from

