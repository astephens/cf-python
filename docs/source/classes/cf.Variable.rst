.. currentmodule:: cf
.. default-role:: obj

cf.Variable
===========

.. autoclass:: cf.Variable
   :no-members:
   :no-inherited-members:

Variable CF Properties
----------------------
 
.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Variable.add_offset
   ~cf.Variable.calendar
   ~cf.Variable.comment
   ~cf.Variable._FillValue
   ~cf.Variable.history
   ~cf.Variable.leap_month
   ~cf.Variable.leap_year
   ~cf.Variable.long_name
   ~cf.Variable.missing_value
   ~cf.Variable.month_lengths
   ~cf.Variable.scale_factor
   ~cf.Variable.standard_name
   ~cf.Variable.units
   ~cf.Variable.valid_max
   ~cf.Variable.valid_min
   ~cf.Variable.valid_range

Variable data attributes
------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Variable.array

   ~cf.Variable.Data
   ~cf.Variable.dtype
   ~cf.Variable._FillValue
   ~cf.Variable.first_datum
   ~cf.Variable.hardmask
   ~cf.Variable.isscalar
   ~cf.Variable.last_datum
   ~cf.Variable.mask
   ~cf.Variable.ndim
   ~cf.Variable.shape
   ~cf.Variable.size
   ~cf.Variable.Units
   ~cf.Variable.varray

Variable attributes
-------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Variable.id
   ~cf.Variable.properties
   ~cf.Variable.subspace

Variable methods
----------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Variable.binary_mask
   ~cf.Variable.chunk
   ~cf.Variable.clip
   ~cf.Variable.copy
   ~cf.Variable.cos
   ~cf.Variable.delprop
   ~cf.Variable.dump
   ~cf.Variable.equals
   ~cf.Variable.expand_dims
   ~cf.Variable.flip
   ~cf.Variable.getprop
   ~cf.Variable.hasprop
   ~cf.Variable.identity
   ~cf.Variable.insert_data
   ~cf.Variable.match
   ~cf.Variable.name
   ~cf.Variable.override_units
   ~cf.Variable.select
   ~cf.Variable.setitem
   ~cf.Variable.setmask
   ~cf.Variable.setprop
   ~cf.Variable.sin
   ~cf.Variable.squeeze
   ~cf.Variable.transpose
