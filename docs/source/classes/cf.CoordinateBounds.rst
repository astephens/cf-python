.. currentmodule:: cf
.. default-role:: obj

cf.CoordinateBounds
===================

.. autoclass:: cf.CoordinateBounds
   :no-members:
   :no-inherited-members:

CoordinateBounds CF Properties
------------------------------
 
.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~CoordinateBounds.add_offset
   ~CoordinateBounds.calendar
   ~CoordinateBounds.comment
   ~CoordinateBounds._FillValue
   ~CoordinateBounds.history
   ~CoordinateBounds.leap_month
   ~CoordinateBounds.leap_year
   ~CoordinateBounds.long_name
   ~CoordinateBounds.missing_value
   ~CoordinateBounds.month_lengths
   ~CoordinateBounds.scale_factor
   ~CoordinateBounds.standard_name
   ~CoordinateBounds.units
   ~CoordinateBounds.valid_max
   ~CoordinateBounds.valid_min
   ~CoordinateBounds.valid_range

CoordinateBounds data attributes
--------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~CoordinateBounds.array
   ~CoordinateBounds.Data
   ~CoordinateBounds.dtype
   ~CoordinateBounds._FillValue
   ~CoordinateBounds.first_datum
   ~CoordinateBounds.hardmask
   ~CoordinateBounds.isscalar
   ~CoordinateBounds.last_datum
   ~CoordinateBounds.mask
   ~CoordinateBounds.ndim
   ~CoordinateBounds.shape
   ~CoordinateBounds.size
   ~CoordinateBounds.Units
   ~CoordinateBounds.varray

CoordinateBounds attributes
---------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CoordinateBounds.properties
   ~cf.CoordinateBounds.subspace

CoordinateBounds methods
------------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~CoordinateBounds.binary_mask
   ~CoordinateBounds.chunk
   ~CoordinateBounds.clip
   ~CoordinateBounds.copy
   ~CoordinateBounds.cos
   ~CoordinateBounds.delprop
   ~CoordinateBounds.dump
   ~CoordinateBounds.equals
   ~CoordinateBounds.expand_dims
   ~CoordinateBounds.flip
   ~CoordinateBounds.getprop
   ~CoordinateBounds.hasprop
   ~CoordinateBounds.identity
   ~CoordinateBounds.match
   ~CoordinateBounds.name
   ~CoordinateBounds.override_units
   ~CoordinateBounds.select
   ~CoordinateBounds.setitem
   ~CoordinateBounds.setmask
   ~CoordinateBounds.setprop
   ~CoordinateBounds.sin
   ~CoordinateBounds.squeeze
   ~CoordinateBounds.transpose
