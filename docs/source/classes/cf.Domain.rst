.. currentmodule:: cf
.. default-role:: obj

cf.Domain
=========

.. autoclass:: cf.Domain
   :no-members:
   :no-inherited-members:

Domain methods
--------------

Undocumented methods behave exactly as their counterparts in a
built-in dictionary.
     
.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~cf.Domain.analyse
   ~cf.Domain.aux_coords
   ~cf.Domain.cell_measures
   ~cf.Domain.clear
   ~cf.Domain.close
   ~cf.Domain.coord
   ~cf.Domain.coordinates
   ~cf.Domain.copy
   ~cf.Domain.dim_coords
   ~cf.Domain.dimension_name
   ~cf.Domain.direction
   ~cf.Domain.directions
   ~cf.Domain.dump
   ~cf.Domain.equals
   ~cf.Domain.expand_dims
   ~cf.Domain.get
   ~cf.Domain.get_keys
   ~cf.Domain.has_key
   ~cf.Domain.insert_aux_coordinate
   ~cf.Domain.insert_cell_measure
   ~cf.Domain.insert_dim_coordinate
   ~cf.Domain.insert_dimension
   ~cf.Domain.insert_transform
   ~cf.Domain.items
   ~cf.Domain.itercoordinates
   ~cf.Domain.iteritems
   ~cf.Domain.iterkeys
   ~cf.Domain.itervalues
   ~cf.Domain.keys
   ~cf.Domain.map_dims
   ~cf.Domain.new_auxiliary_identifier
   ~cf.Domain.new_cell_measure_identifier
   ~cf.Domain.new_dimension_identifier
   ~cf.Domain.new_transform_identifier
   ~cf.Domain.pop
   ~cf.Domain.popitem
   ~cf.Domain.remove_cell_measure
   ~cf.Domain.remove_coordinate
   ~cf.Domain.remove_dimension
   ~cf.Domain.remove_transform
   ~cf.Domain.setdefault
   ~cf.Domain.squeeze
   ~cf.Domain.update
   ~cf.Domain.values
