.. currentmodule:: cf
.. default-role:: obj

cf.CellMethods
==============

.. autoclass:: CellMethods
   :no-members:
   :no-inherited-members:

CellMethods methods
-------------------

Undocumented methods behave exactly as their counterparts in a
built-in list.
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.CellMethods.append
   ~cf.CellMethods.copy
   ~cf.CellMethods.count
   ~cf.CellMethods.dump
   ~cf.CellMethods.equals
   ~cf.CellMethods.extend
   ~cf.CellMethods.has_cellmethods
   ~cf.CellMethods.index
   ~cf.CellMethods.insert
   ~cf.CellMethods.netCDF_translation
   ~cf.CellMethods.parse
   ~cf.CellMethods.pop
   ~cf.CellMethods.remove
   ~cf.CellMethods.reverse
   ~cf.CellMethods.strings
