.. currentmodule:: cf
.. default-role:: obj

cf.CellMeasure
==============

.. autoclass:: cf.CellMeasure
   :no-members:
   :no-inherited-members:

CellMeasure CF Properties
-------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CellMeasure.add_offset
   ~cf.CellMeasure.calendar
   ~cf.CellMeasure.comment
   ~cf.CellMeasure._FillValue
   ~cf.CellMeasure.history
   ~cf.CellMeasure.leap_month
   ~cf.CellMeasure.leap_year
   ~cf.CellMeasure.long_name
   ~cf.CellMeasure.missing_value
   ~cf.CellMeasure.month_lengths
   ~cf.CellMeasure.scale_factor
   ~cf.CellMeasure.standard_name
   ~cf.CellMeasure.units
   ~cf.CellMeasure.valid_max
   ~cf.CellMeasure.valid_min
   ~cf.CellMeasure.valid_range

CellMeasure data attributes
---------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CellMeasure.array
   ~cf.CellMeasure.Data
   ~cf.CellMeasure.dtype
   ~cf.CellMeasure._FillValue
   ~cf.CellMeasure.first_datum
   ~cf.CellMeasure.hardmask
   ~cf.CellMeasure.isscalar
   ~cf.CellMeasure.last_datum
   ~cf.CellMeasure.mask
   ~cf.CellMeasure.ndim
   ~cf.CellMeasure.shape
   ~cf.CellMeasure.size
   ~cf.CellMeasure.Units
   ~cf.CellMeasure.varray

CellMeasure attributes
----------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CellMeasure.id
   ~cf.CellMeasure.measure
   ~cf.CellMeasure.properties
   ~cf.CellMeasure.subspace

CellMeasure methods
-------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.CellMeasure.binary_mask
   ~cf.CellMeasure.chunk
   ~cf.CellMeasure.clip
   ~cf.CellMeasure.copy
   ~cf.CellMeasure.cos
   ~cf.CellMeasure.delprop
   ~cf.CellMeasure.dump
   ~cf.CellMeasure.equals
   ~cf.CellMeasure.expand_dims
   ~cf.CellMeasure.flip
   ~cf.CellMeasure.getprop
   ~cf.CellMeasure.hasprop
   ~cf.CellMeasure.identity
   ~cf.CellMeasure.insert_data
   ~cf.CellMeasure.match
   ~cf.CellMeasure.name
   ~cf.CellMeasure.override_units
   ~cf.CellMeasure.select
   ~cf.CellMeasure.setitem
   ~cf.CellMeasure.setmask
   ~cf.CellMeasure.setprop
   ~cf.CellMeasure.sin
   ~cf.CellMeasure.squeeze
   ~cf.CellMeasure.transpose
