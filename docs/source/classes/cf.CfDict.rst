.. currentmodule:: cf
.. default-role:: obj

cf.CfDict
=========

.. autoclass:: cf.CfDict
   :no-members:
   :no-inherited-members:

CfDict methods
--------------
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.CfDict.clear
   ~cf.CfDict.copy
   ~cf.CfDict.equals
   ~cf.CfDict.get
   ~cf.CfDict.get_keys
   ~cf.CfDict.has_key
   ~cf.CfDict.items
   ~cf.CfDict.iteritems
   ~cf.CfDict.iterkeys
   ~cf.CfDict.itervalues
   ~cf.CfDict.keys
   ~cf.CfDict.pop
   ~cf.CfDict.popitem
   ~cf.CfDict.setdefault
   ~cf.CfDict.update
   ~cf.CfDict.values
