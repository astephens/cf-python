.. currentmodule:: cf
.. default-role:: obj

cf.Data
=======

.. autoclass:: cf.Data
   :no-members:
   :no-inherited-members:

Data attributes
---------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Data.array
   ~cf.Data.dtype
   ~cf.Data._FillValue
   ~cf.Data.first_datum
   ~cf.Data.hardmask
   ~cf.Data.ismasked
   ~cf.Data.isscalar
   ~cf.Data.last_datum
   ~cf.Data.mask
   ~cf.Data.ndim
   ~cf.Data.shape
   ~cf.Data.size
   ~cf.Data.Units
   ~cf.Data.varray

**Partition matrix attributes**
 
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: attribute.rst
 
   ~cf.Data.dimensions
   ~cf.Data.directions
   ~cf.Data.partitions
   ~cf.Data.pmdimensions
   ~cf.Data.pmndim
   ~cf.Data.pmshape
   ~cf.Data.pmsize

.. comment
   ~cf.Data.add_partitions
   ~cf.Data.change_dimension_names
   ~cf.Data.expand_partition_dims
   ~cf.Data.new_dimension_name
   ~cf.Data.override_units
   ~cf.Data.partition_boundaries
 
Data methods
------------
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Data.all
   ~cf.Data.any
   ~cf.Data.binary_mask
   ~cf.Data.chunk
   ~cf.Data.clip
   ~cf.Data.conform_args
   ~cf.Data.copy
   ~cf.Data.cos
   ~cf.Data.dump
   ~cf.Data.equals
   ~cf.Data.expand_dims
   ~cf.Data.flat
   ~cf.Data.flip
   ~cf.Data.func
   ~cf.Data.iterindices
   ~cf.Data.new_dimension_identifier
   ~cf.Data.override_units
   ~cf.Data.save_to_disk
   ~cf.Data.setitem
   ~cf.Data.setmask
   ~cf.Data.sin
   ~cf.Data.squeeze
   ~cf.Data.to_disk
   ~cf.Data.to_memory
   ~cf.Data.transpose

**Partition matrix methods**
 
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Data.add_partitions
   ~cf.Data.change_dimension_names
   ~cf.Data.chunk
   ~cf.Data.conform_args
   ~cf.Data.expand_partition_dims
   ~cf.Data.new_dimension_identifier
   ~cf.Data.partition_boundaries
   ~cf.Data.save_to_disk
   ~cf.Data.to_disk
   ~cf.Data.to_memory

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

Data arithmetic and comparison operations
-----------------------------------------

Arithmetic and comparison operations are defined on a field as
element-wise array operations which yield a new `cf.Data` object or,
for augmented arithmetic assignments, modify the master data array
in-place.

**Comparison operators**

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Data.__lt__
   ~cf.Data.__le__
   ~cf.Data.__eq__
   ~cf.Data.__ne__
   ~cf.Data.__gt__
   ~cf.Data.__ge__

**Binary arithmetic operators**

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Data.__add__     
   ~cf.Data.__sub__     
   ~cf.Data.__mul__     
   ~cf.Data.__div__     
   ~cf.Data.__truediv__ 
   ~cf.Data.__floordiv__
   ~cf.Data.__pow__     
   ~cf.Data.__and__     
   ~cf.Data.__or__
   ~cf.Data.__xor__     

**Binary arithmetic operators with reflected (swapped) operands**

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Data.__radd__     
   ~cf.Data.__rsub__     
   ~cf.Data.__rmul__     
   ~cf.Data.__rdiv__     
   ~cf.Data.__rtruediv__ 
   ~cf.Data.__rfloordiv__
   ~cf.Data.__rpow__   
   ~cf.Data.__rand__     
   ~cf.Data.__ror__
   ~cf.Data.__rxor__     

**Augmented arithmetic assignments**

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Data.__iadd__ 
   ~cf.Data.__isub__ 
   ~cf.Data.__imul__ 
   ~cf.Data.__idiv__ 
   ~cf.Data.__itruediv__
   ~cf.Data.__ifloordiv__
   ~cf.Data.__ipow__ 
   ~cf.Data.__iand__     
   ~cf.Data.__ior__
   ~cf.Data.__ixor__     

**Unary arithmetic operators**

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Data.__neg__    
   ~cf.Data.__pos__    
   ~cf.Data.__abs__    
   ~cf.Data.__invert__ 
 
