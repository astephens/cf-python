.. currentmodule:: cf
.. default-role:: obj

cf.AncillaryVariables
=====================

.. autoclass:: cf.AncillaryVariables
   :no-members:
   :no-inherited-members:

AncillaryVariables attributes
-----------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.AncillaryVariables.subspace

AncillaryVariables methods
--------------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.AncillaryVariables.append
   ~cf.AncillaryVariables.coord
   ~cf.AncillaryVariables.copy
   ~cf.AncillaryVariables.count
   ~cf.AncillaryVariables.delprop
   ~cf.AncillaryVariables.dump
   ~cf.AncillaryVariables.equals
   ~cf.AncillaryVariables.extend
   ~cf.AncillaryVariables.getprop
   ~cf.AncillaryVariables.hasprop
   ~cf.AncillaryVariables.index
   ~cf.AncillaryVariables.insert
   ~cf.AncillaryVariables.match
   ~cf.AncillaryVariables.name
   ~cf.AncillaryVariables.override_units
   ~cf.AncillaryVariables.pop
   ~cf.AncillaryVariables.remove
   ~cf.AncillaryVariables.reverse
   ~cf.AncillaryVariables.select
   ~cf.AncillaryVariables.set_equals
   ~cf.AncillaryVariables.setprop
   ~cf.AncillaryVariables.sort
   ~cf.AncillaryVariables.squeeze
   ~cf.AncillaryVariables.unsqueeze
 