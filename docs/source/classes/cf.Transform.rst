.. currentmodule:: cf
.. default-role:: obj

cf.Transform
============

.. autoclass:: cf.Transform
   :no-members:
   :no-inherited-members:

Transform attributes
--------------------

.. autosummary::
   :toctree: ../generated/	
   :template: attribute.rst

   ~cf.Transform.isformula_terms
   ~cf.Transform.isgrid_mapping
   ~cf.Transform.name

Transform methods
-----------------
     
.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~cf.Transform.clear
   ~cf.Transform.copy
   ~cf.Transform.dump
   ~cf.Transform.equals
   ~cf.Transform.get
   ~cf.Transform.get_keys
   ~cf.Transform.has_key
   ~cf.Transform.items
   ~cf.Transform.iteritems
   ~cf.Transform.iterkeys
   ~cf.Transform.itervalues
   ~cf.Transform.keys
   ~cf.Transform.pop
   ~cf.Transform.popitem
   ~cf.Transform.setdefault
   ~cf.Transform.update
   ~cf.Transform.values
