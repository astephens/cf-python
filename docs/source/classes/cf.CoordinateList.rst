.. currentmodule:: cf
.. default-role:: obj

cf.CoordinateList
=================

.. autoclass:: cf.CoordinateList
   :no-members:
   :no-inherited-members:

CoordinateList attributes
-------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CoordinateList.subspace

CoordinateList methods
----------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~cf.CoordinateList.append
   ~cf.CoordinateList.copy
   ~cf.CoordinateList.count
   ~cf.CoordinateList.delprop
   ~cf.CoordinateList.dump
   ~cf.CoordinateList.equals
   ~cf.CoordinateList.extend
   ~cf.CoordinateList.getprop
   ~cf.CoordinateList.hasprop
   ~cf.CoordinateList.index
   ~cf.CoordinateList.insert
   ~cf.CoordinateList.match
   ~cf.CoordinateList.name
   ~cf.CoordinateList.override_units
   ~cf.CoordinateList.pop
   ~cf.CoordinateList.remove
   ~cf.CoordinateList.reverse
   ~cf.CoordinateList.select
   ~cf.CoordinateList.set_equals
   ~cf.CoordinateList.setprop
   ~cf.CoordinateList.sort
