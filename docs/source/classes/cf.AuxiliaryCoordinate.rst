.. currentmodule:: cf
.. default-role:: obj

cf.AuxiliaryCoordinate
======================

.. autoclass:: cf.AuxiliaryCoordinate
   :no-members:
   :no-inherited-members:

AuxiliaryCoordinate CF properties
---------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.AuxiliaryCoordinate.add_offset
   ~cf.AuxiliaryCoordinate.axis
   ~cf.AuxiliaryCoordinate.calendar
   ~cf.AuxiliaryCoordinate.comment
   ~cf.AuxiliaryCoordinate._FillValue
   ~cf.AuxiliaryCoordinate.history
   ~cf.AuxiliaryCoordinate.leap_month
   ~cf.AuxiliaryCoordinate.leap_year
   ~cf.AuxiliaryCoordinate.long_name
   ~cf.AuxiliaryCoordinate.missing_value
   ~cf.AuxiliaryCoordinate.month_lengths
   ~cf.AuxiliaryCoordinate.positive
   ~cf.AuxiliaryCoordinate.scale_factor
   ~cf.AuxiliaryCoordinate.standard_name
   ~cf.AuxiliaryCoordinate.units
   ~cf.AuxiliaryCoordinate.valid_max
   ~cf.AuxiliaryCoordinate.valid_min
   ~cf.AuxiliaryCoordinate.valid_range

AuxiliaryCoordinate data attributes
-----------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.AuxiliaryCoordinate.array
   ~cf.AuxiliaryCoordinate.Data
   ~cf.AuxiliaryCoordinate.dtype
   ~cf.AuxiliaryCoordinate._FillValue
   ~cf.AuxiliaryCoordinate.first_datum
   ~cf.AuxiliaryCoordinate.hardmask
   ~cf.AuxiliaryCoordinate.isscalar
   ~cf.AuxiliaryCoordinate.last_datum
   ~cf.AuxiliaryCoordinate.mask
   ~cf.AuxiliaryCoordinate.ndim
   ~cf.AuxiliaryCoordinate.shape
   ~cf.AuxiliaryCoordinate.size
   ~cf.AuxiliaryCoordinate.Units
   ~cf.AuxiliaryCoordinate.varray

AuxiliaryCoordinate attributes
------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.AuxiliaryCoordinate.attributes
   ~cf.AuxiliaryCoordinate.bounds
   ~cf.AuxiliaryCoordinate.climatology
   ~cf.AuxiliaryCoordinate.id
   ~cf.AuxiliaryCoordinate.isbounded
   ~cf.AuxiliaryCoordinate.islatitude
   ~cf.AuxiliaryCoordinate.islongitude
   ~cf.AuxiliaryCoordinate.isscalar
   ~cf.AuxiliaryCoordinate.properties
   ~cf.AuxiliaryCoordinate.subspace
   ~cf.AuxiliaryCoordinate.transforms

AuxiliaryCoordinate methods
---------------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.AuxiliaryCoordinate.asauxiliary
   ~cf.AuxiliaryCoordinate.asdimension
   ~cf.AuxiliaryCoordinate.binary_mask
   ~cf.AuxiliaryCoordinate.chunk
   ~cf.AuxiliaryCoordinate.close
   ~cf.AuxiliaryCoordinate.contiguous
   ~cf.AuxiliaryCoordinate.copy
   ~cf.AuxiliaryCoordinate.cos
   ~cf.AuxiliaryCoordinate.delprop
   ~cf.AuxiliaryCoordinate.dump
   ~cf.AuxiliaryCoordinate.equals
   ~cf.AuxiliaryCoordinate.expand_dims
   ~cf.AuxiliaryCoordinate.fill_value
   ~cf.AuxiliaryCoordinate.flip
   ~cf.AuxiliaryCoordinate.get_data
   ~cf.AuxiliaryCoordinate.getprop
   ~cf.AuxiliaryCoordinate.hasprop
   ~cf.AuxiliaryCoordinate.identity
   ~cf.AuxiliaryCoordinate.insert_bounds
   ~cf.AuxiliaryCoordinate.insert_data
   ~cf.AuxiliaryCoordinate.insert_transform
   ~cf.AuxiliaryCoordinate.match
   ~cf.AuxiliaryCoordinate.name
   ~cf.AuxiliaryCoordinate.override_units
   ~cf.AuxiliaryCoordinate.select
   ~cf.AuxiliaryCoordinate.setitem
   ~cf.AuxiliaryCoordinate.setmask
   ~cf.AuxiliaryCoordinate.setprop
   ~cf.AuxiliaryCoordinate.sin
   ~cf.AuxiliaryCoordinate.squeeze
   ~cf.AuxiliaryCoordinate.transpose
