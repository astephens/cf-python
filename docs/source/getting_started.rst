Getting started
===============

.. toctree::
   :maxdepth: 1

   installation
   a_first_example
   further_examples
