.. currentmodule:: cf
.. default-role:: obj

.. _function:

Functions of the :mod:`cf` module
=================================

Input and output
----------------

.. autosummary::
   :nosignatures:
   :toctree: generated/
   :template: function.rst

   cf.close_files
   cf.close_one_file
   cf.dump
   cf.open_files
   cf.open_files_threshold_exceeded
   cf.pickle
   cf.read 
   cf.write
   cf.unpickle

Aggregation
-----------

.. autosummary::
   :nosignatures:
   :toctree: generated/
   :template: function.rst

   cf.aggregate

.. _functions-mathematical-operations:

Mathematical operations
-----------------------

.. autosummary::
   :nosignatures:
   :toctree: generated/
   :template: function.rst

   cf.clip
   cf.collapse
   cf.cos
   cf.sin

Data array manipulations
------------------------

.. autosummary::
   :nosignatures:
   :toctree: generated/
   :template: function.rst

   cf.expand_dims
   cf.flip
   cf.squeeze
   cf.transpose
   cf.unsqueeze

Comparison
----------

.. autosummary::
   :nosignatures:
   :toctree: generated/
   :template: function.rst

   cf.equals
   cf.equivalent
   cf.eq
   cf.ge
   cf.gt
   cf.le
   cf.lt
   cf.ne
   cf.set
   cf.wi
   cf.wo

Retrieval and setting of constants
----------------------------------

.. autosummary::
   :nosignatures:
   :toctree: generated/
   :template: function.rst

   cf.ATOL
   cf.CHUNKSIZE
   cf.FM_THRESHOLD
   cf.MINNCFM
   cf.OF_FRACTION
   cf.RTOL
   cf.TEMPDIR

Miscellaneous
-------------

.. autosummary::
   :nosignatures:
   :toctree: generated/
   :template: function.rst

   cf.abspath
   cf.dirname
   cf.flat
   cf.pathjoin
   cf.relpath
