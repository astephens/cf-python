.. currentmodule:: cf
.. default-role:: obj

.. _class:

Classes of the :mod:`cf` module
===============================

Field class
-----------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   cf.Field		              

Field component classes
-----------------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   cf.AncillaryVariables
   cf.AuxiliaryCoordinate
   cf.CellMeasure
   cf.CellMethods
   cf.CoordinateBounds
   cf.Data
   cf.DimensionCoordinate
   cf.Domain
   cf.Flags
   cf.Transform
   cf.Units

Miscellaneous classes
---------------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   cf.Comparison     
   cf.CoordinateList
   cf.FieldList

Data component classes
----------------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   cf.Partition
   cf.PartitionMatrix

Base classes
------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   cf.CfDict       
   cf.CfList         
   cf.Coordinate
   cf.Variable       
   cf.VariableList


.. _inheritance_diagrams:

Inheritance diagram
-------------------

The classes defined by the cf package inherit as follows:

.. image:: images/inheritance.png

.. commented
   .. inheritance-diagram:: cf.Data
                           cf.Flags	
                           cf.Units
                           cf.Field
                           cf.CellMeasure
                           cf.CoordinateBounds
       			   cf.FieldList
       			   cf.AncillaryVariables
                           cf.CoordinateList
                           cf.CellMethods
      			   cf.Domain		 
                           cf.Transform
                           cf.Partition		
                           cf.PartitionMatrix
                           cf.AuxiliaryCoordinate
                           cf.DimensionCoordinate
       :parts: 1

This inheritance diagram shows, for example:

* A `cf.Field` object and a `cf.DimensionCoordinate` both have all of
  the features of a `cf.Variable` object, e.g. they may have a data
  array and CF attributes.

* A `cf.FieldList` object is an iterable [#f1]_ which supports efficient
  element access using integer indices and defines a :py:obj:`len`
  method that returns the length of the sequence.

* A `cf.Domain` object supports arbitrary, hashable key lookups and
  implements dictionary-like methods, such as :py:obj:`~dict.values`.

----

.. rubric:: Footnotes

.. [#f1] An object capable of returning its members one at a time.
