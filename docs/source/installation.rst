Installation
============

Downloads, installation instructions, documentation and source code
are all available from the `cf-python home page
<http://cfpython.bitbucket.org>`_.

Installation instructions and hardware and software dependencies are
detailed in the README.md file contained with the downloaded source
code, which may also be read at
`<https://bitbucket.org/cfpython/cf-python>`_.

Please raise any questions or problems through the `cf-python home
page <http://cfpython.bitbucket.org>`_.

Dependencies
------------

* At present, the package only runs under Linux. However, the
  operating system dependencies are few (relating to the management of
  memory and open files) and most likely easily remedied for other
  operating systems.

* Requires a python version from 2.6 up to, but not including, 3.0.

* Requires the python numpy package at version 1.6 or newer.

* Requires the python netCDF4 package at version 0.9.3 or newer. This
  package requires the netCDF, HDF5 and zlib libraries.

* Requires UNIDATA's Udunits-2 package This is a library which
  provides support for units of physical quantities.
