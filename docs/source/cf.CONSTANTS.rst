.. currentmodule:: cf
.. default-role:: obj

cf.CONSTANTS
============

.. data:: CONSTANTS

   A dictionary of useful constants.

   Whilst the dictionary may be modified directly, it is safer to
   retrieve and set the values with a function where one is
   provided. This is due to interdependencies between some values.

   :Keys:

        ATOL : float
	    The value of absolute tolerance for testing numerically
	    tolerant equality. Retrieved and set with the `ATOL`
	    function.

        CHUNKSIZE : int
	    The chunk size (in bytes) for data storage and
	    processing. Retrieved and set with the `CHUNKSIZE`
	    function.

        FM_THRESHOLD : float
	    The minimum amount of memory (in kibibytes) to be kept
	    free for temporary work space. Retrieved and set with the
	    `FM_THRESHOLD` function.

        MINNCFM : int
	    The number of chunk sizes to be kept free for temporary
	    work space. Retrieved and set with the `MINNCFM` function.

        OF_FRACTION : float
	    The fraction of the maximum number of concurrently open
	    files which may be used for files containing data
	    arrays. Retrieved and set with the `OF_FRACTION` function.

        RTOL : float
	    The value of relative tolerance for testing numerically
	    tolerant equality. Retrieved and set with the `RTOL`
	    function.

        TEMPDIR : str
	    The location to store temporary files. By default, this
	    will be the default directory used by the
	    :py:mod:`tempfile` module. Retrieved and set with the
	    `TEMPDIR` function.

   **Examples**

   >>> cf.CONSTANTS
   {'ATOL': 2.2204460492503131e-16,
    'CHUNKSIZE': 104857600,
    'FM_THRESHOLD': 1024000.0,
    'MINNCFM': 10,
    'OF_FRACTION': 0.5,
    'RTOL': 2.2204460492503131e-16,
    'TEMPDIR': '/tmp'}
   >>> cf.TEMPDIR('/home/me/tmp')
   >>> cf.CONSTANTS['MINNCFM'] = 15
   >>> cf.CHUNKSIZE(1073741824)
   >>> cf.CONSTANTS
   {'ATOL': 2.2204460492503131e-16,
    'CHUNKSIZE': 1073741824,
    'FM_THRESHOLD': 15728640.0,
    'MINNCFM': 15,
    'OF_FRACTION': 0.5,
    'RTOL': 2.2204460492503131e-16,
    'TEMPDIR': '/home/me/tmp'}