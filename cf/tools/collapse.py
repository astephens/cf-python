from numpy import average  as numpy_average
from numpy import broadcast_arrays  as numpy_broadcast_arrays
from numpy import dtype    as numpy_dtype
from numpy import empty    as numpy_empty
from numpy import max      as numpy_max
from numpy import multiply as numpy_multiply
from numpy import min      as numpy_min
from numpy import ones     as numpy_ones
from numpy import resize   as numpy_resize
from numpy import sum      as numpy_sum

from numpy.ma import average     as numpy_ma_average
from numpy.ma import count       as numpy_ma_count
from numpy.ma import empty       as numpy_ma_empty
from numpy.ma import masked      as numpy_ma_masked
from numpy.ma import is_masked   as numpy_ma_is_masked
from numpy.ma import MaskedArray as numpy_ma_MaskedArray
from numpy.ma import resize      as numpy_ma_resize

from ..ancillaryvariables import AncillaryVariables
from ..cellmethods        import CellMethods
from ..coordinate         import Coordinate, DimensionCoordinate
from ..coordinatebounds   import CoordinateBounds
from ..fieldlist          import FieldList
from ..variable           import Variable

from ..data.data import Data

import sys #dch

def collapse(fields, method, axes=None, weights='infer', unbiased=False,
             ignore_size1=True, squeeze=False):
    '''

Return new fields with dimensions collapsed by statistical operations.

In all cases, the presence of missing data in an input field's data
array is accounted for during the collapse.

The following collapse methods are available over any subset of the
fields' dimensions, where the methods are defined exactly as for CF
cell methods:

==================  =======================================  =======================
Method              Description                              Options
==================  =======================================  =======================
min                 Minima over the specified dimensions    
                                                                                    
max                 Maxima over the specified dimensions

mid_range           Means of the minima and maxima over 
                    the specified  dimensions

mean                Means over the specified  dimensions     Weighted or unweighted
                                                                                    
sum                 Sums over the specified dimensions       Weighted or unweighted

standard_deviation  Standard deviations over the specified   Weighted or unweighted,
                    dimensions                               biased or unbiased

variance            Variances over the specified dimensions  Weighted or unweighted,
                                                             biased or unbiased
==================  =======================================  =======================

:Parameters:

    fields : (sequence of) Field or FieldList
        The field or fields to be collapsed.

    method : str or CellMethods
        Define the collapse method (or methods). There are two ways of
        setting the collapse method:

           * A string containing a single method (such as ``'min'``).
           
             - The method is one of the available CF cell methods.

             - All of the dimensions specified by the *axes* parameter
               are collapsed simultaneously with this method.                 
        ..

           * A CF cell methods-like string (such as ``'time: max'`` or
             ``'time: max dim0: mean 2: min'``) or a `cf.CellMethods`
             object equivalent to such a string.
           
             - Specifies both collapse methods and the axes to apply
               them to.

             - Each method is one of the available CF cell
               methods. The axes are interpreted as for the *axes*
               parameter.

             - When multiple cell methods are specified, each collapse
               is carried out independently and in the order that they
               are given. The *axes* parameter must not be set.

    axes : (sequence of) int or str, optional
        The axis or axes which are to be simultaneously collapsed with
        the method specified by the *method* parameter. May be any
        one, or any combination of:

           * A dimension identity, as a defined by an appropriate
             coordinate's `~Coordinate.identity` method (such as
             ``'time'``).
             
           * The integer position of a dimension in the field's data
             array (such as ``2``).
             
           * The internal name of a dimension in the field's domain
             (such as ``'dim0'``).

        By default all dimensions with size greater than 1 are
        collapsed, unless the *method* is a CF cell methods-like
        string or a `cf.CellMethods` object, in which case the
        collapse axes are already specified and the *axes* parameter
        must not be set.

    weights : str or None or dict, optional   
        Specify the weights to be used in the collapse. One of:

           =============  ===============================================
           weights        Description
           =============  ===============================================
           ``'equal'``    Perform an unweighted collapse.
           
           `None`         The same as ``'equal'``.
           
           ``'infer'``    Infer weights based on cell measures and
                          dimension coordinates. Cell measures are
                          used if possible, otherwise the weights are
                          based on dimension coordinate values as
                          follows: A (rotated) latitude dimension
                          coordinate is weighted linearly by the cell
                          sizes of the sine of its values and all
                          other dimension coordinates are weighted
                          linearly by cell sizes. If there are no cell
                          measures or dimension coordinates spanning a
                          dimension then it is unweighted.
           
           `dict`         Specify weights for one or more combinations
                          of one or more of the dimensions. The keys
                          specify a dimension as for the *axes*
                          parameter, or a tuple of axes. Each value is
                          an array-like or `cf.Variable`-like object
                          containing the weights for the dimension(s).
                          If a dimension coordinate is given then
                          weights are calculated as for the
                          ``'infer'`` option. A single dimension's
                          weights may also be one of ``'equal'``,
                          `None` or ``'infer'``, in which case that
                          dimension's weights are calculated as for
                          the given option. Unspecified collapse
                          dimensions have their weights inferred. The
                          weights used in the calculation are the
                          outer product of those specified. The
                          dictionary may contain weights for
                          non-collapse dimensions.

           =============  ===============================================

        By default the ``'infer'`` option is used. Note that
        specifying an empty dictionary is equivalent to the
        ``'infer'`` option.

    unbiased : bool, optional
        If True then calculate unbiased statisics where applicable. By
        default biased statistics are calculated.

    ignore_size1 : bool, optional
        If False then raise a ValueError if a dimension to be
        collapsed has size 1, regardless of whether or not it spans
        the data array. By default size 1 collapse dimensions are
        ignored, regardless of whether or not they span the data
        array.

    squeeze : bool, optional
        If True then remove collapsed dimensions from the output data
        array. By default the output data array retains collapsed
        dimensions with size 1 if they were spanned by the original
        data array. It is not possible to remove collapsed dimensions
        when mutliple methods have been specified.

:Returns:

   out : Field or FieldList
        The collapsed fields.

**Examples**

>>> g = cf.collapse(f, 'max')
>>> g = cf.collapse(f, 'min', axes=[2, 1])
>>> g = cf.collapse(f, 'sum', axes='dim2')
>>> g = cf.collapse(f, 'mid_range', axes=('latitude', 0, dim2'))
>>> g = cf.collapse(f, 'mean', axes='latitude', weights=None)
>>> g = cf.collapse(f, 'mean', axes='longitude', weights=None)
>>> g = cf.collapse(f, 'mean', axes=['longitude', 'latitude'], weights=None)
>>> g = cf.collapse(f, 'standard_deviation', weights=None)
>>> g = cf.collapse(f, 'variance', weights=None, unbiased=True)
>>> g = cf.collapse(f, 'mean', axes='latitude')
>>> g = cf.collapse(f, 'mean', axes='longitude')
>>> g = cf.collapse(f, 'mean', axes=['longitude', 'latitude'])
>>> g = cf.collapse(f, 'variance')
>>> g = cf.collapse(f, 'standard_deviation', unbiased=True)
>>> g = cf.collapse(f, 'longitude: mean latitude: max')

>>> weights = {'time': None,
...            (2, 'dim1'): numpy.arange(45).reshape(5, 9))
>>> g = cf.collapse(f, 'mean', weights=weights)

'''
    # ---------------------------------------------------------------
    # Flatten an input list or tuple of fields and field lists into a
    # single field list
    # ---------------------------------------------------------------
    if isinstance(fields, (list, tuple)):
        fields = [f for element in fields for f in element]

    # If method is a cell methods string (such as 'area: mean time:
    # max') then convert it to a CellMethods object
    if ':' in method:
        method = CellMethods(method)

    # ================================================================
    # If method is a CellMethods object then process its methods one
    # by one with recursive calls and then return.
    # ================================================================
    if isinstance(method, CellMethods):
        if squeeze:            
            raise ValueError(
"Can't collapse: Can't squeeze collapsed axes with multiple methods")

        if axes is not None:
            raise ValueError(
                "Can't collapse: Can't set both axes and cell methods")

        methods = [cm['method'] for cm in method]
        axes    = [cm['name']   for cm in method]
        for name, m in zip(axes, methods):
            if None in name:
                raise ValueError(
                    "Can't collapse: Invalid '%s' cell method name: %s" %
                    (m, None))
        #--- End: if

        # Collapse according to each cell method in turn with
        # recursive calls
        new = fields

        while methods:
            new = collapse(new, methods.pop(0), axes=axes.pop(0), weights=weights, 
                           unbiased=unbiased, ignore_size1=ignore_size1,
                           squeeze=False)
        #--- End: while

        return new
    #--- End: if

    # ================================================================
    # Still here? Then method is a string describing a single method
    # (e.g. 'mean')
    # ================================================================
    out = FieldList()

    for f in fields:
        new = f.copy(_omit_Data=True)

        domain = f.domain
        dimension_sizes = domain.dimension_sizes

        #-------------------------------------------------------------
        # Parse the collapse axes
        #-------------------------------------------------------------
        axes, collapse_dims = f._parse_axes(axes, 'collapse', dims=True,
                                            ignore=ignore_size1,
                                            default=range(f.ndim))
        
        if ignore_size1:
            # Ignore size 1 dimensions
            collapse_dims = [dim for dim in collapse_dims if dimension_sizes[dim] > 1]
            axes = f._parse_axes(collapse_dims, 'collapse', dims=False)
        else:
            # Fail if any size 1 dimensions are specified
            for dim in collapse_dims:
                if dimension_sizes[dim] == 1:
                    raise ValueError(
                        "Can't collapse: Size 1 dimension has been specified")
        #--- End: if

        # ------------------------------------------------------------
        # Calculate inferred weights, if required
        # ------------------------------------------------------------
        if weights == 'infer':
            wdims = collapse_dims[:]
            weights = {}
            
            # Get weights from cell measures, if possible.
            for key, cm in domain.cell_measures().iteritems():
                if cm.size == 1:
                    wdims = [dim for dim in wdims 
                             if dim not in domain.dimensions[key]]
                    continue

                if not set(wdims).issuperset(domain.dimensions[key]):
                    # Only use cell measure as weights if all of its
                    # dimensions are in wdims
                    continue

                cm = cm.copy()

                cm_dims = domain.dimensions[key][:]
                for dim in cm_dims[:]:
                    if dim not in domain.dimensions['data']:
                        cm.squeeze(dim)
                        cm_dims.remove(dim)
                #--- End: for

                weights[tuple(cm_dims)] = cm.get_data()

                wdims = [dim for dim in wdims if dim not in cm_dims]
            #--- End: for

            # Get weights for any remaining dimensions from dimension
            # coordinates, if possible.
            if wdims:
                s = domain.analyse()
                for dim in wdims:
                    size = dimension_sizes[dim]
                    if size > 1:
                        if dim in s['dim_to_coord']:
                            weights[dim] = s['dim_to_coord'][dim]
                        else:
                            weights[dim] = _equal_weights(size)
            #--- End: if

        elif weights in (None, 'equal'):
            weights = {}

        elif isinstance(weights, dict):
            new_weights = {}
            all_dims_with_weights = []

            for key in weights.keys():
                if not isinstance(key, tuple):
                    key = (key,)

                new_key = tuple([f.coord(dim, key=True) for dim in key])

                if not set(new_key).intersection(collapse_dims): 
                    # Ignore a weight which doesn't span any collapse
                    # dimensions
                    continue

                if not set(collapse_dims).issuperset(new_key):
                    raise ValueError(
"ERROR: Weight spans collapse and non-collapse dimensions: %s" % new_key)

                if set(new_key).intersection(all_dims_with_weights):
                    raise ValueError(
"ERROR: Two weights span the same collapse dimension: %s, %s" % new_key)

                all_dims_with_weights.extend(new_key)

                if len(new_key) == 1:
                    new_key = new_key[0]

                new_weights[new_key] = weights[key]
            #--- End: for            

            weights = new_weights

            # Sort out 1-d weights
            s = domain.analyse()
    
            for dim in collapse_dims:
                if dim not in all_dims_with_weights:
                    weights[dim] = 'infer'
                elif dim not in weights:
                    continue

                if weights[dim] == 'infer':
                    found_cell_measure = False
                    for key, cm in domain.cell_measures().iteritems():
                        if cm.ndim == 1 and domain.dimensions[key][0] == dim:
                            weights[dim] = cm
                            found_cell_measure = True
                            continue
                    #--- End: for
                    if not found_cell_measure:
                        if dim in s['dim_to_coord']:
                            weights[dim] = s['dim_to_coord'][dim]
                        else:
                            weights[dim] = _equal_weights(dimension_sizes[dim])

                elif weights[dim] in (None, 'equal'):
                    weights[dim] = _equal_weights(dimension_sizes[dim])
            #--- End: if                
        else:
            raise TypeError("Can't collapse: Invalid weights: %s" % repr(weights))

        # ------------------------------------------------------------
        # Parse any weights which are Variables into Data objects (or
        # None)
        # ------------------------------------------------------------
        for key, value in weights.iteritems():
            if isinstance(value, DimensionCoordinate):
                # Convert a coordinate into a weights Data object
                weights[key] = calc_weights(value, infer_bounds=True)
            elif isinstance(value, Coordinate):
                raise ValueError("Can't use %s object as weights" % 
                                 value.__class__.__name__)
            elif isinstance(value, Variable):
                weights[key] = value.get_data()
        #--- End: for

#        print weights
        x = []
#        print domain.dimensions['data'], f.Data.pmdimensions, 
        non_partition_dims = set(domain.dimensions['data']).difference(f.Data.pmdimensions)
#        print non_partition_dims
        for dims in weights:
            if dims in non_partition_dims:
                x.append(dims)
            elif non_partition_dims.issuperset(dims):
                x.append(dims)
        
        if len(x) > 1:
            base = [1]*len(x)
            w=[]
            new_key = []
            for j, i in enumerate(x):
#                print i
                shape = base[:]
                if isinstance(i, tuple): 
                    shape[j] = weights[i].size
                    w.append(weights[i].unsafe_array.reshape(shape))
                    del weights[i]
                    new_key.extend(i)
                else:
                    shape[j] = weights[i].size
                    w.append(weights[i].unsafe_array.reshape(shape))
                    del weights[i]
                    new_key.append(i)
        
#            weights[tuple(new_key)] = reduce(numpy_multiply, w)
            weights[tuple(new_key)] = Data(reduce(numpy_multiply, w).copy())
            
#        print 'x=', x, weights

#        for dims in x:
            

        # ------------------------------------------------------------
        # Collapse the data array
        # ------------------------------------------------------------
        new.Data = collapse_data(f.get_data(), method, axes=axes, weights=weights,
                                 unbiased=unbiased)

        # ------------------------------------------------------------
        # Update the cell methods
        # ------------------------------------------------------------
        update_cell_methods(new, method, collapse_dims)

        # ------------------------------------------------------------
        # Update ancillary variables
        # ------------------------------------------------------------
        update_ancillary_variables(new, collapse_dims)

        #-------------------------------------------------------------
        # Collapse the domain
        #-------------------------------------------------------------
        collapse_domain(new.domain, collapse_dims)

        # ------------------------------------------------------------
        # Squeeze collapsed dimensions
        # ------------------------------------------------------------
        if squeeze:
            new.squeeze(collapse_dims)

        # We need to finalize the new field because some of the
        # coordinates will have been redefined
        new.finalize()

        out.append(new)
    #--- End: for

    if isinstance(fields, out[0].__class__):
        # Return a Field
        return out[0]
    else:
        # Return a FieldList
        return out
#--- End: def

def collapse_data(data, method, axes=None, weights={}, unbiased=False):
    '''

Return a data array collapsed over one or more dimensions.

:Parameters:

    data : cf.Data
        Data to be collapsed.

    method : str

    axes : list of ints, optional

    weights : dict, optional

:Returns:

    out : cf.Data

**Examples**

'''
    data_dimensions = data.dimensions
    data_directions = data.directions

    collapse_dims = [data_dimensions[i] for i in axes]
    collapse_dirs = [data_directions[dim] for dim in collapse_dims]

    #-----------------------------------------------------------------
    # Parse the collapse axes
    #-----------------------------------------------------------------
    axes, dims = data._parse_axes(axes, 'collapse', dims=True,
                                  default=range(data.ndim))
    axes, dims = zip(*sorted(zip(axes, dims)))
    axes = list(axes)

    #-----------------------------------------------------------------
    # Initialize the output data array
    #-----------------------------------------------------------------
    indices = [slice(None)] * data.ndim
    for i in axes:
        indices[i] = 0

    new = data[tuple(indices)]
    
    # Squeeze the collapse axes out of the output data. This is
    # important. But note that we'll put them back at the end.
    new.squeeze(axes)
    
    new_dimensions = new.dimensions
    new_directions = new.directions

    #-----------------------------------------------------------------
    # Transpose the input data so that the collapse dimensions are all
    # in the fastest varying (outer) positions.
    # ----------------------------------------------------------------
    non_collapse_axes = [i for i in xrange(data.ndim) if i not in axes]
    if non_collapse_axes + axes != range(data.ndim):
        data = data.copy()
        data.transpose(non_collapse_axes + axes)

    #-----------------------------------------------------------------
    # Parse the weights.
    #
    # * Change the keys from dimension names to the integer positions
    #   of the dimensions.
    #
    # * Make sure all non-null weights are Data objects.
    # -----------------------------------------------------------------
    if weights:

        weights = weights.copy()

        # It is important to reset data_dimensions because we may have
        # transposed data
        data_dimensions = data.dimensions

        for key in weights.keys():
            weight = weights.pop(key)

            # Ignore undefined weights
            if weight is None:
                continue
            
            # Make sure the weight is a Data object
            if not isinstance(weight, data.__class__):
                weight = type(data)(weight)
            
            # Ignore string-valued weights
            if weight.dtype.char == 'S':
                continue
                
            # Ignore size 1 weights
            if weight.size == 1:
                continue

            if key in data_dimensions:
                weights[(data_dimensions.index(key),)] = weight
            elif isinstance(key, tuple):
                a = [key.index(dim) for dim in collapse_dims if dim in key]
                if a not in ([], range(len(key))):
                    nc = [i for i in xrange(len(key)) if i not in a]
                    weight = weight.copy()
                    weight.transpose(nc + a)
                    
                    key = [key[i] for i in nc+a]
                #--- End: if

                if not set(key).issubset(data_dimensions):
                    raise ValueError(
                        "Can't collapse: Incorrect weights dimensions: %s" %
                        repr(key))

                weights[tuple([data_dimensions.index(k) for k in key])] = weight
        #--- End: for

        # Check that the shape of the weights match the sizes of their
        # corresponding data array dimensions.
        data_shape = data.shape
        for key, weight in weights.iteritems():
            weight_shape = weight.shape
            for i, j in enumerate(key):
                if weight_shape[i] != data_shape[j]:
                    raise ValueError(
                        "Can't collapse: Incorrect weights shape: %s" %
                        repr(weight_shape))
    #--- End: if

    if not weights:
        weights = None

    units = new.Units

    #-------------------------------------------------------------
    # Set the output data type
    # ------------------------------------------------------------
    datatype = data.dtype
    if datatype.kind is 'i' and method not in ['max', 'min', 'mode']:
        # The output data-type is float if data is of integer type
        datatype = numpy_dtype(float)

    # ----------------------------------------------------------------
    # Define the collapse function
    # ----------------------------------------------------------------
    function = _collapse_functions[method]

    for partition in new.partitions.flat:
        del partition.subarray

    data.to_memory()

    save = new.save_to_disk()

    for partition in new.partitions.flat:

        # It is very important to set the part attribute to [] because
        # the value inherited from the input partition matrix could be
        # inappropriate.
        partition.part = []

        # We need to reset the dimensions attribute because
        # Data.squeeze() does not change it.
        partition._dimensions = new_dimensions[:]

        # We need to reset the directions attribute because
        # Data.squeeze() does not change it.
        partition._directions = new._copy_directions(new_directions)

        partition.Units = units

        # Initialize an empty array for this partition of the output
        # collapsed data array
        array = numpy_empty(partition.shape, dtype=datatype)

        # Set each element of the output partition's array
        masked = False
        for a_indices, m_indices in zip(partition.ndindex(), #iterarray_indices(),
                                        partition.master_ndindex()): #itermaster_indices()):

            value = function(data, m_indices, weights=weights,
                             unbiased=unbiased)

            if not masked and value is numpy_ma_masked:
                array = array.view(numpy_ma_MaskedArray)
                masked = True

            array[a_indices] = value
        #--- End: for

        partition.subarray = array
        partition.close(save)
    #--- End: for

    # Put the collapsed dimensions back in with size 1
    for axis, dim, direction in zip(axes, collapse_dims, collapse_dirs):
        new.expand_dims(axis, dim, direction)

    # ----------------------------------------------------------------
    # Return the collapsed data
    # ----------------------------------------------------------------
    return new
#--- End: def

def collapse_domain(domain, dims):
    '''

Collapse dimensions of a domain to size 1 in place.

A dimension coordinate for a collapse dimension is replaced with a
size 1 coordinate whose bounds are the maximum and minimum of the
original bounds array (of the original data array if there are no
bounds) and whose datum is the average of the new bounds.

All auxiliary coordinates and cell measures which span any of the
collapse dimensions are removed.

:Parameters:

    domain : Domain
        The domain to be collapsed.

    dims : sequence of strs 
        The dimensions to be collapsed, defined by their domain
        identifiers.

:Returns:

    None

**Examples**

>>> collapse_domain(f.domain, ['dim2', 'dim1'])

'''
    for dim in dims:
        # Ignore dimensions which are already size 1
        if domain.dimension_sizes[dim] == 1:
            continue
        
        if dim in domain:
            # This dimension has a dimension coordinate, so derive a
            # new size 1 dimension coordinate for this dimension
            coord = domain[dim]
        
            if coord._isbounded:
                bounds = [coord.bounds.first_datum, coord.bounds.last_datum]
            else:
                bounds = [coord.first_datum, coord.last_datum]

            units = coord.Units
            data   = Data([(bounds[0] + bounds[1])*0.5], units)
            bounds = Data([bounds], units)

#            coord.Data = Data([array], coord.Units)
#        
#            coord.bounds = CoordinateBounds(data=Data([bounds], coord.Units))
#
            coord.insert_data(data, bounds=bounds, copy=False)

            # Remove a formula_terms transform referenced by this
            # coordinate
            if hasattr(coord, 'transforms'):
                tkey = coord.transforms.values()[0]
                if (tkey not in domain.transforms
                    or domain.transforms[tkey].isformula_terms):
                    domain.remove_transform(tkey)
            #--- End: if

            # Put the new dimension coordinate into the domain
            domain.insert_dimension(1, dim=dim, replace=True)
            domain.insert_dim_coordinate(coord, key=dim, copy=False,
                                         replace=True)
        #--- End: if
        
        # Remove all auxiliary coordinates which span this dimension
        for aux in domain.aux_coords(dim):
            domain.remove_coordinate(aux)
        
        # Remove all cell measures which span this dimension
        for cm in domain.cell_measures(dim):
            domain.remove_cell_measure(cm)
    #--- End: for
#--- End: def

def update_cell_methods(f, method, dims):
    '''

:Parameters:

    f : Field

    method : str

    dims : list of strs

:Returns:

    None

**Examples**

'''
    if not hasattr(f, 'cell_methods'):
         f.cell_methods = CellMethods()

    domain = f.domain

    name = []
    for dim in dims:
        if dim in domain:
            name.append(domain[dim].identity(default=dim))
        else:
            name.append(dim)
    #--- End: for

    string = '%s: %s' % (': '.join(name), method)

    cell_method = CellMethods(string)
    
    cell_method[0]['dim'] = dims[:]

    f.cell_methods += cell_method
#--- End: def

def update_ancillary_variables(f, collapse_dims):
    '''

:Parameters:

    f : Field

    collapse_dims : sequence of strs

:Returns:

    None

**Examples**

'''
    if not hasattr(f, 'ancillary_variables'):
        return

    new_av = AncillaryVariables()

    for av in f.ancillary_variables:

        dim_map = av.domain.map_dims(f.domain)

        # Don't keep the ancillary variable if its master data array
        # has an undefined dimension with size > 1.
        keep = True
        for dim1 in set(av.domain.dimensions['data']).difference(dim_map):
            if av.domain.dimension_sizes[dim1] > 1:
                keep = False
                break
        #--- End: for
        if not keep:
            continue

        # Don't keep the ancillary variable if its master data array
        # has a defined dimension with size > 1 which is a collapse
        # dimension.
        keep = True
        for dim1, dim0 in dim_map.iteritems():
            if dim0 in collapse_dims and av.domain.dimension_sizes[dim1] > 1:
                keep = False
                break
        #--- End: for
        if not keep:
            continue

        new_av.append(av)
    #--- End: for

    if new_av:
        f.ancillary_variables = new_av
    else:
        del f.ancillary_variables 
#--- End: def

def _max(data, indices, *args, **kwargs):
    '''
        
Return the maximum value of a data array over the given indices.

If the entire array is masked then the returned value will be
`cf.masked`.

:Parameters:

    data : cf.Data

    args, kwargs :
        Ignored.

:Returns:

    out : scalar
        The maximum value, or `cf.masked` if the entire array is
        masked.

**Examples**

>>> print d
[0 1 2 --]
>>> _max(d)
2
>>> d[...] = cf.masked
>>> print d
[-- -- -- --]
>>> _max(d)
masked

'''
    # Note: numpy.max(array) works correctly for masked arrays.
    out = _weighted_map(data, numpy_max, numpy_max, indices, 1, data.dtype)
    return numpy_max(out)
#--- End: def

def _mean(data, indices, weights=None, **kwargs):
    '''
        
Return the weighted mean of a data array over the given indices.

If no weights are specified then equal weights are assumed.

The return type is numpy.float64 if the master data array is of
integer type, otherwise it is of the same type as master data array.

:Parameters:

    data : cf.Data

    indices: tuple

    weights : dict, optional        

:Returns:

    out : scalar
        The weighted mean.

'''
    datatype = data.dtype
    if datatype.kind is 'i':
        # The output datatype is float if data is of integer type
        datatype = numpy_dtype(float)

    out = _weighted_map(data, numpy_average, numpy_ma_average, indices, 2, datatype,
                        weights=weights, returned=True)

    if data.pmsize == 1:
        mean = out[0, 0]
    else:
        mean = numpy_ma_average(out[:, 0], weights=out[:, 1])

    return mean
#--- End: def

def _mid_range():
    '''
 
Return the average of the minimum and maximum values of a data array
over the given indices.

If the entire array is masked then the returned value will be
`cf.masked`.

:Parameters:

    data : cf.Data

    args, kwargs :
        Ignored if set.

:Returns:

    out : scalar
        The mid range value, or `cf.masked` if the entire array is
        masked.

**Examples**

>>> print d
[-- 1 2 4]
>>> _mid_range(d)
2.5
>>> d[...] = cf.masked
>>> print d
[-- -- -- --]
>>> _mid_range(d)
masked

'''
    return 0.5*(_min(data) + _max(data))
#--- End: def

    pass

def _min(data, indices, *args, **kwargs):
    '''
        
Return the minimum value of a data array over the given indices.

If the entire array is masked then the returned value will be
`cf.masked`.

The return type is numpy.float64 if the master data array is of
integer type, otherwise it is of the same type as master data array.

:Parameters:

    data : cf.Data

    args, kwargs :
        Ignored if set.

:Returns:

    out : scalar
        The minimum value, or `cf.masked` if the entire array is
        masked.

**Examples**

>>> print d
[-- 1 2 3]
>>> _min(d)
1
>>> d[...] = cf.masked
>>> print d
[-- -- -- --]
>>> _min(d)
masked

'''  
    # Note: numpy.min(array) works correctly for masked arrays.
    out = _weighted_map(data, numpy_min, numpy_min, indices, 1, data.dtype)
    return numpy_min(out)
#--- End: def

def _mode():
    '''

Return the mode of the master data array.
    
If the entire array is masked then the returned value will be
`cf.masked`.

'''
    raise NotImplementedError("Haven't coded mode, yet. Sorry.")
#--- End: def

def _standard_deviation(data, indices, weights=None, unbiased=False):
    '''

Return the standard deviation of a data array over the given indices.
    
If the entire array is masked then the returned value will be
`cf.masked`.

The return type is numpy.float64 if the master data array is of
integer type, otherwise it is of the same type as master data array.

:Parameters:

    data : cf.Data

    indices: tuple

    unbiased : bool, optional
        If True then the unbiased sample standard deviation will be
        returned. By default the biased population standard deviation
        will be returned.

    weights : dict, optional        

:Returns:

    out : scalar
        The standard deviation, or `cf.masked` if the entire array is
        masked.

**Examples**

>>> print d
[-- 1 2 3]
>>> _standard_deviation(d)

>>> d[...] = cf.masked
>>> print d
[-- -- -- --]
>>> _standard_deviation(d)
masked

'''
    variance = _variance(data, indices, weights=weights, unbiased=unbiased)
    return variance**0.5
#--- End: def

def _sum(data, indices, *args, **kwargs):
    '''
        
Return the sum of a data array over the given indices.

If the entire array is masked then the returned value will be
`cf.masked`.

The return type is numpy.float64 if the master data array is of integer type,
otherwise it is of the same type as master data array.

:Parameters:

    data : cf.Data

    args, kwargs :
        Ignored if set.

:Returns:

    out : scalar
        The sum value, or `cf.masked` if the entire array is masked.

**Examples**

>>> print d
[-- 1 2 3]
>>> _sum(d)
6
>>> d[...] = cf.masked
>>> print d
[-- -- -- --]
>>> _sum(d)
masked

'''
    datatype = data.dtype
    if datatype.kind is 'i':
        # The output data-type is float if data is of integer type
        datatype = numpy_dtype(float)

    # Note: numpy.sum(array) works correctly for masked arrays.
    out = _weighted_map(data, numpy_sum, numpy_sum, indices, 1, datatype)
    return numpy_sum(out)
#--- End: def

def _components(array, weights=None):
    '''

Return selected statistics derived from an array:

* Mean of the array
* Sum of weights for the array
* Sum of squares of weights for the array
* Biased population variance of the array

:Parameters:

    array : array-like
        The array from which the statistics are calculated.

    weights : array-like, optional
        The importance that each element has in the computations. If
        set then the weights array must have the same shape as
        *array*. By default all data in *array* are assumed to have a
        weight equal to one.

:Returns:

    out : numpy.ma.array
        The output array is structured as follows:
        * out[0] -> mean of the array
        * out[1] -> sum of weights for the array
        * out[2] -> sum of squares of weights for the array
        * out[3] -> biased population variance of the array

'''
    # Methodology:
    # http://en.wikipedia.org/wiki/Standard_deviation#Population-based_statistics
    # http://en.wikipedia.org/wiki/Weighted_mean#Weighted_sample_variance

    # Note: numpy.sum(array) works correctly for masked arrays.

    # Note: numpy.average(array) does NOT work correctly for
    # masked arrays.

    masked = numpy_ma_is_masked(array)
    if not masked:
        n = array.size
    else:
        n = numpy_ma_count(array)

    out = numpy_ma_empty(4)

    if not n:
        # Every element of this array is masked
        out[...] = numpy_ma_masked

    else:
        # Some elements of this this array are unmasked
        if weights is None:
            # Unweighted
            if not masked:
                mean = numpy_average(array)
            else:
                # Do masked case separately since it is much slower
                mean = numpy_ma_average(array)

            sw = n
            sw2 = n               
            sigma2 = numpy_sum((array - mean)**2/sw)
        else:
            # Weighted
            if not masked:
                mean, sw = numpy_average(array, weights=weights,
                                         returned=True)
            else:
                # Do masked case separately since it is much slower
                mean, sw = numpy_ma_average(array, weights=weights,
                                            returned=True)
                
            sw2 = numpy_sum(weights*weights)
            sigma2 = numpy_sum(weights*(array - mean)**2)/sw
        #--- End: if

        out[...] = [mean, sw, sw2, sigma2]
    #--- End: if

    return out
#--- End: def

def _variance(data, indices, weights=None, unbiased=False):
    '''

Return the variance of a data array over the given indices.

The biased population variance or the unbiased sample variance may be
calculated.
    
If the entire array is masked then the returned value will be
`cf.masked`.

The return type is numpy.float64 if the master array is of integer
type, otherwise it is of the same type as master data array.

:Parameters:

    data : cf.Data

    indices: tuple

    unbiased : bool, optional
        If True then the unbiased sample variance will be returned. By
        default the biased population variance will be returned.

    weights : dict, optional        

:Returns:

    out : scalar
        The variance, or `cf.masked` if the entire array is masked.

**Examples**

>>> print d
[-- 1 2 3]
>>> _variance(d)

>>> d[...] = cf.masked
>>> print d
[-- -- -- --]
>>> _variance(d)
masked

'''
    # Methodology:
    # http://en.wikipedia.org/wiki/Standard_deviation#Population-based_statistics
    # http://en.wikipedia.org/wiki/Weighted_mean#Weighted_sample_variance

    datatype = data.dtype
    if datatype.kind is 'i':
        # The output data type is float if the input data is of
        # integer type
        datatype = numpy_dtype(float)

    out = _weighted_map(data, _components, _components, indices, 4, datatype,
                        weights=weights)

    # ----------------------------------------------------------------
    # Calculate:
    # * The global mean (if required)
    # * The global sum of weights
    # * The global sum of squares of weights 
    # * The global variance
    # ----------------------------------------------------------------
    if data.pmsize == 1:
        gsum_of_weights  = out[0, 1]
        gsum_of_weights2 = out[0, 2]
        gvariance        = out[0, 3]

    else:
        mean     = out[:, 0]
        sw       = out[:, 1]
        sw2      = out[:, 2]
        variance = out[:, 3]

        if not numpy_ma_is_masked(mean):            
            gmean = numpy_average(mean, weights=sw)
        else:
            # Do masked case separately since it is much slower
            gmean = numpy_ma_average(mean, weights=sw)

        gsum_of_weights  = numpy_sum(sw)
        gsum_of_weights2 = numpy_sum(sw2)

        gvariance  = numpy_sum(sw*(variance + mean*mean))
        gvariance /= gsum_of_weights
        gvariance -= gmean * gmean
    #--- End: if

    if unbiased:
        # Convert the population variance to the unbiased sample
        # variance
        factor = gsum_of_weights/(gsum_of_weights**2 - gsum_of_weights2) 
        gvariance *= gsum_of_weights * factor
    #--- End: if

    return gvariance
#--- End: def

def _create_weights(partition, master_indices, master_weights):
    '''

:Parameters:

    partition : cf.Partition

    master_indices : tuple

    master_weights : dict

:Returns:

    out : numpy.ma.array

**Examples**

'''
    if not master_weights:
        return None

    partition_indices = partition.indices
    partition_shape   = tuple(partition.shape)
    base_shape = [1] * len(partition_shape)

    master_indices += partition_indices[len(master_indices):]

    weights = []
#    print master_weights.keys(), master_indices
    for key, weight in master_weights.iteritems():
        shape = base_shape[:]
        indices = []
        for i in key:
            shape[i] = partition_shape[i]
            indices.append(master_indices[i])
        #--- End: for
#        print weight.shape, shape, indices

        weight = weight[tuple(indices)].unsafe_array

        if weight.shape != partition_shape:
            weight = weight.reshape(shape)
            
        if not (weight.size == 1 and weight.item() == 1):
            weights.append(weight)
    #--- End: for

    # Create the outer product of the weights - i.e. create a weights
    # array which has the same shape as the partition's data array.
    if len(weights) > 1:
#        print 'REDUNCING'  , weights      
        weights = reduce(numpy_multiply, weights)
    elif weights:
        weights = weights[0]
    else:
        weights = None

#    print weights
#    print 'weights.shape, partition_shape=',weights.shape, partition_shape

#    # Broadcast the weights across all dimensions of the
#    # partition, and return them
#    if weights.shape != partition_shape:
#        if not numpy_ma_is_masked(weights):
#            weights, j = numpy_broadcast_arrays(weights, partition.subarray)
#    #        return numpy_resize(weights, partition_shape)
#        else:
#            weights = numpy_ma_resize(weights, partition_shape)
#    #--- End: if

    return weights
#--- End: def

def _weighted_map(data, function, ma_function, indices, dim2size, datatype, **kwargs):
    '''

Return a list of the results of applying the function to the
partitions of the arguement Data object.

All keyword arguments are passed into the function call. If there is a
`weights` keyword argument then this should either evaluate to False
or be a dictionary of weights for at least one of the data dimensions.

:Parameters:

    data : cf.Data

    function : function

    ma_function : function

    indices: tuple or None

    dim2size : int

    datatype : numpy.dtype

    kwargs :

:Returns:

    out : numpy.ma.array

**Examples**

'''
    data = data[indices]

    if dim2size == 1:
        shape = data.pmsize
    else:
        shape = (data.pmsize, dim2size)

    out = numpy_ma_empty(shape, dtype=datatype)

    weights = kwargs.get('weights', None)
    if weights is not None:
        kwargs = kwargs.copy()

    conform_args = data.conform_args(revert_to_file=True, readonly=True)

    for i, partition in enumerate(data.partitions.flat):
        array = partition.dataarray(**conform_args)

        if weights is not None:
            kwargs['weights'] = _create_weights(partition, indices, weights)

        if not numpy_ma_is_masked(array):
            # Array has no masked values
            out[i] = function(array, **kwargs)
        else:
            # Array has at least one masked value, so use the masked
            # array function
            out[i] = ma_function(array, **kwargs)

        partition.close()
    #--- End: for

    return out
#--- End: def

def calc_weights(coord, infer_bounds=False):
    '''

Calculate weights for collapsing. See def collapse for help

:Parameters:

    coord : cf.DimensionCoordinate

:Returns:

    out : cf.Data or None

'''

    if coord.size == 1:
        return None

#    if coord.dtype.kind == 'S':
#        return _equal_weights(coord)

#    if coord.size == 1 or coord.dtype.kind == 'S':
#        return _equal_weights(coord)

    if coord.identity() in ('latitude', 'grid_latitude'): #dch
        return _latitude_area_weights(coord, infer_bounds=infer_bounds)
    
    else:    
        return _linear_weights(coord, infer_bounds=infer_bounds)
#--- End: def

def _equal_weights(size):
    '''

Return equal weights

:Parameters:

    size : int

:Returns:

    out : cf.Data

'''
    return Data(numpy_ones(size))
#--- End: def

def _linear_weights(coord, infer_bounds):
    '''

Calculate linear weights based on cell bounds.

:Parameters:

    coord : cf.DimensionCoordinate

:Returns:

    out : cf.Data

'''
#    if coord.size == 1:
#        return None

    bounds = coord.find_bounds(create=infer_bounds).get_data()
    bounds = abs(bounds[..., 1] - bounds[..., 0])
    bounds.squeeze(bounds.dimensions[-1])

    if (bounds == bounds[0]).all():
        # The weights are equal
        return _equal_weights(bounds.size) #Data(numpy_ones(bounds.size))

    return bounds
#--- End: def

def _latitude_area_weights(coord, infer_bounds):
    '''

Calculate latitude-area weights based on cell bounds

:Parameters:

    coord : cf.DimensionCoordinate

:Returns:

    out : cf.Data

'''
#    if coord.size == 1:
#        return None

    bounds = coord.find_bounds(create=infer_bounds).get_data()
    bounds.sin()
    bounds = abs(bounds[..., 1] - bounds[..., 0])
    bounds.squeeze(bounds.dimensions[-1])

    if (bounds == bounds[0]).all():
        # The weights are equal
        return _equal_weights(bounds.size) #Data(numpy_ones(bounds.size))

    return bounds
#--- End: def

_collapse_functions = {'mean': _mean,
                       'max': _max,
                       'min': _min,
                       'mid_range': _mid_range,
                       'variance': _variance,
                       'standard_deviation': _standard_deviation,
                       'sum': _sum,
                       'mode': _mode,
                       }
