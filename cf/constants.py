import sys
from numpy.ma import masked as numpy_ma_masked
from tempfile import gettempdir

# Find the total amount of memory, in bytes.
_meminfo_file = open('/proc/meminfo', 'r', 1)
for line in _meminfo_file:
    field_size = line.split()
    if field_size[0] == 'MemTotal:':
        MemTotal = float(field_size[1]) * 1024
        break
#--- End: for
_meminfo_file.close()

# --------------------------------------------------------------------
#   A dictionary of useful constants.
#
#   Whilst the dictionary may be modified directly, it is safer to
#   retrieve and set the values with a function where one is
#   provided. This is due to interdependencies between some values.
#
#   :Keys:
#
#        ATOL : float
#	    The value of absolute tolerance for testing numerically
#	    tolerant equality. Retrieved and set with :func:`.ATOL`.
#
#        CHUNKSIZE : int
#	    The chunk size (in bytes) for data storage and
#	    processing. Retrieved and set with :func:`.CHUNKSIZE`.
#
#        FM_THRESHOLD : float
#	    The minimum amount of memory (in bytes) to be kept
#	    free for temporary work space. Retrieved and set with
#	    :func:`.FM_THRESHOLD`.
#
#        MINNCFM : int
#	    The number of chunk sizes to be kept free for temporary
#	    work space.
#
#        OF_FRACTION : float
#	    The fraction of the maximum number of concurrently open
#	    files which may be used for files containing data
#	    arrays. Retrieved and set with :func:`.OF_FRACTION`.
#
#        RTOL : float
#	    The value of relative tolerance for testing numerically
#	    tolerant equality. Retrieved and set with :func:`.RTOL`.
#
#        TEMPDIR : str
#	    The location to store temporary files. By default, use the
#	    default directory used by the :mod:`tempfile` module.
#
#        FREE_MEMORY : float
# --------------------------------------------------------------------
CONSTANTS = {'RTOL'       : sys.float_info.epsilon,
             'ATOL'       : sys.float_info.epsilon,
             'TEMPDIR'    : gettempdir(),
             'MINNCFM'    : 10,
             'OF_FRACTION': 0.5,
             'CHUNKSIZE'  : MemTotal * 0.01,
             'FREE_MEMORY': None,
             }

CONSTANTS['FM_THRESHOLD'] = CONSTANTS['MINNCFM']*CONSTANTS['CHUNKSIZE'] #/1024.0

masked = numpy_ma_masked

_file_to_fh = {}

