from .variablelist import VariableList

# ====================================================================
#
# CoordinateList object
#
# ====================================================================

class CoordinateList(VariableList):
    '''

An ordered sequence of coordinates stored in a list-like object.

In some contexts, whether an object is a coordinate or a coordinate
list is not known and does not matter. So to avoid ungainly type
testing, some aspects of the CoordinateList interface are shared by a
coordinate and vice versa.

Any attribute or method belonging to a coordinate may be used on a
coordinate list and will be applied independently to each element.

Just as it is straight forward to iterate over the coordinates in a
coordinate list, a coordinate will behave like a single element
coordinate list in iterative and indexing contexts.

'''
    pass
#--- End: class
