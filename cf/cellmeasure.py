from .variable import Variable

# ====================================================================
#
# CellMeasure object
#
# ====================================================================

class CellMeasure(Variable):
    '''
    
A CF cell measure construct containing information that is needed
about the size, shape or location of the field's cells.

It is a variable which contains a data array and metadata comprising
properties to describe the physical nature of the data.

The name of spatial measure being represented is stored in the
`measure` attribute.

'''
    def dump(self, domain=None, key=None, level=0):
        '''

Return a string containing a full description of the cell measure.

:Parameters:

    id: str, optional
       Set the common prefix of variable component names. By default
       the instance's class name is used.

:Returns:

    out : str
        A string containing the description.

**Examples**

>>> x = c.dump()
>>> print c.dump()
>>> print c.dump(id='area_measure')

'''
        indent1 = '    ' * level
        indent2 = '    ' * (level+1)

        if hasattr(self, 'measure'):
            string = ['%sCell measure: %s' % (indent1, self.measure)]
        elif hasattr(self.Units, 'units'):
            string = ['%sCell measure: %s' % (indent1, self.units)]
        else:
            string = ['%sCell measure: %s' % (indent1, self.name(default=''))]

        if self._hasData:
            if domain:
                x = ['%s(%d)' % (domain.dimension_name(dim),
                                 domain.dimension_sizes[dim])
                     for dim in domain.dimensions[key]]
                string.append('%sData(%s) = %s' % (indent2, ', '.join(x), str(self.Data)))
            else:
                x = [str(s) for s in self.shape]
                string.append('%sData(%s) = %s' % (indent2, ', '.join(x), str(self.Data)))
        #--- End: if

        if self._simple_properties():
            string.append(self.dump_simple_properties(level=level+1))
          
        return '\n'.join(string)
    #--- End: def

    def identity(self, default=None):
        '''

Return the cell measure's identity.

The idendity is the value of the `measure` attribute, or if that
doesn't exist the `standard_name` property or, if that does not exist,
the `id` attribute.

:Parameters:

    default : optional
        If none of `measure`, `standard_name` and `id` exist then
        return *default*. By default, *default* is None.

:Returns:

    out :
        The identity.

**Examples**

'''
        return getattr(self, 'measure',
                       self.getprop('standard_name',
                                    getattr(self, 'id', default)))
    #--- End: def

    def name(self, long_name=None, ncvar=None, default=None):
        '''

'''  
        if hasattr(self, 'measure'):
            return self.measure
        
        if hasattr(self, 'standard_name'):
            return self.standard_name
        
        if long_name is not None and hasattr(self, 'long_name'):
            return self.long_name
        
        if ncvar is not None and hasattr(self, 'ncvar'):
            return self.ncvar
        
        return default
    #--- End: def

#--- End: class
