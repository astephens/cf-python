from itertools import izip
from copy import deepcopy

from .utils     import CfDict
from .functions import RTOL, ATOL, equals
from .units     import Units


# ====================================================================
#
# Domain object
#
# ====================================================================

class Domain(CfDict):
    '''

Completely describe a field's coordinate system (domain).

It contains the dimensions constructs, auxiliary coordinate
constructs, cell measure constructs and transform constructs defined
by the CF data model.

The domain is a dictionary-like object whose key/value pairs identify
dimension coordinate, auxiliary coordinate and cell measure constructs
and store them as `cf.DimensionCoordinate`, `cf.AuxiliaryCoordinate`
and `cf.CellMeasure` objects respectively.

'''

    def __init__(self, dimension_sizes={}, dimensions={}, transform_map={},
                 dims=None, auxs=None, cms=None, trans=None, copy=True,
                 finalize=True, **kwargs):
        '''

**Initialization**

.. note:: `cf.DimensionCoordinate`, `cf.AuxiliaryCoordinate` and
          `cf.Coordinate` objects may almost always be used
          interchangably in the initialization (for example, a
          string-valued auxiliary coordinate object may not be
          provided as a dimension coordinate), but there may be
          performance improvements if the specified type is provided.

:Parameters:

    kwargs : optional
        Initialize coordinates, cell measures and transforms by
        setting key/value pairs exactly as for a built-in dict. Keys
        are dimension coordinate identifiers (``'dimN'``), auxiliary
        coordinate identifiers (``'auxN'``), cell measure construct
        identifiers (``'cmN'``) and transform identifiers
        (``'transN'``), and values are `cf.DimensionCoordinate`,
        `cf.AuxiliaryCoordinate`, `cf.CellMeasure` and `cf.Transform`
        instances as appropriate.

        The ``N`` part of each key identifier should be replaced by an
        arbitrary integer greater then or equal to zero, the only
        restriction being that the resulting identifier is not already
        in use by another coordinate or cell measure. No meaning
        should be inferred from the integer part of the identifiers,
        which need not include zero nor be consecutive. However,
        careful selection of dimension coordinate identifiers
        (``'dimN'``) may prevent the need to set the *dimensions*
        parameter.

        See also the *dims*, *auxs*, *cms*, *trans* and
        *transform_map* parameters.

    dims : (sequence of) cf.DimensionCoordinate, optional
        Initialize dimension coordinates from a scalar or a
        list. Identifiers will be automatically initialized as
        ``'dim0'``, ``'dim1'``, etc. for the first to the last
        dimension coordinates in the ordered sequence.

    auxs : (sequence of)  cf.AuxiliaryCoordinate, optional
        Initialize axuilliary coordinates from a scalar or a
        list. Identifiers will be automatically initialized as
        ``'aux0'``, ``'aux1'``, etc. for the first to the last
        axuilliary coordinates in the ordered sequence.

    cms : (sequence of) cf.CellMeasure, optional
        Initialize cell measures from a scalar or a list. Identifiers
        will be automatically initialized as ``'cm0'``, ``'cm1'``,
        etc. for the first to the last cell measures in the ordered
        sequence.

    trans : (sequence of) cf.Transform, optional
        Initialize transforms from a scalar or a list. Identifiers
        will be automatically initialized as ``'trans0'``,
        ``'trans1'``, etc. for the first to the last transform in
        the ordered sequence.

    dimension_sizes : dict, optional
        Initialize the size of each dimension. The dictionary is
        stored in the domain's `dimension_sizes` attribute. By default
        it is intitialized as follows:

           * For each unique dimension implied by the dimension
             coordinates, auxiliary coordinates and cell measures, a
             key/value pair is created for which the key is the
             dimension's identifier (``'dimN'``) and the value is the
             dimension's integer size.

        Explicitly setting a dictionary key does not alter the default
        behaviour for other dimensions.

        Note that a dimension may be intialized which is not spanned
        by any of the given constructs.

    dimensions : dict, optional
        Initialize the mapping of which dimensions span each component
        of the domain. The dictionary is stored in the domain's
        `dimensions` attribute. By default it is intitialized as
        follows:

           * For each dimension coordinate, a key/value pair is created
             for which the key is the dimension coordinate's identifier
             (``'dimN'``) and the value is a single element list of the
             same identifier (``'dimN'``). I.e. a dimension coordinate
             identifier is identical to the identifier of the dimension
             which it spans.
        .. 
           
           * For each auxiliary coordinate and cell measure, a
             key/value pair is created for which the key is the
             auxiliary coordinate's or cell measure's identifier
             (``'auxN'`` or ``'cmN'``) and the value is a list of
             dimension identifiers (``'dimN'``), stating the assumed
             dimensions, in order, of the construct concerned. These
             dimension identifiers will be the sequence of consecutive
             dimension identifiers ``'dim0'`` up to ``'dimM'``, where
             ``M-1`` is the number of dimensions of the construct. An
             assumed dimension will be initialized with the correct
             size if it has not been specified elsewhere (by the
             *kwargs*, *dims* or *dimension_sizes* parameters).

        Explicitly setting a dictionary key does not alter the default
        behaviour for other constructs.

        Any dimension given in a value list that has not been
        specified elsewhere (by the *kwargs*, *dims* or
        *dimension_sizes* parameters) will be initialized with the
        correct size.

        Note that it is possible to set the mapping of which
        dimensions relate to a field's data array by creating the key
        ``'data'`` whose value is a list of dimension identifiers
        (``'dimN'``), stating the dimensions, in order, of the field's
        data array.

    transform_map : dict, optional
        A dictionary mapping which transforms relate to which
        coordinates. Keys are transform identifiers (``'transN'``) and
        values are a coordinate identifier (``'dimN'`` or ``'auxN'``),
        or list of such identifiers, stating the coordinates to which
        the transforms relate.

        The ``N`` part of each key identifier should be replaced by an
        integer greater then or equal to zero, such that the key is
        equal to a transform identifier that has been defined by the
        *kwargs* or *trans* parameters and each named coordinate
        identifier must have been defined with the *kwargs*, *dims* or
        *auxs* parameters.

**Examples**

In this example, ``....`` refers to the appropriate initializations of
the coordinates, cell measure and transform, which are
omitted here for clarity.

>>> dim_coordA = cf.DimensionCoordinate(....)
>>> dim_coordB = cf.DimensionCoordinate(....)
>>> dim_coordA.size
73
>>> dim_coordB.size 
96
>>> aux_coordA = cf.AuxiliaryCoordinate(....)
>>> aux_coordA.shape
(96, 73)
>>> cell_measureA = cf.CellMeasure(....)
>>> cell_measureA.shape
(73, 96)
>>> transformA = cf.Transform(grid_mapping_name='latitude_longitude', ....)
>>> d = cf.Domain(dim0=dim_coordA, dim1=dim_coordB,
...               aux0=aux_coordA,
...               cm0=cell_measureA,
...               trans0=transformA,
...               dimensions={'aux0': ['dim1', 'dim0']}
...               transform_map={'trans0': 'aux0'})
...
>>> d.dimensions
{'aux0': ['dim1', 'dim0'],
 'cm0' : ['dim0', 'dim1'],
 'dim1': ['dim1'],
 'dim0': ['dim0']}
>>> d.transforms
{'trans0' : <CF Transform: latitude_longitude>}

Note that it was necessary to specifiy the dimension mapping for
``'aux0'`` because the auxiliary coordinate doesn't have the default
dimension order of ``['dim0', 'dim1']``. Equivalent ways of
initializing the same domain are as follows:

>>> d = cf.Domain(dims=[dim_coordA, dim_coordB],
...               auxs=[aux_coordA],
...               cms=cell_measureA,
...               trans=transformA,
...               dimensions={'aux0': ['dim1', 'dim0']}
...               transform_map={'trans0': 'aux0'})

Here, note that we may specify an entry in the *dimensions*
dictionary, even if the default behaviour aplies:
 
>>> d = cf.Domain(dims=[dim_coordA, dim_coordB],
...               aux0=aux_coordA,
...               cm0=cell_measureA,
...               trans=transformA,
...               dimensions={'aux0': ['dim1', 'dim0'],
...                           'cm0' : ['dim0', 'dim1']},
...               transform_map={'trans0': 'aux0'})

Here, the dimensions do not have default names (``'dim0'``,
``'dim1'``), so it in this case it is necessary to specifiy all
auxiliary coordinates and cell measures in the *dimensions*
dictionary:

>>> d = cf.Domain(dim4=dim_coordA, dim12=dim_coordB,
...               auxs=aux_coordA,
...               cms=cell_measureA,
...               trans=(transformA,),
...               dimensions={'aux0': ['dim12', 'dim4'],
...                           'cm0' : ['dim4', 'dim12']},
...               transform_map={'trans0': 'aux0'})

In the last example, omitting the ``'cm0'`` key from the *dimensions*
dictionary would have been possible, but would have resulted in two
new dimensions being created (``'dim0'``, ``'dim1'`` with sizes 73 and
96 respectively), which was not the intention in this case.

'''
        super(Domain, self).__init__(**kwargs)

        self.dimension_sizes = dimension_sizes.copy()
        self.dimensions      = dimensions.copy()
        self.transforms      = CfDict()

        # ------------------------------------------------------------
        # Initialize dimension coordinates
        # ------------------------------------------------------------
        if dims:
            if self.dim_coords():
                raise ValueError("asdas dim")
            
            for N, coord in enumerate(dims):
                key = 'dim%d' % N
                self.insert_dim_coordinate(coord, key=key, copy=copy,
                                           finalize=False)

        else:
            for key, coord in self.dim_coords().iteritems():
                self.insert_dim_coordinate(coord, key=key, copy=copy,
                                           finalize=False)

        # ------------------------------------------------------------
        # Initialize auxiliary coordinates
        # ------------------------------------------------------------
        if auxs:
            if self.aux_coords():
                raise ValueError("asdas aux")

            for N, coord in enumerate(auxs):
                key = 'aux%d' % N
                dimensions = self.dimensions.get(key, None)
                self.insert_aux_coordinate(coord, key=key, 
                                           dimensions=dimensions, copy=copy,
                                           finalize=False)
        else:
            for key, coord in self.aux_coords().iteritems():
                dimensions = self.dimensions.get(key, None)
                self.insert_aux_coordinate(coord, key=key,
                                           dimensions=dimensions, copy=copy,
                                           finalize=False)     

        # ------------------------------------------------------------
        # Initialize cell measures
        # ------------------------------------------------------------
        if cms:
            if self.cell_measures():
                raise ValueError("asdas cell")

            for N, cm in enumerate(cms):
                key = 'cm%d' % N
                dimensions = self.dimensions.get(key, None)
                self.insert_cell_measure(cm, key=key, dimensions=dimensions,
                                         copy=copy, finalize=False)
        else:
            for key, cm in self.cell_measures().iteritems():
                dimensions = self.dimensions.get(key, None)
                self.insert_cell_measure(cm, key=key, dimensions=dimensions,
                                         copy=copy, finalize=False)

        # ------------------------------------------------------------
        # Initialize transforms
        # ------------------------------------------------------------
        if trans:
            if self.get_keys('^trans'):
                raise ValueError("asdas transform")

            for N, transform in enumerate(tuple(trans)):
                key = 'trans%d' % N
                self.insert_transform(transform, key=key, copy=copy)
        else:
            for key in self.get_keys('^trans'):
                self.insert_transform(self.pop(key), key=key, copy=copy)

        # ------------------------------------------------------------
        # Attach transforms to coordinates
        # ------------------------------------------------------------
        for tkey, ckeys in transform_map.iteritems():
            if isinstance(ckeys, basestring):
                ckeys = (ckeys,)

            for key in ckeys:
                self[key].transforms = {self.transforms[tkey].name: tkey}

        if finalize:
            self.finalize()
    #--- End: def

    def equal_transform(self, t, u, domain=None, rtol=None, atol=None, 
                        traceback=False):
        '''

:Parameters:

    t : cf.Transform

    u : cf.Transform

    domain : cf.Domain, optional

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns:

    out : bool

**Examples**

>>>

'''

        if rtol is None:
            rtol = RTOL()
        if atol is None:
            atol = ATOL()              

        if not t.equals(u, rtol=rtol, atol=atol, traceback=traceback):
            if traceback:
                print("%s: Unequal transforms: %s" % 
                      (self.__class__.__name__, repr(t)))
            return False
         
        if not t.isformula_terms:
            return True

        # if transform has coordinate pointers - check
        # them                        
        for term, key in t.iteritems():
            if not isinstance(key, basestring):
                continue
            
            if (key not in self or
                u[term] not in domain or
                not self[key].equals(domain[u[term]], rtol=rtol, atol=atol,
                                     traceback=traceback)): 
                if traceback:
                    print("%s: Unequal transforms: %s" % 
                          (self.__class__.__name__, repr(t)))
                return False
        #--- End: for

        return True
    #--- End: def

    def equivalent(self, other, rtol=None, atol=None, traceback=False):
        '''

True if and only if two domain are logically equivalent.

Equivalence is defined as both domain having the same data arrays
after accounting for different but equivalent units, size one
dimensions, dimension directions and dimension orders.

:Parameters:

    other :
        The object to compare for equivalence.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        objects differ.

:Returns: 

    out : bool
        Whether or not the two objects are equivalent.
      



'''
        if sorted(self.dimension_sizes.values()) != sorted(self.dimension_sizes.values()):
            if traceback:
                print("%s: Different dimensionality: %s, %s" %
                      (self.__class__.__name__,
                       self.dimension_sizes.values(),
                       other.dimension_sizes.values()))
            return False

        s = self.analyse()
        t = other.analyse()
        
        if set(s['id_to_coord']) != set(t['id_to_coord']):
            if traceback:
                print("%s: Non-matching dimensions: %s" %
                      (self.__class__.__name__,
                       set(s['id_to_coord']).difference(t['id_to_coord'])))
            return False
        
        for identity, coord0 in s['id_to_coord'].iteritems():
            coord1 = t['id_to_coord'][identity]
            if not coord0._equivalent_data(coord1, rtol=rtol, atol=atol):
                if traceback:
                    print("%s: Non-equivalent 1-d coordinate data array: %s" %
                          (self.__class__.__name__, identity))
                           
                return False
        #--- End: for

        return True
    #--- End: def

    def equivalent_transform(self, t, u, domain, rtol=None, atol=None,
                             traceback=False):
        '''

:Parameters:

    t : cf.Transform

    u : cf.Transform

    domain : cf.Domain

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns:

    out : bool

**Examples**

>>>

'''
        if not t.equivalent(u, rtol=rtol, atol=atol, traceback=traceback):
            return False

        for term, value in t.iteritems():
            if isinstance(value, basestring):
                # Both values are pointers to coordinates
                if not self[value]._equivalent_data(domain[u[term]],
                                                    rtol=rtol, atol=atol):
                    return False
            #--- End: if
        #--- End: for

        # Still here?
        return True
    #--- End: def

#    def transform_map(self):
#        '''
#
#Return a dictionary mapping coordinates to their transforms.
#
#:Returns:
#
#    out : dict
#        A dictionary mapping coordinate identifiers to their
#        transforms' identifiers. Coordinates without transforms are
#        not included in the dictionary. Transforms which do not exist
#        in the domain are omitted.
#
#**Examples**
#
#>>> d.transform_map()
#{}
#
#>>> d.transform_map()
#{'dim0': set(['trans0']),
# 'dim1': set(['trans0']),
# 'aux1': set(['trans0']),
# 'aux2': set(['trans0'])}
#
#>>> d.transform_map()
#{'dim1': set(['trans0', 'trans1']),
# 'dim2': set(['trans1', 'trans0']),
# 'aux0': set(['trans0']),
# 'aux1': set(['trans0']),
# 'aux2': set(['trans1']),
# 'aux3': set(['trans1'])}
#
#'''  
#        transforms = set(self.transforms)
#
#        out = {}
#        for key, coord in self.coordinates().iteritems:
#            t = getattr(coord , 'transforms', None)
#            if t:
#                t = set(t).intersection(transforms)
#                if t:
#                    out[key] = t
#        #--- End: for
#
#        return out
#    #--- End: def

    def __repr__(self):
        '''
x.__repr__() <==> repr(x)

'''
        return '<CF %s: %s>' % (self.__class__.__name__, 
                                tuple(self.dimension_sizes.values()))
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''
        mmm = {}
        def _print_coord(domain, key, dimension_coord):
            '''Private function called by __str__'''

            if dimension_coord:
                name = "%s(%d)" % (domain.dimension_name(key), domain.dimension_sizes[key])
                mmm[key] = name
                if key not in domain:
                    return name

                x = [name]
            #--- End: if

            # Still here?
            variable = domain[key]

            if not dimension_coord:               
                # Auxiliary coordinate
                shape = [mmm[dim] for dim in domain.dimensions[key]]
                shape = str(tuple(shape)).replace("'", "")
                shape = shape.replace(',)', ')')
                x = [variable.name(default='domain:'+key)]
                x.append(shape)
            #--- End: if

            try:
                variable.compress
            except AttributeError:
                if variable._hasData:
                    x.append(' = ')
                    x.append(str(variable.Data))
            else:
                x.append(' -> compressed ')
                compressed = []
                for unc in domain[key].compress:
                    shape = str(unc.size)
#                    compressed.append(unc.name(ncvar=True,
#                                               default='unc')+'('+shape+')')
                    compressed.append(unc.name(default='unc')+'('+shape+')')
                x.append(', '.join(compressed))

            return ''.join(x)
        #--- End: def

        string = []

        x = [_print_coord(self, dim, True) for dim in sorted(self.dimension_sizes)]
        if x:
            string.append('Dimensions      : ')
            string.append('\n                : '.join(x))

        x = [_print_coord(self, aux, False) for aux in sorted(self.get_keys('^aux'))]
        if x:
            string.append('\n')
            string.append('Auxiliary coords: ')
            string.append('\n                : '.join(x))

        # Cell measures
        x = [_print_coord(self, cm, False) for cm in sorted(self.get_keys('^cm'))]
        if x:
            string.append('\n')
            string.append('Cell measures   : ')
            string.append('\n                : '.join(x))

        # Transforms
        x = [repr(transform) for transform in self.transforms.values()]
        if x:
            string.append('\n')
            string.append('Transforms      : ')
            string.append('\n                : '.join(x))
        #--- End: if

        return ''.join(string)
    #--- End: def

#    # ----------------------------------------------------------------
#    # Attribute: dimension_sizes (can't delete)
#    # ----------------------------------------------------------------
#    @property
#    def dimension_sizes(self):
#        '''
#        
#A dictionary mapping the domain's dimensions to their sizes.
#
#Keys are dimension identifiers (``'dimN'``) and values are integers
#giving the size of each dimension.
#
#The ``N`` part of each key identifier should be replaced by an
#arbitrary integer greater then or equal to zero, the only restriction
#being that the resulting identifier is not already in use. No meaning
#should be inferred from the integer part of the identifiers, which
#need not include zero nor be consecutive (although these will commonly
#be the case).
#
#The `dimensions` attribute stores how these dimensions relate to the
#domain's dimensions and auxiliary coordinates and cell measures, as
#well as the containing field's data array.
#
#**Examples**
#
#In this example, the domain has four dimensions denoted ``'dim1'``,
#``'dim2'``, ``'dim3'`` and ``'dim7'`` with sizes ``73``, ``96``,
#``12`` and ``1`` respectively.
#
#>>> d.dimension_sizes
#{'dim7': 1, 'dim3': 12, 'dim1': 73, 'dim2': 96}
#
#
#'''
#        return self._dimension_sizes
#    #--- End: def
#    @dimension_sizes.setter
#    def dimension_sizes(self, value):
#        self._dimension_sizes = value

    # ----------------------------------------------------------------
    # Attribute: dimensions (can't delete)
    # ----------------------------------------------------------------
#    @property
#    def dimensions(self):
#        '''
#                
#A dictionary mapping the domain's components (including the field's
#data array) to their dimensions.
#
#Keys are dimension coordinate identifiers (``'dimN'``), auxiliary
#coordinate identifiers (``'auxN'``) and cell measure construct
#identifiers (``'cmN'``), and values are lists of dimension identifiers
#(``'dimN'``), stating the dimensions, in order, of the construct
#concerned. The dimension identifiers must all exist as keys to the
#`dimension_sizes` dictionary.
#
#The special key ``'data'`` stores the ordered list of dimension
#identifiers (``'dimN'``) relating to a containing field's data array.
#
#The ``N`` part of each key identifier should be replaced by an
#arbitrary integer greater then or equal to zero, the only restriction
#being that the resulting identifier is not already in use. No meaning
#should be inferred from the integer part of the identifiers, which
#need not include zero nor be consecutive (although these will commonly
#be the case).
#
#**Examples**
#
#In this example, the domain has:
#
#* Four dimensions (``'dim0'``, ``'dim1'``, ``'dim2'`` and ``'dim7'``)
#
#* Three dimension coordinates (dimension ``'dim7'`` has no dimension
#  coordinate)
#
#* Three auxiliary coordinates (``'aux0'``, ``'aux1'`` and ``'aux3'``),
#  two of which span dimensions ``'dim1'`` and ``'dim2'`` (but in
#  different orders) and one is one dimensional.
#
#* One cell measure variable spanning two dimensions
#
#* The identifiers of the field's data array dimensions.
#
#>>> d.dimension_sizes
#{'dim0': 12, 'dim1': 73, 'dim2': 96, 'dim7': 1}
#>>> d.dimensions
#{'aux0': ['dim1', 'dim2'],
# 'aux1': ['dim2', 'dim1'],
# 'aux3': ['dim0'],
# 'cm0' : ['dim1', 'dim2'],
# 'data': ['dim0', 'dim1', 'dim2'],
# 'dim0': ['dim0'],
# 'dim1': ['dim1'],
# 'dim2': ['dim2']}
#
#'''
#        return self._dimensions
#    #--- End: def
#    @dimensions.setter
#    def dimensions(self, value):
#        self._dimensions = value

#    # ----------------------------------------------------------------
#    # Attribute: transforms (can't delete)
#    # ----------------------------------------------------------------
#    @property
#    def transforms(self):
#        '''
#
#A dictionary-like storing the domain's transforms.
#
#The `transforms` attribute is a `cf.CfDict` instance.
#
#Keys are transform identifiers (``'transN'``) and values are
#`cf.Transform` objects.
#
#The ``N`` part of each key identifier should be replaced by an
#arbitrary integer greater then or equal to zero, the only restriction
#being that the resulting identifier is not already in use. No meaning
#should be inferred from the integer part of the identifiers, which
#need not include zero nor be consecutive (although these will commonly
#be the case).
#
#**Examples**
#
#>>> isinstance(d.transforms, cf.CfDict)
#True
#>>> d.transforms
#{'trans0': <CF Transform: ocean_sigma_z_coordinate>,
# 'trans1': <CF Transform: rotated_latitude_longitude>}
#
#'''
#        return self._transforms
#    #--- End: def
#    @transforms.setter
#    def transforms(self, value):
#        self._transforms = value

    def analyse(self):
        '''

'''
        a = {}

        # ------------------------------------------------------------
        # Map each dimension's identity to its domain identifier, if
        # such a mapping exists.
        #
        # For example:
        # >>> id_to_dim
        # {'time': 'dim0', 'height': dim1'}
        # ------------------------------------------------------------
        id_to_dim = {}

        # ------------------------------------------------------------
        # For each dimension that is identified by a 1-d auxiliary
        # coordinate, map its dimension's its domain identifier.
        #
        # For example:
        # >>> id_to_aux
        # {'region': 'aux0'}
        # ------------------------------------------------------------
        id_to_aux = {}

        # ------------------------------------------------------------
        # Map each dimension's identity to the coordinate which
        # provides that identity.
        #
        # For example:
        # >>> id_to_coord
        # {'time': <CF Coordinate: time(12)>}
        # ------------------------------------------------------------
        id_to_coord = {}

        dim_to_coord = {}
        aux_to_coord = {}

        # ------------------------------------------------------------
        #
        # ------------------------------------------------------------
        aux_coords = {}
        aux_coords['N-d'] = {}

        cell_measures = {}
        cell_measures['N-d'] = set()

        # ------------------------------------------------------------
        # List the dimensions which are undefined, in that no unique
        # identity can be assigned to them.
        #
        # For example:
        # >>> undefined_dims
        # ['dim2']
        # ------------------------------------------------------------
        undefined_dims = []

        # ------------------------------------------------------------
        #
        # ------------------------------------------------------------
        warnings = []

        for dim in self.dimension_sizes:

            # Find this dimension's 1-d and N-d auxiliary coordinates
            aux_coords_dim = self.aux_coords(dim)
            aux_coords[dim]        = {}
            aux_coords[dim]['1-d'] = {}
            aux_coords[dim]['N-d'] = {}
            for aux, coord in aux_coords_dim.iteritems():
                if coord.ndim > 1:
                    aux_coords['N-d'][aux] = coord
                    aux_coords[dim]['N-d'][aux] = coord
                else:
                    aux_coords[dim]['1-d'][aux] = coord
            #--- End: for

            # Find this dimension's 1-d and N-d cell measures
            cell_measures_dim = self.cell_measures(dim)
            cell_measures[dim]        = {}
            cell_measures[dim]['1-d'] = {}
            cell_measures[dim]['N-d'] = {}
            for cm, cell_measure in cell_measures_dim.iteritems():
                if cell_measure.ndim > 1:
                    cell_measures['N-d']      = self[cm]
                    cell_measures[dim]['N-d'] = self[cm]
                else:
                    cell_measures[dim]['1-d'] = self[cm]
            #--- End: for

            if dim in self:
                # This dimension of the domain has a dimension
                # coordinate
                dim_coord = self[dim]
                identity = dim_coord.identity()
                if identity is not None and dim_coord._hasData:
                    if identity in id_to_dim:
                        warnings.append(
                            "Domain has more than one '%s' dimension" % identity)

                    id_to_dim[identity]   = dim
                    id_to_coord[identity] = dim_coord
                    dim_to_coord[dim]     = dim_coord
                    continue

            elif len(aux_coords[dim]['1-d']) == 1:
                # This dimension of the domain does not have a
                # dimension coordinate but it does have exactly one
                # 1-d auxiliary coordinate, so that will do.
                aux       = list(aux_coords[dim]['1-d'])[0]
                aux_coord = self[aux]
                
                identity = aux_coord.identity()
                if identity is not None and aux_coord._hasData:
                    if identity in id_to_dim:
                        warnings.append(
                            "Domain has more than one '%s' dimension" % identity)

                    id_to_aux[identity]   = aux
                    id_to_dim[identity]   = dim
                    id_to_coord[identity] = aux_coord
                    aux_to_coord[dim]     = aux_coord
                    continue
            #--- End: if

            # Still here? Then this dimension is undefined
            undefined_dims.append(dim)
        #--- End: for

        # ------------------------------------------------------------
        # Invert the mapping between dimensions and identities
        # ------------------------------------------------------------
#        dim_to_id = dict([(v, k) for k, v in id_to_dim.iteritems()])
        dim_to_id = {}
        for k, v in id_to_dim.iteritems():
            dim_to_id[v] = k

        return {'aux_coords'    : aux_coords,
                'aux_to_coord'  : aux_to_coord,
                'cell_measures' : cell_measures,
                'dim_to_coord'  : dim_to_coord,
                'dim_to_id'     : dim_to_id,
                'id_to_aux'     : id_to_aux,
                'id_to_coord'   : id_to_coord,
                'id_to_dim'     : id_to_dim,
                'undefined_dims': undefined_dims,
                'warnings'      : warnings,                
                }    
    #--- End def 

    def dimension_name(self, dim):
        '''

Return a name for a dimension.

:Parameters:

    dim : str
        The identifier of the dimension.

:Returns:

    out : str
        A name for the dimension.

**Examples**

>>> d.dimension_name('dim0')
'time'
>>> d.dimension_name('dim1')
'domain:dim1'
>>> d.dimension_name('dim2')
'ncdim:lat'

'''
        if dim in self:
            return self[dim].name(default='domain:%s' % dim)                

        if dim in self.dimension_sizes:
            aux = self.aux_coords(dim)
            for key in aux.keys():
                if aux[key].ndim > 1:
                    aux.pop(key)
            #--- End: for
            if len(aux) == 1:
                return self[aux.keys()[0]].name(default='domain:'+dim)
            else:
                try:
                    return 'ncdim:%s' % self.nc_dimensions[dim]
                except (KeyError, AttributeError):
                    return 'domain:%s' % dim
        #--- End: if

        return 'domain:%s' % dim
    #--- End: def

    def direction(self, dim):
        '''

Return True if a dimension is increasing, otherwise return False.

A dimension is considered to be increasing if its dimension coordinate
values are increasing in index space or if it has no dimension
coordinate.

The direction is taken directly from the appropriate coordinate's Data
object, if available. (This is because we can assume that the domain
has been finalized.)

:Parameters:

    dim : str
        The identifier of the dimension, such as ``'dim0'``.

:Returns:

    out : bool
        Whether or not the dimension is increasing.
        
**Examples**

>>> d.dimension_sizes
{'dim0': 3, 'dim1': 1, 'dim2': 2, 'dim3': 2, 'dim4': 99}
>>> d.dimensions
{'dim0': ['dim0'],
 'dim1': ['dim1'],
 'aux0': ['dim0'],
 'aux1': ['dim2'],
 'aux2': ['dim3'],
}
>>> d['dim0'].array
array([  0  30  60])
>>> d.direction('dim0')
True
>>> d['dim1'].array
array([15])
>>> d['dim1'].bounds.array
array([  30  0])
>>> d.direction('dim1')
False
>>> d['aux1'].array
array([0, -1])
>>> d.direction('dim2')
True
>>> d['aux2'].array
array(['z' 'a'])
>>> d.direction('dim3')
True
>>> d.direction('dim4')
True

'''
        if dim in self:
            return self[dim].direction()
        
        return True        
    #--- End: def
  
    def directions(self):
        '''

Return a dictionary mapping dimensions to their directions.

:Returns:

    out : dict
        A dictionary whose key/value pairs are dimension identifiers
        and their directions.

**Examples**

>>> d.directions()
{'dim1': True, 'dim0': False}

'''
        self_direction = self.direction

        directions = {}
        for dim in self.dimension_sizes:
            directions[dim] = self_direction(dim)

        return directions
    #--- End: def

    def map_dims(self, other):
        '''

Map the dimensions of one domain to the equivalent dimensions of
another.

:Returns:

   out : dict
       A dictionary whose key/value pairs are dimension identifiers
       and their equivalent dimension identifiers in the other domain.

**Examples**

>>> d.map_dims(e)
{'dim0': 'dim1',
 'dim1': 'dim0',
 'dim2': 'dim2'}

'''
        s = self.analyse()
        t = other.analyse()
        
        out = {}
        
        for identity, dim in s['id_to_dim'].iteritems():
            if identity in t['id_to_dim']:
                out[dim] = t['id_to_dim'][identity]
        #--- End: for

        return out
    #--- End: def

    def insert_dimension(self, size, dim=None, replace=True):
        '''

Insert a new dimension to the domain in place, preserving internal
consistency.

:Parameters:

    size : int
        The size of the new dimension.

    dim : str
        The identifier for the new dimension, ``'dimN'``. The ``N``
        part of the identifier should be replaced by an integer
        greater then or equal to zero.
  
    replace : bool, optional
        If False then do not replace an existing dimension with the
        same identifier but a different size. By default an existing
        dimension with the same identifier is changed to have the new
        size.

:Returns:

    out : str
        The identifier for the new dimension (see the *dim*
        parameter).

**Examples**

>>> d.insert_dimension(1)
>>> d.insert_dimension(90, dim='dim4')
>>> d.insert_dimension(23, dim='dim0', replace=False)

'''
        if dim is not None:
            if (dim in self.dimension_sizes and not replace and 
                self.dimension_sizes[dim] != size):
                raise ValueError(
"Can't insert dimension: Existing dimension '%s' has different size (%d, %d)" %
(dim, size, self.dimension_sizes[dim]))

            self.dimension_sizes[dim] = size
            return dim

        # Still here? Then no identifier was specified for the
        # dimension, so create a new one.
        dim = self.new_dimension_identifier()
        self.dimension_sizes[dim] = size

        return dim
    #--- End: def

    def new_dimension_identifier(self):
        '''

Return a new, unique dimension identifier for the domain.

:Returns:

    out : str
        The new identifier.

**Examples**

>>> d.dimension_sizes.keys()
['dim2', 'dim0']
>>> d.new_dimension_identifier()
'dim3'

>>> d.dimension_sizes.keys()
[]
>>> d.new_dimension_identifier()
'dim0'

'''
        dimensions = set(self.dimension_sizes)

        n = len(dimensions)
        new_dim = 'dim%d' % n

        while new_dim in dimensions:
            n += 1
            new_dim = 'dim%d' % n
        #--- End: while

        return new_dim
    #--- End: def

    def new_auxiliary_identifier(self):
        '''

Return a new, unique auxiliary coordinate identifier for the domain.

:Returns:

    out : str
        The new identifier.

**Examples**

>>> d.aux_coords().keys()
['aux2', 'aux0']
>>> d.new_auxiliary_identifier()
'aux3'

>>> d.aux_coords().keys()
[]
>>> d.new_auxiliary_identifier()
'aux0'

'''
        keys = set(self.get_keys('^aux'))

        n = len(keys)
        new_key = 'aux%d' % n

        while new_key in keys:
            n += 1
            new_key = 'aux%d' % n
        #--- End: while

        return new_key
    #--- End: def

    def new_cell_measure_identifier(self):
        '''

Return a new, unique cell measure identifier for the domain.

:Returns:

    out : str
        The new identifier.

**Examples**

>>> d.cell_measures().keys()
['cm2', 'cm0']
>>> d.new_cell_measure_identifier()
'cm3'

>>> d.cell_measures().keys()
[]
>>> d.new_cell_measure_identifier()
'cm0'


'''
        keys = set(self.get_keys('^cm'))

        n = len(keys)
        new_key = 'cm%d' % n

        while new_key in keys:
            n += 1
            new_key = 'cm%d' % n
        #--- End: while

        return new_key
    #--- End: def

    def new_transform_identifier(self):
        '''
Return a new 

'''
        if not self.transforms:
            return 'trans0'
        
        keys = set(self.transforms)

        n = len(keys)
        new_key = 'trans%d' % n

        while new_key in keys:
            n += 1
            new_key = 'trans%d' % n
        #--- End: while

        return new_key
    #--- End: def

    def insert_coordinate(self, variable, key, copy=True, dimensions=None,
                          transforms=None, finalize=True):
        '''

Insert a new dimension coordinate or auxiliary coordinate into the
domain in place, preserving internal consistency.

:Parameters:

    variable : cf.AuxiliaryCoordinate or cf.DimensionCoordinate or cf.Coordinate
        The new dimension coordinate or auxiliary coordinate.

    key : str
        The identifier for the new coordinate. The identifier must
        start with the string ``dim`` or ``aux``, corresponding to
        whether the *variable* is to be a dimension coordinate or an
        auxiliary coordinate respectively.

    dimensions : list, optional
        If the *variable* is an auxiliary coordinate then the
        identities of its dimensions must be provided. Ignored if the
        *variable* is a dimension coordinate.

    copy: bool, optional
        If False then the *variable* is not copied before
        insertion. By default it is copied.

    transforms : sequence, optional  

    finalize : bool, optional
        If False then do not finalize the domain for the new
        coordinate. By default the domain is finalized. Only set to
        False if the domain is guaranteed to be finalized
        subsequently, in which case there may be a performance
        advantage.

:Returns:

    None

**Examples**

>>>

'''
        if key.startswith('d'):
            self.insert_dim_coordinate(variable, key=key, copy=copy,
                                       transforms=transforms, finalize=finalize)
            
        elif key.startswith('a'):
            self.insert_aux_coordinate(variable, key=key, copy=copy,
                                       transforms=transforms,
                                       dimensions=dimensions, finalize=finalize)

        else:
            raise ValueError("bad key in insert_coordinate: '%s'" % key)
    #--- End: def

    def insert_dim_coordinate(self, coord, key=None, transforms=None,
                              copy=True, replace=True, finalize=True):
        '''

Insert a new dimension coordinate to the domain in place, preserving
internal consistency.

:Parameters:

    coord : cf.AuxiliaryCoordinate or cf.DimensionCoordinate or cf.Coordinate
        The new coordinate.       

    key : str, optional
        The identifier for the new dimension coordinate. The
        identifier is of the form ``'dimN'`` where the ``N`` part
        should be replaced by an arbitrary integer greater then or
        equal to zero. By default a unique identifier will be
        generated.

    transforms : sequence, optional        

    copy: bool, optional
        If False then the dimension coordinate is not copied before
        insertion. By default it is copied.
      
    replace : bool, optional
        If False then do not replace an existing dimension coordinate
        with the same identifier. By default an existing dimension
        coordinate with the same identifier is replaced with *coord*.
    
    finalize : bool, optional
        If False then do not finalize the domain for the new
        coordinate. By default the domain is finalized. Only set to
        False if the domain is guaranteed to be finalized
        subsequently, in which case there may be a performance
        advantage.

:Returns:

    out : str
        The identifier for the new dimension coordinate (see the *key*
        parameter).

**Examples**

>>>

'''
        coord = coord.asdimension(copy=copy)            

        if key is None:
            key = self.insert_dimension(coord.size)
        else:
            if key in self and not replace:
                raise ValueError(
"Can't insert dimension coordinate: '%s' identifier already exists" % key)

            self.insert_dimension(coord.size, key, replace=False)

        dimensions = self.dimensions

        dimensions[key] = [key]

        # ------------------------------------------------------------
        # Turn scalar dimension coordinate into size 1, 1-d
        # ------------------------------------------------------------
        if coord.isscalar:
            coord.expand_dims(0, key, coord.direction())
       
        if hasattr(coord, 'transforms'):
            del coord.transforms

        if finalize:
            self.finalize()

        self[key] = coord

        return key
    #--- End: def

    def _insert_item(self, variable, key, role, dimensions=None, copy=True,
                     replace=True, finalize=True):
        '''

Insert a new auxiliary coordinate into the domain in place, preserving
internal consistency.

:Parameters:

    coord :cf.Coordinate
        The new auxiliary coordinate.

    key : str
        The identifier for the auxiliary coordinate or cell
        measure.

    role : str

    dimensions : sequence, optional
        The ordered dimensions of the new coordinate. Ignored if the
        coordinate is a dimension coordinate. Required if the
        coordinate is an auxiliary coordinate.

    copy: bool, optional
        If False then the auxiliary coordinate is not copied before
        insertion. By default it is copied.

    replace : bool, optional
        If False then do not replace an existing dimension coordinate
        with the same identifier. By default an existing dimension
        coordinate with the same identifier is replaced with *coord*.

:Returns:

    out : str
        The identifier for the new auxiliary coordinate (see the 'key'
        parameter).


**Examples**

>>>

'''
        if key in self and not replace:
            raise ValueError("Can't insert %s: '%s' identifier already exists" % (role, key))

        if not dimensions:
            # try default dimensions
            dimensions = ['dim%d' % n for n in range(variable.ndim)]
            for dim, size in zip(dimensions, variable.shape):
                if dim not in self.dimension_sizes:
                    raise ValueError(
"Can't insert %s: Default dimension '%s' doesn't exist" % (role, dim))

                if self.dimension_sizes[dim] != size:
                    raise ValueError(
"Can't insert %s: Default dimension '%s' has wrong size (%d)" %
(role, dim, size))

        else:
            for dim, size in zip(dimensions, variable.shape):
                self.insert_dimension(size, dim=dim, replace=False)


        dimensions = list(dimensions)

        self.dimensions[key] = dimensions

        if copy:
            variable = variable.copy()

        # ------------------------------------------------------------
        # Turn scalar variable into size 1, 1-d
        # ------------------------------------------------------------
        if variable.isscalar:            
            variable.expand_dims(0, dimensions[0], True)           

        if len(set(dimensions)) != variable.ndim:
            raise ValueError(
"Can't insert %s: Mismatched number of dimensions" % role)
        
        if finalize:
            self.finalize()

        self[key] = variable

        return key
    #--- End: def

    def finalize(self):
        '''


:Returns:

    out : dict

**Examples**

>>> directions = d.finalize()

'''
        dimensions = self.dimensions

        # Dimension names
        for key, variable in self.iteritems():
            v_dimensions = variable.Data.dimensions
            d_dimensions = dimensions[key]

            if v_dimensions == d_dimensions:
                continue

            dim_name_map = {}
            for k, v in izip(v_dimensions, d_dimensions):
                dim_name_map[k] = v
                
            variable._change_dimension_names(dim_name_map)
        #--- End: def   

        # Directions
        directions = self.directions()

        for key, variable in self.iteritems():
            d_dimensions = dimensions[key]

            if key in d_dimensions:
                # Skip dimension coordinates, as these have been dealt
                # with by the directions method.
                continue

            variable._override_directions(directions, None)
        #--- End: for

        return directions
    #--- End: def

    def insert_aux_coordinate(self, coord, key=None, dimensions=None,
                              transforms=None, copy=True, replace=True,
                              finalize=True):
        '''

Insert a new auxiliary coordinate into the domain in place, preserving
internal consistency.

:Parameters:

    coord : cf.AuxiliaryCoordinate or cf.DimensionCoordinate or cf.Coordinate
        The new coordinate.

    key : str, optional
        The identifier for the new dimension coordinate. The
        identifier is of the form ``'auxN'`` where the ``N`` part
        should be replaced by an arbitrary integer greater then or
        equal to zero. By default a unique identifier will be
        generated.

    dimensions : list, optional
        The ordered dimensions of the new coordinate. Ignored if the
        coordinate is a dimension coordinate. Required if the
        coordinate is an auxiliary coordinate.

    transforms : sequence, optional        
       
    copy: bool, optional
        If False then the auxiliary coordinate is not copied before
        insertion. By default it is copied.

    replace : bool, optional
        If False then do not replace an existing auxiliary coordinate
        with the same identifier. By default an existing auxiliary
        coordinate with the same identifier is replaced with *coord*.

    finalize : bool, optional
        If False then do not finalize the domain for the new
        coordinate. By default the domain is finalized. Only set to
        False if the domain is guaranteed to be finalized
        subsequently, in which case there may be a performance
        advantage.

:Returns:

    out : str
        The identifier for the new auxiliary coordinate (see the *key*
        parameter).


**Examples**

>>>

'''
        coord = coord.asauxiliary(copy=copy)

        if not key:
            key = self.new_auxiliary_identifier()

        self._insert_item(coord, key, 'auxiliary coordinate',
                          dimensions=dimensions, copy=copy, finalize=finalize)

        if hasattr(coord, 'transforms'):
            del coord.transforms

        if transforms:
            coord.transforms = list(transforms) #  DCH  attention!

        return key
    #--- End: def

    def itercoordinates(self, role=None):
        '''
asdasdasdasdasdas
'''
        if role is None:
            regex = '^dim|^aux'
        elif role == 'dim':
            regex = '^dim'
        elif role == 'aux':
            regex = '^aux'

        return iter(self[key] for key in self.get_keys(regex))
    #--- End: if

    def insert_cell_measure(self, cm, key=None, dimensions=None, copy=True,
                            replace=True, finalize=True):
        '''

Insert a new cell measure into the domain in place, preserving
internal consistency.

:Parameters:

    cm : cf.CellMeasure
        The new cell measure.

    key : str, optional
        The identifier for the new cell measure. The identifier is of
        the form ``'cmN'`` where the ``N`` part should be replaced by
        an arbitrary integer greater then or equal to zero. By default
        a unique identifier will be generated.

    dimensions : sequence, optional
        The ordered dimensions of the new cell measure.

    copy : bool, optional
        If False then the cell measure is not copied before
        insertion. By default it is copied.

    replace : bool, optional
        If False then do not replace an existing cell measure with the
        same identifier. By default an existing cell measure with the
        same identifier is replaced with *cm*.

    finalize : bool, optional
        If False then do not finalize the domain for the new
        coordinate. By default the domain is finalized. Only set to
        False if the domain is guaranteed to be finalized
        subsequently, in which case there may be a performance
        advantage.

:Returns:

    out : str
        The identifier for the new cell measure (see the *key*
        parameter).

**Examples**

>>>

'''
        if key is None:
            key = self.new_cell_measure_identifier()

        self._insert_item(cm, key, 'cell measure',
                          dimensions=dimensions, copy=copy, finalize=finalize)

        return key
    #--- End: def

    def insert_transform(self, transform, coords=(), copy=True, key=None):
        '''

Insert a new transform into the domain in place, preserving internal
consistency.

:Parameters:

    transform : cf.Transform
        The new transform.

    coords : sequence of str, optional

    copy : bool, optional
        If False then the new transform will not be deep copied. By
        default the transform will be deep copied prior to insertion.

    key : str, optional
        The identifier for the new coordinate. By default a unique
        identifier will be generated.

:Returns:

    out : str
        The key of the new transform in the domain's transforms
        dictionary.

**Examples**

>>>

'''
        if key is None:
            key = self.new_transform_identifier()
        elif key in self.transforms:
            raise ValueError("Can't overwrite an existing transform key")

        if copy:
            transform = transform.copy()

        self.transforms[key] = transform

        for coord_key in coords:
            self[coord_key].insert_transform(transform, key)
            
#            if key in self.transform_map:
#                self.transform_map[key].append(coord_key)
#            else:                  
#                self.transform_map[key] = [coord_key]
        #--- End: for

#        if transform.isformula_terms:
#            for value in transform.itervalues():
#                if isinstance(value, basestring):
#                    if key in self.transform_map:
#                        self.transform_map[key].append(value)
#                    else:                  
#                        self.transform_map[key] = [value]
#        #--- End: if

        return key
    #--- End: def

#    def pop(self, key, *default):
#        '''
#
#Remove specified key from the domain in-place and return the
#corresponding value.
#
#Note that the `dimensions` attribute is not updated.
#
#:Parameters:
#
#    key : str
#
#    default : optional
#        If *key* is not found then *default* is returned if given,
#        otherwise a KeyError is raised.
#
#:Returns:
#
#    out : cf.Coordinate or cf.CellMeasure
#         The returned variable (or *default*).
#
#**Examples**
#
#>>> d.pop('dim0')
#>>> d.pop('aux1')
#>>> d.pop('cm2')
#>>> d.pop('aux1', None)
#
#'''
#        try:
#            v = self._dict.pop(key, *default)
#        except KeyError:
#            raise KeyError("Can't remove domain key '%s': Doesn't exist" % key)
#
#        return v
#    #--- End: def

    def remove_cell_measure(self, key):
        '''

Remove a cell measure from the domain in place, preserving internal
consistency.

Note that this will never remove a dimension from the domain.

:Parameters:

    key : str
        The identifier of the cell measure to be removed.

:Returns: 

    out : cf.CellMeasure
        The removed cell measure

**Examples**

>>> d.dimension_sizes
{'dim0': 73, 'dim1': 96, 'dim2': 1}
>>> d.dimensions
{'dim0': ['dim0'],
 'dim1': ['dim1'],
 'cm0' : ['dim1', 'dim0'],
 'data': ['dim2', 'dim0, 'dim1']}
>>> d.remove_cell_measure('cm0')
>>> d.dimension_sizes
{'dim0': 73, 'dim1': 96, 'dim2': 1}}
>>> d.dimensions
{'dim0': ['dim0'],
 'dim1': ['dim1'],
 'data': ['dim2', 'dim0, 'dim1']}

'''
        try:
            v = self.pop(key)
        except KeyError:
            raise ValueError("  fgfgfgfgf cellmm ")
        
        self.dimensions.pop(key, None)

        return v
    #--- End: def

    def remove_coordinate(self, key):
        '''

Remove a coordinate from the domain in place, preserving internal
consistency.

Note that this will never remove a dimension from the domain.

:Parameters:

    key : str
        The identifier of the coordinate to be removed.

:Returns: 

    out : cf.Coordinate
        The removed coordinate.

**Examples**

>>> d.dimension_sizes
{'dim0': 73, 'dim1': 96, 'dim2': 1}
>>> d.dimensions
{'dim0': ['dim0'],
 'dim1': ['dim1'],
 'aux0': ['dim2'],
 'data': ['dim2', 'dim0, 'dim1']}
>>> d.remove_coordinate('aux0')
>>> d.remove_coordinate('dim1')
>>> d.dimension_sizes
{'dim0': 73, 'dim1': 96, 'dim2': 1}}
>>> d.dimensions
{'dim0': ['dim0'],
 'data': ['dim2', 'dim0, 'dim1']}

'''
        try:
            v = self.pop(key)
        except KeyError:
            raise ValueError("  fgfgfgfgf coord")
        
        self.dimensions.pop(key, None)

        return v
    #--- End: def

    def remove_dimension(self, dim):
        '''

Remove a dimension from the domain in place, preserving internal
consistency.

If the dimension has size 1 then one dimensionsal coordinates and cell
measures which span it are removed and the dimension is removed from
multidimensional auxiliary coordinates and cell measures if they span
it.

A size 1 dimension is *not* removed the list of dimensions of a
containing field's data array, so care must be taken to remove
concurrently the dimension from the field with its `~Field.squeeze`
method.

If the dimension has size greater than and one and is spanned by a
coordinate, cell measure or a containing field's data array then it
can not be removed.

:Parameters:

    dim : str
        The identifier of the dimension to be removed.

:Returns: 

    out : str
        The identifier of the removed dimension.

**Examples**

>>> d.remove_dimension('dim3')
>>> d.remove_dimension('latitude')

'''
        if dim not in self.dimension_sizes:
            raise ValueError("sdfsdfd")

        if self.dimension_sizes[dim] > 1:
            for x in self.dimensions:
                if dim in x:
                    raise ValueError("sdfsdfd----------999999999999")
            return self.dimension_sizes.pop(dim)

        self.squeeze(dim)

        return dim
    #--- End: def

    def remove_transform(self, key):
        '''

Remove a transform from the domain in place, preserving internal
consistency.

The transform is also de-referenced from coordinates using it is as
metadata, regardless of whether or not the transform itself exists.

:Parameters:

    key : str
        The domain identifier of the transform to be removed.

:Returns: 

    out : cf.Transform or None
        The removed transform, or None if it didn't exist.

**Examples**

>>> d.remove_transform('trans0')

'''
        # Remove transform
        transform = self.transforms.pop(key, None)

        # De-reference the transform from coordinates
        for coord in self.itercoordinates():
            if (hasattr(coord, 'transforms') and 
                key in coord.transforms.values()):
                del coord.transforms          
        #--- End: for

#        del self.transform_map[key]
        
        return transform
    #--- End: def

    def squeeze(self, dims):
        '''

Remove a size 1 dimension from the domain in place.

If the dimension has a dimension coordinate then it is removed, as are
1-d auxiliary coordinates and cell measures which span the
dimension. The dimension is squeezed from multidimensional auxiliary
coordinates and cell measures if they span it.

The dimension is not squeezed from the field's data array if it spans
it, therefore the field's data array may need to be squeezed
concurrently.

:Parameters:

    dims : str or sequence of strs
        The identifier of the dimension to remove.
    
:Returns:

    None

**Examples**

>>> d.dimension_sizes
{'dim0': 12, 'dim1': 73, 'dim2': 1}
>>> d.dimensions
{'data': ['dim0', 'dim1', 'dim2'],
 'aux0': ['dim1', 'dim2'],
 'aux1': ['dim2', 'dim1'],
 'dim0': ['dim0'],
 'dim1': ['dim1'],
 'dim2': ['dim2'],
 'cm0' : ['dim1', 'dim2']}
>>> d.squeeze('dim2')
>>> d.dimension_sizes
{'dim0': 12, 'dim1': 73}
>>> d.dimensions
{'data': ['dim0', 'dim1', 'dim2'],
 'aux0': ['dim1'],
 'aux1': ['dim1'],
 'dim0': ['dim0'],
 'dim1': ['dim1'],
 'cm0' : ['dim1']}

'''
        if isinstance(dims, basestring):
            dims = (dims,)

        for dim in dims:
            if dim in self.dimension_sizes:
                if self.dimension_sizes[dim] > 1:
                    raise ValueError("Can't squeeze a dimension with size > 1 from %s" %
                                     self.__class__.__name__)
            
            else:
                raise ValueError("Can't remove non-existent dimension from %s" %
                                 self.__class__.__name__)
            
#            if dim in self:
#                self.pop(dim)
            
            dimensions = self.dimensions.copy()
            del dimensions['data']
            
            for key, dims in dimensions.iteritems():
                if dim not in dims:
                    continue
            
                if len(dims) == 1:
                    # Remove the dimension's 1-d dimension and auxiliary coordinates
                    self.remove_coordinate(key)
                else:
                    # Squeeze the dimension out of N-d auxiliary coordinates
                    axis = dims.index(dim)
                    dims.pop(axis)
                    self[key].squeeze(axis)
            #--- End: for   

            # Remove the dimension
            del self.dimension_sizes[dim]

        #--- End: for
    #--- End: def

    def coordinates(self, dim=None):
        '''

Return a dictionary mapping coordinate identifiers to their
coordinates.

:Parameters:

    dim : str or int, optional
        Only select coordinates which span this dimension, defined by
        one of:

           * A dimension identity, as a defined by an appropriate
             coordinate's `~Coordinate.identity` method (such as
             ``'time'``).
             
           * The integer position of a dimension in the field's data
             array (such as ``2``).
             
           * The internal name of a dimension in the field's domain
             (such as ``'dim0'``).

:Returns:

    out : dict   
        A dictionary whose keys are coordinate identifiers and values
        are their coordinates.

**Examples**

>>> d.dimensions
{'aux0': ['dim0'],
 'aux1': ['dim0'],
 'aux2': ['dim2', 'dim1'],
 'aux3': ['dim1', 'dim2'],
 'cm0' : ['dim1', 'dim2'],
 'data': ['dim0', 'dim1', 'dim2'],
 'dim0': ['dim0'],
 'dim1': ['dim1'],
 'dim2': ['dim2']}
>>> d.coordinates()
{'aux0': <CF AuxiliaryCoordinate: atmosphere_hybrid_height_coordinate_ak(1) m>,
 'aux1': <CF AuxiliaryCoordinate: atmosphere_hybrid_height_coordinate_bk(1)>,
 'aux2': <CF AuxiliaryCoordinate: latitude(10, 9) degree_N>,
 'aux3': <CF AuxiliaryCoordinate: longitude(9, 10) degreesE>,
 'dim0': <CF DimensionCoordinate: atmosphere_hybrid_height_coordinate(1)>,
 'dim1': <CF DimensionCoordinate: grid_longitude(9) degrees_east>,
 'dim2': <CF DimensionCoordinate: grid_latitude(10) degrees_north>}
>>> d.coordinates('dim1')
{'aux2': <CF AuxiliaryCoordinate: latitude(10, 9) degree_N>,
 'aux3': <CF AuxiliaryCoordinate: longitude(9, 10) degreesE>,
 'dim1': <CF DimensionCoordinate: grid_longitude(9) degrees_east>}
>>> d.coordinates('grid_latitude')
{'aux2': <CF AuxiliaryCoordinate: latitude(10, 9) degree_N>,
 'aux3': <CF AuxiliaryCoordinate: longitude(9, 10) degreesE>,
 'dim2': <CF DimensionCoordinate: grid_latitude(10) degrees_north>}

'''
        if dim is not None:
            dim = self.coord(dim, key=True)
            if dim is None:
                return {}

        out = self.dim_coords(dim)
        out.update(self.aux_coords(dim))

        return out
    #--- End: def

    def copy(self):
        '''

Return a deep copy.

Equivalent to ``copy.deepcopy(d)``.

:Returns:

    out : 
        The deep copy.

**Examples**

>>> e = d.copy()

'''
#        new = type(self)()
        X = type(self)
        new = X.__new__(X)

        new__dict__ = self.__dict__.copy()
        self_dict = new__dict__.pop('_dict')

        new_transforms = new__dict__.pop('transforms').copy()      

        new__dict__ = deepcopy(new__dict__)

        new_dict = {}
        for key, value in self_dict.iteritems():
            new_dict[key] = value.copy()

        new__dict__['_dict'] = new_dict
        new__dict__['transforms'] = new_transforms

        new.__dict__ = new__dict__

        return new
    #--- End: def

    def aux_coords(self, dim=None):
        '''

Return a dictionary whose values are the auxiliary coordinates which
span the given dimension and keys of the domain's auxiliary coordinate
identifiers.

:Parameters:

    dim : str, optional
        The identifier of the dimension to be spanned. By default all
        dimensions are considered (so all auxiliary coordinates are
        returned).

:Returns:

    out : dict
        The auxiliary coordinates and their identifiers.

**Examples**

>>> d.dimensions
{'data': ['dim0', 'dim1', 'dim2'],
 'dim0': ['dim0'],
 'dim1': ['dim1'],
 'dim2': ['dim2'],
 'dim3': ['dim3'],
 'aux0': ['dim1', 'dim2'],
 'aux1': ['dim0'],
 'aux2': ['dim2', 'dim1']}
>>> d.aux_coords()
{'aux0': <CF AuxiliaryCoordinate: ...>,
 'aux1': <CF AuxiliaryCoordinate: ...>,
 'aux2': <CF AuxiliaryCoordinate: ...>}
>>> d.aux_coords('dim2')
{'aux0': <CF AuxiliaryCoordinate: ...>,
 'aux2': <CF AuxiliaryCoordinate: ...>}
>>> d.aux_coords('dim3')
{}

'''       
        out = {}

        if dim is None:            
            for key in self.get_keys('^aux'):
                out[key] = self[key]

        else:
            dimensions = self.dimensions.copy()
            del dimensions['data']
            for key in self.get_keys('^aux'):
                if dim in dimensions[key]:
                    out[key] = self[key]
        #--- End: if

        return out
    #--- End: def

    def dim_coords(self, dim=None):
        '''

Return a dictionary whose values are the dimension coordinates which
span the given dimension and keys of the domain's dimension coordinate
identifiers.

:Parameters:

    dim : str, optional
        The identifier of the dimension. By default all dimension
        coordinates are returned.

:Returns:

    out : dict
        The dimension coordinates and their identifiers.

**Examples**

>>> d.dimensions
{'data': ['dim0', 'dim1', 'dim2'],
 'dim0': ['dim0'],
 'dim1': ['dim1'],
 'dim2': ['dim2'],
 'dim3': ['dim3'],
 'aux0': ['dim1', 'dim2'],
 'aux1': ['dim0'],
 'aux2': ['dim2', 'dim1']}
>>> d.dim_coords()
{'dim0': <CF DimensionCoordinate: ...>,
 'dim1': <CF DimensionCoordinate: ...>,
 'dim2': <CF DimensionCoordinate: ...>,
 'dim3': <CF DimensionCoordinate: ...>}
>>> d.dim_coords('dim2')
{'dim2': <CF DimensionCoordinate: ...>}

'''       
        out = {}
        if dim is None: 
            for key in self.get_keys('^dim'):
                out[key] = self[key]          
        else:
            if dim in self:
                out[dim] = self[dim]
        #--- End: if
      
        return out
    #--- End: def

    def cell_measures(self, dim=None):
        '''

Return a dictionary whose values are the cell measures which span the
given dimension and keys of the cell measure identifiers.

:Parameters:

    dim : str, optional
        The identifier of the dimension to be spanned. By default all
        dimensions are considered (so all cell measures are returned).

:Returns:

    out : dict
        The cell measures and their identifiers.

**Examples**

>>> d.dimensions
{'data': ['dim0', 'dim1', 'dim2'],
 'dim0': ['dim0'],
 'dim1': ['dim1'],
 'dim2': ['dim2'],
 'cm0' : ['dim1', 'dim2'],
 'cm1' : ['dim1', 'dim2', 'dim3']}
>>> d.cell_measures()
{'cm0': <CF CellMeasure: ...>,
 'cm1': <CF CellMeasure: ...>}
>>> d.cell_measures('dim3')
{'cm1': <CF CellMeasure: ...>}
>>> d.cell_measures('dim0')
{}

'''       
        out = {}

        if dim is None:            
            for key in self.get_keys('^cm'):
                out[key] = self[key]

        else:
            dimensions = self.dimensions #.copy()
#x            del dimensions['data']
            for key in self.get_keys('^cm'):
                if dim in dimensions[key]:
                    out[key] = self[key]
        #--- End: if

        return out
    #--- End: def

    def close(self):
        '''

Close all referenced open data files.

:Returns:

    None

**Examples**

>>> d.close()

'''
        for c in self.itervalues():
            c.close()

        for t in self.transforms.itervalues():
            t.close()
    #--- End: def

    def coord(self, arg, role=None, key=False, exact=False, dimensions=False, 
              one_d=False, maximal_match=True):
        '''

Return a coordinate of the domain.

Note that the returned coordinate is an object identity (not a copy)
to the coordinate stored in the domain so, for example, a coordinate's
properties may be changed in-place:

>>> f.coord('height').long_name
AttributeError: Coordinate has no CF property 'long_name'
>>> f.coord('height').long_name = 'height'
>>> f.coord('height').long_name
'height'

:Parameters:

    arg : str or dict or int
        The identify of the coordinate. One of:

        * A string containing an unambiguous abbreviation (see the
          *exact* parameter) of its identity as returned by the
          coordinate's `~Coordinate.identity`.

        * A string containing a 1-d coordinate's dimension identifier
          in the domain.

        * A dictionary containing one or more CF property names and
          their unambiguous abbreviations (see the *exact* parameter)
          values as its key/value pairs (such as ``{'long_name':
          'temperature'}``). See the *maximal_match* parameter for
          when two or more properties are specified in this manner.

        * An integer giving the position of a 1-d coordinate's
          dimension in the field's data array (negative integers count
          from the slowest moving domension).

    exact : bool, optional
        If True then do not string abbreviations given by the *arg*
        parameter will not match, nor will different but equivalent
        units. By default unambiguous string abbreviations will match,
        as will different but equivalent units.

    role : str, optional
        Restrict the search to coordinates of the given role. Valid
        values are ``'dim'`` and ``'aux'`` for dimension and auxiliary
        coordinate repectively. By default both types are considered.

    one_d : bool, optional
        Restrict the search to one dimensionsal coordinates. By
        default coordinates with any number of dimensions are
        considered.

    key : bool, optional
        If True then return the domain's identifier for the
        coordinate. By default the unique coordinate itself is
        returned.

    dimensions : bool, optional
        If True then return a list of the unique coordinate's
        dimensions described by their domain identifiers. By default
        the unique coordinate itself is returned.

    maximal_match : bool, optional
        If False and two or more properties are specified by the *arg*
        parameter then a unique coordinate will satisfy at least one
        of the properties' criteria. By default a unique coordinate
        will satisfy all of the properties' criteria.

:Returns:

    out : cf.Coordinate or str or list or None
        The unique coordinate or, if *key* is True, the unique
        coordinate's identifier in the domain  or, if *dimensions* is
        True, the unique coordinate's dimensions described by their
        domain identifiers, if no unique coordinate could be found,
        None.

**Examples**

>>> d['dim2'].shape
(360,)
>>> d['dim2'].properties
{'_FillValue': None,
 'axis': 'X',
 'long_name': 'longitude',
 'standard_name': 'longitude',
 'units': 'degrees_east'}

>>> d.coord('longitude')
<CF DimensionCoordinate: longitude(360)>
>>> d.coord('long')
<CF DimensionCoordinate: longitude(360)>
>>> d.coord('long', key=True)
'dim2'
>>> d.coord('long', dimensions=True)
['dim2']
>>> d.coord('long', oned_d=True)
<CF DimensionCoordinate: longitude(360)>

>>> d.coord('lon', exact=True)
None
>>> d.coord('longitude', exact=True)
<CF DimensionCoordinate: longitude(360)>

>>> d.coord({'standard_name': 'long', 'axis': X})
<CF DimensionCoordinate: longitude(360)>
>>> d.coord({'standard_name': 'long', 'axis': 'X'}, maximal_match=False)
<CF DimensionCoordinate: longitude(360)>
>>> d.coord({'standard_name': 'long', 'axis': 'Y'})
None
>>> d.coord({'standard_name': 'long', 'axis': 'Y'}, maximal_match=False)
<CF DimensionCoordinate: longitude(360)>

>>> d.coord('long', role='dim')
<CF DimensionCoordinate: longitude(360)>
>>> d.coord('long', role='aux')
None

'''       
        if isinstance(arg, basestring) and arg in self:
            # --------------------------------------------------------
            # arg is a coordinate's identifier in the domain
            # --------------------------------------------------------
            result_key = arg
            if key:
                return result_key
            elif dimensions:
                return self.dimensions[result_key][:]
            else:
                return self[result_key]

        elif isinstance(arg, (int, long)):
            # --------------------------------------------------------
            # arg is an integer giving the position of a 1-d
            # coordinate's dimension in the field's data array.
            # --------------------------------------------------------
            try:
                result_key = self.dimensions['data'][arg]
            except KeyError:
                raise ValueError("Can't find coordinate: No data array")
            except IndexError:
                raise IndexError(
                    "Can't find coordinate: Out of range index: %d" % arg)
            
            if result_key not in self:
                if role == 'dim':
                    # No dimension coordinate for this dimension
                    return None

                found_match = False
                for k in self.dimensions:
                    if k == [result_key]:
                        if found_match:
                            # More than one 1-d auxiliary coordinate
                            # for this dimension
                            return None
                        found_match = result_key
                #--- End: for
                result_key = found_match
            #--- End: if

            if key:                        
                return result_key            
            elif dimensions:
                return self.dimensions[result_key][:]
            else:
                return self[result_key]
        #--- End: if

        # Still here?
        if isinstance(arg, basestring):
            arg = {'standard_name': arg}

        # Change 'units' and 'calendar' keys to a 'Units' key
        if 'Units' in arg:
            arg.pop('units', None)
            arg.pop('calendar', None)
        elif 'units' in arg:
            arg['Units'] = Units(arg.pop('units'), 
                                 calendar=arg.pop('calendar', None))
        #--- End: if

        if role is None:
            domain_keys = self.get_keys('^dim|^aux')
        elif role == 'dim':
            domain_keys = self.get_keys('^dim')
        elif role == 'aux':
            domain_keys = self.get_keys('^aux')       

        result_key = None

        found_coordinate = 0

        for domain_key in domain_keys:

            if one_d and len(self.dimensions[domain_key]) > 1:
                continue

            found_match = False

            for name, value in arg.iteritems():
           
                if name == 'standard_name':
                    x = self[domain_key].identity()
                else:
                    x = self[domain_key].getprop(name, None)

                match = (x is not None and 
                         (exact and x == value) or
                         (not exact and 
                          ((isinstance(x, str)   and x.startswith(value)) or
                           (isinstance(x, Units) and x.equivalent(value)) or
                           x == value))
                         )

                if maximal_match:
                    if match:
                        found_match = True
                    else:
                        found_match = False
                        break

                elif match:
                    found_match = True
                    break
            #--- End: for

            if found_match:
                found_coordinate += 1
                if found_coordinate > 1:
                    # Found two coordinates which match, so return
                    # None
                    return None

                result_key = domain_key
             #--- End: if
        #--- End: for

        if result_key is None:
            # Found no coordinates which match, so return None
            return None

        # Still here? Then found exactly one coordinate which matches,
        # so return it or its key
        if key:
            return result_key
        elif dimensions:
            if one_d:
                return self.dimensions[result_key][0]
            else:
                return self.dimensions[result_key][:]
        else:
            return self[result_key]
    #--- End: def

    def expand_dims(self, coord=None, size=1, finalize=True):
        '''

Expand the domain with a new dimension in place.

The new dimension may by of any size greater then 0.

:Parameters:

    coord : cf.Coordinate, optional
        A dimension coordinate for the new dimension. The new
        dimension's size is set to the size of the coordinate's array.

    size : int, optional
        The size of the new dimension. By default a dimension of size
        1 is introduced. Ignored if *coord* is set.

:Returns:

    None

**Examples**

>>> d.expand_dims()
>>> d.expand_dims(size=12)
>>> c
<CF Coordinate: >
>>> d.expand_dims(coord=c)

'''
        if coord:            
            self.insert_dim_coordinate(coord, finalize=finalize)

        else:
            self.insert_dimension(size)

#        if coord:
#            self[dim] = coord.copy()
#            if hasattr(coord, 'size'):
#                size = coord.size
#        #--- End: if
#
#        self.dimension_sizes[dim] = size
    #--- End: def

    def dump_dimensions(self, level=0):
        '''
        
Return a string containing a description of the domain.

:Parameters:

    level : int, optional

:Returns:

    out : str
        A string containing the description.

**Examples**

'''
        indent1 = '    ' * level
        indent2 = '    ' * (level+1)

        string = ['%sDimensions' % indent1]
        
        x = sorted(['%s%s(%d)' % (indent2,
                                  self.dimension_name(dim),
                                  self.dimension_sizes[dim])
                    for dim in self.dimension_sizes])
        string.extend(x)

        return '\n'.join(string)
         
    def dump_components(self, level=0, complete=False):
        '''
        
Return a string containing a full description of the domain.

:Parameters:

    level : int, optional

    complete : bool, optional

:Returns:

    out : str
        A string containing the description.

**Examples**

'''
        indent1 = '    ' * level
        
        string = []
         
        for key in sorted(self.get_keys('^dim')):
            string.append('')
            string.append('%sDimension coordinate: %s' % (indent1, self[key].name(default='')))
            string.append(self[key].dump(domain=self, key=key, level=level+1))
               
        for key in sorted(self.get_keys('^aux')):
            string.append('')
            string.append('%sAuxiliary coordinate: %s' % (indent1, self[key].name(default='')))
            string.append(self[key].dump(domain=self, key=key, level=level+1))
               
        for key in sorted(self.get_keys('^cm')):            
            string.append('')
            string.append(self[key].dump(domain=self, key=key, level=level))

        for key in sorted(self.transforms.keys()):
            string.append('')
            string.append(self.transforms[key].dump(domain=self, level=level, 
                                                    complete=complete))

        return '\n'.join(string)
    #--- End: def

    def dump(self, level=0, complete=False):
        '''

Return a string containing a full description of the domain.

:Parameters:

    level : int, optional

    complete : bool, optional

:Returns:

    out : str
        A string containing the description.

complete : bool, optional

**Examples**

>>> print d.dump()

'''
        string = [self.dump_dimensions()]
        string.append('')
        string.append(self.dump_components(level=0, complete=complete))

        return '\n'.join(string)


    def equals(self, other, rtol=None, atol=None, traceback=False):
        '''

True if two domains are equal, False otherwise.

Equality is defined as follows:

* There is one-to-one correspondence between dimensions and dimension
  sizes between the two domains.

* For each domain component type (dimension coordinate, auxiliary
  coordinate and cell measures), the set of constructs in one domain
  equals that of the other domain. The component identifiers need not
  be the same.

* The set of transforms in one domain equals that of the other
  domain. The transform identifiers need not be the same.

Equality of numbers is to within a tolerance.

:Parameters:

    other :
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns: 

    out : bool
        Whether or not the two instances are equal.

**Examples**

>>> d.equals(s)
True

>>> d.equals(t)
False

>>> d.equals(t, traceback=True)


'''
        if self is other:
            return True
        
        # Check that each instance is the same type
        if type(self) != type(other):
            print("%s: Different types: %s, %s" %
                  (self.__class__.__name__,
                   self.__class__.__name__,
                   other.__class__.__name__))
            return False
        #--- End: if

        if (sorted(self.dimension_sizes.values()) != 
            sorted(other.dimension_sizes.values())):
            # There is not a 1-1 correspondence between dimensions and
            # dimension sizes between the two domains.           
            if traceback:
                print("%s: Different ranks: %s, %s" %
                      (self.__class__.__name__,
                       sorted(self.dimension_sizes.values()),
                       sorted(other.dimension_sizes.values())))
            return False
        #--- End: if

        if rtol is None:
            rtol = RTOL()
        if atol is None:
            atol = ATOL()              

        # ------------------------------------------------------------
        # Test the coordinates and cell measures. Don't worry about
        # transforms yet - we'll do so later.
        # ------------------------------------------------------------
        key_map = {}
        for coord_type in ('^dim', '^aux', '^cm'):
            self_keys  = sorted(self.get_keys(coord_type))
            other_keys = sorted(other.get_keys(coord_type))
            for key0 in self_keys:
                found_match = False
                for key1 in other_keys: 
                    if self[key0].equals(other[key1], rtol=rtol, atol=atol,
                                         traceback=False):
                        found_match = True
                        key_map[key1] = key0
                        other_keys.remove(key1)
                        break
                #--- End: for

                if not found_match:
                    if traceback:
                        print("%s: Different coordinate: %s" %
                              (self.__class__.__name__, repr(self[key0])))
                    return False
            #--- End: for
        #--- End: for

        # ------------------------------------------------------------
        # Test the transforms
        # ------------------------------------------------------------
        transform_map = {}
        if not self.transforms:
            if other.transforms:
                # Self doesn't have any transforms but other does
                if traceback:
                    print("%s: Different numbers of transforms: 0 != %d" %
                          (self.__class__.__name__, len(other.transforms)))
                return False
        else:
            if not other.transforms:
                # Other doesn't have any transforms but self does
                if traceback:
                    print("%s: Different numbers of transforms: %d != 0" %
                          (self.__class__.__name__, len(self.transforms)))
                return False
            #--- End: if

            transforms1 = list(other.transforms)
            
            for key0, transform0 in self.transforms.iteritems():
                found_match = False
                for key1 in transforms1:
                    transform1 = other.transforms[key1]
                    if self.equal_transform(transform0, transform1,
                                             domain=other, traceback=False):
                        found_match = True
                        transform_map[key0] = key1
                        transforms1.remove(key1)
                        break
                #--- End: for
                if not found_match: 
                    if traceback:
                        print("%s: Unequals transform: %s" %
                              (self.__class__.__name__, repr(transform0)))
                    return False
            #--- End: for                    
        #--- End: if

#        map1 = other.transform_map()
#        for key0, t0 in self.transform_map().iteritems():
#            key1 = key_map[key0]
#            if (key1 not in map1 or
#                t0 != set(transform_map[t] for t in map1[key1])):
#                if traceback:
#                    print("hhhhhhhhhhhhhhhhhhhhhhh")
#                return False
#        #--- End: for

        # ------------------------------------------------------------
        # Still here? Then the two domains are equal
        # ------------------------------------------------------------
        return True
    #--- End: def
#--- End: class
