import numpy
import re
from netCDF4  import Dataset as netCDF4_Dataset
from operator import mul
from json     import loads as json_loads
from ast      import literal_eval as ast_literal_eval

from ..ancillaryvariables import AncillaryVariables
from ..coordinate         import DimensionCoordinate, AuxiliaryCoordinate
from ..coordinatebounds   import CoordinateBounds
from ..cellmeasure        import CellMeasure
from ..transform          import Transform
from ..field              import Field
from ..cellmethods        import CellMethods
from ..fieldlist          import FieldList
from ..units              import Units
from ..functions          import abspath, dirname, pathjoin

from ..data.data import Data

from .functions import _open_netCDF_file
from .filearray import NetCDFFileArray

def read(filename, file_format=None):
    ''' 

Read fields from an input netCDF file on disk or from an OPeNDAP
server location.

The file may be big or little endian.

NetCDF dimension names are stored in the `nc_dimensions` attribute of
a field's domain and netCDF variable names are stored in the `ncvar`
attributes of the field and its domain components (coordinates,
coordinate bounds, cell measures and transformations).

:Parameters:

    filename : str or file
        A string giving the file name or OPenDAP URL, or an open file
        object, from which to read fields. Note that if a file object
        is given it will be closed and reopened.

    file_format : str, optional
        Only read the file if it is the given format. Valid formats
        are ``'NETCDF'`` for a CF-netCDF file and ``'CFA'`` for
        CFA-netCDF file. By default a file of any of these formats is
        read.

:Returns:

    out : FieldList
        The fields in the file.

**Examples**

>>> f = cf.netcdf.read('file.nc')
>>> type(f)
<class 'cf.field.FieldList'>
>>> f
[<CF Field: pmsl(30, 24)>,
 <CF Field: z-squared(17, 30, 24)>,
 <CF Field: temperature(17, 30, 24)>,
 <CF Field: temperature_wind(17, 29, 24)>]

>>> cf.netcdf.read('file.nc')[0:2]
[<CF Field: pmsl(30, 24)>,
 <CF Field: z-squared(17, 30, 24)>]

>>> cf.netcdf.read('file.nc', units='K')
[<CF Field: temperature(17, 30, 24)>,
 <CF Field: temperature_wind(17, 29, 24)>]

>>> cf.netcdf.read('file.nc')[0]
<CF Field: pmsl(30, 24)>

'''
    if isinstance(filename, file):
        name = filename.name
        filename.close()
        filename = name
    #--- End: if
    
    filename = abspath(filename)

    # Read the netCDF file 
    nc = _open_netCDF_file(filename) 

    # Set of all of the netCDF variable names in the file.
    #
    # For example:
    # >>> variables
    # set(['lon','lat','tas'])
    variables = set(map(str, nc.variables))

    # ----------------------------------------------------------------
    # Put the file's global attributes into the global
    # 'global_attributes' dictionary
    # ----------------------------------------------------------------
    global_attributes = {}
    for attr in map(str, nc.ncattrs()):
        try:
            value = nc.getncattr(attr)
            if isinstance(value, basestring):
                try:
                    global_attributes[attr] = str(value)
                except UnicodeEncodeError:
                    global_attributes[attr] = value.encode(errors='ignore')          
            else:
                global_attributes[attr] = value     
        except UnicodeDecodeError:
            pass
    #--- End: for
        
    # Find out if this is a CFA file
    cfa = 'CFA' in global_attributes.get('Conventions', [])

    if (file_format and 
        (not cfa and file_format == 'CFA') or (cfa and file_format == 'NETCDF')):
        # Return an empty field list
        return FieldList()

    # ----------------------------------------------------------------
    # Create a dictionary keyed by nc variable names where each key's
    # value is a dictionary of that variable's nc
    # attributes. E.g. attributes['tas']['units']='K'
    # ----------------------------------------------------------------
    attributes = {}
    for ncvar in variables:
        attributes[ncvar] = {}
        for attr in map(str, nc.variables[ncvar].ncattrs()):
            try:
                attributes[ncvar][attr] = nc.variables[ncvar].getncattr(attr)
                if isinstance(attributes[ncvar][attr], basestring):
                    try:
                        attributes[ncvar][attr] = str(attributes[ncvar][attr])
                    except UnicodeEncodeError:
                        attributes[ncvar][attr] = attributes[ncvar][attr].encode(errors='ignore')
            except UnicodeDecodeError:
                pass
        #--- End: for

        # Check for bad units
        if 'units' in attributes[ncvar]:
            # Check for bad units
            try:
                Units(attributes[ncvar]['units'])
            except (ValueError, TypeError):
                # Units in file have been set to unknown units so 1)
                # give a warning, 2) set the 'nonCF_units' property to
                # the bad units and 3) remove the offending units.
                print(
"WARNING: Moving unsupported units to 'nonCF_units': '%s'" %
attributes[ncvar]['units'])
                attributes[ncvar]['nonCF_units'] = attributes[ncvar].pop('units')
        #--- End: if
    
        # Check for bad calendar
        if 'calendar' in attributes[ncvar]:
            try:
                Units(calendar=attributes[ncvar]['calendar']) # DCH attention
            except ValueError:
                # Calendar in file has been set to unknown calendar so
                # 1) give a warning, 2) set the 'nonCF_calendar'
                # property to the bad calendar and 3) remove the
                # offending calendar.
                print(
"WARNING: Moving unsupported calendar to 'nonCF_calendar': '%s'" %
attributes[ncvar]['calendar'])
                attributes[ncvar]['nonCF_calendar'] = attributes[ncvar].pop('calendar')
    #--- End: for

    # ----------------------------------------------------------------
    # Remove everything bar data variables from the list of
    # variables. I.e. remove dimension and auxiliary coordinates,
    # their bounds and grid_mapping variables
    # ----------------------------------------------------------------
    for ncvar in variables.copy():

        # Remove dimension coordinates and their bounds
        if ncvar in map(str, nc.dimensions):

            if ncvar in variables:
                variables.discard(ncvar)
                for attr in ('bounds', 'climatology'):
                    if attr not in attributes[ncvar]:
                        continue
                
                    # Check the dimensionality of the coordinate's
                    # bounds. If it is not right, then it can't be a
                    # bounds variable and so promote to an independent
                    # data variable
                    bounds = attributes[ncvar][attr]
                    if bounds in nc.variables:
                        if nc.variables[bounds].ndim == nc.variables[ncvar].ndim+1:
                            variables.discard(bounds)
                        else:
                            del attributes[ncvar][attr]

                        break
                    else:
                        del attributes[ncvar][attr]
                        print(
"WARNING: Missing bounds variable '%(bounds)s' in %(filename)s" %
locals())
                #--- End: for
            #--- End: if

            continue
        #--- End: if

        # Still here? Then remove auxiliary coordinates and their
        # bounds
        if 'coordinates' in attributes[ncvar]:
            # Allow for (incorrect) comma separated lists
            for aux in re.split('\s+|\s*,\s*', attributes[ncvar]['coordinates']):
                if aux in variables:
                    variables.discard(aux)
                    for attr in ('bounds', 'climatology'):
                        if attr not in attributes[aux]:
                            continue

                        # Check the dimensionality of the coordinate's
                        # bounds. If it is not right, then it can't be
                        # a bounds variable and so promote to an
                        # independent data variable.
                        bounds = attributes[aux][attr]
                        if bounds in nc.variables:
                            if nc.variables[bounds].ndim == nc.variables[aux].ndim+1:
                                variables.discard(bounds)
                            else:
                                del attributes[aux][attr]

                            break
                        else:
                            del attributes[aux][attr]
                            print(
"WARNING: Missing bounds variable '%(bounds)s' in %(filename)s" %
locals())
                    #--- End: for
                #--- End: if
            #--- End: for
        #--- End: if
    
        # Remove grid mapping variables
        if 'grid_mapping' in attributes[ncvar]:
            variables.discard(attributes[ncvar]['grid_mapping'])

        # Remove cell measure variables
        if 'cell_measures' in attributes[ncvar]:
            cell_measures = re.split('\s*(\w+):\s*',
                                     attributes[ncvar]['cell_measures'])
            for ncvar in cell_measures[2::2]:
                variables.discard(ncvar)
        #--- End: if

    #--- End: for

    # ----------------------------------------------------------------
    # Everything left in the variables set is now a proper data
    # variable, so make a list of fields, each of which contains one
    # data variable and the relevant shared metadata.
    # ----------------------------------------------------------------

    # Dictionary mapping netCDF variable names of domain components to
    # their cf Variables.
    #
    # For example:
    # >>> seen_in_file
    # {'lat': <CF Coordinate: (73)>}
    seen_in_file = {}

    # Dictionary of ?
    #
    # For example:
    # >>> formula_terms
    #
    formula_terms = {}

    # Set
    #
    # For example:
    # >>> 
    #
    not_fields = set()

    ancillary_variables = set()
    fields_in_file      = FieldList()

    for data_var in variables:

        # Don't turn private CFA variables into fields
        if _is_cfa_private_variable(nc.variables[data_var], cfa):
            continue

        f = _create_Field(filename,
                          nc,
                          data_var,
                          attributes,
                          seen_in_file,
                          formula_terms, 
                          ancillary_variables, 
                          not_fields,
                          global_attributes,
                          cfa=cfa)
        
        fields_in_file.append(f)
    #--- End: for

    # ----------------------------------------------------------------
    # If any transform equation terms are fields then find out which
    # fields they are
    # ----------------------------------------------------------------
    if formula_terms:

        # Find the formula terms fields, identified by their netCDF
        # variable names.
        ft_ncvars = set()
        for transform in formula_terms.itervalues():
            for value in transform.itervalues():
                if isinstance(value, dict):
                    ncvar = value['ncvar']
                    ft_ncvars.add(ncvar)
        #--- End: for

        # 
        ncvar_to_field = {}
        i = 0
        while ft_ncvars:
            try:
                f = fields_in_file[i]
            except IndexError:
                # No more fields
                break

            # Still here?
            ncvar = f.ncvar            
            if ncvar in ft_ncvars:
                # ----------------------------------------------------
                # This field is being used within a transform
                # ----------------------------------------------------
                # Finalize the field
                f.finalize()
                # Remove formula_terms transforms from fields
                # contained within formula_terms transforms
                for key, transform in f.domain.transforms.items():
                    if transform.isformula_terms:
                        f.domain.remove_transform(key)
                #--- End: for
                ncvar_to_field[ncvar] = f
                fields_in_file.pop(i)
                # Make sure that the field is not in the list of
                # variables which have been turned into auxiliary
                # coordinates
                not_fields.discard(ncvar)
            else:
                i += 1
        #--- End: while

        # Remove fields which have been turned into auxiliary
        # coordinates and have not already been removed as transform
        # fields.
        if not_fields:
            ncvars = fields_in_file.ncvar
            for ncvar in not_fields:
                try:
                    i = ncvars.index(ncvar)
                except ValueError:
                    pass
                else:
                    ncvars.pop(i)
                    fields_in_file.pop(i)                    
        #--- End: if

        for f in fields_in_file:
            # Loop round this field's transforms (if any)
            transforms = f.domain.transforms.values()
            for transform in transforms:
                if transform.isgrid_mapping:
                    continue

                # Still here? Then replace any pointers to fields with
                # the actual fields.
                for term, value in transform.iteritems():
                    if isinstance(value, dict):
                        transform[term] = ncvar_to_field[value['ncvar']].copy()
                        
                #--- End: for
            #--- End: for
        #--- End: for

    #--- End: if
        
    # ----------------------------------------------------------------
    # For each field that has ancillary variables, replace its
    # built-in list of netCDF variable names with an
    # AncillaryVariables object.
    # ----------------------------------------------------------------
    if ancillary_variables:
        ncvar_to_field = {}
        index = 0
        while ancillary_variables:
            try:
                f = fields_in_file[index]
            except IndexError:
                # No more fields
                break
            
            # Still here?
            ncvar = f.ncvar            
            if ncvar in ancillary_variables:
                # ----------------------------------------------------
                # This field is being used as an ancillary variable
                # field
                # ----------------------------------------------------
                # Finalize the field
                f.finalize()
                ancillary_variables.discard(ncvar)
                ncvar_to_field[ncvar] = f
                fields_in_file.pop(index)
            else:
                index += 1
        #--- End: while
        
        for f in fields_in_file:
            if not hasattr(f, 'ancillary_variables'):
                continue

            av = AncillaryVariables()
            for ncvar in f.ancillary_variables:
                av.append(ncvar_to_field[ncvar].copy())

            f.ancillary_variables = av
        #--- End: for
    #--- End: if

    # Finalize the fields
    for f in fields_in_file:
        f.finalize()

    return fields_in_file
#--- End: def

def _create_Field(filename,
                  nc,
                  data_var,
                  attributes,
                  seen_in_file,
                  formula_terms, 
                  ancillary_variables, 
                  not_fields,
                  global_attributes,
                  cfa=False):
    '''

Create a field for a given netCDF variable.

:Parameters:

    filename : str
        The name of the netCDF file.

    nc : netCDF4.Dataset
        The entire netCDF file in a `netCDF4.Dataset` instance.

    data_var : str
        The netCDF name of the variable to be turned into a field.

    attributes : dict
        Dictionary of the data variable's netCDF attributes.

    seen_in_file : dict

    formula_terms : dict

    ancillary_variables : set

    global_attributes : dict

    cfa : bool
        If True then netCDF file is a CFA file. By default it is
        assumed that the file is not a CFA file.

:Returns:

    out : Field
        The new field.

'''
    # Add global attributes to the data variable's properties, unless
    # the data variables already has a property with the same name.
    for attr, value in global_attributes.iteritems():
        if attr not in attributes[data_var]:
            attributes[data_var][attr] = value

    # Take cell_methods out of the data variable's properties since it
    # will need special processing once the domain has been defined
    if 'cell_methods' in attributes[data_var]:
        cell_methods = CellMethods(attributes[data_var].pop('cell_methods'))
    else:
        cell_methods = None

    # Take add_offset and scale_factor out of the data variable's
    # properties since they will be dealt with by the variable's Data
    # object
    for attr in ('add_offset', 'scale_factor'):
        if attr in attributes[data_var]:
            del attributes[data_var][attr]

    # Change numpy arrays to tuples for selected attributes
    for attr in ('valid_range',):
        if attr in attributes[data_var]:
            attributes[data_var][attr] = tuple(attributes[data_var][attr])

    # ----------------------------------------------------------------
    # Initialize the field with the data variable and its attributes
    # ----------------------------------------------------------------
    f = Field(properties=attributes[data_var], finalize=False)

    f.ncvar = data_var
    f.file = filename

#    if 'units' not in attributes[data_var]:
#        f.Units = Units()

    # Map netCDF variable and dimension names to coordinates.
    # 
    # For example:
    # >>> ncvar_to_coord
    # {'geo_region':  <CF Coordinate:...>}
#    ncvar_to_coord = {}

    # Map netCDF dimension dimension names to domain dimension names.
    # 
    # For example:
    # >>> ncdim_to_dim
    # {'lat': 'dim0', 'time': 'dim1'}
    ncdim_to_dim = {}

    ncvar_to_key = {}
        
    f.domain.dimensions['data'] = []
    f.domain.nc_dimensions      = {}
  
    # ----------------------------------------------------------------
    # Add dimensions and dimension coordinates to the field
    # ----------------------------------------------------------------
    field_ncdimensions = _ncdimensions(nc.variables[data_var], cfa)

    for ncdim in field_ncdimensions:

        if ncdim in nc.variables:
            # There is a dimension coordinate for this dimension, so
            # create the coordinate and the dimension.
            if ncdim in seen_in_file:
                coord = seen_in_file[ncdim].copy()
            else:
                coord = _create_Coordinate(nc, ncdim, attributes, f, cfa=cfa,
                                           dimension=True)
                seen_in_file[ncdim] = coord                
            #--- End: if

            dim = f.domain.insert_dim_coordinate(coord, copy=False,
                                                 finalize=False)

            ncvar_to_key[ncdim] = dim
        else:
            # There is no dimension coordinate for this dimension, so
            # just create a dimension with the correct size.
#            dim = f.domain.new_dimension(size=len(nc.dimensions[ncdim]))
            dim = f.domain.insert_dimension(size=len(nc.dimensions[ncdim]))
        #--- End: if

        # Update data dimension name and set dimension size
        f.domain.nc_dimensions[dim] = ncdim
        f.domain.dimensions['data'].append(dim)
        
        ncdim_to_dim[ncdim] = dim
    #--- End: for
            
    f.Data = _set_Data(nc, nc.variables[data_var], f, f, cfa=cfa)

    # ----------------------------------------------------------------
    # Add scalar dimension coordinates and auxiliary coordinates to
    # the field
    # ----------------------------------------------------------------
    coordinates = f.getprop('coordinates', None)
    if coordinates is not None:
        
        # Split the list (allowing for incorrect comma separated
        # lists).
        for ncvar in re.split('\s+|\s*,\s*', coordinates):
            # Skip dimension coordinates which are in the list
            if ncvar in field_ncdimensions: #nc.variables[data_var].dimensions:
                continue

            # Skip auxiliary coordinates which are in the list but not
            # in the file
            if ncvar not in nc.variables:
                continue

            # Set dimensions 
            aux_ncdimensions = _ncdimensions(nc.variables[ncvar], cfa)
            dimensions = [ncdim_to_dim[ncdim] for ncdim in aux_ncdimensions
                          if ncdim in ncdim_to_dim]    

            if ncvar in seen_in_file:
                coord = seen_in_file[ncvar].copy()
            else:
                coord = _create_Coordinate(nc, ncvar, attributes, f, cfa=cfa,
                                           dimension=False)
                seen_in_file[ncvar] = coord
            #--- End: if

            # --------------------------------------------------------
            # Turn a ..
            # --------------------------------------------------------
            is_dimension_coordinate = False
            if not dimensions:
                if nc.variables[ncvar].dtype.kind is 'S':
                    # String valued scalar coordinate. Is this CF
                    # complaint? Don't worry about it - it'll get
                    # turned into a 1-d, size 1 auxiliary coordinate
                    # construct, anyway
                    dim = f.domain.new_dimension_identifier()
                    dimensions = [dim]
                else:  
                    # Numeric valued scalar coordinate                           
                    is_dimension_coordinate = True
            #--- End: if

            if is_dimension_coordinate:
                # Insert dimension coordinate
                coord = coord.asdimension(copy=False)
                dim = f.domain.insert_dim_coordinate(coord, copy=False,
                                                     finalize=False)
                f.domain.nc_dimensions[dim]= ncvar
                ncvar_to_key[ncvar] = dim
                seen_in_file[ncvar] = coord
            else:
                # Insert auxiliary coordinate
                aux = f.domain.insert_aux_coordinate(coord, dimensions=dimensions,
                                                     copy=False, finalize=False)
                ncvar_to_key[ncvar] = aux
        #--- End: for

        f.delprop('coordinates')
    #--- End: if

    # Make sure that the coordinates start with no transforms
    for coord in f.domain.itercoordinates(): 
        if hasattr(coord, 'transforms'):
            del coord.transforms
    #--- End: for

    # ----------------------------------------------------------------
    # Add formula_terms to coordinates' Transform attributes
    # ----------------------------------------------------------------
    for key, coord in f.domain.coordinates().iteritems():
        coord_ncvar = coord.ncvar

        if coord_ncvar in formula_terms:
            # This coordinate's formula_terms transform has already
            # been created, so just copy it. (We have to do this
            # because the 'formula_terms' public attribute on the
            # coordinate has been deleted.)
            f.domain.insert_transform(formula_terms[coord_ncvar], coords=(key,)) # wrong

#            if hasattr(coord, 'transforms'):
#                coord.transforms.append(transform_id)
#            else:
#                coord.transforms = [transform_id]
            
            continue
        #-- End: if

        # Still here?
        form_terms = coord.getprop('formula_terms', None)
        if form_terms is None:
            # This coordinate doesn't have a formula_terms attribute
            continue

        # Still here? Then this coordinate has a formula_terms
        # transform that needs creating
        transform = Transform(standard_name=coord.standard_name)
        
        # Add the equation terms and references to their values to to
        # new auxiliary coordinate's transform.
        ft = re.split('\s+|\s*:\s+', form_terms)
        for term, ncvar in zip(ft[0::2], ft[1::2]):

            if ncvar in ncvar_to_key:
                # CASE 1: This variable is a coordinate of the field,
                #         so we point to it from the transform.
                value = ncvar_to_key[ncvar]

            elif 'bounds' not in attributes[ncvar]:
                # CASE 2: This variable is not a coordinate of the
                #         field and it has no bounds, so it goes in to
                #         the transform as an independent field.
                value = {'ncvar': ncvar}
                
            else:
                # CASE 3: This variable is not a coordinate of the
                #         field and it has bounds
                aux_dims = []
                var_ncdimensions = _ncdimensions(nc.variables[ncvar], cfa)
                for ncdim in var_ncdimensions: #nc.variables[ncvar].dimensions:
                    if ncdim not in ncdim_to_dim:
                        aux_dims = None
                        break

                    aux_dims.append(ncdim_to_dim[ncdim])
                #--- End: for

                if aux_dims is None:
                    # CASE 3a: This variable's dimensions are not a
                    #          subset of the field's dimensions.
                    #          Therefore it goes into the transform as
                    #          an independent field with the bounds
                    #          deleted.
                    value = {'ncvar': ncvar}

                else:
                    # CASE 3b: This variable's dimensions are a subset
                    #          of the field's dimensions. Therefore
                    #          add it to the field as an auxiliary
                    #          coordinate and point to it from the
                    #          transform.
                    aux = f.domain.new_auxiliary_identifier()
                    coord = _create_Coordinate(nc, ncvar, attributes, f, cfa=cfa,
                                               dimension=False)
                    if coord.hasprop('formula_terms'):
                        coord.delprop('formula_terms')

                    aux = f.domain.insert_aux_coordinate(coord,
                                                         dimensions=aux_dims,
                                                         key=aux, copy=False,
                                                         finalize=False)

                    ncvar_to_key[ncvar] = aux

#                    aux = 'aux%(auxN)d' % locals()##
#                    auxN += #1
                    
#                    f.domain.dimensions[aux] = aux_dims
#                    f.domain[aux] = _create_Coordinate(nc, ncvar, attributes)
#                    if f.domain[aux].hasprop('formula_terms'):
#                        f.domain[aux].delprop('formula_terms')
                    
                    # Add this variable name and its bounds name to
                    # the set of variables which are not fields. Note
                    # that if it turns out that this variable is also
                    # being used as a field in another transform then
                    # its name will be removed from this set.
                    not_fields.update((ncvar, attributes[ncvar]['bounds']))
                    
                    value = aux
            #--- End: if
                    
            transform[term] = value
        #--- End: for 

        f.domain.insert_transform(transform, coords=(key,), copy=False)

#        if hasattr(coord, 'transforms'):
#            coord.transforms.append(transform_id)
#        else:
#            coord.transforms = [transform_id]

        formula_terms[coord_ncvar] = transform

        if coord.hasprop('formula_terms'):
            coord.delprop('formula_terms')
    #--- End: for

    # ----------------------------------------------------------------
    # Add grid mapping to coordinates' Transform attributes
    # ----------------------------------------------------------------
    grid_mapping = f.getprop('grid_mapping', None)

    if grid_mapping is not None:

        for ncvar in grid_mapping.split():

            if ncvar not in attributes:
                continue

            if 'grid_mapping_name' not in attributes[ncvar]:
                attributes[ncvar]['grid_mapping_name'] = ncvar

            grid_mapping_name = attributes[ncvar]['grid_mapping_name']

          #  transform_id = 'trans%(transN)d' % locals()
          #  transN += 1     

#            f.domain.transforms[transform_id] = Transform(**attributes[ncvar])
 #           f.domain.transforms[transform_id].ncvar = ncvar
#            f.domain.transforms[transform_id].name         = grid_mapping_name
#            f.domain.transforms[transform_id].grid_mapping = True

            transform = Transform(**attributes[ncvar])
            transform.ncvar = ncvar

            transform_id = f.domain.insert_transform(transform, copy=False)

            # Work out which coordinates need to have the transform
            # attached to them
            for key, coord in f.domain.coordinates().iteritems():
                # NOTE: This will need updating for ticket #70

                if (grid_mapping_name == 'latitude_longitude' and
                    coord.islatitude or coord.islongitude):
                    coord.insert_transform(transform, transform_id)
                    continue

                coord_name = coord.identity()

                if ((grid_mapping_name == 'rotated_latitude_longitude' and 
                     coord_name in ('grid_longitude', 'grid_latitude')) or
                    coord_name in ('projection_x_coordinate', 
                                   'projection_y_coordinate')):
                    coord.insert_transform(transform, transform_id)
                    
#                    if transform_id in f.domain.transform_map:
#                        f.domain.transform_map[transform_id].append(key)
#                    else:
#                        f.domain.transform_map[transform_id] = [key]


#                        if hasattr(coord, 'transforms'):
#                            coord.transforms.append(transform_id)
#                        else:
#                            coord.transforms = [transform_id]
#
#                elif grid_mapping_name == 'latitude_longitude':
#                    if coord_name in ('longitude', 'latitude'):
#                        coord.insert_transform(transform, transform_id)
#                    if hasattr(coord, 'transforms'):
#                        coord.transforms.append(transform_id)
#                    else:
#                        coord.transforms = [transform_id]
#
#
#                elif coord_name in ('projection_x_coordinate', 
#                                    'projection_y_coordinate'):
#                    coord.insert_transform(transform, transform_id)
                    #if hasattr(coord, 'transforms'):
                    #    coord.transforms.append(transform_id)
                    #else:
                    #    coord.transforms = [transform_id]
            #--- End: for
        #--- End: for

        f.delprop('grid_mapping')
    #--- End: try

    # ----------------------------------------------------------------
    # Add cell measures to the field
    # ----------------------------------------------------------------
    cell_measures = f.getprop('cell_measures', None)

    if cell_measures is not None:

        # Parse the cell measures attribute
        cell_measures = re.split('\s*(\w+):\s*', cell_measures)
        
        for measure, ncvar in zip(cell_measures[1::2], 
                                  cell_measures[2::2]):

            if ncvar not in attributes:
                continue

            # Set cell measures' dimensions 
            cm_ncdimensions = _ncdimensions(nc.variables[ncvar], cfa)
            dimensions = [ncdim_to_dim[ncdim] for ncdim in cm_ncdimensions]

            if ncvar in seen_in_file:
                # Copy the cell measure as it already exists
                cell = seen_in_file[ncvar].copy()
            else:
                cell = _create_CellMeasure(nc, ncvar, attributes, f, cfa=cfa)
                cell.measure = measure
                seen_in_file[ncvar] = cell
            #--- End: if

            cm = f.domain.insert_cell_measure(cell, dimensions=dimensions,
                                              copy=False, finalize=False)

            ncvar_to_key[ncvar] = cm
        #--- End: for

        f.delprop('cell_measures')
    #--- End: if

    # -----------------------------
    # Add cell methods to the field
    # -----------------------------
    if cell_methods is not None:
        f.cell_methods = cell_methods.netCDF_translation(f)

    # ----------------------------------------------------------------
    # Parse an ancillary_variables string to a list of netCDF variable
    # names, which will get converted to an AncillaryVariables object
    # later. Add these netCDF variable names to the set of all
    # ancillary data variables in the file.
    # ----------------------------------------------------------------
    if hasattr(f, 'ancillary_variables'):
        f.ancillary_variables = f.ancillary_variables.split()
        ancillary_variables.update(f.ancillary_variables)
    #--- End: if

    # Return the finished field
    return f
#--- End: def

def _create_Coordinate(nc, ncvar, attributes, f, cfa=False,
                       dimension=True):
    '''

Create a coordinate variable, including any bounds.

:Parameters:

    nc : netCDF4.Dataset
        The entire netCDF file in a `netCDF4.Dataset` instance.

    ncvar : str
        The netCDF name of the coordinate variable.

    attributes : dict
        Dictionary of the coordinate variable's netCDF attributes.

    f : Field

    cfa : bool, optional
        If True then netCDF file is a CFA file. By default it is
        assumed that the file is not a CFA file.

:Returns:

    out : cf.DimensionCoordinate or cf.AuxiliaryCoordinate
        The new coordinate.

'''
    attrs = {'ncvar': ncvar}

    properties = attributes[ncvar]

    ncbounds = properties.pop('bounds', None)
    if ncbounds is None:
        ncbounds = attributes[ncvar].pop('climatology', None)
        attrs['climatology'] = True

    if dimension:        
        c = DimensionCoordinate(properties=properties, attributes=attrs,
                                copy=False)
    else:
        c = AuxiliaryCoordinate(properties=properties, attributes=attrs,
                                copy=False)

    coord_data = _set_Data(nc, nc.variables[ncvar], f, c, cfa=cfa)

    # ------------------------------------------------------------
    # Add any bounds
    # ------------------------------------------------------------
    if ncbounds is None:
        bounds = None
    else:
        bounds = CoordinateBounds(properties=attributes[ncbounds], copy=False)

        bounds.ncvar = ncbounds

        bounds.insert_data(
            _set_Data(nc, nc.variables[ncbounds], f, bounds, cfa=cfa),
            copy=False)

        # Make sure that the bounds' dimensions are in the same order
        # as their parent's dimensions
        c_ncdims = nc.variables[ncvar].dimensions
        b_ncdims = nc.variables[ncbounds].dimensions
        if c_ncdims != b_ncdims[:-1]:
            axes = [c_ncdims.index(i) for i in b_ncdims[:-1]]
            axes.append(b_ncdims[-1])
            bounds.Data.transpose(axes)
        #--- End: if
    #--- End: if

    c.insert_data(coord_data, bounds=bounds, copy=False, fix_bounds_units=True)

    # ---------------------------------------------------------
    # Return the coordinate
    # ---------------------------------------------------------
    return c
#--- End: def

def _create_CellMeasure(nc, ncvar, attributes, f, cfa=False): #, key=None):
    '''

Create a cell measure variable.

:Parameters:

    nc : netCDF4.Dataset
        The entire netCDF file in a `netCDF4.Dataset` instance.

    ncvar : str
        The netCDF name of the cell measure variable.

    attributes : dict
        Dictionary of the cell measure variable's netCDF attributes.

    f : Field

    cfa : bool, optional
        If True then netCDF file is a CFA file. By default it is
        assumed that the file is not a CFA file.

:Returns:

    out : CellMeasure
        The new cell measure.

'''
    cm       = CellMeasure(attributes[ncvar])
    cm.ncvar = ncvar

#    if 'units' not in attributes[ncvar]:
#        cm.Units = Units()

#    v = nc.variables[ncvar]
#    data = NetCDFFileArray(file=filename, ncvar=ncvar,
#                           dtype=v.dtype, ndim=v.ndim, shape=v.shape,
#                           size=v.size)
#    cm.Data = Data(data=data, units=cm.Units, _FillValue=cm._FillValue)
    cm.Data = _set_Data(nc, nc.variables[ncvar], f, cm, cfa=cfa)

    return cm
#--- End: def

def _ncdimensions(ncvariable, cfa=False):
    '''

Return a list of the netCDF dimension names for a netCDF variable.

:Parameters:

    ncvariable : netCDF4.Variable
    
    cfa : bool, optional
        If True then netCDF file is a CFA file. By default it is
        assumed that the file is not a CFA file.

:Returns:

    out : list
        The list of netCDF dimension names.

**Examples** 

>>> ncdims = _ncdimensions(ncvariable)
>>> ncdims = _ncdimensions(ncvariable, cfa=True)

'''
    ncattrs = ncvariable.ncattrs()
    if (cfa and 
        'cf_role' in ncattrs and 
        ncvariable.getncattr('cf_role') == 'cfa_variable'):
        # NetCDF variable is a CFA variable
        if 'cfa_dimensions' in ncattrs:
            ncdimensions = ncvariable.getncattr('cfa_dimensions').split()
        else:
            ncdimensions = []
    else:
        # NetCDF variable is not a CFA variable
        ncdimensions = list(ncvariable.dimensions)
        cfa = False
      
    # Remove a string-length dimension, if there is one. dch alert
    if (not cfa and 
        ncvariable.dtype.kind == 'S' and
        ncvariable.ndim >= 2 and ncvariable.shape[-1] > 1):
        ncdimensions.pop()
     
    return map(str, ncdimensions)
#--- End: def

def _set_Data(nc, ncvar, f, variable, cfa=False):
    '''

Set the Data attribute of a variable.

:Parameters:

    nc : netCDf4.Dataset

    ncvar : netCDF4.Variable

    f : Field

    variable : Variable

    cfa : bool, optional
        If True then netCDF file is a CFA file. By default it is
        assumed that the file is not a CFA file.

:Returns:

    None

**Examples** 

'''    
    iscfa_variable = variable.getprop('cf_role', None) == 'cfa_variable'

    if cfa and iscfa_variable:

        cfa_data = json_loads(variable.getprop('cfa_array'))

        cfa_data['file']       = f.file
        cfa_data['_FillValue'] = variable.fill_value() #_FillValue
        cfa_data['Units']      = variable.Units      

        base = cfa_data.get('base', None)
        if base is not None:
            cfa_data['base'] = abspath(pathjoin(dirname(f.file), base))

        ncdimensions = variable.getprop('cfa_dimensions', '').split()
        dtype = ncvar.dtype
        if dtype.kind == 'S' and ncdimensions:
            strlen = len(nc.dimensions[ncdimensions[-1]])
            if strlen > 1:
                ncdimensions.pop()
                dtype = numpy.dtype('S%d' % strlen)
        #--- End: if
        cfa_data['dtype']      = dtype

        cfa_data['dimensions'] = ncdimensions

        cfa_data['shape'] = [len(nc.dimensions[ncdim]) 
                             for ncdim in ncdimensions]

        for attrs in cfa_data['Partitions']:

            sformat = attrs.get('subarray', {}).pop('format', 'netCDF')
            if sformat is not None:
                attrs['format'] = sformat

            dtype = attrs.get('subarray', {}).pop('dtype', None)
            if dtype not in (None, 'char'):
                attrs['subarray']['dtype'] = numpy.dtype(dtype)

            units    = attrs.pop('punits', None)
            calendar = attrs.pop('pcalendar', None)
            if units is not None or calendar is not None:
                attrs['Units'] = Units(units, calendar)

            pdimensions = attrs.pop('pdimensions', None)
            if pdimensions is not None:
                attrs['dimensions'] = pdimensions

            pdirections = attrs.pop('directions', None)
            if pdimensions is not None:
                attrs['directions'] = pdirections

            # Location: Change to python indexing (i.e. range does not
            # include the final index)
            for r in attrs['location']:
                r[1] += 1

            # Part: Change to python indexing (i.e. slice range does
            # not include the final index)
            part = attrs.get('part', None)
            if part:
                p = []
                for x in ast_literal_eval(part):
                    if isinstance(x, tuple):
                        if x[2] > 0:
                            p.append(slice(x[0], x[1]+1, x[2]))
                        elif x[1] == 0:
                            p.append(slice(x[0], None, x[2]))
                        else:
                            p.append(slice(x[0], x[1]-1, x[2]))
                    else:
                        p.append(x)
                #--- End: for
                attrs['part'] = p
        #--- End: for

        variable.delprop('cf_role')
        variable.delprop('cfa_array')
        if variable.hasprop('cfa_dimensions'):
            variable.delprop('cfa_dimensions')

        data = Data(loadd=cfa_data)

    else:        

        dtype = ncvar.dtype
        ndim  = ncvar.ndim
        shape = ncvar.shape
        size  = ncvar.size

        if dtype.kind == 'S' and ndim >= 1: #shape[-1] > 1:
            # Has a trailing string-length dimension
            strlen = shape[-1]
            shape = shape[:-1]
            size /= strlen
            ndim -= 1
            dtype = numpy.dtype('S%d' % strlen)
        #--- End: if

        filearray = NetCDFFileArray(file=f.file, ncvar=ncvar._name,
                                    dtype=dtype, ndim=ndim, shape=shape,
                                    size=size, varid=ncvar._varid)
        
        data = Data(data=filearray,
                    units=variable.Units, fill_value=variable.fill_value()) #_FillValue)
    #--- End: if

    return data
#--- End: def

def _is_cfa_private_variable(ncvar, cfa):
    '''

Return True if a netCDF variable is a CFA private variable.

:Parameters:

    ncvar : netCDF4.Variable

    cfa : bool
        If True then netCDF file is a CFA file. By default it is
        assumed that the file is not a CFA file.

:Returns:

    out : bool
        True if *cfa* is True and *ncvar* is a CFA private
        variable. Otherwise False.

**Examples** 

>>> if _is_cfa_private_variable(x, True): print 'Is private CFA'

>>> False == _is_cfa_private_variable(x, False)
True

'''  
    return (cfa and 
            'cf_role' in ncvar.ncattrs() and 
            ncvar.getncattr('cf_role') == 'cfa_private')
#--- End: def
