from .utils import CfDict
from .functions import RTOL, ATOL, equals, equivalent

# ====================================================================
#
# Transform object
#
# ====================================================================

class Transform(CfDict):
    '''

A CF transform construct.

The named parameters and their values of the transformation (i.e. the
transformation's mappings) comprise the object's key-value pairs.

A transformation is equivalent to either a CF-netCDF 'formula_terms'
or 'grid_mapping' property.   DCH DCH DCH

In the 'formula_terms' case, a mapping to a coordinate (as opposed to
another field) uses the coordinate's domain key name as a pointer
rather than a copy of the coordinate itself.

**Examples**

>>> t
<CF Transform: atmosphere_sigma_coordinate>
>>> print t.dump()
atmosphere_sigma_coordinate transform
-------------------------------------
Transform['ps'] = <CF Field: surface_air_pressure(73, 96)>
Transform['ptop'] = 0.05
Transform['sigma'] = 'dim0'

>>> t
<CF Transform: rotated_latitude_longitude>
>>> print t.dump()
rotated_latitude_longitude transform
------------------------------------
Transform['grid_north_pole_latitude'] = 33.67
Transform['grid_north_pole_longitude'] = 190.0

'''

    def __init__(self, *args, **kwargs):
        '''

**Initialization**

:Parameters:

    args, kwargs :
        Keys and values are initialized exactly as for a built-in
        dict. Keys are transform parameter names (such as
        'grid_north_pole_latitude' or 'sigma') with appropriate
        values. Some key/value pairs are treated as special cases:

        * If the key 'standard_name' is specified then it is assumed
          to be the standard name of a dimensionless vertical
          coordinate, and the transform represents a CF-netCDF
          formula_terms attribute. A 'standard_name' key is not
          created, but its value is assigned to the `name` attribute.

        * If the key 'grid_mapping_name' is specified then is assumed
          that the transform represents a CF-netCDF grid mapping of
          the same name. A 'grid_mapping_name' key is created and its
          value is also assigned to the `name` attribute.

**Examples**

>>> t = Transform(grid_north_pole_latitude=38.0, 
...               grid_north_pole_longitude=190.0,
...               grid_mapping_name='rotated_latitude_longitude')

>>> t = Transform(p0=cf.Data(1000, 'hPa'), lev='dim1', 
...               standard_name='atmosphere_ln_pressure_coordinate')

'''
        super(Transform, self).__init__(*args, **kwargs)

        if 'grid_mapping_name' in self:
            self.grid_mapping = True
            self.name = self.pop('grid_mapping_name')

        elif 'standard_name' in self:
            self.grid_mapping = False
            self.name = self.pop('standard_name')
    #--- End: def


    def __hash__(self):
        '''

x.__hash__() <==> hash(x)

:Raises:

    ValueError:
        If the transform is not a grid mapping.

'''
        if not self.isgrid_mapping:
            raise ValueError("Can't hash a formula_terms transform")

        h = sorted(self.items())
        h.append(self.name)

        return hash(tuple(h))
    #--- End: def

    def __repr__(self):
        '''
x.__repr__() <==> repr(x)

''' 
        try:
            return '<CF %s: %s>' % (self.__class__.__name__, self.name)
        except AttributeError:
            return '<CF %s: >' % self.__class__.__name__
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''    
        return 'Transform       : %s' % repr(self)
    #--- End: def

    @property
    def isformula_terms(self):
        '''

True if the transform is a formula_terms.

**Examples**

>>> t
<CF Transform: atmosphere_sigma_coordinate>
>>> t.isformula_terms
True
>>> t
<CF Transform: rotated_latitude_longitude>
>>> t.isformula_terms
False

'''
        return not self.isgrid_mapping
    #--- End: def

    @property
    def isgrid_mapping(self):
        '''

True if the transform is a grid_mapping.

**Examples**

>>> t
<CF Transform: rotated_latitude_longitude>
>>> t.isgrid_mapping
True

>>> t
<CF Transform: atmosphere_sigma_coordinate>
>>> t.isgrid_mapping
False

'''
        return getattr(self, 'grid_mapping', False)
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: name (can't delete)
    # ----------------------------------------------------------------
    @property
    def name(self): 
        '''

The identifying name of the transformation.

**Examples**

>>> t.name
'atmosphere_hybrid_sigma_pressure_coordinate'
>>> t.name = 'rotated_latitude_longitude'

'''        
        return self._name
    #--- End: def
    @name.setter
    def name(self, value):
        self._name = value

    def close(self):
        '''

Close all referenced open data files.

:Returns:

    None

**Examples**

>>> t.close()

'''
        for value in self.itervalues():
            if hasattr(value, 'close'):
                value.close()
    #--- End: def

    def dump(self, level=0, complete=False, domain=None):
        '''

Return a string containing a full description of the transform.

:Parameters:

    level : int, optional

    complete : bool, optional

    domain : cf.Domain, optional

:Returns:

    out : str
        A string containing the description.

**Examples**

>>> print t.dump()

'''          
        indent0 = '    ' * level
        indent1 = '    ' * (level+1)

        try:
            string = ['%sTransform: %s' % (indent0, self.name)]
        except AttributeError:
            string = ['%sTransform: ' % indent0]

#        string.append(''.ljust(len(string[0]), '-'))

        if domain:
            for key in sorted(self.keys()):
                value = self[key]
                if value in domain:
                    string.append("%s%s = %s" % (indent1, key, repr(domain[value])))
                else:
                    if complete and hasattr(value, 'domain'):
                        string.append("%s%s = \n%s" % 
                                      (indent1, key, 
                                       value.dump(level=level+2, complete=False,
                                                  title='Transform field', q='-')))
                    else:
                        string.append("%s%s = %s" % (indent1, key, repr(value)))
        else:
            for key in sorted(self.keys()):
                value = self[key]
                if complete and hasattr(value, 'domain'):
                    string.append("%s%s = \n%s" % 
                                  (indent1, key, 
                                   value.dump(level=level+2, complete=False,
                                              title='Transform field', q='-')))
                else:
                    string.append("%s%s = %s" % (indent1, key, repr(value)))
                
        return '\n'.join(string)
    #--- End: def

    def equals(self, other, rtol=None, atol=None, traceback=False):
        '''

True if two instances are equal, False otherwise.

:Parameters:

    other : 
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns: 

    out : bool
        Whether or not the two instances are equal.

**Examples**

'''
        if self is other:
            return True
        
        # Check that each instance is the same type
        if self.__class__ != other.__class__:
            if traceback:
                print("%s: Different types: %s, %s" %
                      (self.__class__.__name__,
                       self.__class__.__name__,
                       other.__class__.__name__))
            return False
        #--- End: if
   
        # Check the name
        attr = 'name'
        x = getattr(self, attr, None)
        y = getattr(other, attr, None)
        if not equals(x, y, rtol=rtol, atol=atol, traceback=traceback):
            if traceback:
                print("%s: Different '%s' attributes: %s, %s" %
                      (self.__class__.__name__, attr, x, y))
            return False
        #--- End: if
                
        if rtol is None:
            rtol = RTOL()
        if atol is None:
            atol = ATOL()

#        # Check that the keys are equal
#        if set(self) != set(other):
#            if traceback:
#                print("%s: Different terms: %s" %
#                      (self.__class__.__name__,
#                       set(self).symmetric_difference(other)))
#            return False
#        #--- End: if

        # Check that the key values are equal.
        #
        # If the values for a particular key are both pointers to
        # coordinates, then they are considered equal.
        for term, value in other.iteritems():
            if (term not in self and 
                not isinstance(value, basestring) and 
                not (value.isscalar and value.first_datum == 0.0)):
                if traceback:
                    print("%s: Unequal term: %s" % 
                          (self.__class__.__name__, term))
                return False
        #--- End: for

        for term, value in self.iteritems():
            value1 = other[term]

            if (term not in other and
                not isinstance(value, basestring) and 
                not (value.isscalar and value.first_datum == 0.0)):
                if traceback:
                    print("%s: Unequal term: %s" %
                          (self.__class__.__name__, term))
                return False
            #--- End: if
            
            if isinstance(value, basestring) and isinstance(value1, basestring):
                # Both values are pointers to coordinates
                continue

            if not equals(value, value1, rtol=rtol, atol=atol,
                          traceback=traceback):
                if traceback:
                    print("%s: Different '%s' values: %s, %s" %
                          (self.__class__.__name__, term,
                           repr(value), repr(other[term])))
                return False
        #--- End: for
                   
        # Still here?
        return True
    #--- End: def

    def equivalent(self, other, rtol=None, atol=None,
                   traceback=False):
        '''

True if two transforms are logically equal, False otherwise.

:Parameters:

    other : cf.Transform
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `cf.ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `cf.RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns:

    out : bool
        Whether or not the two objects are equivalent.

**Examples**

>>>

'''
        for term, value in other.iteritems():
            if (term not in other and
                not (isinstance(value, basestring) or 
                     (value.isscalar and value.first_datum == 0.0))):                    
                if traceback:
                    print("%s: Unequal term: %s" %
                          (self.__class__.__name__, term))
                return False
        #--- End: for

        for term, value in self.iteritems():
            if (term not in other and 
                not (isinstance(value, basestring) or 
                     (value.isscalar and value.first_datum == 0.0))):                    
                if traceback:
                    print("%s: Unequal term: %s" %
                          (self.__class__.__name__, term))
                return False
            #--- End: if

            value1 = other[term]
            
            if isinstance(value, basestring) and isinstance(value1, basestring):
                # Both values are pointers to coordinates
                continue
           
            if isinstance(value, basestring) or isinstance(value1, basestring):
                return False # dch add traceback

            if not equivalent(value, value1, rtol=rtol, atol=atol,
                              traceback=traceback):
                return False # dch add traceback

#            if isinstance(value, Field):
#                if not value.equivalent_domain(value1, rtol=rtol, atol=atol,
#                                               traceback=traceback):
#                    return False # dch add traceback
#                if not value.equivalent_data(value1, rtol=rtol, atol=atol):
#                    return False # dch add traceback
#                
#            else:
#                # value is a Data object
#                if not value.equivalent(value1, rtol=rtol, atol=atol):
#                    return False # dch add traceback

        #--- End: for

        # Still here?
        return True
    #--- End: def

#--- End: class
