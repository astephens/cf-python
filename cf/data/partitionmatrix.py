import numpy

from numpy import array       as numpy_array
from numpy import asscalar    as numpy_asscalar
from numpy import ndenumerate as numpy_ndenumerate
from numpy import empty       as numpy_empty
from numpy import expand_dims as numpy_expand_dims
from numpy import rollaxis    as numpy_rollaxis
from numpy import squeeze     as numpy_squeeze

from copy      import deepcopy
from itertools import izip

from operator import mul

from ..functions import CHUNKSIZE, FM_THRESHOLD, parse_indices, subspace_array

from .partition import Partition

# ====================================================================
#
# PartitionMatrix object
#
# ====================================================================

_empty_matrix = numpy_empty((), dtype=object)

class PartitionMatrix(object):
    '''

A hyperrectangular partition matrix of a master data array.

Each of elements (called partitions) span all or part of exactly one
sub-array of the master data array.

A partition matrix is indexable in a similar way to numpy array apart
from two important extensions to the numpy indexing functionality:

* Size 1 dimensions are always removed (note that this is different to
  the behaviour of a `cf.Data` object).

* When advanced indexing is used on more than one dimension, the
  advanced indices work independently.

'''

    def __init__(self, matrix, dimensions):
        '''

**Initialization**

:Parameters:

    matrix : numpy.ndarray
        An array of Partition objects.

    dimensions : list
        The identities of the partition dimensions of the partition
        array. If the partition matrix is a scalar array then it is an
        empty list. The list is not copied.

**Examples**

>>> pm = PartitionMatrix(
...          numpy.array(Partition(location    = [(0, 1), (2, 4)],
...                                shape       = [1, 2],
...                                _dimensions = ['dim2', 'dim0'],
...                                _directions = {'dim2': True, 'dim0': False},
...                                Units       = cf.Units('m'),
...                                part        = [],
...                                data        = numpy.array([[5, 6], [7, 8]])),
...                      dtype=object),
...          dimensions=[])

'''
        self.matrix = matrix
        self.dimensions = dimensions
    #--- End: def

    def __deepcopy__(self, memo):
        '''

Used if copy.deepcopy is called on the variable.

''' 
        return self.copy()
    #--- End: def

    def __getitem__(self, indices):
        '''
x.__getitem__(indices) <==> x[indices]

Normal numpy basic and advanced indexing is supported, but size 1
dimensions are always removed and a partition rather than a partition
matrix is returned if the indexed array has size 1.

Returns either a partition or a partition matrix.

**Examples**

>>> pm.shape
(5, 3)
>>> pm[0, 1]
<cf.data.partition.Partition at 0x1934c80>
>>> pm[:, 1]
<CF PartitionMatrix: 1 partition dimensions>
>>> pm[:, 1].shape
(5,)
>>> pm[1:4, slice(2, 0, -1)].shape
(3, 2)

>>> pm.shape
()
>>> pm[()]
<cf.data.partition.Partition at 0x1934c80>
>>> pm[...]
<cf.data.partition.Partition at 0x1934c80>

'''
        out = self.matrix[indices]

        if isinstance(out, Partition):
            return out

        if out.size == 1:
            return self.matrix.item()
        
        dimensions = [dim for dim, n in izip(self.dimensions, out.shape) 
                      if n != 1] 
        
        return type(self)(numpy_squeeze(out), dimensions)
    #--- End: def

    def __repr__(self):
        '''
x.__repr__() <==> repr(x)

'''
        return '<CF %s: %d partition dimensions>' % (self.__class__.__name__, 
                                                     len(self.dimensions))
    #--- End: def

    def __setitem__(self, indices, value):
        '''

x.__setitem__(indices, y) <==> x[indices]=y

Indices must be an integer, a slice object or a tuple. If a slice
object is given then the value being assigned must be an iterable. If
a tuple of integers (or slices equivalent to an integer) is given then
there must be one index per partition matrix dimension.

**Examples**

>>> pm.shape
(3,)
>>> pm[2] = p1
>>> pm[:] = [p1, p2, p3]

>>> pm.shape
(2, 3)
>>> pm[0, 2] = p1


>>> pm.shape
()
>>> pm[()] = p1
>>> pm[...] = p1

'''
        self.matrix[indices] = value
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''
        return str(self.matrix)
        out = []
        for partition in self.matrix.flat:
            out.append(str(partition))

        return '\n'.join(out)
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: ndim (can't set or delete)
    # ----------------------------------------------------------------
    @property
    def ndim(self):
        '''

The number of partition dimensions in the partition matrix.

Not to be confused with the number of dimensions of the master data
array.

**Examples**

>>> pm.shape
(8, 4)
>>> pm.ndim
2

>>> pm.shape
()
>>> pm.ndim
0

'''       
        return self.matrix.ndim
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: shape (can't set or delete)
    # ----------------------------------------------------------------
    @property
    def shape(self):
        '''

List of the partition matrix's dimension sizes.

Not to be confused with the sizes of the master data array's
dimensions.

**Examples**

>>> pm.ndim
2
>>> pm.size
32
>>> pm.shape
(8, 4)

>>> pm.ndim
0
>>> pm.shape
()

'''
        return self.matrix.shape
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: size (can't set or delete)
    # ----------------------------------------------------------------
    @property
    def size(self):
        '''

The number of partitions in the partition matrix.

Not to be confused with the number of elements in the master data
array.

**Examples**

>>> pm.shape
(8, 4)
>>> pm.size
32

>>> pm.shape
()
>>> pm.size
1

'''
        return self.matrix.size
    #--- End: def

    def add_partitions(self,
                       adimensions,
                       adirections,
                       extra_boundaries, 
                       pdim):
        '''
:Parameters:

    adimensions : list
        The ordered dimension names of the master array.

    adirections : dict or bool
        The ordered dimension names of the master array.

    extra_boundaries : list of int
        The extra boundaries to be added.

    pdim : str
        The name of the dimension to have the extra boundaries added.

'''     
        def _update_p(matrix, location, master_index, 
                      part, dim2index, adirections):
            '''
            '''      
            for partition in matrix.flat:
                partition.location = partition.location[:]
                partition.shape    = partition.shape[:]

                partition.location[master_index] = location
                partition.shape[master_index]    = shape                       
#                partition.part = partition.new_part(part, 
#                                                    dim2index, 
#                                                    adirections)
                partition.new_part(part, dim2index, adirections)
            #--- End: for

            return matrix
        #--- End: def

        
              
        # If no extra boundaries have been provided, just return
        # without doing anything
        if not extra_boundaries:
            return

        master_index = adimensions.index(pdim)
        index        = self.dimensions.index(pdim)

        # Find the position of the extra-boundaries dimension in the
        # list of master array dimensions
        extra_boundaries = extra_boundaries[:]

        # Create the dim2index dictionary required by
        # Partition.new_part
        dim2index = {}
        for i, dim in enumerate(adimensions):
            dim2index[dim] = i

        matrix = self.matrix
        shape = matrix.shape

        # Initialize the new partition matrix
        new_shape = list(shape)
        new_shape[index] += len(extra_boundaries)
        new_matrix = numpy_empty(new_shape, dtype=object)

        part        = [slice(None)] * len(adimensions)
        indices     = [slice(None)] * matrix.ndim
        new_indices = indices[:]
        new_indices[index] = 0

        # Find the first extra boundary
        x = extra_boundaries.pop(0)

        for i in xrange(shape[index]):
            indices[index] = i
            sub_matrix = matrix[indices]
            (r0, r1) = sub_matrix.flat.next().location[master_index]

# Could do better, perhaps, by assigning in blocks
            if not r0 < x < r1:
                new_matrix[new_indices] = sub_matrix
                new_indices[index] += 1
                continue
                
            # Find the new extent of the original partition(s)
            location = (r0, x)
            shape    = x - r0
            part[master_index]  = slice(0, shape)
            
            # Create new partition(s) in place of the original ones(s)
            # and set the location, shape and part attributes
            new_matrix[new_indices] = _update_p(deepcopy(sub_matrix),
                                                location, master_index,
                                                part, dim2index, adirections)
            new_indices[index] += 1

            while x < r1:
                # Find the extent of the new partition(s)
                if not extra_boundaries:
                    # There are no more new boundaries, so the new
                    # partition(s) run to the end of the original
                    # partition(s) in which they lie.
                    location1 = r1
                else:
                    # There are more new boundaries, so this
                    # new partition runs either to the next
                    # new boundary or to the end of the
                    # original partition, which comes first.
                    location1 = min(extra_boundaries[0], r1)
                #--- End: if
                location = (x, location1)
                shape    = location1 - x
                offset   = x - r0
                part[master_index]  = slice(offset, offset + shape)

                # Create the new partition(s) and set the
                # location, shape and part attributes 
                new_matrix[new_indices] = _update_p(deepcopy(sub_matrix),
                                                    location, master_index,
                                                    part, dim2index, adirections)
                new_indices[index] += 1

                if not extra_boundaries:
                    # ------------------------------------------------
                    # There are no more extra boundaries, so we can
                    # return now
                    # ------------------------------------------------
                    new_indices[index] = slice(new_indices[index], None)
                    indices[index]     = slice(i+1, None)  

                    new_matrix[new_indices] = matrix[indices]
                    self.matrix = new_matrix

                    return
                #--- End: if
                                
                # Move on to the next new boundary
                x = extra_boundaries.pop(0)
            #--- End: while                                   
        #--- End: for

        self.matrix = new_matrix
    #--- End: def

    def change_dimension_names(self, dim_name_map):
        '''

dim_name_map should be a dictionary which maps each dimension names in
self.dimensions to its new dimension name. E.g. {'dim0':'dim1',
'dim1':'dim0'}

'''
        # Partition dimensions
        self.dimensions = [dim_name_map[dim] for dim in self.dimensions]

        # Partitions. (Note that a partition may have dimensions which
        # are not in self.dimensions and that these must also be in
        # dim_name_map.)
        for partition in self.matrix.flat:
            partition.change_dimension_names(dim_name_map)
    #--- End: def

    def copy(self):
        '''

Return a deep copy.

Equivalent to ``copy.deepcopy(pm)``.

:Returns:

    out :
        The deep copy.

**Examples**

>>> pm.copy()

''' 
        # ------------------------------------------------------------
        # NOTE: 15 May 2013. It is necesary to treat
        #       self.matrix.ndim==0 as a special case since there is a
        #       bug (feature?) in numpy <= v1.7 (at least):
        #       http://numpy-discussion.10968.n7.nabble.com/bug-in-deepcopy-of-rank-zero-arrays-td33705.html
        # ------------------------------------------------------------
        matrix = self.matrix

        if not matrix.ndim:
            new_matrix = _empty_matrix.copy() #numpy_empty((), dtype=object)
            new_matrix[()] = matrix.item().copy()
            return PartitionMatrix(new_matrix , [])

        new_matrix = numpy.empty(matrix.size, dtype=object)
        new_matrix[...] = [partition.copy() for partition in matrix.flat]
        new_matrix.resize(matrix.shape)


        return PartitionMatrix(new_matrix, self.dimensions[:])
#        return PartitionMatrix(deepcopy(matrix), self.dimensions[:])
    #--- End: def

    def expand_dims(self, pdim):
        '''

Insert a new size 1 partition dimension in place.

The new partition dimension is inserted at position 0, i.e. it becomes
the new slowest varying dimension.

:Parameters:

    pdim : str
        The name of the new partition dimension.

:Returns:

    None

**Examples**

>>> pm.dimensions
['dim0', 'dim1']
>>> pm.expand_dims('dim2')
>>> pm.dimensions
['dim2', 'dim0', 'dim1']

'''
        self.matrix = numpy_expand_dims(self.matrix, 0)
        self.dimensions.insert(0, pdim)              
    #--- End: def
        
    @property
    def flat(self):
        '''

A flat iterator over the partitions in the partition matrix.

**Examples**

>>> pm.shape
[2, 2]
>>> for partition in pm.flat:
...     print repr(partition.Units)
...
<CF Units: m s-1>
<CF Units: km hr-1>
<CF Units: miles day-1>
<CF Units: mm minute-1>

'''
        return self.matrix.flat
    #--- End: def

#    def info(self, attr):
#        '''
#
#'''
#        if not self:
#            return []
#
#        out = []
#        if self._is_1d: #isinstance(self[0], Partition):
#            for partition in self:
#                out.append(getattr(partition, attr, None))
#        else:
#            for nested_partition in self:
#                # Recursive call
#                out.append(nested_partition.info(attr))
#        #--- End: if
#
#        return out
#    #--- End: def

    def ndenumerate(self):
        '''

Return an iterator yielding pairs of array indices and values.

:Returns:

    out : numpy.ndenumerate

'''
        return numpy_ndenumerate(self.matrix)
    #--- End: def

    def partition_boundaries(self, adimensions):
        '''
        
        '''            
        boundaries = {}
        
        matrix = self.matrix
        zeros = [0] * self.ndim
        for i, dim in enumerate(self.dimensions):
            indices    = zeros[:]            
            indices[i] = slice(None)
            b = [partition.location[i][0]
                 for partition in matrix[indices].flat]
            b.append(partition.location[i][1])
            boundaries[dim] = b
        #--- End: for
            
        return boundaries
    #--- End: def

#        zeros = [0] * len(self.dimensions)
#
#        for j, pdim in enumerate(self.dimensions):
#
#            first = self
#
#            indices    = zeros[:]
#            indices[j] = True
#
#            while indices:
#                index = indices.pop(0)
#                if not index:
#                    first = first[index]      # index is 0
#                else:
#                    break
#            #--- End: while
#
#            temp = []
#            for element in first:
#                for index in indices:
#                    element = element[index]  # index is 0
#                temp.append(element)
#            #--- End: for
#            first = temp
#            # 'first' should now be list of Partition objects
#
##            i = self.dimensions.index(pdim)
#            i = adimensions.index(pdim)
#
#            b = [partition.location[i][1] for partition in first]
#            b.insert(0, 0)
#
#            boundaries[pdim] = b
#        #--- End: for
#
#        return boundaries
    #--- End: def

    def rollaxis(self, axis, start=0):
        '''

Roll the specified partition dimension backwards,in place until it
lies in a given position.

Note that this does not change the master data array.

:Parameters:

    axis : int 
        The axis to roll backwards. The positions of the other axes do
        not change relative to one another.

    start : int, optional 
        The axis is rolled until it lies before this position.  The
        default, 0, results in a "complete" roll.

:Returns:

    None

**Examples**

>>> pm.rollaxis(2)
>>> pm.rollaxis(2, start=1)

'''
        if axis != start:
            self.matrix = numpy_rollaxis(self.matrix, axis, start=start)
            #            self._list = numpy_rollaxis(numpy_array(self._list, dtype=object), 
            #                                        axis, start=start
            #                                        ).tolist()
            self.dimensions.insert(start, self.dimensions.pop(axis))
    #--- End: def
             
    def set_location_map(self, adimensions):
        '''
        
Recalculate the `cf.Partition.location` attribute of each partition in
the partition matrix in place.

:Parameters:

    adimensions : list
    
**Examples**

>>> pm.set_location_map(['dim1', 'dim0'])
>>> pm.set_location_map([])

'''
        matrix = self.matrix
        
        shape = matrix.shape
        dimensions = self.dimensions

        slice_None = slice(None)
        
        indices = [slice_None] * matrix.ndim
        
        for n, dim in enumerate(adimensions):           
            if dim in dimensions:
                m = dimensions.index(dim)
                start = 0
                for i in xrange(shape[m]):
                    indices[m] = i
                    flat = matrix[indices].flat
                    partition = flat.next()
                    stop = start + partition.shape[n]
                    location = (start, stop)

                    partition.location = partition.location[:]
            
                    partition.location[n] = location
                    for partition in flat:   

                        partition.location = partition.location[:]

                        partition.location[n] = location
                    #--- End: for
                    start = stop
                #--- End: for
                indices[m] = slice_None
            else:
                flat = matrix.flat
                partition = flat.next()
                location = (0, partition.shape[n])

                partition.location = partition.location[:]

                partition.location[n] = location
                for partition in flat:
                
                    partition.location = partition.location[:]

                    partition.location[n] = location
    #--- End: def

    def squeeze(self):
        '''

Remove all size 1 partition dimensions in place.

Note that this does not change the master data array.

:Returns:

    None

**Examples**

>>> pm.dimensions
['dim0', 'dim1', 'dim2']
>>> pm._list
[[[<cf.partition.Partition object at 0x145daa0>,
   <cf.partition.Partition object at 0x145db90>]],
 [[<cf.partition.Partition object at 0x145dc08>,
   <cf.partition.Partition object at 0x145dd70>]]]
>>> pm.squeeze()
>>> pm.dimensions
['dim0', 'dim2']
>>> pm._list
[[<cf.partition.Partition object at 0x145daa0>,
  <cf.partition.Partition object at 0x145db90>],
 [<cf.partition.Partition object at 0x145dc08>,
  <cf.partition.Partition object at 0x145dd70>]]

'''     
        self.dimensions = [dim for size, dim in izip(self.matrix.shape,
                                                     self.dimensions) 
                           if size > 1]

        self.matrix = self.matrix.squeeze()
    #--- End: def

    def transpose(self, axes):
        '''

Permute the partition dimensions of the partition matrix in place.

Note that this does not change the master data array.

:Parameters:

    axes : sequence of ints 
        Permute the axes according to the values given.

:Returns:

    None

**Examples**

>>> pm.transpose((2,0,1))

'''
        dimensions = self.dimensions
        self.dimensions = [dimensions[i] for i in axes]
        self.matrix = self.matrix.transpose(axes)

#        dimensions = self.dimensions
#        if axes != range(len(dimensions)):
#            self._list = numpy_transpose(numpy_array(self._list, dtype=object), 
#                                         axes=axes
#                                         ).tolist()
#            self.dimensions = [dimensions[i] for i in axes]
    #--- End: def
             
#--- End: class
