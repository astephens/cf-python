import netCDF4

from numpy import array         as numpy_array
from numpy import cos           as numpy_cos
from numpy import dtype         as numpy_dtype
from numpy import empty         as numpy_empty
from numpy import ndarray       as numpy_ndarray
from numpy import ndindex       as numpy_ndindex
from numpy import ones          as numpy_ones
from numpy import result_type   as numpy_result_type
from numpy import sin           as numpy_sin
from numpy import take          as numpy_take
from numpy import unravel_index as numpy_unravel_index
from numpy import zeros         as numpy_zeros

from numpy.ma import array        as numpy_ma_array
from numpy.ma import empty        as numpy_ma_empty
from numpy.ma import getmask      as numpy_ma_getmask
from numpy.ma import getmaskarray as numpy_ma_getmaskarray
from numpy.ma import is_masked    as numpy_ma_is_masked
from numpy.ma import isMA         as numpy_ma_isMA
from numpy.ma import masked       as numpy_ma_masked
from numpy.ma import MaskedArray  as numpy_ma_MaskedArray
from numpy.ma import nomask       as numpy_ma_nomask
from numpy.ma import ones         as numpy_ma_ones
from numpy.ma import take         as numpy_ma_take
from numpy.ma import where        as numpy_ma_where
from numpy.ma import zeros        as numpy_ma_zeros

from operator  import mul
from math      import ceil as math_ceil
from sys       import byteorder as sys_byteorder
from itertools import izip
from itertools import product as itertools_product

from ..netcdf.filearray import NetCDFFileArray
from ..pp.filearray     import PPFileArray

from ..units     import Units
from ..constants import masked as cf_masked
from ..functions import (CHUNKSIZE, FM_THRESHOLD, RTOL, ATOL, FREE_MEMORY,
                         parse_indices, _allclose, abspath,
                         pathjoin, hash_array)

from .partition       import Partition
from .partitionmatrix import PartitionMatrix

netCDF4_netcdftime_utime = netCDF4.netcdftime.utime

_arse = {0: []}
_arse4 = {0: True}
#for i in xrange(1, 2):
#    dimensions = [str(n) for n in xrange(i)]
#    arse[i] = dimensions
#    arse4[i] = dict.fromkeys(dimensions, True)
##--- End: for

_xxx = numpy_empty((), dtype=object)

_units_None    = Units()
_units_1       = Units('1')
_units_radians = Units('radians')
             
# ====================================================================
#
# Data object
#
# ====================================================================

class Data(object):
    '''

An N-dimensional data array with units and masked values.

* Contains an N-dimensional, indexable and broadcastable array with
  many similarities to a numpy array.

* Contains the units of the array elements.

* Supports masked arrays, regardless of whether or not it was
  initialized with a masked array.

* Uses Large Amounts of Massive Arrays (LAMA) functionality to store
  and operate on aggregated (master) data arrays and arrays which are
  larger then the available memory.

**Indexing**

A data array is indexable in a similar way to numpy array

>>> d.shape
(12, 19, 73, 96)
>>> d[...].shape
(12, 19, 73, 96)
>>> d[slice(0, 12), 10:0:-2, :, :].shape
(12, 5, 73, 96)

There are two important extensions to the numpy indexing
functionality:

* Size 1 dimensions are never removed.

  An integer index i takes the i-th element but does not reduce the
  rank of the output array by one:

  >>> d.shape
  (12, 19, 73, 96)
  >>> d[0, ...].shape
  (1, 19, 73, 96)
  >>> d[:, 3, slice(10, 0, -2), 95].shape
  (12, 1, 5, 1)

* When advanced indexing is used on more than one dimension, the
  advanced indices work independently.

  When more than one dimension's slice is a 1-d boolean sequence or
  1-d sequence of integers, then these indices work independently
  along each dimension (similar to the way vector subscripts work in
  Fortran), rather than by their elements:

  >>> d.shape
  (12, 19, 73, 96)
  >>> d[0, :, [0,1], [0,1,2]].shape
  (1, 19, 2, 3)

**Miscellaneous**

Data objects are picklable.

Data objects are hashable, but note that, since Data objects are
mutable, their hash values may change if created at different times.

'''
   
    def __init__(self, data=None, units=None, fill_value=None, hardmask=True,
                 chunk=True, loadd=None):
        '''

**Initialization**

:Parameters:

    data : array-like, optional
        The data for the array.

    units : str or Units, optional
        The units of the data. By default the array elements are
        dimensionless.

    fill_value : optional 
        The fill value of the data. By default, or if None, the numpy
        fill value appropriate to the array's data type will be used.

    hardmask : bool, optional
        If False then the mask is soft. By default the mask is hard.

    chunk : bool, optional
        If True then the data array will be partitioned if it is
        larger than the chunk size. By default the data array will be
        stored in a single partition.

**Examples**

>>> d = cf.Data(5)
>>> d = cf.Data([1,2,3], units='K')
>>> import numpy   
>>> d = cf.Data(numpy.arange(10).reshape(2,5), units=cf.Units('m/s'), fill_value=-999)
>>> d = cf.Data(tuple('fly'))

'''      
        self._hardmask = hardmask
        self._dtype    = None
        
        if loadd is not None:
            self.loadd(loadd)
            return
        #--- End: if

        # Set units if they're not already a Units object
        units = Units(units)

        self._Units      = units
        self._fill_value = fill_value
 
        if data is None:
            return

        if isinstance(data, (list, int, float, tuple, bool, long, basestring)):
            data = numpy_array(data)
        
        shape = data.shape
        ndim  = data.ndim

#        try:
#            dimensions = _arse[ndim]
#            directions = _arse4[ndim]
#        except KeyError:            
#            dimensions = [str(i) for i in xrange(ndim)]
#            directions = dict.fromkeys(dimensions, True)
#            _arse[ndim] = dimensions
#            _arse4[ndim] = directions
#        #--- End: try

        dimensions = [str(i) for i in xrange(ndim)]

        if not ndim:
            # Scalar data
            dimensions = []
            directions = True
        else:
            # N (>1) dimensional data
            dimensions = [str(i) for i in xrange(ndim)]
            directions = dict.fromkeys(dimensions, True)
        #--- End: if

        matrix = _xxx.copy()

        matrix[()] = Partition(location    = [(0, n) for n in shape],
                               shape       = list(shape),
                               _dimensions = dimensions,
                               _directions = directions,
                               Units       = units,
                               subarray    = data,
                               part        = [])
        self.partitions = PartitionMatrix(matrix, [])

        self.dtype = data.dtype

        self._ndim  = ndim
        self._shape = shape
        self._size  = data.size
       
        self.dimensions = dimensions
        '''

An ordered list of identities for dimension of the data array.

**Examples**

>>> d.ndim
3
>>> d.dimensions
['dim2', 'dim0', 'dim3'

>>> d.ndim
0
>>> d.dimensions
[]

'''

        self.directions = directions
        '''

**Examples**

>>> d.dimensions
['dim2', 'dim0', 'dim3']
>>> d.directions
{'dim0': True, 'dim0': False, 'dim3': True}

>>> d.dimensions
[]
>>> d.directions
False

'''
        if chunk:
            self.chunk()
    #--- End: def

    def __deepcopy__(self, memo):
        '''

Used if copy.deepcopy is called on the variable.

''' 
        return self.copy()
    #--- End: def

#    def __getstate__(self):
#        '''
#
#Called when pickling.
#
#:Parameters:
#
#    None
#
#:Returns:
#
#    out : dict
#        A dictionary of the instance's attributes
#
#**Examples**
#
#'''
#        return dict([(attr, getattr(self, attr))
#                     for attr in self.__slots__ if hasattr(self, attr)])
#    #--- End: def        

#    def __setstate__(self, odict):
#        '''
#
#Called when unpickling.
#
#:Parameters:
#
#    odict : dict
#        The output from the instance's `__getstate__` method.
#
#:Returns:
#
#    None
#
#'''
#        for attr, value in odict.iteritems():
#            setattr(self, attr, value)
#    #--- End: def

    def __hash__(self):
        '''

Return the hash value of the data array.

Note that generating the hash temporarily realizes the entire array in
memory, which may not be possible for large arrays.

The hash value is dependent on the data type, shape of the data
array. If the array is a masked array then the hash value is
independent of the fill value and of data array values underlying any
masked elements.

The hash value is not guaranteed to be portable across versions of
Python, numpy and cf.

Note that for `cf.Data` object ``d``, ``hash(d)`` is equivalent to
``cf.hash_array(d.array)``.

:Returns:

    out : int
        The hash value

**Examples**

>>> print d.array
[[0 1 2 3]]
>>> d.hash()
-8125230271916303273
>>> d[1, 0] = numpy.ma.masked
>>> print d.array
[[0 -- 2 3]]
>>> hash(d)
791917586613573563
>>> d.hardmask = False
>>> d[0, 1] = 999
>>> d[0, 1] = numpy.ma.masked
>>> d.hash()
791917586613573563
>>> d.squeeze()
>>> print d.array
[0 -- 2 3]
>>> hash(d)
-7007538450787927902
>>> d.dtype = float
>>> print d.array
[0.0 -- 2.0 3.0]
>>> hash(d)
-4816859207969696442

'''
        return hash_array(self.unsafe_array)
    #--- End: def

    def __nonzero__(self):
        '''
Truth value testing and the built-in operation ``bool``

x.__nonzero__() <==> x != 0 <==> x.varray != 0

'''
        if self._size == 1:
            return self.varray.__nonzero__()

        raise ValueError(
"The truth value of Data with more than one element is ambiguous. Use d.any() or d.all()")
    #--- End: def

    def __repr__(self):
        '''
x.__repr__() <==> repr(x)

'''
        return '<CF %s: %s>' % (self.__class__.__name__, str(self))
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''
        calendar = getattr(self.Units, 'calendar', 'standard')
        if self.Units and self.Units != _units_1:
            units = self.Units.units
            if hasattr(self.Units, 'calendar'):
                units += ' %s calendar' % calendar                
        else:
            units = ''
        
        ndim = self._ndim
        size = self._size
        
        open_brackets = '[' * ndim
        close_brackets = ']' * ndim
        
        first = self.first_datum
        last  = self.last_datum
        fill_value = self._fill_value

        if first == fill_value:
            first = '--'
        elif self.dtype.char == 'S':
            first = "'%s'" % first

        if last == fill_value:
            last = '--'
        elif self.dtype.char == 'S':
            last = "'%s'" % last
                

        if self.Units.isreftime:
            try:
                utime = netCDF4_netcdftime_utime(self.Units.units, calendar=calendar)
            except:
                pass
            else:
                first2, last2 = first, last
                if first2 != '--':
                    try:
                        first2 = utime.num2date(first2)
                    except:
                        first2 = None
                if last != '--':
                    try:
                        last2 = utime.num2date(last2)
                    except:
                        last2 = None
                if first2 is not None and last2 is not None:
                    first, last = first2, last2
                    units = '%s calendar' % calendar                
        #--- End: if            

        if size >= 3:
            x = '%s%s, ..., %s%s %s' % (open_brackets, first,
                                        last, close_brackets,
                                        units)
        elif size == 1:
            x = '%s%s%s %s' % (open_brackets, first,
                               close_brackets, units)
        else:
            x = '%s%s, %s%s %s' % (open_brackets, first,
                                   last, close_brackets,
                                   units)
            
        return x 
    #--- End: def

    def __getitem__(self, indices):
        '''
x.__getitem__(indices) <==> x[indices]

'''
        if self._size == 1 or indices is Ellipsis:
            return self.copy()

#        dim2position = dict([(dim, i) for i, dim in enumerate(dimensions)])

        indices, reverse_dimensions = _parse_indices(self, indices)

        # If it's a scalar, then just copy it. ??? dch trapped in _parse_indices??
#        if not self._ndim and not indices:
#            return self.copy()
        
        dimensions = self.dimensions
        directions = self.directions

        new_shape = tuple(map(_size_of_index, indices, self._shape))
        new_size  = long(reduce(mul, new_shape, 1))

        new = Data.__new__(Data)

        new._shape      = new_shape
        new._size       = new_size
        new._ndim       = self._ndim
        new._fill_value = self._fill_value
        new._hardmask   = self._hardmask
        new.dimensions  = dimensions #[:]
        new.directions  = directions #self._copy_directions(directions)
        new.Units       = self.Units #.copy()
        new.dtype       = self.dtype

        dim2position = {}
        for i, dim in enumerate(dimensions):
            dim2position[dim] = i

        partitions = self.partitions

        new_partitions = PartitionMatrix(_overlapping_partitions(partitions, 
                                                                 indices, 
                                                                 dim2position,
                                                                 directions),
                                         partitions.dimensions[:])

#        new_shape = tuple(map(_size_of_index, indices, self._shape))
#        new_size  = long(reduce(mul, new_shape, 1)) #new._shape, 1))

        if new_size != self._size:
            new_partitions.set_location_map(dimensions)

        new.partitions = new_partitions
#        new._shape     = new_shape #tuple(map(_size_of_index, indices, self._shape))
#        new._size      = new_size
#        new._ndim      = self._ndim
#        new.dimensions = dimensions[:]
#        new.directions = self._copy_directions(directions)
#        new.Units      = self.Units.copy()
#        new._FillValue = self._FillValue
#        new.dtype      = self.dtype
#        new._hardmask  = self._hardmask

        # Reverse dimensions with flipped slices
        if reverse_dimensions:
            new.flip(reverse_dimensions)

        # Remove size 1 partition dimensions
        new_partitions.squeeze()

        return new
    #--- End: def

    def __setitem__(self, indices, value):
        '''
x.__setitem__(indices, y) <==> x[indices]=y

'''            
        def _mirror_index(index, size):
            '''

Note that it assumed that the step of the input slice is 1.

:Parameters:

    index : slice

    size : int

:Returns:

    out : slice

**Examples**

>>> s = slice(2, 6)
>>> t = _mirror_index(slice(2, 6), 8)
>>> t
slice(5, 1, -1)
>>> range(8)[s]
[2, 3, 4, 5]
>>> range(8)[t]
[5, 4, 3, 2]
>>> range(7, -1, -1)[t]
[2, 3, 4, 5]

'''
            start, stop, step = index.indices(size)
            size -= 1
            start = size - start
            stop  = size - stop
            if stop < 0:
                stop = None
                
            return slice(start, stop, -1)
        #--- End: def

        conform_args = self.conform_args()

        # ------------------------------------------------------------
        # parse the indices
        # ------------------------------------------------------------
        indices, reverse_dimensions = _parse_indices(self, indices)

        scalar_value = False
        if value is cf_masked: #numpy_ma_masked:
            scalar_value  = True
        else:
            if not isinstance(value, Data):
                # Convert to the value to a Data object
                value = type(self)(value, units=self.Units)
            else:
                value = value.copy()
                if value.Units and value.Units.equivalent(self.Units):
                    value.Units = self.Units
                elif not value.Units:
                    value.override_units(self.Units)
                else:
                    raise ValueError(" Some bad units %s, %s" %
                                     (repr(self.Units), repr(value.Units)))
                #--- End: if

            if value._size == 1:
                scalar_value = True
                value = value.first_datum
        #--- End: if

        if scalar_value:
            # --------------------------------------------------------
            # The value is logically scalar
            # --------------------------------------------------------           
            for partition in self.partitions.matrix.flat:
#                p_indices, shape = _partition_overlaps(partition, indices)
                p_indices, shape = partition.overlaps(indices)
                if not p_indices:
                    # This partition does not overlap the indices
                    continue
                
                array = partition.dataarray(**conform_args)
                array[p_indices] = value
                partition.close()
            #--- End: for
            
            return
        #--- End: if

        # ------------------------------------------------------------
        # Still here? Then the value is not logically scalar.
        # ------------------------------------------------------------
        self_shape  = self._shape
        value_shape = value._shape
        
        shape0 = map(_size_of_index, indices, self_shape)
        self_ndim  = self._ndim
        value_ndim = value._ndim
        align_offset = self_ndim - value_ndim
        if align_offset >= 0:
            # self has more dimensions than other
            shape0   = shape0[align_offset:]
            shape1   = value_shape
            ellipsis = None 
            
            reverse_dimensions = [i - align_offset
                                  for i in reverse_dimensions
                                  if i >= align_offset]                
        else:
            # value has more dimensions than self
            v_align_offset = -align_offset
            if value_shape[:v_align_offset] != [1] * v_align_offset:
                # Can only allow value to have more dimensions then
                # self if the extra dimensions all have size 1.
                raise ValueError("Can't broadcast shape %s across shape %s" %
                                 (repr(value_shape), repr(self_shape)))
            
            shape1       = value_shape[v_align_offset:]
            ellipsis     = Ellipsis
            align_offset = 0
        #--- End: if

        # Find out which of the dimensions of value are to be
        # broadcast, and those which are not. Note that, as in numpy,
        # it is not allowed for a dimension in value to be larger than
        # a size 1 dimension in self
        base_value_indices       = []
        non_broadcast_dimensions = []

        for i, (a, b) in enumerate(izip(shape0, shape1)):
            if b == 1:
                base_value_indices.append(slice(None))
            elif a == b and b != 1:
                base_value_indices.append(None)
                non_broadcast_dimensions.append(i)
            else:
                raise ValueError("Can't broadcast shape %s across shape %s" %
                                 (repr(shape1), repr(shape0)))
        #--- End: for

        previous_location = ((-1,),) * self_ndim
        start             = [0]      * value_ndim

        save = conform_args['save']

        value.to_memory()

        for partition in self.partitions.matrix.flat:
#            p_indices, shape = _partition_overlaps(partition, indices)        
            p_indices, shape = partition.overlaps(indices)        

            if not p_indices:
                # This partition does not overlap the indices          
                continue

            # Find which elements of value apply to this partition
            value_indices = base_value_indices[:]
       
            for i in non_broadcast_dimensions:
                j                  = i + align_offset
                location           = partition.location[j][0]
                reference_location = previous_location[j][0]

                if location > reference_location:
                    stop             = start[i] + shape[j]
                    value_indices[i] = slice(start[i], stop)
                    start[i]         = stop

                elif location == reference_location:
                    value_indices[i] = previous_slice[i]

                elif location < reference_location:
                    stop             = shape[j]
                    value_indices[i] = slice(0, stop)
                    start[i]         = stop
            #--- End: for

            previous_location = partition.location
            previous_slice    = value_indices[:]
          
            for i in reverse_dimensions:
                value_indices[i] = _mirror_index(value_indices[i], shape1[i])

            if ellipsis:
                value_indices.insert(0, ellipsis)

            v = value[tuple(value_indices)].varray
                    
            if not save:
                v = v.copy()

            # Update the partition's data
            array = partition.dataarray(**conform_args)
            if numpy_ma_isMA(array):
                if not conform_args['hardmask']:
                    # Soft mask
                    array[p_indices] = v
                else:
                    # Hard mask
                    array.soften_mask()
                
                    array = array[p_indices]
                
                    if array.mask is numpy_ma_nomask:
                        mask = True
                    else:
                        mask = ~numpy_ma_getmaskarray(array)
                
                    both_unmasked = mask & ~numpy_ma_getmaskarray(v)
                
                    partition.subarray[p_indices] = numpy_ma_where(both_unmasked,
                                                                   v, array)
                
                    partition.subarray.harden_mask()
            else:
                array[p_indices] = v
            #--- End: if

            partition.close()
        #--- End: For
            
    #--- End: def
    
    def dumpd(self, dimensions=None):
        '''

Serialize the data array as a dictionary loosely following the CFA
framework.

:Parameters:

    dimensions : sequence of str, optional

:Returns:

    out : dict

**Examples**

>>> x = d.dumpd()
>>> x = d.dumpd(dimensions=['time', 'latitude', 'longitutude'])

''' 
        if dimensions is not None:
            dimensions = list(dimensions)

            dim_name_map = {}
            for dim, ndim in izip(self.dimensions, dimensions):
                dim_name_map[dim] = ndim
                                
#            dim_name_map = dict([(dim, ndim)
#                                 for dim, ndim in izip(self.dimensions,
#                                                       dimensions)])

            xdata = self.copy()
            xdata.change_dimension_names(dim_name_map)
 
        else:
            dimensions = self.dimensions[:]
            dim_name_map = None
            xdata = self

#        dim_name_map = dict([(dim, ndim) for dim, ndim in izip(self.dimensions,
#                                                               dimensions)])
            
        data_isscalar = not xdata.ndim

        directions = xdata.directions
        cfa_data = {
            'dimensions'  : dimensions,
            'directions'  : xdata._copy_directions(directions),
            '_FillValue'  : xdata._fill_value, #_FillValue,
            'dtype'       : xdata.dtype,
            'shape'       : xdata._shape,
            'Units'       : xdata.Units,        
            'pmshape'     : xdata.pmshape,
            'pmdimensions': xdata.pmdimensions,
            'Partitions'  : [],
            }

#        if dim_name_map:
#            cfa_data['pmdimensions'] = [dim_name_map[dim]
#                                       for dim in xdata.pmdimensions]
#        else:
#            cfa_data['pmdimensions'] = xdata.pmdimensions[:]

#        if not dimensions or not dim_name_map:
#            directions = copy(xdata.directions)
#        else:
#            directions = dict([(dim_name_map[dim], xdata.directions[dim])
##                               for dim in xdata.dimensions])
#        #--- End: if
#        cfa_data['directions'] = directions

#        for index in iterindices(xdata.partitions):
        for index, partition in xdata.partitions.ndenumerate():

            attrs = {}

#            partition = xdata.partitions[index]
#            partition = self.partitions[index].copy()
#            if dim_name_map:
#                partition.change_dimension_names(dim_name_map)

            p_dimensions = partition._dimensions
            p_directions = partition._directions
            p_part       = partition.part                       
            p_Units      = partition.Units
            p_dtype      = partition.subarray.dtype
            
            # Location in partition matrix
            if index:
                attrs['index'] = index

#            if not partition.subarray_in_external_file:
#                # This partition's sub-array is either in memory or is
#                # in an internal temporary file
#                attrs['partition'] = partition
#                cfa_data['Partitions'].append(attrs)
#                continue
# 
#                # Still here? Then this partition's sub-array is in an
#                # external file (such as a netCDF or PP file).

            # Sub-array location
            attrs['location'] = partition.location
            
            # Sub-array part
            if p_part:
                attrs['part'] = p_part

            # Sub-array dimensions
            if p_dimensions != dimensions:
                attrs['dimensions'] = p_dimensions              

            # Sub-array units
            if p_Units != xdata.Units:
                attrs['Units'] = p_Units

            # Sub-array directions
            if p_directions != directions:
                if not p_dimensions or data_isscalar:
                    # The sub-array is a scalar or the data array is scalar
                    attrs['directions'] = p_directions                                  
                elif (not set(p_directions).issubset(directions) or
                      not all(p_directions[dim] == directions[dim]
                              for dim in p_dimensions)):
                    attrs['directions'] = p_directions
            #--- End: if

            if isinstance(partition.subarray, NetCDFFileArray):
                # ----------------------------------------------------
                # NetCDF File Array
                # ----------------------------------------------------
                attrs['format'] = 'netCDF'

                d = {}
                for attr in ('file', 'shape', 'ncvar'):
                    d[attr] = getattr(partition.subarray, attr)
                attrs['subarray'] = d         

#                attrs['subarray'] = dict([(attr, getattr(partition.subarray, attr)) 
#                                          for attr in ('file', 'shape', 'ncvar')])
                
                if p_dtype != xdata.dtype:
                    attrs['subarray']['dtype'] = p_dtype

                varid = getattr(partition.subarray, 'varid', None)
                if varid is not None:
                    attrs['subarray']['varid'] = varid
                      
            elif isinstance(partition.subarray, PPFileArray):
                # ----------------------------------------------------
                # PP File Array
                # ----------------------------------------------------
                attrs['format'] = 'PP'
            
                d = {}
                for attr in ('file', 'shape', 'file_offset', 'lbpack',
                             'scale_factor', 'add_offset'):
                    value = getattr(partition.subarray, attr, None)
                    if value is not None:
                        d[attr] = value
                #--- End: for
                attrs['subarray'] = d         


#                attrs['subarray'] = dict([(attr, getattr(partition.subarray, attr)) 
#                                          for attr in ('file', 'shape', 
#                                                       'file_offset', 'lbpack',
#                                                       'scale_factor', 'add_offset')
#                                          if hasattr(partition.subarray, attr)]) 
                
                if p_dtype != xdata.dtype:
                    attrs['subarray']['dtype'] = p_dtype
                      
                if (partition.subarray.dtype.byteorder == '<' or
                    partition.subarray.dtype.byteorder == '=' and
                    sys_byteorder == 'little'):
                    attrs['subarray']['endian'] = 'little'

                fill_value = getattr(partition.subarray, '_FillValue', None)
                if fill_value is not None:
                    attrs['subarray']['_FillValue'] = fill_value

                      
            else:
                attrs['subarray'] = partition.subarray   
            #--- End: if
                 
            cfa_data['Partitions'].append(attrs)            
        #--- End: for

        return cfa_data
    #--- End: def
        
    def loadd(self, x):
        '''

Deserialize the dictionary, updating the master data array in place.

:Parameters:

:Returns:

    None

**Examples**

'''
        dimensions = list(x['dimensions'])
        directions = self._copy_directions(x.get('directions', True))
        units      = x['Units'] #.copy()
        shape      = tuple(x['shape'])
        dtype      = x['dtype']

        self.dimensions  = dimensions
        self.directions  = directions
        self.Units       = units
        self._fill_value = x['_FillValue']
        self._dtype      = dtype
        self._shape      = shape

        self._ndim = len(shape)
        self._size = long(reduce(mul, shape, 1))
        
        filename = x.get('file', None)
        if filename is not None:
            filename = abspath(filename)

        base = x.get('base', None)
        if base is not None:
            base = abspath(base)

        # ------------------------------------------------------------
        # Initialize an empty partition array
        # ------------------------------------------------------------
#        partition_array = empty(x.get('pmshape', []), 
#                                dimensions=x.get('pmdimensions', []))
        partition_matrix = PartitionMatrix(
            numpy_empty(x.get('pmshape', ()), dtype=object),
            x.get('pmdimensions', [])
            )
        
        # ------------------------------------------------------------
        # Fill the partition array with partitions
        # ------------------------------------------------------------
        for attrs in x['Partitions']:

            index = attrs.get('index', ())
            if len(index) == 1:
                index = index[0]
#            elif not index:
#                index = 0
            else:
                index = tuple(index)

            partition = attrs.get('partition', None)
            if partition:
                partition_matrix[index] = partition.copy()
                continue

#            partition = Partition(
#                _dimensions = attrs.get('dimensions', dimensions[:]),
#                location    = attrs['location'],
#                Units       = attrs.get('Units', units.copy()),
#                part        = attrs.get('part', []),
#                )
            partition = Partition(
                _dimensions = attrs.get('dimensions', dimensions),
                location    = attrs['location'],
                Units       = attrs.get('Units', units),
                part        = attrs.get('part', []),
                )
            
            format = attrs['format']

#            # Set the 'dimensions' attribute
#            partition._dimensions = attrs.get('dimensions', dimensions[:])
#
#            # Set the 'Units' attribute
#            partition.Units = attrs.get('Units', units.copy())
#
#            # Set the 'part' attribute
#            partition.part = attrs.get('part', [])

            # Set the 'directions' attribute
            d = attrs.get('directions', None)
            if d is not None:
                partition._directions = d
            else:
                # Take the default directions
                if not partition._dimensions:
                    if not dimensions:
                        partition._directions = directions
                    else:
                        partition._directions = True
                else:
                    d = {}
                    for dim in partition._dimensions:
                        d[dim] = directions.get(dim, True)
                    partition._directions = d
#                    partition._directions = dict([(dim, directions.get(dim, True))
#                                                 for dim in partition._dimensions])
            #--- End: if

            # Set the 'subarray' attribute 
            kwargs = attrs['subarray'].copy()
            kwargs['shape'] = tuple(kwargs['shape'])
            if format in ('netCDF', 'PP'):
                kwargs['ndim'] = len(kwargs['shape'])
                kwargs['size'] = long(reduce(mul, kwargs['shape'], 1))

                kwargs.setdefault('dtype', dtype)

                if 'file' in kwargs:
                    f = kwargs['file']
                    if f == '':
                        kwargs['file'] = filename
                    else:
                        if base is not None:
                            f = pathjoin(base, f)
                        kwargs['file'] = abspath(f)
                else:
                    kwargs['file'] = filename

                if format == 'netCDF':                
                    partition.subarray = NetCDFFileArray(**kwargs)
                elif format == 'PP':
                                        
                    endian = kwargs.get('endian', 'big')
                    kwargs['dtype'] = kwargs['dtype'].newbyteorder(endian)

                    partition.subarray = PPFileArray(**kwargs)
            else:
                raise TypeError(
                    "Don't know how to load sub-array from file format '%s'" %
                    format)
            #--- End: if

            # Put the partition into the partition array 
            partition_matrix[index] = partition
        #--- End: for

        # Save the partition array
        self.partitions = partition_matrix

        self.chunk()
    #--- End: def

    def chunk(self, chunksize=None):
        '''

Partition the data array

:Parameters:

    chunksize : int, optional
        The 

:Returns:

    None

**Examples**

>>> d.chunk()
>>> d.chunk(100000)

'''
        if not chunksize:
            # Set the default chunk size
            chunksize = CHUNKSIZE()

        # Define the factor which, when multiplied by the size of a
        # partition, determines how many chunks are in the partition.
        factor = (self.dtype.itemsize + 1.0)/chunksize

        if self._size*factor <= 1:
            # Don't do any partitioning if the data array is already
            # smaller than the chunk size
            return

        # Initialize the dictionary relating each dimension to new
        # partition boundaries for that dimension
#        d = dict([(dim, set()) for dim in self.dimensions])
        dimensions = self.dimensions
        d = {} #dict_fromkeys(dimensions, set())

        # ------------------------------------------------------------
        # Find any new partition boundaries for each dimension
        # ------------------------------------------------------------
        for partition in self.partitions.matrix.flat:
            
            n_chunks = int(partition.size*factor + 0.5)
            
            if n_chunks <= 1:
                continue
            
            extra_boundaries = []
            chunk_dims       = []
            
            for size, location, dim in izip(partition.shape, 
                                            partition.location,
                                            dimensions):
                if size == 1 or n_chunks <= 1:
                    continue
                
                if size <= n_chunks:
                    d[dim] = range(location[0]+1, location[1])

                    n_chunks = int(math_ceil(n_chunks/float(size)))

                else:
                    step = int(size/n_chunks)
                    d[dim] = range(location[0]+step, location[1], step)
                    
                    break
                #--- End: if
            #--- End: for                 
        #--- End: for

        # ------------------------------------------------------------
        # Create any new partition boundaries for each dimension
        # ------------------------------------------------------------
        for dim in dimensions[::-1]: #, extra_bounds in d.iteritems():
            extra_bounds = d.get(dim, None)
#            extra_bounds = d[dim]

            if not extra_bounds:
                continue
            
            if dim not in self.partitions.dimensions:
                # Create a new partition dimension
                self.partitions.expand_dims(dim)

            # Create the new partitions
            self.add_partitions(sorted(extra_bounds), dim)
        #--- End: for       
    #--- End: def
    
    def _combined_units(self, other, method):
        '''

:Parameters:

    other : cf.Data

    method : str
    
:Returns:

    out : cf.Units

**Examples**

>>> d._combined_units(e, '__sub__')

'''
        self_units  = self.Units
        other_units = other.Units

        if not self_units and not other_units:
            return self_units

        if not other_units or not self_units:
            raise ValueError("Can't combine %s with %s" %
                             (repr(self_units), repr(other_units)))

        method_type  = method[-5:-2]

        if method_type in ('mul', 'div'):
            return getattr(self_units, method)(other_units)
          
        if method_type in ('sub', 'add'):
          if self_units.equivalent(other_units):
              other.Units = self_units
#              if self_units.isreftime and other_units.isreftime:
#                  return getattr(self_units, method)(other_units)
#              else:
              return self_units #.copy()
          else:
              if self_units.isreftime and other_units.istime:
                  other.Units = Units(self_units._units) # ~ DCH 
                  return self_units #.copy()
              else:
                  # Raise an exception for an illegal combination of
                  # units
                  getattr(self_units, method)(other_units)
        #--- End: if

        if method_type in ('_eq', '_ne', '_lt', '_le', '_gt', '_ge'):
            if self_units.equivalent(other_units):
                other.Units = self_units
                return _units_None
            else: 
                raise ValueError("Can't compare %s to %s" %
                                 (repr(self_units), repr(other_units)))
        #--- End: if

        if method_type in ('and', '_or', 'xor', 'ior'):
            if self_units.equivalent(other_units):
                other.Units = self_units
                return self_units #.copy()
            else: 
                raise ValueError("Can't compare %s to %s" %
                                 (repr(self_units), repr(other_units)))
        #--- End: if

        if method_type == 'pow':
            if other._size > 1:
                raise ValueError(
"Can't raise %s to the power of an array with more than one element" %
self.__class__.__name__)

            if other_units.equivalent(_units_1):
                other.Units = _units_1
                return self_units ** other.first_datum #array.item()                    
            else:
                # Raise an exception for an illegal combination of
                # units
                getattr(self_units, method)(other_units)
        #--- End: if

        raise ValueError("doo bee doo")
    #--- End: def

    def _binary_operation(self, other, method):
        '''

Implement binary arithmetic and comparison operations with the numpy
broadcasting rules.

It is intended to be called by the binary arithmetic and comparison
methods, such as __sub__() and __lt__().

:Parameters:

    other : object
        The object on the right hand side of the operator.

    method : str
        The binary arithmetic or comparison method name (such as
        "__imul__" or "__ge__").

:Returns:

    new : Data
        A new Data object, or if the operation was in place, the same
        Data object.

**Examples**

>>> d = cf.Data([0, 1, 2, 3])
>>> e = cf.Data([1, 1, 3, 4])

>>> f = d._binary_operation(e, '__add__')
>>> print f.array
[1 2 5 7]

>>> e = d._binary_operation(e, '__lt__')
>>> print e.array
[ True False  True  True]

>>> d._binary_operation(2, '__imul__')
>>> print d.array
[0 2 4 6]

'''
        inplace      = method[2] == 'i'
        method_type  = method[-5:-2]

        if isinstance(other, self.__class__):
            # --------------------------------------------------------
            # other is Data object
            # --------------------------------------------------------
            other = other.copy()            
            result_Units = self._combined_units(other, method)

        elif isinstance(other, (float, int, long)):
            # --------------------------------------------------------
            # other is a number
            # --------------------------------------------------------
            if method_type == 'pow':
                result_Units = self.Units ** other
            elif method_type in ('_eq', '_ne', '_lt', '_le', '_gt', '_ge'):
                result_Units = Units()              
            else:
                result_Units = self.Units #.copy()

            other = type(self)(other)
            
        elif isinstance(other, numpy_ndarray):
            # --------------------------------------------------------
            # other is a numpy array
            # --------------------------------------------------------
            if method_type == 'pow':
                if other.size > 1:
                    raise ValueError(
"Can't raise %s to the power of an array with more than one element" %
self.__class__.__name__)

                result_Units = self.Units ** other.item()
            elif method_type in ('_eq', '_ne', '_lt', '_le', '_gt', '_ge'):
                result_Units = Units()
            else:
                result_Units = self.Units #.copy()

            other = type(self)(other)
   
        elif isinstance(other, (list, tuple)):
            # --------------------------------------------------------
            #
            # --------------------------------------------------------
            if method_type == 'pow':
                if len(other) > 1:
                    raise ValueError(
"Can't raise %s to the power of a sequence with more than one element" %
self.__class__.__name__)

                result_Units = self.Units ** other.varray.item()
            elif method_type in ('_eq', '_ne', '_lt', '_le', '_gt', '_ge'):
                result_Units = Units()
            else:
                result_Units = self.Units #.copy()

            other = type(self)(other)
   
        else:
            # --------------------------------------------------------
            # other is something else
            # --------------------------------------------------------
            return NotImplemented
        #--- End: if

        # ------------------------------------------------------------
        # Bring other into memory, if appropriate.
        # ------------------------------------------------------------
        other.to_memory()

        copy_directions = self._copy_directions

        # ------------------------------------------------------------
        # Find which dimensions need to be broadcast in one or other
        # of the arrays.
        #
        # For each common dimension, the 'broadcast_indices' list will
        # have a value of None if there is no broadcasting required
        # (i.e. the two arrays have the same size along that
        # dimension) or a value of slice(None) if broadcasting is
        # required (i.e. the two arrays have the different sizes along
        # that dimension and one of the sizes is 1).
        #
        # For example, if c.shape is [7,1,6,1,5] and d.shape is
        # [6,4,1] then broadcast_indices will be
        # [None,slice(None),slice(None)].
        #
        # The indices to d which correspond to a partition of c, are
        # the relevant subset of partition.indices updated with the
        # non None elements of the broadcast_indices list.
        #
        # In the above example, if a partition of c has
        # partition.indices of
        # (slice(0,3),slice(0,1),slice(2,4),slice(0,1),slice(0,5)),
        # the relevant subset of these is partition.indices[2:] and
        # the corresponding indices to d are
        # (slice(2,4),slice(None),slice(None))
        # ------------------------------------------------------------
        align_offset = self._ndim - other._ndim
        if align_offset >= 0:
            # self has more dimensions than other
            shape0         = self._shape[align_offset:]
            shape1         = other._shape
            ellipsis       = None
            new_shape      = self._shape[:align_offset]
            new_dimensions = self.dimensions #[:]
            new_directions = copy_directions(self.directions)
        else:
            # other has more dimensions than self
            align_offset   = -align_offset
            shape0         = self._shape
            shape1         = other._shape[align_offset:]
            ellipsis       = (Ellipsis,)
            new_shape      = other._shape[:align_offset]
            new_dimensions = [i for i in xrange(align_offset)] + self.dimensions[:]

            if not self._ndim: #self.isscalar:
                new_directions = {}
            else:
                new_directions = self.directions.copy()

            for dim in other.dimensions[:align_offset]:
                new_directions[dim] = other.directions[dim]

            align_offset  = 0
        #--- End: if

        broadcast_indices = []
        for a, b in izip(shape0, shape1):
            if a == b:
                new_shape += (a,)
                broadcast_indices.append(None)
                continue
            
            if a > 1 and b == 1:
                new_shape += (a,)
            elif b > 1 and a == 1:
                new_shape += (b,)
            else:
                raise ValueError("Can't broadcast shape %s against shape %s" %
                                 (repr(other._shape), repr(self._shape)))
            
            broadcast_indices.append(slice(None))
        #--- End: for

        # ------------------------------------------------------------
        # Create a Data object which just contains the metadata for
        # the result. If we're doing a binary arithmetic operation
        # then result will get filled with data and returned. If we're
        # an augmented arithmetic assignment then we'll use result's
        # metadata to update self.
        # ------------------------------------------------------------
        new_ndim = len(new_shape)

        if new_shape != self._shape:
            set_location_map = True
            dummy_location   = [None] * new_ndim
        else:
            set_location_map = False

        if not set_location_map:
            new_size = long(reduce(mul, new_shape, 1))
        else:
            new_size = self._size

        result              = self.copy()
#        result._FillValueP  = self._FillValue # dch - isn't this done in the above copy?
#        result.pmdimensions = self.pmdimensions[:] # dch - isn't this done in the above copy?
        result._shape       = new_shape
        result._ndim        = new_ndim 
        result._size        = new_size 
        result.dimensions   = new_dimensions
        result.directions   = new_directions

        # ------------------------------------------------------------
        # Set flags to control whether or not the data of result and
        # self should be kept in memory
        # ------------------------------------------------------------
        if method_type in ('_eq', '_ne', '_lt', '_le', '_gt', '_ge'):
            result_dtype = numpy_dtype(bool)
        else:
            result_dtype = numpy_result_type(self.dtype, other.dtype)

        save_result = result.save_to_disk(result_dtype.itemsize)

        if not inplace:
            # When doing a binary arithmetic operation (like
            # result=self+2), we need to decide whether or not to keep
            # self's data in memory.
            revert_to_file = True
            save_self      = self.save_to_disk(self.dtype.itemsize)
        else:
            # When doing an augmented arithmetic assignment (like
            # self+=2), we don't need to keep self's original data in
            # memory.
            revert_to_file = False
            save_self      = False
        #--- End: if

#        dimensions     = self.dimensions
#        direction = self.direction
#        units     = self.Units

        conform_args = self.conform_args(save=save_self,
                                         revert_to_file=revert_to_file)

# Think about dtype, here.

        for partition_r, partition_s in izip(result.partitions.matrix.flat,
                                             self.partitions.matrix.flat):
            
            indices = tuple([                
                    (index 
                     if not broadcast_index else
                     broadcast_index) 
                    for index, broadcast_index in izip(partition_s.indices[align_offset:],
                                                       broadcast_indices)
                    ])
            
            # If other has more dimensions than self then we must
            # prepend an Ellipsis object to the indices
            if ellipsis:
                indices = ellipsis + indices

            array0 = partition_s.dataarray(**conform_args)
            # POSSIBLE ISSUE HERE: array1 could be much larger
            # than the chunk size.
            array1 = other[indices].varray

            if not inplace:
                partition = partition_r
                partition.update_inplace_from(partition_s)

            else:
                partition = partition_s

            # --------------------------------------------------------
            # Do the binary operation on this partition's data
            # --------------------------------------------------------
            partition.subarray = getattr(array0, method)(array1)

            partition.Units       = result_Units
            partition._dimensions = new_dimensions #[:]
            partition._directions = new_directions #copy_directions(new_directions)

            if set_location_map:
                partition.location = dummy_location #[:]
                partition.shape    = list(array0.shape)
            #--- End: if

            partition.close(save_result)            

            if not inplace:
                partition_s.close() 
        #--- End: for

        if not inplace:
            result.Units = result_Units #.copy()
            result.dtype = result_dtype

            if set_location_map:
                result.partitions.set_location_map(result.dimensions)

            return result
        else:
            # Update the metadata for the new master array in place
            self.Units      = result_Units #.copy()
            self.dtype      = result_dtype
            self._shape     = new_shape
            self._ndim      = new_ndim
            self._size      = new_size
            self.dimensions = new_dimensions
            self.directions = new_directions
                
            if set_location_map:
                self.partitions.set_location_map(new_dimensions)

            return self
    #--- End: def

#    def _set_location_map(self):
#        '''
#
#Recalculate each partition's `~Partition.location` attribute in the
#partition matrix in place.
#
#**Examples**
#
#>>> d._set_location_map()
#
#'''
#        self.partitions.set_location_map(self.dimensions)
#    #--- End: def

    def _extend_pm(self, other, reversed=False):
        '''
     
:Parameters:

    other : cf.Data

    reversed : bool

:Returns:

    None

**Examples**

'''
        partitions = self.partitions
        matrix = partitions.matrix

        partitions2 = other.partitions
        matrix2 = partitions2.matrix

        if not matrix.ndim:
            new_shape = [1]
            shape0 = 1
        else:
            new_shape = list(matrix.shape)
            shape0 = new_shape[0]
       
        if matrix2.size == 1:
            new_shape[0] += 1
        else:
            new_shape[0] += matrix2.shape[0]

        new_matrix = numpy_empty(new_shape, dtype=object)

        new_matrix[:shape0] = matrix
        if not reversed:
            new_matrix[shape0:] = matrix2
        else:
            new_matrix[:shape0-1:-1] = matrix2
         
        partitions.matrix = new_matrix
    #--- End: def

    def _prepend_pm(self, other, reversed=False):
        '''

:Parameters:

:Returns:

    None

**Examples**

'''
        partitions = self.partitions
        matrix = partitions.matrix

        partitions2 = other.partitions
        matrix2 = partitions2.matrix

        if not matrix.ndim:
            new_shape = [1]
        else:
            new_shape  = list(matrix.shape)

        if matrix2.size == 1:
            other_shape0 = 1
        else:
            other_shape0 = matrix2.shape[0]

        new_shape[0] += other_shape0
        
        new_matrix = numpy_empty(new_shape, dtype=object)

        if not reversed:
            new_matrix[:other_shape0] = matrix2
        else:
            new_matrix[other_shape0-1::-1] = matrix2

        new_matrix[other_shape0:] = matrix
           
        partitions.matrix = new_matrix
    #--- End: def

    def _insert_data(self, other, extend, reversed,
                     PDim, PDim_direction, dim_name_map):
        '''

'''
        data0 = self
        data1 = other
            
        # ------------------------------------------------------------
        # 1. Change the dimension names in data1 to match those in the
        #    the domain of data0.
        # ------------------------------------------------------------
        data1.change_dimension_names(dim_name_map)

        # ------------------------------------------------------------
        # 2. Insert (size 1) dimensions into data0 which are present
        #    in data1 but not in data0
        #
        # Note that the dimensions to be inserted into data0 will
        # always have size 1 in data1 (because the two domains are
        # aggregatable).
        #
        # Note that after this process data1.dimensions will be a
        # subset of data0.dimensions (because the two domains are
        # aggregatable and so have the same rank).
        # ------------------------------------------------------------
        dimensions0 = data0.dimensions
        directions1 = data1.directions
        for dim1 in data1.dimensions[::-1]:
            if dim1 not in dimensions0:
                data0.expand_dims(0, dim1, directions1[dim1])
        #--- End: for
    
        # ------------------------------------------------------------
        # 3. If the Aggregating Dimension is missing from data0 then
        #    insert it with size 1 in the slowest varying position.
        # 
        # Note that after this process data1.dimensions will be a
        # subset of data0.dimensions
        # ------------------------------------------------------------
        if PDim not in data0.dimensions:
            data0.expand_dims(0, PDim, PDim_direction)

        # ------------------------------------------------------------
        # 4. Permute the dimensions of data1 to match data0.
        #
        # For example: If data0.dimensions=['dim0','dim1','dim2'] and
        #              data1.dimensions=['dim2','dim0'] to start with
        #              then data1.dimensions will become
        #              ['dim0','dim2']
        #
        # Note that by now, data1.dimensions is a subset of
        # data0.dimensions (this was was ensured by steps 2 and 3).
        # ------------------------------------------------------------
        dimensions1 = data1.dimensions
        axes   = [dimensions1.index(dim)
                  for dim in data0.dimensions if dim in dimensions1] 
        data1.transpose(axes)

        # ------------------------------------------------------------
        # 5. Insert (size 1) dimensions into data1 which are present
        #    in data0 but not in data1.
        #
        # For example: If data0.dimensions=['dim0','dim1','dim2'] and
        #              data1.dimensions=['dim0','dim2'] to start with
        #              then data1.dimensions will become
        #              ['dim0','dim1','dim2']
        #
        # Note that after this process data0 and data1 will span the
        # same dimensions.
        #
        # Note that the orders of dimensions in data0 and data1 have
        # to be the same (so that their partitions' locations
        # conform).
        #
        # Note that after this process the Aggregating Dimension will
        # definitely be in both data0 and data1.
        # ------------------------------------------------------------
        directions0 = data0.directions
        dimensions1 = data1.dimensions
        for i, dim in enumerate(data0.dimensions):
            if dim not in dimensions1:
                data1.expand_dims(i, dim, directions0)
        #--- End: for

        # ------------------------------------------------------------
        # 6. Make sure that the partition dimensions of data0 are the
        #    same as data1 (although, for now, they may have different
        #    orders).
        #
        # Note that this may involve adding new partition dimensions
        # to either or both of data0 and data1.
        #            
        # Note that if the Aggregating Dimension needs to be added it
        # is inserted as the outer (slowest varying) dimension to
        # reduce the likelihood of having to transpose (expensively)
        # the partition array.
        # ------------------------------------------------------------
        for f, g in izip((data0, data1), 
                         (data1, data0)):

            g_pmdimensions = g.partitions.dimensions[:]
            try:
                g_pmdimensions.remove(PDim)
            except ValueError:
                pass
    
            f_pmdimensions = f.partitions.dimensions[:]
            for pdim in g_pmdimensions[::-1]:
                if pdim not in f_pmdimensions:
                    f.expand_partitions_dims(pdim)

            if PDim not in f.partitions.dimensions:
                f.expand_partition_dims(PDim)
        #--- End: for

        # ------------------------------------------------------------
        # 7. Permute the partition dimensions of the partition array
        #    of data0 so that the Aggregating Dimension is the
        #    outermost (slowest varying) dimension.
        # ------------------------------------------------------------
        axis = data0.partitions.dimensions.index(PDim)
        data0.partitions.rollaxis(axis, 0)
            
        # ------------------------------------------------------------
        # 8. Permute the partition dimensions of the partition array
        #    of data1 to match data0.
        # ------------------------------------------------------------
        pmdimensions1 = data1.partitions.dimensions
        axes = [pmdimensions1.index(dim) for dim in data0.partitions.dimensions]
        data1.partitions.transpose(axes)
            
        # ------------------------------------------------------------
        # 9. Create new partition boundaries in the partition arrays
        #    of data0 and data1 so that their partition arrays may be
        #    considered as different slices of a common, larger
        #    hyperrectangular partition array.
        #
        # Note that there is no need to add any boundaries across the
        # Aggregating Dimension.
        # ------------------------------------------------------------
        boundaries0 = data0.partition_boundaries()        
        boundaries1 = data1.partition_boundaries()

        for dim in data0.partitions.dimensions[1:]:
            
            # Still here? Then see if there are any partition
            # boundaries to be created for this partition dimension
            bounds0 = boundaries0[dim]
            bounds1 = boundaries1[dim]

            symmetric_diff = set(bounds0).symmetric_difference(bounds1)            
            if not symmetric_diff:
                # The partition boundaries for this partition
                # dimension are already the same in data0 and data1
                continue
            
            # Still here? Then there are some partition boundaries to
            # be created for this partition dimension in data0 and/or
            # data1.
            for f, g, bf, bg in izip((data0, data1, bounds0, bounds1), 
                                     (data1, data0, bounds1, bounds0)):                
                extra_bounds = [i
                                for i in bg
                                if i in symmetric_diff]
                
                f.add_partitions(extra_bounds, dim)
            #--- End: for
        #--- End: for
                
        # ------------------------------------------------------------
        # 10. Merge data1 into data0 along the Aggregating Dimension.
#        #
#        # If no indices have been provided then, along the Aggregating
#        # Dimension, concatenate the partitions of data1 onto the end
#        # of the partitions of data0.
        # ------------------------------------------------------------
#        if indices is None:
#            npartitions0 = data0.npartitions
#            indices = xrange(npartitions0, npartitions0+data1.npartitions)
#        #--- End: if
#
#        partitions0 = data0.partitions
#        partitions1 = data1.partitions
#        for i, index in enumerate(indices):
#            partitions0.insert(index, partitions1[i])
                                          
        if extend:
#            data0.partitions._extend(data1.partitions, reversed=reversed)
            data0._extend_pm(data1, reversed=reversed)
        else:
            data0._prepend_pm(data1, reversed=reversed)
 
        # Update the size of data0
        data0._size += long(data1._size)

        # Update the shape of data0
        index = data1.dimensions.index(PDim)
        shape = list(data0._shape)
        shape[index] += data1._shape[index]
        data0._shape = tuple(shape)

        # Update the location map of the partition array of data0
        data0.partitions.set_location_map(data0.dimensions)

        # Set the new dtype
        dtype0 = data0.dtype
        dtype1 = data1.dtype
        if dtype0 != dtype1:
            data0.dtype = numpy_result_type(dtype0, dtype1)        
        
        # ------------------------------------------------------------
        # 11. Done
        # ------------------------------------------------------------
    #--- End: def

    def _copy_directions(self, directions):
        '''
'''
        if isinstance(directions, dict):
            return directions.copy()

        return directions
    #--- End: def

    def _parse_axes(self, axes, method, default=[], dims=None):
        '''
        
:Parameters:

    axes : (sequence of) int or str
        The axes of the data array. May be one of, or a sequence of
        any combination of zero or more of:

            * The integer position of a dimension in the data array
              (negative indices allowed).
            * The internal name a dimension.

    method : str

:Returns:

    out : list

**Examples**

'''
        if axes is None: #not axes and axes is not 0:
            axes = default

        if isinstance(axes, (str, int, long)):
            # Convert an integer to a single element sequence
            axes = [axes]
        else:
            axes = list(axes)
            
        dimensions = self.dimensions
        for i, axis in enumerate(axes):            
            if axis in dimensions:
                axes[i] = dimensions.index(axis)
            elif isinstance(axis, (int, long)):
                if axis < 0:
                    axes[i] = axis + self._ndim
            else:
                raise ValueError(
                    "Can't %s: Axis must be integer or dimension name: %s" %
                    (method, axis))
        #--- End: for
            
        if axes:
            # Check for duplicate axes
            if len(axes) != len(set(axes)):
                raise ValueError("Can't %s: Repeated axis: %s" %
                                 (method, repr(axes)))
            
            # Check for out of range axes
            if max(axes) >= self._ndim:
                raise ValueError("Can't %s: Invalid axis for this array: %d" %
                                 (method, max(axes)))
        #--- End: if

        if dims:
            # Convert axes from integer positions to dimension identifiers
            dimensions = self.dimensions
            return axes, [dimensions[i] for i in axes]
        else:
            return axes
    #--- End: def

    def _unary_operation(self, operation):
        '''

Implement unary arithmetic operations.

It is intended to be called by the unary arithmetic methods, such as
__abs__().

:Parameters:

    operation : str
        The unary arithmetic method name (such as "__abs__").

:Returns:

    new : Data
        A new Data array.

**Examples**

>>> d = cf.Data([1, 2, -3, -4, -5])

>>> e = d._unary_operation('__abs__')
>>> print e.array
[1 2 3 4 5]]

>>> e = d.__abs__()
>>> print e.array
[[1 2 3 4 5]]

>>> e = abs(d)
>>> print e.array
[[1 2 3 4 5]]

'''
        self.to_memory()

        new = self.copy()

        conform_args = new.conform_args()

        for partition in new.partitions.matrix.flat:
            array = partition.dataarray(**conform_args)
            partition.subarray = getattr(array, operation)()
            partition.close()
        #--- End: for

        return new
    #--- End: def

    def __add__(self, other):
        '''
The binary arithmetic operation ``+``

x.__add__(y) <==> x+y

'''
        return self._binary_operation(other, '__add__')
    #--- End: def

    def __iadd__(self, other):
        '''
The augmented arithmetic assignment ``+=``

x.__iadd__(y) <==> x+=y

'''
        return self._binary_operation(other, '__iadd__')
    #--- End: def

    def __radd__(self, other):
        '''
The binary arithmetic operation ``+`` with reflected operands

x.__radd__(y) <==> y+x

'''
        return self._binary_operation(other, '__radd__')
    #--- End: def

    def __sub__(self, other):
        '''
The binary arithmetic operation ``-``

x.__sub__(y) <==> x-y

'''
        return self._binary_operation(other, '__sub__')
    #--- End: def

    def __isub__(self, other):
        '''
The augmented arithmetic assignment ``-=``

x.__isub__(y) <==> x-=y

'''
        return self._binary_operation(other, '__isub__')
    #--- End: def

    def __rsub__(self, other):
        '''
The binary arithmetic operation ``-`` with reflected operands

x.__rsub__(y) <==> y-x

'''
        return self._binary_operation(other, '__rsub__')
    #--- End: def

    def __mul__(self, other):
        '''
The binary arithmetic operation ``*``

x.__mul__(y) <==> x*y

'''
        return self._binary_operation(other, '__mul__')
    #--- End: def

    def __imul__(self, other):
        '''
The augmented arithmetic assignment ``*=``

x.__imul__(y) <==> x*=y

'''
        return self._binary_operation(other, '__imul__')
    #--- End: def

    def __rmul__(self, other):
        '''
The binary arithmetic operation ``*`` with reflected operands

x.__rmul__(y) <==> y*x

'''
        return self._binary_operation(other, '__rmul__')
    #--- End: def

    def __div__(self, other):
        '''
The binary arithmetic operation ``/``

x.__div__(y) <==> x/y

'''
        return self._binary_operation(other, '__div__')
    #--- End: def

    def __idiv__(self, other):
        '''
The augmented arithmetic assignment ``/=``

x.__idiv__(y) <==> x/=y

'''
        return self._binary_operation(other, '__idiv__')
    #--- End: def

    def __rdiv__(self, other):
        '''
The binary arithmetic operation ``/`` with reflected operands

x.__rdiv__(y) <==> y/x

'''
        return self._binary_operation(other, '__rdiv__')
    #--- End: def

    def __floordiv__(self, other):
        '''
The binary arithmetic operation ``//``

x.__floordiv__(y) <==> x//y

'''
        return self._binary_operation(other, '__floordiv__')
    #--- End: def

    def __ifloordiv__(self, other):
        '''
The augmented arithmetic assignment ``//=``

x.__ifloordiv__(y) <==> x//=y

'''
        return self._binary_operation(other, '__ifloordiv__')
    #--- End: def

    def __rfloordiv__(self, other):
        '''
The binary arithmetic operation ``//`` with reflected operands

x.__rfloordiv__(y) <==> y//x

'''
        return self._binary_operation(other, '__rfloordiv__')
    #--- End: def

    def __truediv__(self, other):
        '''
The binary arithmetic operation ``/`` (true division)

x.__truediv__(y) <==> x/y

'''
        return self._binary_operation(other, '__truediv__')
    #--- End: def

    def __itruediv__(self, other):
        '''
The augmented arithmetic assignment ``/=`` (true division)

x.__itruediv__(y) <==> x/=y

'''
        return self._binary_operation(other, '__itruediv__')
   #--- End: def

    def __rtruediv__(self, other):
        '''
The binary arithmetic operation ``/`` (true division) with reflected operands

x.__rtruediv__(y) <==> y/x

'''
        return self._binary_operation(other, '__rtruediv__')
    #--- End: def

    def __pow__(self, other, modulo=None):
        '''
The binary arithmetic operations ``**`` and ``pow``

x.__pow__(y) <==> x**y

'''  
        if modulo is not None:
            raise NotImplementedError("3-argument power not supported for '%s'" %
                                      self.__class__.__name__)

        return self._binary_operation(other, '__pow__')
    #--- End: def

    def __ipow__(self, other, modulo=None):
        '''
The augmented arithmetic assignment ``**=``

x.__ipow__(y) <==> x**=y

'''  
        if modulo is not None:
            raise NotImplementedError("3-argument power not supported for '%s'" %
                                      self.__class__.__name__)

        return self._binary_operation(other, '__ipow__')
    #--- End: def

    def __rpow__(self, other, modulo=None):
        '''
The binary arithmetic operations ``**`` and ``pow`` with reflected operands

x.__rpow__(y) <==> y**x

'''  
        if modulo is not None:
            raise NotImplementedError("3-argument power not supported for '%s'" %
                                      self.__class__.__name__)

        return self._binary_operation(other, '__rpow__')
    #--- End: def

    def __eq__(self, other):
        '''
The rich comparison operator ``==``

x.__eq__(y) <==> x==y

'''
        return self._binary_operation(other, '__eq__')
    #--- End: def

    def __ne__(self, other):
        '''
The rich comparison operator ``!=``

x.__ne__(y) <==> x!=y

'''
        return self._binary_operation(other, '__ne__')
    #--- End: def

    def __ge__(self, other):
        '''
The rich comparison operator ``>=``

x.__ge__(y) <==> x>=y

'''
        return self._binary_operation(other, '__ge__')
    #--- End: def

    def __gt__(self, other):
        '''
The rich comparison operator ``>``

x.__gt__(y) <==> x>y

'''
        return self._binary_operation(other, '__gt__')
    #--- End: def

    def __le__(self, other):
        '''
The rich comparison operator ``<=``

x.__le__(y) <==> x<=y

'''
        return self._binary_operation(other, '__le__')
    #--- End: def

    def __lt__(self, other):
        '''
The rich comparison operator ``<``

x.__lt__(y) <==> x<y

'''
        return self._binary_operation(other, '__lt__')
    #--- End: def

    def __and__(self, other):
        '''
The binary arithmetic operation ``&``

x.__and__(y) <==> x&y

'''
        return self._binary_operation(other, '__and__')
    #--- End: def

    def __iand__(self, other):
        '''
The augmented arithmetic assignment ``&=``

x.__iand__(y) <==> x&=y

'''
        return self._binary_operation(other, '__iand__')
    #--- End: def

    def __rand__(self, other):
        '''
The binary arithmetic operation ``&`` with reflected operands

x.__rand__(y) <==> y&x

'''
        return self._binary_operation(other, '__rand__')
    #--- End: def

    def __or__(self, other):
        '''
The binary arithmetic operation ``|``

x.__or__(y) <==> x|y

'''
        return self._binary_operation(other, '__or__')
    #--- End: def

    def __ior__(self, other):
        '''
The augmented arithmetic assignment ``|=``

x.__ior__(y) <==> x|=y

'''
        return self._binary_operation(other, '__ior__')
    #--- End: def

    def __ror__(self, other):
        '''
The binary arithmetic operation ``|`` with reflected operands

x.__ror__(y) <==> y|x

'''
        return self._binary_operation(other, '__ror__')
    #--- End: def

    def __xor__(self, other):
        '''
The binary arithmetic operation ``^``

x.__xor__(y) <==> x^y

'''
        return self._binary_operation(other, '__xor__')
    #--- End: def

    def __ixor__(self, other):
        '''
The augmented arithmetic assignment ``^=``

x.__ixor__(y) <==> x^=y

'''
        return self._binary_operation(other, '__ixor__')
    #--- End: def

    def __rxor__(self, other):
        '''
The binary arithmetic operation ``^`` with reflected operands

x.__rxor__(y) <==> y^x

'''
        return self._binary_operation(other, '__rxor__')
    #--- End: def

    def __abs__(self):
        '''
The unary arithmetic operation ``abs``

x.__abs__() <==> abs(x)

'''
        return self._unary_operation('__abs__')
    #--- End: def

    def __neg__(self):
        '''
The unary arithmetic operation ``-``

x.__neg__() <==> -x

'''
        return self._unary_operation('__neg__')
    #--- End: def

    def __invert__(self):
        '''
The unary arithmetic operation ``~``

x.__invert__() <==> ~x

'''
        return self._unary_operation('__invert__')
    #--- End: def

    def __pos__(self):
        '''
The unary arithmetic operation ``+``

x.__pos__() <==> +x

'''
        return self._unary_operation('__pos__')
    #--- End: def

    def _set(self, attr, value):
        '''

Set an attribute which actually belongs to the partition matrix.
'''
        setattr(self.partitions, attr, value)  
    #--- End: def

    def _get(self, attr):
        '''

Get an attribute which actually belongs to the partition matrix.

'''
        return getattr(self.partitions, attr)
    #--- End: def

    def _del(self, attr):
        '''

Delete an attribute which actually belongs to the partition
array. This is used by the decorated properties,

'''
        try:
            delattr(self.partitions, attr)
        except AttributeError:
            raise AttributeError("Can't delete '%s' attribute '%s'" %
                                 (self.__class__.__name__, attr))
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: Units
    # ----------------------------------------------------------------
    @property
    def Units(self):
        '''

The `Units` object containing the units of the data array.

Deleting this attribute is equivalent to setting it to an undefined
units object, so this attribute is guaranteed to always exist.

**Examples**

>>> d.Units = cf.Units('m')
>>> d.Units
<CF Units: m>
>>> del d.Units
>>> d.Units
<CF Units: >

'''
        return self._Units #self._get('Units')
    #--- End: def

    @Units.setter
    def Units(self, value): self._Units = value #self._set('Units', value)
    @Units.deleter
    def Units(self)       : self._Units = Units()

#    # ----------------------------------------------------------------
#    # Attribute: direction
#    # ----------------------------------------------------------------
#    @property
#    def direction(self):
#        '''
#
#
#
#**Examples**
#
#>>> d.dimensions
#['dim2', 'dim0', 'dim3']
#>>> d.direction
#{'dim0': True, 'dim1': False, 'dim2': True}
#
#>>> d.dimensions
#[]
#>>> d.direction
#True
#
#>>> d.dimensions
#[]
#>>> d.direction
#False
#
#'''
#        return self._get('direction')
#    @direction.setter
#    def direction(self, value): self._set('direction', value)
#    @direction.deleter
#    def direction(self)       : self._del('direction')

    # ----------------------------------------------------------------
    # Attribute: dtype
    # ----------------------------------------------------------------
    @property
    def dtype(self):
        '''

The numpy data type of the data array.

By default this is the data type with the smallest size and smallest
scalar kind to which all data array partitions may be safely cast
without loss of information. For example, if the partitions have data
types 'int64' and 'float32' then the data array's data type will be
'float64'; or if the partitions have data types 'int64' and 'int32'
then the data array's data type will be 'int64'.

Setting the data type to a `numpy.dtype` object, or any object
convertible to a `numpy.dtype` object, will cause the data array
elements to be recast to the specified type at the time that they are
next accessed. This does not immediately change the data array
elements, so, for example, reinstating the original data type prior to
data access results in no loss of information.

Deleting the data type reinstates the default behaviour. Note that if
the data type of any partitions have been changed after `dtype` has
been set (which could occur if the data array is accessed) then the
reinstated default data type may be different to the data type prior
to `dtype` being set.

Setting the data type to None is equivalent to deleting the `dtype`
attribute.

**Examples**

>>> print d.array
[0.5 1.5 2.5]
>>> d.dtype
dtype(float64')
>>> type(d.dtype)
<type 'numpy.dtype'>

>>> print d.array
[0.5 1.5 2.5]
>>> import numpy
>>> d.dtype = numpy.dtype(int)
>>> print d.array
[0 1 2]
>>> d.dtype = bool
>>> d.dtype = int
>>> print d.array
[0 1 2]
>>> d.dtype = bool
>>> print d.array
[False True True]
>>> d.dtype = 'float64'
>>> print d.array
[ 0.  1.  1.]

'''
        datatype = self._dtype
        if datatype is None:
            return numpy_result_type(*(partition.subarray
                                       for partition in self.partitions.matrix.flat))

        return datatype
    #--- End: def
    @dtype.setter
    def dtype(self, value):
        self._dtype = numpy_dtype(value)
    #--- End: def
    @dtype.deleter
    def dtype(self):
        self._dtype = None
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: fill_value
    # ----------------------------------------------------------------
    @property
    def fill_value(self):
        '''

The fill_value CF attribute.

If set to None then the default numpy fill value appropriate to the
data array's data type will be used.

Deleting this attribute is equivalent to setting it to None, so this
attribute is guaranteed to always exist.

**Examples**

>>> d.fill_value = 9999.0
>>> d.fill_value
9999.0
>>> del d.fill_value
>>> d.fill_value
None

'''
        return self._fill_value #_get('_FillValue')
    #--- End: def
    @fill_value.setter
    def fill_value(self, value): self._fill_value = value #self._set('_FillValue', value)
    @fill_value.deleter
    def fill_value(self)       : self._fill_value = None #self._FillValue = None

    # ----------------------------------------------------------------
    # Attribute: hardmask
    # ----------------------------------------------------------------
    @property
    def hardmask(self):
        '''

Whether the mask is hard (True) or soft (False).

When the mask is hard, masked entries of the data array can not be
unmasked by assignment.

By default, the mask is hard.

**Examples**

>>> d.hardmask = False
>>> d.hardmask
False
>>> d.hardmask = True
>>> d.hardmask
True

'''
        return self._hardmask
    @hardmask.setter
    def hardmask(self, value):
        self._hardmask = value
    @hardmask.deleter
    def hardmask(self):
        raise AttributeError("Won't delete %s attribute 'hardmask'" %
                             self.__class__.__name__)
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: ismasked (read only)
    # ----------------------------------------------------------------
    @property
    def ismasked(self):
        '''

True if the data array has any masked values.

**Examples**

>>> d.ismasked
True

'''
        conform_args = self.conform_args(revert_to_file=True)

        for partition in self.partitions.matrix.flat:
            array = partition.dataarray(**conform_args)
            partition.close()
            if numpy_ma_is_masked(array):
                return True
        #--- End: for

        return False
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: isscalar (read only)
    # ----------------------------------------------------------------
    @property
    def isscalar(self):
        '''

True if the data array is a 0-d scalar array.

**Examples**

>>> d.ndim
0
>>> d.isscalar
True

>>> d.ndim >= 1
True
>>> d.isscalar
False

'''
        return not self._ndim
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: ndim (read only)
    # ----------------------------------------------------------------
    @property
    def ndim(self):
        '''

Number of dimensions in the data array.

**Examples**

>>> d.shape
(73, 96)
>>> d.ndim
2

>>> d.shape
()
>>> d.ndim
0

'''
        return self._ndim
    #--- End: def

#    # ----------------------------------------------------------------
#    # Attribute: dimensions
#    # ----------------------------------------------------------------
#    @property
#    def dimensions(self):
#        '''
#
#'''
#        return self._get('dimensions')
#    @dimensions.setter
#    def dimensions(self, value): self._set('dimensions', value)
#    @dimensions.deleter
#    def dimensions(self)       : self._del('dimensions')

    # ----------------------------------------------------------------
    # Attribute: pmdimensions (read only)
    # ----------------------------------------------------------------
    @property
    def pmdimensions(self):
        '''
'''
        return self.partitions.dimensions
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: pmndim (read only)
    # ----------------------------------------------------------------
    @property
    def pmndim(self):
        '''

Number of dimensions in the partition matrix.

>>> d.pmshape
(4, 7)
>>> d.pmndim
2

>>> d.pmshape
()
>>> d.pmndim
0

'''
        return self.partitions.ndim
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: pmsize (read only)
    # ----------------------------------------------------------------
    @property
    def pmsize(self):
        '''

Number of partitions in the partition matrix.

**Examples**

>>> d.pmshape
(4, 7)
>>> d.pmsize
28

'''
        return self.partitions.size
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: pmshape
    # ----------------------------------------------------------------
    @property
    def pmshape(self):
        '''

Tuple of the partition matrix's dimension sizes.

**Examples**

>>> d.pmshape
(4, 7)

>>> d.pmndim
0
>>> d.pmshape
()

'''
        return self.partitions.shape
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: shape (read only)
    # ----------------------------------------------------------------
    @property
    def shape(self):
        '''

Tuple of the data array's dimension sizes.

**Examples**

>>> d.shape
(73, 96)

>>> d.ndim
0
>>> d.shape
()

'''
        return self._shape
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: size (read only)
    # ----------------------------------------------------------------
    @property
    def size(self):
        '''

Number of elements in the data array.

**Examples**

>>> d.shape
(73, 96)
>>> d.size
7008

>>> d.shape
()
>>> d.size
0

'''
        return self._size
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: array (read only)
    # ----------------------------------------------------------------
    @property
    def array(self):
        '''

A numpy array copy the data array.

**Examples**

>>> a = d.array
>>> isinstance(a, numpy.ndarray)
True

'''
        conform_args = self.conform_args(revert_to_file=True)

        partitions = self.partitions

        if partitions.size == 1:
            # If there is only one partition we can speed things up
            partition = partitions.matrix.item()
            array_out = partition.dataarray(copy_regardless=True, **conform_args)
            partition.close()
            return array_out
        #--- End: if

        # Still here?
        array_out = numpy_empty(self._shape, dtype=self.dtype)
        masked = False

        for partition in partitions.matrix.flat:
            data = partition.dataarray(readonly=True, **conform_args)

            if not masked and numpy_ma_isMA(data):
                array_out = array_out.view(numpy_ma_MaskedArray)
                array_out.set_fill_value(self._fill_value)
                masked = True

            array_out[partition.indices] = data
            partition.close()
        #--- End: for

        if masked:
            # Shrink the mask
            array_out.shrink_mask()
            if self._hardmask:
                # Harden the mask of the output array
                array_out.harden_mask()
        #--- End: if
      
        return array_out
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: _array (read only)
    # ----------------------------------------------------------------
    @property
    def unsafe_array(self):
        '''

A numpy array of the data array.

It is unsafe because it might (or might not) be view to the master
data array - you just don't know. So changing the returned array in
place might have dire consequences.

It is useful because if you are 100% certain that you're not going to
change the returned array in place then this method may be much faster
than the `array` method.

Why not just use the `varray` method? Well, you might not want to
destroy the partition matrix.

The data type of the array is as returned by the `dtype` attribute.

**Examples**

>>> a = d.unsafe_array
>>> isinstance(a, numpy.ndarray)
True

'''
        partitions = self.partitions

        if partitions.size == 1:
            # If there is only one partition we can speed things up
            conform_args = self.conform_args(revert_to_file=True)
            partition = partitions.matrix.item()
            array_out = partition.dataarray(readonly=True, **conform_args)
            partition.close()
            return array_out
        #--- End: if

        # Still here?
        return self.array
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: varray (read only)
    # ----------------------------------------------------------------
    @property
    def varray(self):
        '''

A numpy array view the data array.

The data type of the array is as returned by the `dtype` attribute.

Note that making changes to elements of the returned view changes the
underlying data.

**Examples**

>>> a = d.varray
>>> type(a)
<type 'numpy.ndarray'>
>>> a
array([0, 1, 2, 3, 4])
>>> a[0] = 999
>>> d.varray
array([999, 1, 2, 3, 4])

'''
        if self.partitions.size == 1:
            # If there is only one partition, then we can return a
            # view of the partition's data array without having to
            # create an empty array and then filling it up partition
            # by partition.
            partition = self.partitions.matrix.item()
            conform_args = self.conform_args(save=False, revert_to_file=False)
            array = partition.dataarray(**conform_args)
            # Note that there is no need to close the partition here.
            return array
        #--- End: if

        # Still here?  
        conform_args = self.conform_args(save=False, dtype=None)
        shape = self._shape
        array = numpy_empty(shape, dtype=self.dtype)
        masked = False

        for partition in self.partitions.matrix.flat:
            data = partition.dataarray(readonly=True, **conform_args)

            if not masked and numpy_ma_isMA(data):
                array = array.view(numpy_ma_MaskedArray)
                array.set_fill_value(self._fill_value)
                masked = True

            array[partition.indices] = data
            # Note that there is no need to close the partition here.
        #--- End: for

        if masked:
            array.shrink_mask()
            if self._hardmask:
                # Harden the mask of the output array
                array.harden_mask()
        #--- End: if

        matrix = _xxx.copy() #numpy_empty((), dtype=object)

        matrix[()] = Partition(subarray    = array,
                               location    = [(0, n) for n in shape],
                               _dimensions = self.dimensions,
                               _directions = self.directions,
                               shape       = list(shape),
                               Units       = self.Units,
                               part        = []
                               )

        self.partitions = PartitionMatrix(matrix, [])

        return array
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: mask (read only)
    # ----------------------------------------------------------------
    @property
    def mask(self):
        '''

The boolean missing data mask of the data array.

The boolean mask has True where the data array has missing data and
False otherwise.

**Examples**

>>> d.shape
(12, 73, 96)
>>> m = d.mask
>>> m
<CF Data: >
>>> m.dtype
dtype('bool')
>>> m.shape
(12, 73, 96])

'''
        self.to_memory()

        mask = self.copy()

        mask_units = Units()

        conform_args = mask.conform_args(
            save=self.save_to_disk(numpy_dtype(bool).itemsize))
        
        for partition in mask.partitions.matrix.flat:
            array = partition.dataarray(**conform_args)
            
            if numpy_ma_is_masked(array):
                # data is a numpy.ma.array instance and has a mask
                # with at least one True element.
                partition.subarray = array.mask.copy()
            else:
                # data is not a numpy.ma.array instance or data is a
                # numpy.ma.array instance and has a mask of
                # numpy.ma.nomask.
                partition.subarray = numpy_zeros(array.shape, dtype=bool)

            partition.Units = mask_units
            partition.close()
        #--- End: for

        mask.Units = mask_units
        mask.dtype = bool

        mask._hardmask = False

        return mask
    #--- End: def

    @mask.setter
    def mask(self, value):
        raise AttributeError(
            "Can't set the 'mask' attribute directly, use the 'setmask' method")
    #--- End: def
    @mask.deleter
    def mask(self):
        raise AttributeError(
"Can't delete the 'mask' attribute directly, use the 'setmask()' method")
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: first_datum (read only)
    # ----------------------------------------------------------------
    @property
    def first_datum(self):
        ''' 

The first element of the data array.

**Examples**

>>> print d.array
[[1 2 3 4]]
>>> d.first_datum
1

>>> print d.array
[[-- 2 3 4]]
>>> d.first_datum
--

'''
        array = self[(slice(0, 1),)*self._ndim].unsafe_array

        if not numpy_ma_isMA(array):
            return array.item()
        
        mask = array.mask
        if mask is numpy_ma_nomask or not mask.item():
            return array.item()
        
        return numpy_ma_masked
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: last_datum (read only)
    # ----------------------------------------------------------------
    @property
    def last_datum(self):
        ''' 

The last element of the data array.

**Examples**

>>> print d.array
[[1 2 3 4]]
>>> d.last_datum
4

>>> print d.array
[[1 2 3 --]]
>>> d.last_datum
--

'''
        array = self[(slice(-1, None),)*self._ndim].unsafe_array

        if not numpy_ma_isMA(array):
            return array.item()
        
        mask = array.mask
        if mask is numpy_ma_nomask or not mask.item():
            return array.item()
        
        return numpy_ma_masked
    #--- End: def

    def add_partitions(self,
                       extra_boundaries, 
                       pdim):
        '''

Add partition boundaries.

**Examples**

>>> d.add_partitions(    )

'''            
        self.partitions.add_partitions(self.dimensions,
                                       self.directions,
                                       extra_boundaries, 
                                       pdim)
    #--- End: def

    def all(self):
        '''

Test whether all data array elements evaluate to True.

Performs a logical and over the data array and returns the
result. Masked values are considered as True during computation.

**Examples**

>>> print d.array
[[0 3 0]]
>>> d.all()
False

>>> print d.array
[[1 3 --]]
>>> d.all()
True

'''
        conform_args = self.conform_args(revert_to_file=True)
              
        for partition in self.partitions.matrix.flat:
            array = partition.dataarray(**conform_args)
            partition.close()
            if not array.all():
                return False
        #--- End: for

        return True
    #--- End: def

    def any(self):
        '''

Test whether any data array elements evaluate to True.

Performs a logical or over the data array and returns the
result. Masked values are considered as False during computation.

**Examples**

>>> print d.array
[[0 0 0]]
>>> d.any()
False

>>> print d.array
[[-- 0 0]]
>>> d.any()
False

>>> print d.array
[[0 3 0]]
>>> d.any()
True

'''         
        conform_args = self.conform_args(revert_to_file=True)

        for partition in self.partitions.matrix.flat:
            array = partition.dataarray(**conform_args)
            partition.close()
            if array.any():
                return True
        #--- End: for

        return False
    #--- End: def

    def binary_mask(self):
        '''

Return a binary missing data mask of the data array.

The binary mask's data array comprises dimensionless 8-bit integers
and has 0 where the data array has missing data and 1 otherwise.

:Returns:

    out : Data
        The binary mask.

**Examples**

>>> print d.mask.array
[[ True False  True False]]
>>> b = d.binary_mask().array
>>> print b
[[0 1 0 1]]

'''
        self.to_memory()

        binary_mask = self.copy()

        conform_args = binary_mask.conform_args(
            save=self.save_to_disk(numpy_dtype('int32').itemsize),
            dtype=None)

        _units_1 = _units_1

        for partition in binary_mask.partitions.matrix.flat:            
            array = partition.dataarray(**conform_args)        
            if numpy_ma_is_masked(array):
                # data is a numpy.ma.array instance and has a mask
                # with at least one True element.
                partition.subarray = numpy_array(~array.mask, dtype='int32')
            else:
                # data is not a numpy.ma.array instance or data is a
                # numpy.ma.array instance and has a mask of
                # numpy.ma.nomask.
                partition.subarray = numpy_ones(array.shape, dtype='int32')

            partition.Units = _units_1
            partition.close()
        #--- End: for

        binary_mask.Units = _units_1

        return binary_mask
    #--- End: def

    def clip(self, a_min, a_max, units=None):
        '''

Clip (limit) the values in the data array in place.

Given an interval, values outside the interval are clipped to the
interval edges. For example, if an interval of [0, 1] is specified
then values smaller than 0 become 0 and values larger than 1 become 1.

Parameters :
 
    a_min : scalar

    a_max : scalar

    units : str or Units

:Returns: 

    None

**Examples**

'''
        if units is not None:
            # Convert the limits to the same units as the data array
            units = Units(units)

            self_units = self.Units

            if self_units != units:
                a_min = Units.conform(a_min, units, self_units)
                a_max = Units.conform(a_max, units, self_units)
        #--- End: if
            
        conform_args = self.conform_args()
            
        for partition in self.partitions.matrix.flat:            
            array = partition.dataarray(**conform_args)            
            array.clip(a_min, a_max, out=array)
            partition.close()
        #--- End: if
    #--- End: def

    def close(self):
        '''

Close all files referenced by the data array.

Note that a closed file will be automatically reopened if its contents
are subsequently required.

:Returns:

    None

**Examples**

>>> d.close()

'''    
        for partition in self.partitions.matrix.flat:
            partition.file_close()
    #--- End: def

            
    def copy(self):
        '''

Return a deep copy.

Equivalent to ``copy.deepcopy(d)``.

:Returns:

    out : 
        The deep copy.

**Examples**

>>> e = d.copy()

'''
        new = Data.__new__(Data)   

        new.__dict__ = self.__dict__.copy()
#        d=new.__dict__
#       new            = type(self)()

#        new.dtype      = self.dtype
#        new._FillValue = self._FillValue
#        new._hardmask  = self._hardmask
#        new._ndim      = self._ndim
#        new._shape     = self._shape
#        new._size      = self._size

        new.partitions = self.partitions.copy()
#        new.dimensions = self.dimensions[:]
#        new.directions = self._copy_directions(self.directions)
#        new.Units      = self.Units.copy()

        return new
    #--- End: def
            
    def cos(self):
        '''

Take the trigonometric cosine of the data array in place.

Units are accounted for in the calculation. If the units are not
equivalent to radians (such as Kelvin) then they are treated as if
they were radians. For example, the the cosine of 90 degrees_east is
0.0, as is the sine of 1.57079632 kg m-2.

The Units are changed to '1' (nondimensionsal).

:Returns:

    None

**Examples**

>>> d.Units
<CF Units: degrees_east>
>>> print d.array
[[-90 0 90 --]]
>>> d.cos()
>>> d.Units
<CF Units: 1>
>>> print d.array
[[0.0 1.0 0.0 --]]

>>> d.Units
<CF Units: m s-1>
>>> print d.array
[[1 2 3 --]]
>>> d.cos()
>>> d.Units
<CF Units: 1>
>>> print d.array
[[0.540302305868 -0.416146836547 -0.9899924966 --]]

'''
        if self.Units.equivalent(_units_radians):
            self.Units = _units_radians

        self.func(numpy_cos)

        self.override_units(_units_1)
    #--- End: def
    
    def dump(self, prefix=None):
        '''

Return a string containing a full description of the instance.

:Parameters:

    id : str, optional
       Set the common prefix of component names. By default the
       instance's class name is used.

:Returns:

    out : str
        A string containing the description.

**Examples**

>>> x = d.dump()
>>> print d.dump()
>>> print d.dump(id='data1')

'''
        if prefix is None:
            prefix = self.__class__.__name__
            
        string = ['%s.shape = %s' % (prefix, self._shape)]

        if self._size == 1:
            string.append('%s.first_datum = %s' % (prefix, self.first_datum))
        else:
            string.append('%s.first_datum = %s' % (prefix, self.first_datum))
            string.append('%s.last_datum  = %s' % (prefix, self.last_datum))
        #-- End: if
            
        for attr in ('fill_Value', 'Units'):
            string.append('%s.%s = %s' % (prefix, attr,
                                          repr(getattr(self, attr))))
        #--- End: for

        return '\n'.join(string)
    #--- End: def

    def ndindex(self):
        '''

Return an iterator over the N-dimensional indices of the data array.

At each iteration a tuple of indices is returned, the last dimension
is iterated over first.

:Returns:

    out : numpy.ndindex
        An iterator over tuples of indices of the data array.

**Examples**

>>> d.shape
(2, 1, 3)
>>> for i in d.ndindex():
...     print i
...
(0, 0, 0)
(0, 0, 1)
(0, 0, 2)
(1, 0, 0)
(1, 0, 1)
(1, 0, 2)

> d.shape
()
>>> for i in d.ndindex():
...     print i
...
()

'''
#        return numpy_ndindex(self._shape)
        return itertools_product(*[xrange(0, r) for r in self._shape])  
    #return iterindices([(0, n) for n in self._shape])
    #--- End: def

    def equals(self, other, rtol=None, atol=None, traceback=False):
        '''

True if two data arrays are logically equal, False otherwise.

:Parameters:

    other : 
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns: 

    out : bool
        Whether or not the two instances are equal.

**Examples**

>>> d.equals(d)
True
>>> d.equals(d + 1)
False

'''
        # Check each instance's id
        if self is other:
            return True
 
        # Check that each instance is the same type
        if self.__class__ != other.__class__:
            return False

        # Check that each instance has the same shape
        if self._shape != other._shape:
            if traceback:
                print("%s: Different shape: %s, %s" %
                      (self.__class__.__name__, self._shape, other._shape))
            return False
        #--- End: if

        # Check that each instance has the same units
        if self.Units != other.Units:
            if traceback:
                print("%s: Different Units: %s, %s" %
                      (self.__class__.__name__,
                       repr(self.Units), repr(other.Units)))
            return False
        #--- End: if

        # Check that each instance has the same fill value
        if self._fill_value != other._fill_value:
            if traceback:
                print("%s: Different array fill_value: %s, %s" %
                      (self.__class__.__name__,
                       self._fill_value, other._fill_value))
            return False
        #--- End: if

        # ------------------------------------------------------------
        # Check that each instance has the array values
        # ------------------------------------------------------------
        # Set default tolerances
        if rtol is None:
            rtol = RTOL()
        if atol is None:
            atol = ATOL()        

        conform_args = self.conform_args(revert_to_file=True)

        other.to_memory()

        for partition in self.partitions.matrix.flat:
            array0 = partition.dataarray(**conform_args)
            array1 = other[partition.indices].varray
            partition.close()
            if not _allclose(array0, array1, rtol=rtol, atol=atol):
                if traceback:
                    print("%s: Different data arrays" %
                          self.__class__.__name__)
                return False
        #--- End: for

        # ------------------------------------------------------------
        # Still here? Then the two instances are equal
        # ------------------------------------------------------------
        return True            
    #--- End: def

    def expand_dims(self, axis=0, dim=None, direction=True):
        '''

Expand the shape of the data array in place.

Insert a new size 1 axis, corresponding to a given position in the
data array shape.

Not to be confused with the `expand_partition_dims` method.

:Parameters:

    axis : int, optional
        Position (amongst axes) where new axis is to be inserted. By
        default, insert at position 0.

    dim : str, optional
        
    direction : bool, optional

:Returns:

    None

:Raises:
 
    ValueError :
        If the given dimension identity is already in use.

**Examples**

>>> d.expand_dims(1)
>>> d.expand_dims(2, 'dim3')
>>> d.expand_dims(0, None, False)

'''
        dimensions = self.dimensions

        if dim is None:
            dim = self.new_dimension_identifier()
        elif dim in dimensions:
            raise ValueError("sadasvbvbqqqq 93746802 shgsdf______")
        elif dim in self.all_dimension_names():
            self.change_dimension_names({dim: self.new_dimension_identifier()})        
 
        # Expand directions
        if not self._ndim:
            # Data aray is a scalar
            self.directions = {dim: bool(direction)}
        else:
            # Data array has one or more dimensions
            #self.directions[dim] = bool(direction)
            directions = self.directions.copy()
            directions[dim] = bool(direction)
            self.directions = directions

        # Expand dimensions
        dimensions = dimensions[:]
        dimensions.insert(axis, dim)
        self.dimensions = dimensions
        #self.dimensions.insert(axis, dim)

        # Expand shape and ndim
        self._ndim += 1
        shape = list(self._shape)
        shape.insert(axis, 1)   
        self._shape = tuple(shape)

        # Expand the locations map
        location = (0, 1)
        for partition in self.partitions.matrix.flat:
            partition.location = partition.location[:]
            partition.shape    = partition.shape[:]
            
            partition.location.insert(axis, location)
            partition.shape.insert(axis, 1)
    #--- End: def

    def flat(self, ignore_masked=True):
        '''

Return a flat iterator over elements of the data array.

:Parameters:

    ignore_masked : bool, optional
        If False then masked and unmasked elements will be returned. By
        default only unmasked elements are returned

:Returns:

    out : generator
        An iterator over elements of the data array.

**Examples**

>>> print d.array
[[1 -- 3]]
>>> for x in d.flat():
...     print x
...
1
3

>>> for x in d.flat(False):
...     print x
...
1
--
3

'''
        self.to_memory()

        mask = self.mask

        if ignore_masked:
            for index in self.ndindex():
                if not mask[index]:
                    yield self[index].unsafe_array.item()
        else:
            for index in self.ndindex():
                if not mask[index]:
                    yield self[index].unsafe_array.item()
                else:
                    yield numpy_ma_masked
    #--- End: def

    def override_units(self, units):
        '''

Override the data array units in place.

Not to be confused with setting the `Units` attribute to units which
are equivalent to the original units. This is different because in
this case the new units need not be equivalent to the original ones
and the data array elements will not be changed to reflect the new
units.

:Parameters:

    units : str or Units
        The new units for the data array.

:Returns:

    None

**Examples**

>>> d.Units
<CF Units: hPa>
>>> d.first_datum
1012.0
>>> d.override_units('km')
>>> d.Units
<CF Units: km>
>>> d.first_datum
1012.0
>>> d.override_units(cf.Units('watts'))
>>> d.Units
<CF Units: watts>
>>> d.first_datum
1012.0

'''
#        if isinstance(units, basestring):
        units = Units(units)
 
        conform_args = self.conform_args()

        for partition in self.partitions.matrix.flat:            
            if partition.Units == units:
                # No need to create the data array if the sub-array
                # units are the same as the master data array units
                partition.Units = units #.copy()
                continue
            #--- End: if

            partition.dataarray(**conform_args)
            partition.Units = units #.copy()
            partition.close()
        #--- End: for

        self.Units = units #.copy() # not copy ???
    #--- End: def

    def to_disk(self):
        '''

Store the data array on disk in place.

There is no change to partition's whose sub-arrays are already on
disk.

:Returns:

    None

**Examples**

>>> d.to_disk()

'''
        conform_args = self.conform_args(save=True, revert_to_file=False)

        for partition in self.partitions.matrix.flat:
            if partition.in_memory:
                partition.dataarray(**conform_args)
                partition.close()
    #--- End: def

    def to_memory(self, regardless=False):
        '''

Store each partition's data in memory in place if the master array is
smaller than the chunk size.

There is no change to partitions with data that are already in memory.

:Parameters:
    
    regardless : bool, optional
        If True then store all partitions' data in memory regardless
        of the size of the master array. By default only store all
        partitions' data in memory if the master array is smaller than
        the chunk size.

:Returns:

    None

**Examples**

>>> pa.to_memory()
>>> pa.to_memory(True)

'''
        if not self.save_to_disk(self.dtype.itemsize) or regardless:
            conform_args = self.conform_args(save=False, revert_to_file=False)
            for partition in self.partitions.matrix.flat:
                if partition.on_disk:
                    partition.dataarray(**conform_args)
                    partition.close()
    #--- End: def

    @property
    def in_memory(self):
        '''

:Returns:

**Examples**

>>> d.in_memory

'''
        for partition in self.partitions.matrix.flat:
            if not partition.in_memory:
                return False
        #--- End: for

        return True
    #--- End: def

    def equivalent(self, other, rtol=None, atol=None, traceback=False, 
                   transpose=None, squeeze=True, use_directions=False):
        '''

True if and only if two data arrays are logically equivalent.

Equivalence is defined as both data arrays being the same after
accounting for different but equivalent:

* units

* size one dimensions (if *squeeze* is True), 

* dimension directions (if *use_directions* is True) 

* dimension orders (if *transpose* is set to a dictionary).

:Parameters:

    other : Data

    transpose : dict, optional

    squeeze : bool, optional

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    use_directions : bool, optional

:Returns:

    out : bool
        Whether or not the two variables are equivalent.

'''     
        if not self.Units.equivalent(other.Units):
            return 
       
        data1 = other.copy()
        
        if squeeze:
            data0 = self.copy()
            data0.squeeze()
            data1.squeeze()
        else:
            data0 = self

        if data0._ndim != data1._ndim:
            return
 
        data0_dimensions = data0.dimensions
        data1_dimensions = data1.dimensions        

        if transpose:
            try:
                axes = [data0_dimensions.index(transpose[dim1])
                        for dim1 in data1_dimensions]
            except ValueError:
                # transpose dictionary is insufficient
                if traceback:
                    print("%s: Can't transpose" % self.__class__.__name__)
                return False

            data1.transpose(axes)
        #--- End: if

        if data0._shape != data1._shape:
            return
 
        if use_directions and self._size > 1:            
            data0_directions = data0.directions
            data1_directions = data1.directions
            
            axes = [dim1
                    for dim1, dim0 in izip(data0_dimensions, data1_dimensions)
                    if data1_directions[dim1] != data0_directions[dim0]]
            data1.flip(axes)
        #--- End: if
        
        data1.Units       = data0.Units #.copy()
        data1._fill_value = data0._fill_value

        return data0.equals(data1, rtol=rtol, atol=atol, traceback=False)
    #--- End: def

    def change_dimension_names(self, dim_name_map):
        '''

Change the dimension names.

The dimension names are arbitrary, so mapping them to another
arbitrary collection does not change the data array values, units,
dimension directions nor dimension order.

**Examples**

>>> d.dimensions
['dim0', 'dim1', 'dim2']
>>> dim_name_map
{'dim0': 'dim1',
 'dim1': 'dim0',
 'dim2': 'dim2',
 'dim3': 'dim3'}
>>> d.change_dimension_names(dim_name_map)
>>> d.dimensions
['dim1', 'dim0', 'dim2']

'''       
        # Check for a null dimension name mapping
        # (e.g. 'dim0'->'dim0', etc.) and return if there is nothing
        # to change. I think that this is worth it, on the grounds
        # that this is often be the case
#        map_is_null = True
#        for dim0, dim1 in dim_name_map.iteritems():
#            if dim0 != dim1:
#                map_is_null = False
#                break
#        #--- End: for       
#        if map_is_null:
#            return

        # Find any partition dimension names which are not master
        # array dimension names. If there are any, then update
        # dim_name_map.
        d = set(self.all_dimension_names()).difference(dim_name_map)
      
        if d:
            dim_name_map = dim_name_map.copy()
            values = set(dim_name_map.itervalues())            
            for dim in d:
                if dim in values:
                    new_dim = self.new_dimension_identifier(values)
                else:
                    new_dim = dim

                values.add(new_dim)
                dim_name_map[dim] = new_dim 
        #--- End: if

#        if all([dim0==dim1 for dim0, dim1 in dim_name_map.iteritems()]):
#            return
        
        # Dimensions
        self.dimensions = [dim_name_map[dim] for dim in self.dimensions]

        # Directions
        if self._ndim:
            directions = {}
            for dim, dir in self.directions.iteritems():
                directions[dim_name_map[dim]] = dir

            self.directions = directions    
        #--- End: if           

        # Dimensions and directions of the partitions in the partition
        # matrix
        self.partitions.change_dimension_names(dim_name_map)
    #--- End: def

    def override_directions(self, new_directions=None, scalar_dir=True):
        '''

:Parameters:

    new_directions : dict, optional
    
    scalar_dir : bool, optional
    
:Returns:

    None

**Examples**


'''        
        orginal_directions = self.directions

        if not self._ndim:
            # Scalar data array
            if orginal_directions == scalar_dir:
                return

            self.directions = scalar_dir
            partition = self.partitions.matrix.item()
            if not partition._dimensions:
                # Scalar sub-array
                partition._directions = scalar_dir

            return
        #--- End: if

        # Still here?
        if orginal_directions == new_directions or new_directions is None:
            return

        directions = {}
        for dim, dir in orginal_directions.iteritems():
            directions[dim] = new_directions.get(dim, dir)
            
        self.directions = directions

        new_directions = directions

        # Directions of the partitions in the partition matrix
        for partition in self.partitions.matrix.flat:
            if not partition._dimensions:
                # Scalar sub-array
                continue

            p_directions = partition._directions

            directions = {}
            for p_dim, p_dir in p_directions.iteritems():
                orginal_dir = orginal_directions.get(p_dim, None)
                if p_dir is orginal_dir:
                    directions[p_dim] = new_directions[p_dim]
                elif orginal_dir is None:
                    directions[p_dim] = p_dir
                else:
                    directions[p_dim] = not new_directions[p_dim]
            #--- End: for

            partition._directions = p_directions
        #--- End: for
    #--- End: def

    def conform_args(self, save=None, **kwargs):
        '''
        
Return a dictionary of arguments for the partitions'
`~Partition.dataarray` methods.

The values are inferred from the state of the Data object and any
keyword arguments.

:Parameters:

    save : bool, optional
        Set the 'save' key. By default it is set automatically
        depending on the size of te master array, the chunk size and
        the available memory.
    
    kwargs :
    

:Returns:

    out : dict

**Examples**


'''
        dtype = self.dtype

        conform_args = {'dimensions': self.dimensions,
                        'directions': self.directions,
                        'units'     : self.Units,
                        'hardmask'  : self._hardmask,
                        'dtype'     : dtype}

        if save is None:
            save = self.save_to_disk(dtype.itemsize)
            
        conform_args['save'] = save
            
        if kwargs:
            conform_args.update(kwargs)

        return conform_args
    #--- End: def

#    def equivalent(self, other, rtol=None, atol=None):
#        '''
#
#Equivelence is defined as both instances having the same data arrays
#after accounting for different but equivalent units, different
#dimension directions (unless `use_directions` is False) and different
#dimension orders. Equivalent data arrays must have equal numbers of
#dimensions, unless both have one or fewer dimensions.
#
#:Parameters:
#
#    other : Data
#
#    atol : float, optional
#        The absolute tolerance for all numerical comparisons, By
#        default the value returned by the `ATOL` function is used.
#
#    rtol : float, optional
#        The relative tolerance for all numerical comparisons, By
#        default the value returned by the `RTOL` function is used.
#
#:Returns:
#
#    out : bool
#        Whether or not the two variables are equivalent.
#
#'''
#        if self.size != other.size:
#            return False
#        
#        if not self.Units.equivalent(other.Units):
#            return False
#
#        data0 = self.copy()
#        data1 = other.copy()
#
#        data0.squeeze()
#        data1.squeeze()
#        
#        data1.Units = data0.Units.copy()
#
#        data1._FillValue = data0._FillValue
#
#        return data0.equals(data1, rtol=rtol, atol=atol, traceback=False)
#    #--- End: def

    def expand_partition_dims(self, pdim):
        '''

Expand the shape of the partition matrix in place.

Insert a new size partition matrix dimension at position 0. The new
dimension must be a dimension of the data array.

Not to be confused with the `expand_dims` method.

:Parameters:

    axis : str
        The name of the new partition dimension.

:Returns:

    None

**Examples**

>>> d.pmdimensions
['dim0', 'dim1']
>>> d.expand_partition_dims('dim2')
>>> d.pmdimensions
['dim2', 'dim0', 'dim1']

'''
        self.partitions.expand_dims(pdim)
    #--- End: def

    def all_dimension_names(self):
        '''

Return a set of all the dimension names in use by the data array.

Note that the output set includes dimensions of individual partitions
which are not dimensions of the master data array.

:Returns:

    out : set
        The dimension names.

**Examples**

>>> d.dimensions
['dim1', 'dim0']
>>> d.partitions.info('dimensions')
[['dim0', 'dim0'],
 ['dim1', 'dim0', 'dim2']]
>>> d.all_dimension_names()
set(['dim2', dim0', 'dim1'])

'''
        out = {}

        for partition in self.partitions.matrix.flat:
            for dim in partition._dimensions:
                out[dim] = dim
            
        return out

#        out = self.dimensions[:]
#
#        for partition in self.partitions.matrix.flat:
#            out.extend(partition._dimensions)
#            
 #       return set(out)

#        out = set(self.dimensions)
#
#        for partition in self.partitions.matrix.flat:
#            out.update(partition._dimensions)
#            
#        return out
    #--- End: def

    def new_dimension_identifier(self, values=None):
        '''

Return a dimension name not being used by the data array.

Note that a partition of the data array may have dimensions which
don't belong to the data array itself.

:Parameters:

    values : sequence

:Returns:

    out : str
        The new dimension name.

**Examples**

>>> d.all_dimension_names()   
['dim1', 'dim0']
>>> d.new_dimension_identifier()
'dim2'

>>> d.all_dimension_names()   
['dim1', 'dim0', 'dim3']
>>> d.new_dimension_identifier()
'dim4'

>>> d.all_dimension_names()   
['dim5', 'dim6', 'dim7']
>>> d.new_dimension_identifier()
'dim3'

'''
        if values is None:
            values = self.all_dimension_names()           

        n = len(values)
        dim = 'dim%d' % n
        while dim in values:
            n += 1
            dim = 'dim%d' % n
        #--- End: while

        return dim
    #--- End: def

    def partition_boundaries(self):
        '''
'''            
        return self.partitions.partition_boundaries(self.dimensions)
    #--- End: def

    def flip(self, axes=None):
        '''

Flip dimensions of the data array in place.

:Parameters:

    axes : (sequence of) int, optional
        Flip the dimensions whose positions are given. By default all
        dimensions are flipped.

:Returns:

    out : list of ints
        The axes which were flipped, in arbitrary dimensions.

**Examples**

>>> d.flip()
>>> d.flip(1)
>>> d.flip([0, 1])

>>> e = d[::-1, :, ::-1]
>>> d.flip([2, 0]).equals(e)
True

'''
        # Scalar data. Allow axis in (None, [], (), etc.) to reverse
        # the direction
        if not self._ndim: #self.isscalar:
            if not (axes is None or (not axes and axes != 0)):
                raise ValueError(
                    "Can't reverse: Invalid axes for this array: %s" % (axes,))
            self.directions = not self.directions
            return []
        #--- End: if

        axes = self._parse_axes(axes, 'flip', default=range(self._ndim))

        directions = self.directions.copy()
        dimensions = self.dimensions       
        partitions = self.partitions
        pmdimensions = partitions.dimensions

        flip_partition_matrix = False
        if pmdimensions:
            indices = [slice(None)] * partitions.ndim

        for i in axes:
            dim = dimensions[i]
            directions[dim] = not directions[dim]                          

            if dim in pmdimensions:
                indices[pmdimensions.index(dim)] = slice(None, None, -1)                
                flip_partition_matrix = True
        #--- End: for
                
        self.directions = directions

        if flip_partition_matrix:
            partitions = partitions[tuple(indices)]  # dch could do inaplce?
            partitions.set_location_map(dimensions)
            self.partitions = partitions
        #--- End: if

        return axes
    #--- End: def

    def save_to_disk(self, itemsize=None):
        '''

Return True if the master array is large enough to be saved to disk.

:Parameters:

    itemsize : int, optional
        The number of bytes per word of the master data array. By
        default it taken from the array's data type.

:Returns:

    out : bool

**Examples**

>>> print d.save_to_disk()
True

>>> print d.save_to_disk(8)
False

'''
        if not itemsize:
            try:
                itemsize = self.dtype.itemsize
            except AttributeError:
                raise ValueError(
                    "save_to_disk: Must set itemsize if there is no dtype")
        #--- End: if

        # ------------------------------------------------------------
        # Note that self._size*(itemsize+1) is the array size in bytes
        # including space for a full boolean mask
        # ------------------------------------------------------------

        return self._size*(itemsize+1) > FREE_MEMORY() - FM_THRESHOLD()
    #--- End: def

    def setitem(self, value, indices=Ellipsis, condition=None, masked=None,
                ref_mask=None, hardmask=None):
        '''

write me!

'''
        original_hardmask = self._hardmask
        if hardmask is None:
            hardmask = self._hardmask
        else:
            self._hardmask = hardmask        

        args = [condition, masked, ref_mask]
        n_set = len(args) - args.count(None)
        
        if n_set > 1:
            raise ValueError("can't set more than one thing")

        if not n_set:
            # --------------------------------------------------------
            # Set elements described by indices
            # --------------------------------------------------------
            self[indices] = value
        
        else:
            conform_args = self.conform_args()
        
            if value is not cf_masked:
                if not isinstance(value, Data):
                    # Convert to the value to a Data object
                    value = type(self)(value, units=self.Units)
                else:
                    value = value.copy()
                    if value.Units and value.Units.equivalent(self.Units):
                        value.Units = self.Units
                    elif not value.Units:
                        value.override_units(self.Units)
                    else:
                        raise ValueError(" Some bad units %s, %s" %
                                         (repr(self.Units), repr(value.Units)))
                #--- End: if

                # The following options all require a logically scalar
                # value
                if value._size > 1:
                    raise ValueError("not size 1 oooooh")

                value = value.first_datum
            #--- End: if

            if indices is not Ellipsis:
                indices, dummy = _parse_indices(self, indices)
        
            if masked is not None:
                if not masked:
                    # ------------------------------------------------
                    # Set unmasked elements
                    # ------------------------------------------------
                    self._hardmask = True
                    self[indices] = value

#                    if indices is Ellipsis:
#                        # Do this everywhere
#                        for partition in self.partitions.matrix.flat:
#                            partition.dataarray(**conform_args)
#                            partition.subarray[...] = value
#                            partition.close()
#                        #--- End: for
#                
#                    else:
#                        # Do this only for the specified indices
#                        for partition in self.partitions.matrix.flat:
#                            p_indices, shape = _partition_overlaps(partition, indices)
#                            if not p_indices:
#                                # This partition does not overlap the indices
#                                continue
#                            
#                            array = partition.dataarray(**conform_args)
#                            partition.subarray[p_indices] = value
#                            partition.close()
#                        #--- End: for    
#
                else:
                    # ------------------------------------------------
                    # Unmask masked elements and set them
                    # ------------------------------------------------
                    conform_args['hardmask'] = False
                    if indices is Ellipsis:
                        # Do this everywhere
                        for partition in self.partitions.matrix.flat:
                            array = partition.dataarray(**conform_args)
                            mask = numpy_ma_getmaskarray(array)
                            partition.subarray = numpy_ma_where(mask==True, value, array)
                            partition.close()
                        #--- End: for

                    else:
                        # Do this only for the specified indices
                        for partition in self.partitions.matrix.flat:
#                            p_indices, shape = _partition_overlaps(partition, indices)
                            p_indices, shape = partition.overlaps(indices)
                            if not p_indices:
                                # This partition does not overlap the indices
                                continue

                            array = partition.dataarray(**conform_args)
                            array = array[p_indices]
                            mask = numpy_ma_getmaskarray(array)
                            partition.subarray[p_indices] = numpy_ma_where(mask==True,
                                                                        value, array)
                            partition.close()
                        #--- End: for    
                #--- End: if

            elif ref_mask is not None:
                # ----------------------------------------------------
                # Set elements which correspond to False elements of
                # another array-like object.
                # ----------------------------------------------------
                self_mask = self.mask
                self_mask[indices] = ref_mask

                if indices is Ellipsis:
                    # Do this everywhere
                    for partition in self.partitions.matrix.flat:
                        array = partition.dataarray(**conform_args)
                        mask  = self_mask[partition.indices].varray
                        array = numpy_ma_where(array.mask==mask, value, array)
                        partition.close()
                    #--- End: for
                        
                else:
                    # Do this only for the specified indices
                    for partition in self.partitions.matrix.flat:
#                        p_indices, shape = _partition_overlaps(partition, indices)
                        p_indices, shape = partition.overlaps(indices)
                        if not p_indices:
                            # This partition does not overlap the indices
                            continue
                        
                        array = partition.dataarray(**conform_args)
                        array = array[p_indices]
                        mask  = self_mask[partition.indices]
                        mask  = mask[p_indices].varray
                        partition.subarray[p_indices] = numpy_ma_where(array.mask==mask,
                                                                    value, array)
                        partition.close()
                    #--- End: for
        
            elif condition is not None:
                # ----------------------------------------------------
                # Set elements whose values pass a condition.
                # ----------------------------------------------------
                if indices is Ellipsis:
                    # Do this everywhere
                    for partition in self.partitions.matrix.flat:
                        array = partition.dataarray(**conform_args)
                        partition.subarray = numpy_ma_where(array==condition,
                                                         value, array)
                        partition.close()
                    #--- End: for

                else:
                    # Do this only for the specified indices
                    for partition in self.partitions.matrix.flat:
#                        p_indices, shape = _partition_overlaps(partition, indices)
                        p_indices, shape = partition.overlaps(indices)
                        if not p_indices:
                            # This partition does not overlap the indices
                            continue
                        
                        array = partition.dataarray(**conform_args)
                        array = array[p_indices]
                        partition.subarray[p_indices] = numpy_ma_where(array==condition,
                                                                    value, array)
                        partition.close()
            #--- End: if

        #--- End: if

        # Reset hardmask
        self._hardmask = original_hardmask
    #--- End: def

    def setmask(self, value, indices=Ellipsis):
        '''

Set selected elements of the data array's mask in place.

The value to which the selected elements of the mask will be set may
be any object which is broadcastable across the selected elements. The
broadcasted value may be of any data type but will be evaluated as
boolean.

Unmasked elements are set to the fill value.

The mask may be effectively removed by setting every element to False
with ``f.setmask(False)``.

Note that if and only if the value to be assigned is logically scalar
and evaluates to True then ``f.setmask(value, indices)`` is equivalent
to ``f.setitem(cf.masked, indices)``. This is consistent with the
behaviour of numpy masked arrays.

:Parameters:

    value : array-like
        The value to which the selected elements of the mask will be
        set. Must be an object which is broadcastable across the
        selected elements.

    indices : optional
        Indices of the data array. Only elements of the mask described
        by the indices are set to *value*. By default, the entire mask
        is considered.

:Returns:

    None

**Examples**

'''
        original_hardmask = self._hardmask
        self._hardmask = False

        conform_args = self.conform_args() #dtype=None)
             
        if value is not cf_masked: #numpy_ma_masked:
            if not isinstance(value, Data):
                value = type(self)(value)

            if not value.any():
                value = numpy_ma_nomask
            elif value.all():
                value = cf_masked #numpy_ma_masked
        #--- End: if

        if indices is not Ellipsis:
            indices, dummy = _parse_indices(self, indices)

        if value is numpy_ma_nomask:
            # ------------------------------------------------
            # Unmask elements
            # ------------------------------------------------
            if indices is Ellipsis:
                # Set the mask to False (unmasked) everywhere
                for partition in self.partitions.matrix.flat:
                    array = partition.dataarray(**conform_args)
                    if numpy_ma_is_masked(array):
                        array = array.filled()
                        array.shrink_mask()
                        partition.subarray = array
                    #--- End: if
                    partition.close()
                #--- End: for
                  
            else:
                # Set the mask to False (unmasked) at the selected
                # indices
                for partition in self.partitions.matrix.flat:
#                    p_indices, shape = _partition_overlaps(partition, indices)
                    p_indices, shape = partition.overlaps(indices)
                    if not p_indices:
                        # This partition does not overlap the indices
                        continue
                    
                    array = partition.dataarray(**conform_args)
                    
                    a = array[p_indices]
                    if numpy_ma_is_masked(a):
                        array[p_indices] = a.filled()

                    if numpy_ma_is_masked(array):
                        array.shrink_mask()
                    else:
                        partition.subarray = array.data

                    partition.close()
                #--- End: for    

        elif value is cf_masked:
            # --------------------------------------------------------
            # Mask elements
            # --------------------------------------------------------  
            if indices is Ellipsis:
                # Set the mask to True (masked) everywhere
                for partition in self.partitions.matrix.flat:
                    array = partition.dataarray(**conform_args) # dch shortcircuit

                    if not numpy_ma_isMA(array):
                        array = numpy_ma_array(array)
                        partition.subarray = array
                    #--- End: if

                    array.mask = numpy_ma_masked
                    partition.close()
                #--- End: for
                  
            else:
                # Set the mask to True (masked) at the selected
                # indices
                for partition in self.partitions.matrix.flat:
#                    p_indices, shape = _partition_overlaps(partition, indices)
                    p_indices, shape = partition.overlaps(indices)
                    if not p_indices:
                        # This partition does not overlap the indices
                        continue
                    
                    array = partition.dataarray(**conform_args)
                    
                    if not numpy_ma_isMA(array):
                        array = numpy_ma_array(array)
                        partition.subarray = array
                    #--- End: if

                    array[p_indices] = numpy_ma_masked
                    partition.close()
                #--- End: for   
                
        else:            
            # --------------------------------------------------------
            # Set mask from an scalar or array
            # --------------------------------------------------------            
            mask = self.mask
            mask[indices] = value

            if indices is Ellipsis:
                # Set the mask everywhere
                for partition in self.partitions.matrix.flat:

                    array = partition.dataarray(**conform_args)

                    partition_mask = mask[partition.indices].unsafe_array

                    if numpy_ma_isMA(array):
                        array.mask = partition_mask
                        array.shrink_mask()
                    elif partition_mask.any():
                        array = array.view(numpy_ma_MaskedArray)
                        array.mask = partition_mask
                        array.shrink_mask()
                        partition.subarray = array
                    #--- End: if 

                    partition.close()
                #--- End: for
            
            else:
                # Set the mask at the selected indices
                for partition in self.partitions.matrix.flat:
#                    p_indices, shape = _partition_overlaps(partition, indices)
                    p_indices, shape = partition.overlaps(indices)
                    if not p_indices:
                        # This partition does not overlap the indices
                        continue
                    
                    array = partition.dataarray(**conform_args)

                    partition_mask = mask[partition.indices].unsafe_array

                    if numpy_ma_isMA(array):
                        array.mask = partition_mask
                        array.shrink_mask()
                    elif partition_mask.any():
                        array = array.view(numpy_ma_MaskedArray)
                        array.mask = partition_mask
                        array.shrink_mask()
                        partition.subarray = array
                    #--- End: if 

                    partition.close()
                #--- End: for    
        #--- End: if

        # Reset hardmask
        self._hardmask = original_hardmask
    #--- End: def

    def sin(self):
        '''
Take the trigonometric sine of the data array in place.

Units are accounted for in the calcualtion. If the units are not
equivalent to radians (such as Kelvin) then they are treated as if
they were radians. For example, the the cosine of 90 degrees_east is
1.0, as is the sine of 1.57079632 radians.

The Units are changed to '1' (nondimensionsal).

:Returns:

    None

**Examples**

>>> d.Units
<CF Units: degrees_north>
>>> print d.array
[[-90 0 90 --]]
>>> d.sin()
>>> d.Units
<CF Units: 1>
>>> print d.array
[[-1.0 0.0 1.0 --]]

>>> d.Units
<CF Units: m s-1>
>>> print d.array
[[1 2 3 --]]
>>> d.sin()
>>> d.Units
<CF Units: 1>
>>> print d.array
[[0.841470984808 0.909297426826 0.14112000806 --]]

'''
        if self.Units.equivalent(_units_radians):
            self.Units = _units_radians

        self.func(numpy_sin)

        self.override_units(_units_1)
    #--- End: def

    def squeeze(self, axes=None):
        '''

Remove size 1 dimensions from the data in place.

:Parameters:

    axes : (sequence of) int or str
        The axes to be squeezed. May be one of, or a sequence of any
        combination of zero or more of:

            * The integer position of a dimension in the data array
              (negative indices allowed).
              
            * The internal name a dimension.

        No dimensions are squeezed if an empty sequence is given.

:Returns:

    out : list of ints
        The axes which were squeezed as a tuple of their positions.

**Examples**

>>> v.shape
(1,)
>>> v.squeeze()
>>> v.shape
()

>>> v.shape
(1, 2, 1, 3, 1, 4, 1, 5, 1, 6, 1)
>>> v.squeeze((0,))
>>> v.shape
(2, 1, 3, 1, 4, 1, 5, 1, 6, 1)
>>> v.squeeze(1)
>>> v.shape
(2, 3, 1, 4, 1, 5, 1, 6, 1)
>>> v.squeeze([2, 4])
>>> v.shape
(2, 3, 4, 5, 1, 6, 1)
>>> v.squeeze([])
>>> v.shape
(2, 3, 4, 5, 1, 6, 1)
>>> v.squeeze()
>>> v.shape
(2, 3, 4, 5, 6)

'''
        ndim = self._ndim
        if not ndim: #self.isscalar:
            if axes or axes == 0:
                raise ValueError(
                    "Can't squeeze: Can't remove a dimension from a scalar %s" %
                    self.__class__.__name__)
            return []
        #--- End: if

        shape = list(self._shape)

        if axes is None:
            axes = [i for i in range(ndim) if shape[i] == 1]
        else:
            axes = self._parse_axes(axes, 'squeeze')

            # Check the squeeze dimensions
            for i in axes:
                if shape[i] > 1:
                    raise ValueError(
                        "Can't squeeze %s: Can't remove dimension of size > 1" %
                        self.__class__.__name__)
        #--- End: if

        if not axes:
            return axes

        # Still here? Then the data array is not scalar and at least
        # one size 1 dimension needs squeezing.
        dimensions = self.dimensions[:]
        directions = self.directions.copy()

        for dim in [dimensions[i] for i in axes]:
            i = dimensions.index(dim)
            shape.pop(i)
            dir = directions.pop(dimensions.pop(i))

            for partition in self.partitions.matrix.flat:
                p_location = partition.location[:]
                p_shape    = partition.shape[:]

                p_location.pop(i)
                p_shape.pop(i)

                partition.location = p_location
                partition.shape    = p_shape
        #--- End: for

        ndim = len(shape)
        self._ndim  = ndim
        self._shape = tuple(shape)

        # Set the dimensions
        self.dimensions = dimensions

        # Set the directions
        if not ndim:
            # The data has been squeezed to a scalar
            self.directions = dir
        else:
            self.directions = directions

        # Remove size 1 partition dimensions       
        self.partitions.squeeze()

        return axes
    #--- End: def

    def transpose(self, axes=None):
        '''
        
Permute the dimensions of the data array in place.

:Parameters:

    axes : sequence, optional
        The new order of the data array. By default, reverse the
        dimensions' order, otherwise the axes are permuted according
        to the values given. The values of the sequence may be any
        combination of:

            * The integer position of a dimension in the data array.
            * The internal name a dimension.

:Returns:

    out : list of ints
        The axes which were transposed


**Examples**

>>> d.ndim
3
>>> d.shape
(19, 73, 96)
>>> d.dimensions
['dim0', 'dim1', 'dim2']
>>> d.transpose()
>>> d.shape
(96, 73, 19)
>>> d.transpose([1, 0, 2])
>>> d.shape
(73, 96, 19)
>>> d.transpose(['dim2', 'dim0', 'dim1'])
>>> d.shape
(96, 19, 73)
>>> d.dimensions
['dim2', 'dim0', 'dim1']
>>> d.transpose((1,  'dim1', 0))
>>> d.shape
(19, 73, 96)

'''
        ndim = self._ndim    
        if ndim <= 1:
            return range(ndim-1, -1, -1)
        
        # Parse the axes. By default, reverse the dimensions
        if axes is None:
            axes = range(ndim-1, -1, -1)
        else:
            axes = self._parse_axes(axes, 'transpose')

            # Return unchanged if axes are in the same order as the data
            if axes == range(ndim):
                return axes

            if len(axes) != ndim:
                raise ValueError(
                    "Can't tranpose: Axes don't match array: %s" % (axes,))
        #--- End: if

        # Permute the order
        dimensions = self.dimensions
        self.dimensions = [dimensions[i] for i in axes]

        # Permute the shape
        shape = self._shape
        self._shape = tuple([shape[i] for i in axes])
        
        # Permute the locations map
        for partition in self.partitions.matrix.flat:
            location = partition.location
            shape    = partition.shape

            partition.location = [location[i] for i in axes]
            partition.shape    = [shape[i]    for i in axes]
        #--- End: for

        return axes
    #--- End: def

    def func(self, f, *args, **kwargs):
        '''

Apply an element-wise array operation to the data array in place.

:Parameters:

    f : function
        The function to be applied.

    args, kwargs : 
        Any arguments and keyword arguments passed to the function
        given by *f*.

:Returns:

   None

**Examples**

>>> print d.array
[[ 0.          1.57079633]
 [ 3.14159265  4.71238898]]
>>> import numpy
>>> d.func(numpy.cos)
>>> print d.array
[[ 1.0  0.0]
 [-1.0  0.0]]
>>> def f(x, y, a=0):
...     return x*y + a
...
>>> d.func(f, 2, a=10)
>>> print d.array
[[ 12.0  10.0]
 [-12.0  10.0]]

'''      
        conform_args = self.conform_args()

        for partition in self.partitions.matrix.flat:
            array = partition.dataarray(**conform_args)
            partition.subarray = f(array, *args, **kwargs)
            partition.close()
        #--- End: for
    #--- End: def

    def getitem(self, value, indices=Ellipsis, condition=None, masked=None,
                ref_mask=None, hardmask=None):
        '''
        '''
        pass

#--- End: class
   
def _size_of_index(index, size=None):
    '''

Return the number of elements resulting in applying an index to a
sequence.

:Parameters:

    index : slice or list of ints
        The index being applied to the sequence.

    size : int, optional
        The number of elements in the sequence being indexed. Only
        required if *index* is a slice object.

:Returns:

    out : int
        The length of the sequence resulting from applying the index.

**Examples**

>>> _size_of_index(slice(None, None, -2), 10)
5
>>> _size_of_index([1, 4, 9])
3

'''
    if isinstance(index, slice):
        # Index is a slice object
        start, stop, step = index.indices(size)
        div, mod = divmod(stop-start, step)
        if mod != 0:
            div += 1
        return div
    else:
        # Index is a list of integers
        return len(index)
#--- End: def

def _parse_indices(data, indices):
    '''

:Parameters:

   data : 

   indices : sequence of indices

:Returns:

    parsed_indices, reverse_dimensions : {list, list} 

**Examples**

'''
    parsed_indices = parse_indices(data, indices)
#    parsed_indices = data.parse_indices(indices)

    reverse_dimensions = []

    for i, index in enumerate(parsed_indices):      

        if isinstance(index, slice):
            size = data.shape[i]
            if index.step < 0:              
                # If the slice step is negative, then transform the
                # original slice to a new slice with a positive step
                # such that the result of the new slice is the reverse
                # of the result of the original slice.
                #
                # For example, if the original slice is slice(6,0,-2)
                # then the new slice will be slice(2,7,2):
                #
                # >>> a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                # >>> a[slice(6, 0, -2)]
                # [6, 4, 2]
                # >>> a[slice(2, 7, 2)]
                # [2, 4, 6]
                # a[slice(6, 0, -2)] == list(reversed(a[slice(2, 7, 2)]))
                # True
                start, stop, step = index.indices(size)
                step    *= -1
                div, mod = divmod(start-stop-1, step)
                div_step = div*step
                start   -= div_step
                stop     = start + div_step + 1
                
                index = slice(start, stop, step)
                reverse_dimensions.append(i)         
            #--- End: if      

            # If step is greater than one, make sure that index.stop isn't
            # bigger than it needs to be.
            if index.step > 1:
                start, stop, step = index.indices(size)
                div, mod = divmod(stop-start-1, step)
                stop     = start + div*step + 1
                index    = slice(start, stop, step)
            #--- End: if

            parsed_indices[i] = index    

        else:
            # --------------------------------------------------------
            # Check that an integer list is strictly monotonic and if
            # it's descending reverse it so that it's ascending
            # --------------------------------------------------------
            step = index[1] - index[0]

            if step > 0:                        
                if not all(x < y for x, y in izip(index, index[1:])):
                    raise ValueError("Bad slice (not strictly monotonic): %s" % index)

            elif step < 0:
                if not all(x > y for x, y in izip(index, index[1:])):
                    raise ValueError("Bad slice (not strictly monotonic): %s" % index)
                
                # Reverse the list so that it's strictly monotonically
                # increasing and make a note that this dimension will
                # need reversing later
                index.reverse()
                reverse_dimensions.append(i)
            else:
                # Step is 0
                raise ValueError("Bad slice (not strictly monotonic): %s" % index)
        #--- End: if
    #--- End: for
    
    return parsed_indices, reverse_dimensions
#--- End: def

#def _partition_overlaps(partition, indices):
#    '''
#
#:Parameters:
#
#   partition : cf.Partition
#
#   indices : sequence
#       Indices describing a subset of the master array. Each index is
#       either a slice object or a list.
#
#:Returns:
#
#    p_indices, shape : {tuple, list}
#        If the partition overlaps the indices then return a list of
#        indices which will subset the partition's data to where it
#        overlaps the master indices and the subsetted partition's
#        shape as a list. Otherwise return`(None, None)`.
#
#**Examples**
#
#>>> indices = (slice(None), slice(5, 1, -2), [1, 3, 4, 8])
#>>> _partition_overlaps(p, indices)
#(slice(), ddfsfsd), [3, 5, 4]
#
#'''
#    p_indices = []
#    shape     = []
#
#    for index, (r0, r1), size in izip(indices, 
#                                      partition.location, partition.shape):
#        
#        if isinstance(index, slice):
#            stop = size
#            if index.stop < r1:
#                stop -= (r1 - index.stop)
#              
#            start = index.start - r0
#            if start < 0:
#                start %= index.step   # start is now +ve
#            
#            if start >= stop:
#                # This partition is not in the slice
#                return None, None
#                
#            # Still here?
#            index      = slice(start, stop, index.step)
#            index_size = _size_of_index(index, size)
#
#        else:
#            if not set(index).intersection(xrange(r0, r1)):
#                # This partition is not in the slice
#                return None, None
#            
#            # Still here?
#            index = [i - r0 for i in index if r0 <= i < r1]
#            index_size = len(index)
#            if index == range(index_size):
#                index = slice(0, index_size, 1)
#            elif index == range(index_size-1, -1, -1):
#                index = slice(index_size-1, None, -1)
#        #--- End: if
#
#        # Still here?
#        p_indices.append(index)
#        shape.append(index_size)
#    #--- End: for
#        
#    # Still here? Then the elements of this partition specified by
#    # p_indices are in the slice.
#    return tuple(p_indices), shape
##--- End: def

def _overlapping_partitions(partitions, indices, dim2position, 
                            master_directions):
    '''

Return the nested list of (modified) partitions which overlap the
given indices to the master array.

:Parameters:

    partitions : cf.PartitionMatrix

    indices : tuple

    dim2position : dict

    master_directions : dict or bool

:Returns:

    out : list
        A (possibly nested) list of cf.Partition objects.

**Examples**

>>> type f.Data
<class 'cf.data.Data'>
>>> d.dimensions
['dim1', 'dim2', 'dim0']
>>> dim2position = {'dim0': 2, 'dim1': 0, 'dim2' : 1}
>>> indices = (slice(None), slice(5, 1, -2), [1,3,4,8])
>>> x = _overlapping_partitions(d.partitions, indices, dim2position, master_directions)
>>> type(x)
list

'''

    if partitions.size == 1:
        partition = partitions.matrix.item()

        # Find out if this partition overlaps the original slice
#        p_indices, shape = _partition_overlaps(partition, indices)
        p_indices, shape = partition.overlaps(indices)

        if not p_indices:
            # This partition is not in the slice 
            # out of bounds  - raise error?
            return
        
        # Still here? Create a new partition                      
        partition = partition.copy()
#        partition.part = partition.new_part(list(p_indices),
#                                            dim2position,
#                                            directions)
        partition.new_part(list(p_indices), dim2position, master_directions)
        partition.shape = shape
        
        new_partition_matrix      = numpy_empty(partitions.shape, dtype=object)
        new_partition_matrix[...] = partition
        
        return new_partition_matrix
    #--- End: if

    partitions_list        = []
    partitions_list_append = partitions_list.append

    flat_pm_indices        = []
    flat_pm_indices_append = flat_pm_indices.append

    partitions_flat = partitions.matrix.flat

    i = partitions_flat.index

    for partition in partitions_flat:
        # Find out if this partition overlaps the original slice
#        p_indices, shape = _partition_overlaps(partition, indices)
        p_indices, shape = partition.overlaps(indices)
        
        if not p_indices:
            # This partition is not in the slice 
            i = partitions_flat.index
            continue

        # Still here? Create a new partition                      
        partition = partition.copy()
#        partition.part = partition.new_part(list(p_indices),
#                                            dim2position,
#                                            master_directions)
        partition.new_part(list(p_indices), dim2position, master_directions)
        partition.shape = shape
        
        partitions_list_append(partition)

        flat_pm_indices_append(i)

        i = partitions_flat.index 
    #--- End: for

    new_shape = [len(set(s)) 
                 for s in numpy_unravel_index(flat_pm_indices, partitions.shape)]

    new_partition_matrix = numpy_empty((len(flat_pm_indices),), dtype=object)
    new_partition_matrix[...] = partitions_list
    new_partition_matrix.resize(new_shape)

    return new_partition_matrix
#--- End: def

def subspace_array(array, indices):
    '''

Subset the input numpy array with the given indices. Indexing is similar to
that of a numpy array. The differences to numpy array indexing are:

1. An integer index i takes the i-th element but does not reduce the rank of
   the output array by one.

2. When more than one dimension's slice is a 1-d boolean array or 1-d sequence
   of integers then these indices work independently along each dimension
   (similar to the way vector subscripts work in Fortran).

indices must contain an index for each dimension of the input array.
'''
    gg = []
    for i, x in enumerate(indices):
        if not isinstance(x, slice):
            gg.append(i)
    #--- End: for

    len_gg = len(gg)
    if len_gg > 1:
        # Slice the dimensions one at a time
        indices = list(indices)
        if numpy_ma_isMA(array):
            for axis in gg:
                array = numpy_ma_take(array, indices[axis], axis=axis)
                indices[axis] = slice(None)
        else:
            for axis in gg:
                array = numpy_take(array, indices[axis], axis=axis)
                indices[axis] = slice(None)
        #--- End: if

        if len_gg < len(indices):
            array = array[tuple(indices)]
    else:
        # Slice all dimensions at the same time
        array = array[tuple(indices)]

    return array
#--- End: def
