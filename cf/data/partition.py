import atexit
#import numpy

from numpy import ndarray     as numpy_ndarray
from numpy import expand_dims as numpy_expand_dims
from numpy import transpose   as numpy_transpose

from numpy.ma import resize      as numpy_ma_resize
from numpy.ma import expand_dims as numpy_ma_expand_dims
from numpy.ma import isMA        as numpy_ma_isMA

from sys       import getrefcount
from os        import remove
from operator  import mul
from itertools import izip
from itertools import product as itertools_product

from ..units     import Units
from ..functions import subspace_array

from .filearray import FileArray

# --------------------------------------------------------------------
# Set of partitions' temporary files
#
# For example:
# >>> _temporary_files
# set(['/tmp/cf_array_B8SSw2.npy',
#      '/tmp/cf_array_iRekAW.npy'])
# --------------------------------------------------------------------
_temporary_files = set()

def _remove_temporary_files(filename=None):
    '''

Remove temporary partition files from disk.

The removed files' names are deleted from the _temporary_files set.

It is intended to delete individual files as part of the garbage
collection process and to delete all files when python exits. It is
not recommended to be as a general tidy-up function.

This is quite brutal and may break partitions if used unwisely. It is
not recommended to be used as a general tidy-up function.

:Parameters:

    filename : str, optional
        The name of file to remove. The file name must be in the
        _temporary_files set. By default all files given in the
        _temporary_files set are removed.

:Returns:

    None

**Examples**

>>> _temporary_files
set(['/tmp/cf_array_B8SSw2.npy',
     '/tmp/cf_array_G756ks.npy',
     '/tmp/cf_array_iRekAW.npy'])
>>> _remove_temporary_files('/tmp/cf_array_G756ks.npy')
>>> _temporary_files
set(['/tmp/cf_array_B8SSw2.npy',
     '/tmp/cf_array_iRekAW.npy'])
>>> _remove_temporary_files()
>>> _temporary_files
set()

'''
    if filename is not None:
        if filename in _temporary_files:
            # Remove the given temporary file        
            _temporary_files.remove(filename)
            remove(filename)
        #--- End: if
        return
    #--- End: if

    # Still here? Then remove all temporary files
    for filename in _temporary_files:
        remove(filename)

    _temporary_files.clear()
#--- End: def

# --------------------------------------------------------------------
# Instruction to remove all of the temporary files from all partition
# arrays at exit.
# --------------------------------------------------------------------
atexit.register(_remove_temporary_files)


# ====================================================================
#
# Partition object
#
# ====================================================================

class Partition(object):
    '''

A partition of a master data array.

The partition spans all or part of exactly one sub-array of the master
data array

'''
#    __slots__ = ('subarray',   
#                 'directions',
#                 'location',
#                 'dimensions',
#                 'part',
#                 'shape',
#                 'Units',
#                 '_on_disk',
#                 '_original',
#                 '_save',
#                 )
#
    def __init__(self, subarray=None, _directions=None, location=None,
                 shape=None, Units=None, part=None, _dimensions=None):
        ''' 

**Initialization**

:Parameters:

    subarray : numpy array-like, optional
        The sub-array for the partition. Must be a numpy array or any
        array storing object with a similar interface.

    _directions : dict or bool, optional
        The direction of each dimension of the partition's
        sub-array. It is a boolean if the partition's sub-array is a
        scalar array, otherwise it is a dictionary keyed by the
        dimensions' identities as found in *_dimensions*.

    location : list, optional
        The location of the partition's data array in the master
        array.

    _dimensions : list, optional
        The identities of the dimensions of the partition's
        sub-array. If the partition's sub-array a scalar array then it
        is an empty list.

    part : list, optional
        The part of the partition's sub-array which comprises its data
        array. If the partition's data array is to the whole sub-array
        then *part* may be an empty list.

    shape : list, optional
        The shape of the partition's data array as a subspace of the
        master array. If the master array is a scalar array then
        *shape* is an empty list. By default the shape is inferred
        from *location*.

    Units : Units, optional
        The units of the partition's sub-array.

**Examples**

>>> p = Partition(subarray   = numpy.arange(20).reshape(2,5,1),
...               _directions = {'dim0', True, 'dim1': False, 'dim2': True},
...               location   = [(0, 6), (1, 3), (4, 5)],
...               _dimensions = ['dim1', 'dim0', 'dim2'],
...               part       = [],
...               Units      = cf.Units('K'))

>>> p = Partition(subarray       = numpy.arange(20).reshape(2,5,1),
...               _directions = {'dim0', True, 'dim1': False, 'dim2': True},
...               location   = [(0, 6), (1, 3), (4, 5)],
...               _dimensions = ['dim1', 'dim0', 'dim2'],
...               shape      = [5, 2, 1],
...               part       = [slice(None, None, -1), [0,1,3,4], slice(None)],
...               Units      = cf.Units('K'))

>>> p = Partition(subarray   = numpy.array(4),
...               _directions = True,
...               location   = [(4, 5)],
...               _dimensions = ['dim1'],
...               part       = [],
...               Units      = cf.Units('K'))

'''
        self._save = None
        '''
        asfdasdfsa

'''
        self._original = None
        '''

sdasdasdsa

'''
        self.part = part
        '''

A list defining the part of the partition's sub-array which comprises
its data array.

**Examples**

>>> p.ndim
0
>>> p.part
[]

>>> p.subarray.ndim
2
>>> p.part
[slice(None), [1,3,8,9]]

>>> p.subarray.ndim
3
>>> p.part
[[45, 7], slice(10, 3, -2), [5]]

'''

        self.location = location
        '''

A list of ranges of indices, one for each dimension of the master data
array, which describe the contiguous section of the master data array
spanned by the partition.

**Examples**

>>> p.location
[]

>>> p.location
[(2, 5), (7, 13)]

'''
        self.shape = shape
        '''

A list of the shape of the contiguous section of the master data array
spanned by the partition's data array.

**Examples**

>>> p.location
[]
>>> p.shape
[]

>>> p.location
[(2, 5), (7, 13)]
>>> p.shape
[4, 6]

'''

        self._dimensions = _dimensions
        '''

A list of the partition's sub-array dimensions.

**Examples**

>>> p.subarray.ndim
0
>>> p._dimensions
[]

>>> p.subarray.ndim
3
>>> p._dimensions
['dim3', 'dim0', 'dim1']

'''

        self._directions = _directions
        '''

A mapping of the partition's `_dimensions` to their directions.

If the partition's sub-array is a scalar then `_directions` is a
boolean, otherwise it is a dictionary.

**Examples**

>>> p._dimensions
[]
>>> p._directions
True

>>> p._dimensions
['dim3', 'dim0', 'dim1']
>>> p._directions
{'dim0': True, 'dim1': False, 'dim3': True}

'''

        self.Units = Units

        self.subarray = subarray
        '''

The sub-array which contains the partition's data array.

**Examples**

'''

#        # Set attributes from keyword arguments
#        for attr, value in kwargs.iteritems():
#            setattr(self, attr, value)

#        if self.shape is None and hasattr(self, 'location'):
        if shape is None and location is not None:
            self.shape = [i[1]-i[0] for i in location]
    #--- End: def

    def __deepcopy__(self, memo):
        '''

Used if copy.deepcopy is called on the variable.

''' 
        return self.copy()
    #--- End: def

    def __del__(self):
        '''

Called when the partition's reference count reaches zero.

If the partition contains a temporary file which is not referenced by
any other partition then the temporary file is removed from disk.

If the partition contains a non-temporary file which is not referenced
by any other partition then file is closed.

'''     
        subarray = self.subarray

        if getrefcount is not None:
            if subarray is None or getrefcount(subarray) > 2:
                return
        else:
            # getrefcount has itself been deleted or is in the process
            # of being torn down
            return

        _partition_file = getattr(subarray, '_partition_file', None)
        if _partition_file is not None:
            # This partition contains a temporary file which is not
            # referenced by any other partition, so remove the file
            # from disk.
            _remove_temporary_files(_partition_file)

        elif (numpy_ndarray is not None and 
              not isinstance(subarray, numpy_ndarray)):
            # This partition contains a non-temporary file which is
            # not referenced by any other partition, so close the
            # file.
            subarray.close()           
    #--- End: def

#    def __getstate__(self):
#        '''
#
#Called when pickling.
#
#:Parameters:
#
#    None
#
#:Returns:
#
#    out : dict
#        A dictionary of the instance's attributes
#
#**Examples**
#
#'''
#        return dict([(attr, getattr(self, attr))
#                     for attr in self.__slots__ if hasattr(self, attr)])
#    #--- End: def        
#
#    def __setstate__(self, odict):
#        '''
#
#Called when unpickling.
#
#:Parameters:
#
#    odict : dict
#        The output from the instance's `__getstate__` method.
#
#:Returns:
#
#    None
#
#'''
#        for attr, value in odict.iteritems():
#            setattr(self, attr, value)
#    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''
#        return '%s: %s' % \
#            (self.__class__.__name__,
#             dict([(attr, getattr(self, attr, None))
#                   for attr in self.__slots__])
#             )
        return '%s: %s' % (self.__class__.__name__, self.__dict__)
    #--- End: def

    # ----------------------------------------------------------------
    # Property attribute: indices (can't set or delete)
    # ----------------------------------------------------------------
    @property
    def indices(self):
        '''

The indices of the master array which correspond to this partition's
data array.

:Returns:

    out : tuple
        A tuple of slice objects or, if the master data array is a
        scalar array, an empty tuple.

**Examples**

>>> p.location
[(0, 5), (2, 9)]
>>> p.indices
(slice(0, 5), slice(2, 9))


>>> p.location
[()]
>>> p.indices
()

'''
        return tuple([slice(*r) for r in self.location])
    #--- End: def

    # ----------------------------------------------------------------
    # Property attribute: in_memory (can't set or delete)
    # ----------------------------------------------------------------
    @property
    def in_memory(self):
        '''

True if and only if the partition's sub-array is in memory as opposed
to on disk.

**Examples**

>>> p.in_memory
False

'''
        return isinstance(self.subarray, numpy_ndarray)
    #--- End: if

    # ----------------------------------------------------------------
    # Property attribute: on_disk (can't set or delete)
    # ----------------------------------------------------------------
    @property
    def on_disk(self):
        '''

True if and only if the partition's sub-array is on disk as opposed to
in memory.

**Examples**

>>> p.on_disk
True

'''
        return not isinstance(self.subarray, numpy_ndarray) # return not self.in_memory
    #--- End: if

    # ----------------------------------------------------------------
    # Property attribute: isscalar (can't set or delete)
    # ----------------------------------------------------------------
    @property
    def isscalar(self):
        '''

True if and only if the partition's data array and sub-array are both
a scalar arrays.

**Examples**

>>> p.subarray.ndim
0
>>> p.isscalar
True

>>> p.subarray.ndim
1
>>> p.isscalar
False

'''
        return not self._dimensions
    #--- End: def

    @property
    def size(self):
        '''

Number of elements in the partition's data array.

**Examples**

>>> p.shape
(73, 48)
>>> p.size
3504

'''
        shape = self.shape
        
        if not shape:
            return 0

        return long(reduce(mul, shape, 1))
    #--- End: def

    # ----------------------------------------------------------------
    # Property attribute: subarray_in_external_file (can't set or delete)
    # ----------------------------------------------------------------
    @property
    def subarray_in_external_file(self):
        '''

True if and only if the partition's sub-array is in an external file.


**Examples**

>>> p.subarray_in_external_file
False

'''

        return not (self.in_memory or isinstance(self.subarray, FileArray))
    #--- End: def

    def change_dimension_names(self, dim_name_map):
        '''

Change the dimension names.

The dimension names are arbitrary, so mapping them to another
arbitrary collection does not change the data array values, units,
dimension directions nor dimension order.

:Parameters:

    dim_name_map : dict

:Returns:

    None

**Examples**

>>> p._dimensions
['dim0', 'dim1']
>>> p._directions
{'dim0': True, 'dim1': False}
>>> p.change_dimension_names({'dim0': 'dim2', 'dim1': 'dim0'})
>>> p._dimensions
['dim2', 'dim0']
>>> p._directions
{'dim2': True, 'dim0': False}


>>> p._dimensions
['dim0', 'dim1']
>>> p._directions
{'dim0': True, 'dim1': False}
>>> p.change_dimension_names({'dim0': 'dim1'})
>>> p._dimensions
['dim1', 'dim2']
>>> p._directions
{'dim1': True, 'dim2': False}

'''
#        d = {}
#        for dim in dim_name_map:
#            if dim inself.directions

        dimensions = self._dimensions

        # Partition dimensions
        self._dimensions = [dim_name_map[dim] for dim in dimensions]
        
        # Partition dimension directions
        if dimensions:
#            self._directions = dict(
#                [(dim_name_map[dim], dir)
#                 for dim, dir in self._directions.iteritems()])
            directions = {}
            for dim, dir in self._directions.iteritems():
                directions[dim_name_map[dim]] = dir

            self._directions = directions                 
    #--- End: def

    def close(self, save=False):
        '''

Close the partition after it has been conformed.

Closing the partition is important for memory management. The
partition should always be closed after it is conformed to prevent
memory leaks.

Closing the partition does one of the following, depending on the
values of the partition's `_original` and `_save` attributes and on
the `save` parameter:

* Nothing.

* Stores the partition's data array in a temporary file.

* Reverts the entire partition to a previous state.

:Parameters:

    save : bool, optional
        If True and the partition is not to be reverted to a previous
        state then force its data array to be stored in a temporary
        file.

:Returns:

    None

**Examples**

>>> p.dataarray(...)
>>> p.close()

'''
        if self._original:
            # The whole partition is to replaced with its
            # pre-conformed state
            self.is_partition(self._original)
            self._original = None

        elif self._save or save:
            # The partition's data array is to be saved to a temporary
            # file
            self.to_disk()
            self._save    = None
            self._on_disk = True
    #--- End: def

    def copy(self):
        '''

Return a deep copy.

Equivalent to ``copy.deepcopy(p)``.

:Returns:

    out :
        A deep copy.

**Examples**

>>> q = p.copy()

'''
        new = Partition.__new__(Partition)
        new.__dict__ = self.__dict__.copy()

        return new
    #--- End: def

    def dataarray(self, dimensions=None, directions=None, units=None,
                  save=False, revert_to_file=False, hardmask=True, dtype=None,
                  readonly=False, copy_regardless=False):
        '''

Returns the partition's data array.

    
After a partition has been conformed, the partition must be closed
(with the `close` method) before another partition is conformed,
otherwise a memory leak could occur. For example:

>>> dimensions     = partition_array._dimensions
>>> directions     = partition_array._directions
>>> units          = partition_array.units
>>> save           = partition_array.save_to_disk()
>>> revert_to_file = True
>>> for partition in partition_array.flat():
...
...    # Conform the partition
...    partition.dataarray(**conform_args)
...
...    # [ Some code to operate on the conformed partition ]
...
...    # Close the partition
...    partition.close()
...
...    # Now move on to conform the next partition 
...
>>>  

:Parameters:

    dimensions : list

    directions : dict

    units : Units

    save : bool, optional
        Modifies the behaviour of the `close` method, in conjunction
        with *revert_to_file*. By default is False.

        =====  ==============  =======================================
        save   revert_to_file  Behaviour of the `close` method
        =====  ==============  =======================================
        False  True or False   The `subarray` is replaced with the
                               data array, which is kept in memory
                               when the `close` method is called.
        
        True   False           The `subarray` is replaced with the
                               data array, which is saved to a
                               temporary file on disk when the `close`
                               method is called.
        
        True   True            If the `subarray` was on disk then the
                               file pointer will be reinstated when
                               the `close` method is called.
        =====  ==============  =======================================

    revert_to_file : bool, optional
        Modifies the behaviour of the `close` method, in conjunction
        with *save*. By default is False.

    dtype : numpy.dtype, optional
        Convert the partition's data array to this data type. By
        default no conversion occurs.

    hardmask : bool, optional
        If False then force the partition's data array's mask to be
        soft. By default the mask is forced to be hard.
    
:Returns: 

    out : numpy array
        The partition's data array as a numpy array.

:Raises:

   ValueError :
       A ValueError is raised if the data type conversion specified
       with the *dtype* parameter is not possible (as would be the
       case when attempting to convert a string to a float, for
       example).

'''
        # Find out if 
        unique = getrefcount(self.subarray) <= 2

        p_dimensions = self._dimensions
        p_directions = self._directions
        p_part       = self.part
        p_units      = self.Units
        subarray     = self.subarray

        len_p_dimensions = len(p_dimensions)

        if self.on_disk:
            # --------------------------------------------------------
            # The sub-array is in a file on disk
            # --------------------------------------------------------
            if revert_to_file and save:
                self._original  = self.copy()
                self._save      = None
                _partition_file = None
            else:
                _partition_file = getattr(subarray, '_partition_file', None)
                
            if not p_part:
                indices = Ellipsis
            else:
                indices = tuple(p_part)
                self.part = []
            #--- End: if

            # Read from a file into a numpy array
            p_data = subarray[indices]

            if _partition_file and unique:
                # This partition contains a temporary file which is
                # not referenced by any other partition, so we can
                # remove the file from disk.
                _remove_temporary_files(_partition_file)
            #--- End: if
                
            # Set unique to True
            unique     = True
            not_unique = False
            copy       = False
        else:
            # --------------------------------------------------------
            # The sub-array is in a numpy array in memory
            # --------------------------------------------------------
            not_unique = not unique
            if copy_regardless:
                copy = True
            elif readonly:
                copy = False
            else:
                copy = not_unique

            p_data = subarray

            if p_part:
                p_data = subspace_array(p_data, p_part)
                self.part = []
            elif not_unique:
                p_data = subarray.view()
        #--- End: if

        masked = numpy_ma_isMA(p_data)        

        # ------------------------------------------------------------
        # Make sure that the data array has the correct units. This
        # process will deep copy the data array if required (e.g. if
        # another partition is referencing this numpy array), even if
        # the units are already correct.
        # ------------------------------------------------------------
        if not p_units.equals(units) and bool(p_units) is bool(units):

            # Make sure that we deep copy if the data array is not
            # contiguous or is of integer type
            if (not_unique or
                not p_data.flags['C_CONTIGUOUS'] or p_data.dtype.kind == 'i'):
                inplace = False
            else:
                inplace = True

            p_data = Units.conform(p_data, p_units, units, inplace=inplace)

            self.Units = units
            
            copy = False
        #--- End: if

        # ------------------------------------------------------------
        # Remove excessive size 1 dimensions
        # ------------------------------------------------------------
        if p_dimensions:
            shape = []
            temp_p_dimensions = []            
            for size, dim in izip(p_data.shape, p_dimensions):
                if dim in dimensions:
                    shape.append(size)
                    temp_p_dimensions.append(dim)
            #--- End: for
            if len(shape) != len_p_dimensions:
                p_dimensions = temp_p_dimensions
                p_data = p_data.reshape(shape)
#                if not masked:
#                    p_data.resize(shape)
#                else:
#                    p_data = numpy_ma_resize(p_data, shape)
        #--- End: if

        # ------------------------------------------------------------
        # Check for reversed dimensions
        # ------------------------------------------------------------
        if p_data.size > 1:
            reversed_dimensions = False
            indices = []
            for dim in p_dimensions:
                if p_directions[dim] != directions[dim]:
                    indices.append(slice(None, None, -1))
                    reversed_dimensions = True
                else:
                    indices.append(slice(None))
            #--- End: for
            if reversed_dimensions:
                p_data = p_data[tuple(indices)]
        #--- End: if

        # ------------------------------------------------------------
        # Insert missing size 1 dimensions
        # ------------------------------------------------------------
        if len_p_dimensions < len(dimensions):
            p_dimensions = p_dimensions[:] # Don't update in place
            for i, dim in enumerate(dimensions):
                if dim not in p_dimensions:
                    p_dimensions.insert(i, dim)
                    if not masked:
                        p_data = numpy_expand_dims(p_data, i)
                    else:
                        p_data = numpy_ma_expand_dims(p_data, i)                        
        #--- End: if

        # ------------------------------------------------------------
        # Reorder axes
        # ------------------------------------------------------------
        if p_dimensions != dimensions:
            axes = [p_dimensions.index(dim) for dim in dimensions]
            p_data = numpy_transpose(p_data, axes)

        # ------------------------------------------------------------
        # Make sure the array is a masked array and that the mask is
        # shrunk if possible
        # ------------------------------------------------------------
        if masked:
            p_data.shrink_mask()

            # ------------------------------------------------------------
            # Set the hardness of the mask
            # ------------------------------------------------------------
            if not hardmask:
                p_data.soften_mask()
            else:
                p_data.harden_mask()
         #--- End: if

        # ------------------------------------------------------------
        # If a different data type has been specified then convert the
        # data array
        # ------------------------------------------------------------
        if dtype is not None and dtype != p_data.dtype:
            try:
                p_data = p_data.astype(dtype)
            except ValueError:
                raise ValueError("Can't recast data array from %s to %s" % 
                                 (p_data.dtype.name, dtype.name))
            else:
                copy = False
        #--- End: if

        # ------------------------------------------------------------
        # Update the partition
        # ------------------------------------------------------------
        if copy:
#            print  "COPYING!", p_data.shape
            p_data = p_data.copy()

        self.subarray    = p_data
        self._dimensions = dimensions
        self._directions = directions

        return p_data
    #--- End: def

    def file_close(self):
        '''

Close all file containing the sub-array, if there is one.

:Returns:

    None

**Examples**

>>> p.file_close()

'''
        if self.on_disk:
            self.subarray.close()
    #--- End: def

#    def flat(self):
#        '''
#
#Return an iterator that yields the partition itself.
#
#This is provided as a convienience to make it easier to iterate
#through a partition matrix.
#
#:Returns:
#
#    out : generator
#        An iterator that yields the partition itself.
#
#**Examples**
#
#>>> type(p.flat())
#<generator object flat at 0x519a0a0>
#>>> for q in p.flat():
#...     print q is p
#True
#
#'''
#        yield self
#    #--- End: def

    def ndindex(self): #iterarray_indices(self):
        '''

Return an iterator over the N-dimensional indices of the partition's
data array.
 
At each iteration a tuple of indices is returned, the last dimension
is iterated over first.

:Returns:

    out : generator
        An iterator over indices of the partition's data array.

**Examples**

>>> p.shape
[2, 1, 3]
>>> for index in p.ndindex():
...     print index
...
(0, 0, 0)
(0, 0, 1)
(0, 0, 2)
(1, 0, 0)
(1, 0, 1)
(1, 0, 2)

>>> p.shape
[]
>>> for index in p.ndindex():
...     print index
...
()

'''
#        return iterindices([(0, n) for n in self.shape])
        return itertools_product(*[xrange(0, r) for r in self.shape])
    #--- End: def

    def master_ndindex(self): #itermaster_indices(self):
        '''

Return an iterator over indices of the master array which are spanned
by the conformed array.

:Returns:

    out : generator
        An iterator over indices of the master array which are spanned
        by the conformed array.

**Examples**

>>> p.location
[(3, 5), (0, 1), (0, 3)]
>>> for index in p.master_ndindex():
...     print index
...
(3, 0, 0)
(3, 0, 1)
(3, 0, 2)
(4, 0, 0)
(4, 0, 1)
(4, 0, 2)

'''
        return itertools_product(*[xrange(*r) for r in self.location])
#        return iterindices(self.location)
    #--- End: def

    def new_part(self, indices, dim2position, master_directions):
        '''

Update the `part` attribute updated for new indices of the master
array in place.

:Parameters:

    indices : list

    dim2position : dict

    master_directions : dict

:Returns:

    None

**Examples**

>>> p.new_part(indices, dim2position, master_directions)

''' 
        shape = self.shape

        if indices == [slice(0, stop, 1) for stop in shape]:
            return # part[:]
        
        # ------------------------------------------------------------
        # If a dimension runs in the wrong direction so change its
        # index to account for this.
        #
        # For example, if a dimension with the wrong direction has
        # size 10 and its index is slice(3,8,2) then after the
        # direction is set correctly, the index needs to changed to
        # slice(6,0,-2):
        #
        # >>> a = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
        # >>> a[slice(3, 8, 2)]          
        # [6, 4, 2]
        # >>> a.reverse()
        # >>> print a
        # >>> a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        # >>> a[slice(6, 0, -2)]    
        # [6, 4, 2]
        # ------------------------------------------------------------

        if self.subarray.size > 1:
            indices = indices[:]

            directions = self._directions
  
            for dim, i in dim2position.iteritems():
                      
                if directions.get(dim, None) == master_directions[dim]:
                    continue
            
                # Still here? Then this dimension runs in the wrong
                # direction

                # Reset the direction
                directions[dim] = master_directions[dim]
            
                # Modify the index to account for the changed
                # direction
                size = shape[i]

                if isinstance(indices[i], slice):
                    start, stop, step = indices[i].indices(size)
                    # Note that step is assumed to be always +ve here
                    div, mod = divmod(stop-start-1, step)
                    start = size - 1 - start
                    stop  = start - div*step - 1
                    if stop < 0:
                        stop = None
                    indices[i] = slice(start, stop, -step)
                else:
                    size -= 1
                    indices[i] = [size-j for j in indices[i]]
            #--- End: for
        #--- End: if

        # Reorder the new indices
        indices = [(indices[dim2position[dim]] 
                    if dim in dim2position else
                    slice(None))
                   for dim in self._dimensions]
        
        part = self.part

        if not part:
#            return indices
            self.part = indices
            return

        # Still here? update an existing part
        p_part = []
        for part_index, index, size in izip(part,
                                            indices, 
                                            self.subarray.shape):

            if isinstance(part_index, slice):
                if isinstance(index, slice):

                    start , stop , step  = part_index.indices(size)

                    size1, mod = divmod(stop-start-1, step)            

                    start1, stop1, step1 = index.indices(size1+1)

                    size2, mod = divmod(stop1-start1, step1)

                    if mod != 0:
                        size2 += 1
                
                    start += start1 * step
                    step  *= step1
                    stop   = start + (size2-1)*step
                    if step > 0:
                        stop += 1
                    else:
                        stop -= 1
                    if stop < 0:
                        stop = None
                    p_part.append(slice(start, stop, step))
                    continue
                else:
                    new_part = range(*part_index.indices(size))
                    new_part = [new_part[i] for i in index]
            else:
                if isinstance(index, slice):
                    new_part = part_index[index]
                else:
                    new_part = [part_index[i] for i in index]
            #--- End: if
    
            # Still here? Then the new element of p_part is a list of
            # integers, so let's see if we can convert it to a slice
            # before appending it.
            new_part0 = new_part[0]
            if len(new_part) == 1:
                # Convert a single element list to a slice object
                new_part = slice(new_part0, new_part0+1, 1)
            else:                
                step = new_part[1] - new_part0
                if step:
                    if step > 0:
                        start, stop = new_part0, new_part[-1]+1
                    else:
                        start, stop = new_part0, new_part[-1]-1
                        if new_part == range(start, stop, step):
                            if stop < 0:
                                stop = None
                            new_part = slice(start, stop, step)
            #--- End: if

            p_part.append(new_part)
        #--- End: for
    
#        return p_part
        self.part = p_part
    #--- End: def

    def overlaps(self, indices):
        '''
    
:Parameters:

   indices : sequence
       Indices describing a subset of the master array. Each index is
       either a slice object or a list.

:Returns:

    p_indices, shape : tuple, list or None, None
        If the partition overlaps the indices then return a list of
        indices which will subset the partition's data to where it
        overlaps the master indices and the subsetted partition's
        shape as a list. Otherwise return`(None, None)`.

**Examples**

>>> indices = (slice(None), slice(5, 1, -2), [1, 3, 4, 8])
>>> p.overlaps(indices)
(slice(), ddfsfsd), [3, 5, 4]

'''
        p_indices = []
        shape     = []
    
        for index, (r0, r1), size in izip(indices, 
                                          self.location, self.shape):
            
            if isinstance(index, slice):
                stop = size
                if index.stop < r1:
                    stop -= (r1 - index.stop)
                  
                start = index.start - r0
                if start < 0:
                    start %= index.step   # start is now +ve
                
                if start >= stop:
                    # This partition does not span the slice
                    return None, None
                    
                # Still here?
                step = index.step
                index = slice(start, stop, step)
                index_size, rem = divmod(stop-start, step)
                if rem:
                    index_size += 1

            else:
                if not set(index).intersection(xrange(r0, r1)):
                    # This partition does not span the slice
                    return None, None
                
                # Still here?
                index = [i - r0 for i in index if r0 <= i < r1]
                index_size = len(index)
                if index == range(index_size):
                    index = slice(0, index_size, 1)
                elif index == range(index_size-1, -1, -1):
                    index = slice(index_size-1, None, -1)
            #--- End: if

            p_indices.append(index)
            shape.append(index_size)
        #--- End: for
            
        # Still here? Then this partition does span the slice and the
        # elements of this partition specified by p_indices are in the
        # slice.
        return tuple(p_indices), shape
    #--- End: def

    def to_disk(self):
        '''

Store the partition's sub-array in a temporary file on disk in place.

Assumes that the partition's sub-array is currently in memory, but
this is not checked.

:Returns:

    None

**Examples**

>>> p.to_disk()

'''
        self.subarray = FileArray(self.subarray)  

        _temporary_files.add(self.subarray._partition_file)
    #--- End: if

    def is_partition(self, other):
        '''

Completely update the partition with another partition's attributes in
place.

The updated partitionasdasdasdasdasds is always dependent of the other partition.

:Parameters:

    other : Partition

:Returns:

    None

**Examples**

>>> p.is_partition(q)

'''
        self.__dict__ = other.__dict__

        self._original = None
        self._save     = None
    #--- End: def

    def update_inplace_from(self, other):
        '''

Completely update the partition with another partition's attributes in
place.

:Parameters:

    other : Partition

:Returns:

    None

**Examples**

>>> p.update_inplace_from(q)

'''
        self.__dict__ = other.__dict__.copy()

        self._original = None
        self._save     = None
    #--- End: def

#--- End: class
