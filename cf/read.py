from struct import unpack as struct_unpack

from glob import glob
from os   import SEEK_SET

from .field     import Field
from .fieldlist import FieldList
from .functions import flat
from .aggregate import aggregate as cf_aggregate

from .netcdf.read import read as netcdf_read
from .pp.read     import read as pp_read
from .pp.read     import _stash2standard_name
from .pp.read     import _test_umversion
from .pp.read     import _test_pp_condition

def read(files, verbose=False, index=None, ignore_ioerror=False, aggregate=True,
         umversion=4.5, squeeze=False, unsqueeze=False, format=None,
         prop={}, attr={}, coord={}, cellsize={}):
    '''

Read fields from files from disk or from an OPeNDAP server.

Any amount of any combination of CF-netCDF, CFA-netCDF and Met Office
(UK) PP format files be input for reading.

**CF-netCDF files**
   * The netCDF variable names of the field and its components are
     stored in their `ncvar` attributes.
   
   * Note that fields may be selected by netCDF variable name by
     setting a value (or values) of ``'ncvar'`` via the *attr*
     parameter.
   
   * Fields referenced within CF-netCDF formula_terms or ancillary
     variables are not included in the returned list of fields.

**PP files**
   * If any PP files are read then the *aggregate* option
     ``'no_strict_units'`` is set to True for all input files.
    
   * STASH code to standard conversion uses the table in
     ``cf/etc/STASH_to_CF.txt``.

**Files on an OPeNDAP server**
   * All files on OPeNDAP servers are assumed to be CF-netCDF or
     CFA-netCDF files.

**CFA-netCDF files**
   * See the notes for CF-netCDF files.
    
:Parameters:

    files : (arbitrarily nested sequence of) str
        A string or arbitrarily nested sequence of strings giving the
        file names or OPenDAP URLs from which to read fields. When on
        disk, the file names may contain UNIX file name metacharacters
        as understood by the python `glob` module.
    
    index : int, optional
        Only return the field with this non-negative index in the
        otherwise returned full list of fields. By default return all
        (otherwise selected) fields from the input files.
    
    verbose : bool, optional
        If True then print information to stdout.
    
    umversion : int or float, optional
        For PP format files only, the Unified Model (UM) version to be
        used when decoding the PP header. Valid versions are, for
        example, ``4.2``, ``'6.6.3'`` and ``'8.2'``.  The default
        version is ``4.5``. The version is ignored if it can be
        inferred from the PP headers, which will generally be the case
        for files created at versions 5.3 and later. Note that the PP
        header can not encode tertiary version elements (such as the
        ``3`` in ``'6.6.3'``), so it may be necessary to provide a UM
        version in such cases.

        Ignored for any non-PP input files.

    ignore_ioerror : bool, optional
        If True then ignore any file which raises an IOError whilst
        being read, as would be the case for an empty file, unknown
        file format, etc. By default the IOError is raised.
    
    format : str, optional
        Only read files of the given format, ignoring all other
        files. Valid formats are ``'NETCDF'`` for CF-netCDF files,
        ``'CFA'`` for CFA-netCDF files and ``'PP'`` for PP files. By
        default files of any of these formats are read.

    aggregate : bool or dict, optional
        If True or a dictionary then aggregate the fields read in from
        all input files into as few fields as possible using the CF
        aggregation rules. If a dictionary then it is passed as
        keyword arguments to the `cf.aggregate` function. If False
        then the fields are not aggregated.

    squeeze : bool, optional
        If True then remove size 1 dimensions from each field's data
        array.

    unsqueeze : bool, optional
        If True then insert size 1 dimensions from each field's domain
        into its data array.

    prop : dict, optional
        Only read fields matching the given conditions on their CF
        properties. Refer to the field's `~Field.match` method for
        details.

    attr : dict, optional
        Only read fields matching the given conditions on their
        attributes. Refer to the field's `~Field.match` method for
        details.

    coord : dict, optional
        Only read fields matching the given conditions on their
        coordinates. Refer to the field's `~Field.match` method for
        details.

    cellsize : dict, optional
        Only read fields matching the given conditions on their
        coordinates' cellsizes. Refer to the field's `~Field.match`
        method for details.

:Returns:
    
    out : FieldList
        A list of fields.

:Raises:

    IOError :
        Raised if *ignore_ioerror* is False and there was an I/0
        related failure, including unknown file format.


**Examples**

>>> f = cf.read('file*.nc')
>>> type(f)
<class 'cf.field.FieldList'>
>>> f
[<CF Field: pmsl(30, 24)>,
 <CF Field: z-squared(17, 30, 24)>,
 <CF Field: temperature(17, 30, 24)>,
 <CF Field: temperature_wind(17, 29, 24)>]

>>> cf.read('file*.nc')[0:2]
[<CF Field: pmsl(30, 24)>,
 <CF Field: z-squared(17, 30, 24)>]

>>> cf.read('file*.nc', index=0)
[<CF Field: pmsl(30, 24)>]

>>> cf.read('file*.nc')[-1]
<CF Field: temperature_wind(17, 29, 24)>

>>> cf.read('file*.nc', prop={'units': 'K'})
[<CF Field: temperature(17, 30, 24)>,
 <CF Field: temperature_wind(17, 29, 24)>]

>>> cf.read('file*.nc', attr={'ncvar': 'ta'})
[<CF Field: temperature(17, 30, 24)>]

>>> cf.read('file*.nc', prop={'standard_name': '.*pmsl*', 'units':'K|Pa'})[0]
<CF Field: pmsl(30, 24)>

>>> cf.read('file*.nc', prop={'units':['K, 'Pa']})
[<CF Field: pmsl(30, 24)>,
 <CF Field: temperature(17, 30, 24)>,
 <CF Field: temperature_wind(17, 29, 24)>]

'''
    if squeeze and unsqueeze:
        raise ValueError("Can not squeeze and unsqueeze whilst reading")



    # Initialize the output list of fields
    field_list = FieldList()

    if isinstance(aggregate, dict):
        aggregate_options = aggregate.copy()
        aggregate         = True
    else:
        aggregate_options = {}

    aggregate_options['copy'] = False

    if umversion is not None:
        umversion = str(umversion)
        umversion = umversion.replace('.', '0', 1)

    # Count the number of fields (in all files) and the number of
    # files
    field_counter = -1
    file_counter  = 0

    for file_glob in flat(files):

        if file_glob.startswith('http://'):
            # Do not glob a URL
            files2 = (file_glob,)
        else:
            # Glob files on disk
            files2 = glob(file_glob)
            if not files2 and not ignore_ioerror:
                open(file_glob, 'rb')
        #--- End: if

        for filename in files2:
            # Print some informative messages
            if verbose and index is None:
                print 'File: %s' % filename

            # --------------------------------------------------------
            # Read the file into fields
            # --------------------------------------------------------
            fields = _read_a_file(filename,
                                  ignore_ioerror=ignore_ioerror,
                                  verbose=verbose,
                                  aggregate=aggregate,
                                  aggregate_options=aggregate_options,
                                  umversion=umversion,
                                  file_format=format)
            
            # ----------------------------------------------------------------
            # Select matching fields
            # ----------------------------------------------------------------
            fields = fields.select(prop=prop, attr=attr,
                                   coord=coord, cellsize=cellsize)

            # --------------------------------------------------------
            # Add this file's fields to those already read from other
            # files
            # --------------------------------------------------------
            field_list.extend(fields)
   
            field_counter = len(field_list)
            file_counter += 1

            # Print some informative messages
            if verbose and index is None:
                i = field_counter - len(fields)
                for f in fields:
                    print '%d: %s' % (i, repr(f))
                    i += 1
            #--- End: if

            # --------------------------------------------------------
            # If we only want one field from all input files then
            # break now if we have got it
            # --------------------------------------------------------
            if index is not None and field_counter >= index+1:
                break
        #--- End: for
            
        # ------------------------------------------------------------
        # If we only one field from all input files then break now if
        # we have got it
        # ------------------------------------------------------------
        if index is not None and field_counter >= index:
            break
    #--- End: for     

    # Error check
    if not ignore_ioerror:
        if not file_counter:
            raise RuntimeError('No files found')
        if not field_list:
            raise RuntimeError('No fields found from '+str(file_counter)+' files')
    #--- End: if

    if index >= len(field_list):
        raise IndexError('FieldList index='+str(index)+' is out of range')

    # Print some informative messages
    if verbose:
        if index is None:  
            print("Read %d field%s from %d file%s" % 
                  (field_counter, ('s' if field_counter!=1 else ''),
                   file_counter , ('s' if file_counter !=1 else '')))
        else:
            print 'File: %(filename)s' % locals()
            print '%d: %s' % (0, repr(field_list[index]))
            print 'Read 1 field from 1 file'
    #--- End: if
    
    # ----------------------------------------------------------------
    # 
    # ----------------------------------------------------------------
    try:
        field_list = field_list[index]
    except TypeError:
        pass

    # ----------------------------------------------------------------
    # Aggregate the output fields
    # ----------------------------------------------------------------    
    if aggregate and len(field_list) > 1:
        if verbose:
            org_len = len(field_list)
            
        field_list = cf_aggregate(field_list, **aggregate_options)
        
        if verbose:
            nfields = len(field_list)
            print('%d input field%s aggregated into %d field%s' %
                  (org_len, ('' if org_len==1 else 's'), 
                   nfields, ('' if nfields==1 else 's')))
    #--- End: if

    # Add standard names to PP fields
    for f in field_list:
        p = getattr(f, 'pp', None)
        if p is None:
            continue

        _pp_standard_name(f)
    #--- End: for

    # ----------------------------------------------------------------
    # Squeeze size one dimensions from the data arrays. Do one of:
    # 
    # 1) Squeeze the fields, i.e. remove all size one dimensions from
    #    all field data arrays
    #
    # 2) Unsqueeze the fields, i.e. Include all size 1 domain
    #    dimensions in the data array.
    #
    # 3) Nothing
    # ----------------------------------------------------------------
    if squeeze:
        field_list.squeeze()
    elif unsqueeze:
        field_list.unsqueeze()

    return field_list
#--- End: def

def _read_a_file(filename,
                 aggregate=True,
                 aggregate_options={},
                 ignore_ioerror=False,
                 verbose=False,
                 umversion=None,
                 file_format=None):
    '''

Read the contents of a single file into a field list.

:Parameters:

    filename : str
        The file name.

    aggregate_options : dict, optional
        The keys and values of this dictionary may be passed as
        keyword parameters to an external call of the aggregate
        function.

    ignore_ioerror : bool, optional
        If True then return an empty field list if reading the file
        produces an IOError, as would be the case for an empty file,
        unknown file format, etc. By default the IOError is raised.
    
    verbose : bool, optional
        If True then print information to stdout.
    
:Returns:

    out : FieldList
        The fields in the file.

:Raises:

    IOError :
        If *ignore_ioerror* is False and

        * The file can not be opened.

        * The file can not be opened.

'''
    # Find this input file's format
    if filename.startswith('http://'):
        format = 'netCDF'
        openfile = filename
    else:
        try:
            openfile = open(filename, 'rb')             
        except IOError as io_error:
            if ignore_ioerror:
                if verbose:
                    print('WARNING: Ignoring IOError: %s' % io_error)
                return FieldList()
            raise io_error
        #--- End: try

        try:
            format = _file_format(openfile)        
        except IOError as io_error:
            if ignore_ioerror: 
                if verbose:
                    print('WARNING: Ignoring IOError: %s' % io_error)
                return FieldList()
            raise io_error
        #--- End: try
    #--- End: if

    # ----------------------------------------------------------------
    # Still here? Read the file into fields.
    # ----------------------------------------------------------------
    if format == 'netCDF' and (file_format in (None, 'NETCDF', 'CFA')):
        fields = netcdf_read(openfile, file_format=file_format) #only_nca=only_nca, not_nca=not_nca)
        
    elif format == 'PP' and (file_format in (None, 'PP')):
        fields = pp_read(openfile, umversion=umversion)

        # PP fields are aggregated intrafile prior to interfile
        # aggregation
        if aggregate:
            # For PP fields, the default is strict_units=False
            if 'strict_units' not in aggregate_options:
                aggregate_options['no_strict_units'] = True
    
            # Aggregate the fields in this file (to help deal with massive
            # numbers of fields)
#            fields = cf_aggregate(fields, **aggregate_options)
        #--- End: if

    # Developers: Add more file formats here ...

    else:
        fields = FieldList()

    # ----------------------------------------------------------------
    # Return the fields
    # ----------------------------------------------------------------
    return fields
#--- End: def

def _file_format(openfile):
    '''

Read and interpret a file's magic number.

Takes a file object as input and assumes that we're currently pointing
to the beginning of the file. The file is rewound to the beginning
after reading the magic number.

Developers: For each new file format that is supported, another 'if'
clause needs to be added to this function.

:Parameters:

    openfile : file
        A python file object.

:Returns:

    out : str
        The format of the file.

:Raises:
 
    IOError :
        If the file has an unsupported format.

**Examples**

>>> try:
...     format = _file_format(openfile)        
... except IOError:
...     # Do something
... else:
...     # Do something else

''' 
    # Read the magic number    
    try:
        magic_number = struct_unpack('=L', openfile.read(4))[0]
    except struct.error:
        raise IOError("File %s is empty (contains fewer than 4 words)" %
                      openfile.name)

    # Reset the pointer to the beginning of the file
    openfile.seek(0, SEEK_SET)

    # ----------------------------------------------------------------
    # netCDF
    # ----------------------------------------------------------------
    if magic_number in (21382211, 1128547841, 1178880137, 38159427):
        return 'netCDF'

    # ----------------------------------------------------------------
    # PP
    # ----------------------------------------------------------------
    if magic_number in (256, 65536):
        return 'PP'

    # ----------------------------------------------------------------
    # Developers: Add more file formats here ...
    # ----------------------------------------------------------------

    # Still here?
    raise IOError("File %s has unsupported format: Magic number=%d" % 
                  (openfile.name, magic_number))
#--- End: def

def _pp_standard_name(f):
    # ----------------------------------------------------------------
    # Set the aggregated PP field's standard_name
    # ----------------------------------------------------------------
    p = f.pp

    submodel   = f.getprop('submodel')
    stash_code = f.getprop('stash_code')

    # The STASH code has been set in the PP header, so try to find
    # its standard_name from the conversion table
    if (submodel, stash_code) in _stash2standard_name:
        for (long_name, 
             units,
             valid_from,
             valid_to, 
             standard_name,
             cf_info,
             pp_condition) in _stash2standard_name[(submodel, stash_code)]:

            # Check that conditions are met             
            version_ok      = _test_umversion(valid_from, valid_to, p=p)
            pp_condition_ok = _test_pp_condition(pp_condition, p=p)
    
            if not (version_ok and pp_condition_ok):                    
                continue
    
            # Still here? Then we have our standard_name, etc.
            if standard_name:
                f.standard_name = standard_name
                del f.id

            break
        #--- End: for
    #--- End: if
