import numpy

from numpy import empty   as numpy_empty
from numpy import ndarray as numpy_ndarray

from itertools import izip
from copy import deepcopy

from .functions        import parse_indices
from .coordinatebounds import CoordinateBounds
from .variable         import Variable, SubspaceVariable

from .data.data import Data

# ====================================================================
#
# Coordinate object
#
# ====================================================================

class Coordinate(Variable):
    '''

A CF dimension or auxiliary coordinate construct.

The coordinate array's bounds (if present) are stored in the `bounds`
attribute. The `climatology` CF property indicates if the bounds are
intervals of climatological time.

If the coordinate is connected to one or more transform constructs
then pointers to the transformations are stored in the `transforms`
attribute.

'''

#    _special_properties = Variable._special_properties.union(set(('bounds',
#                                                                  'climatology',
#                                                                  'transforms',
#                                                                  )))

    def __init__(self, properties={}, data=None, attributes={}, bounds=None,
                 copy=True):
        '''

**Initialization**

:Parameters:

    properties : dict, optional
        Initialize a new instance with CF properties from the
        dictionary's key/value pairs. Values are deep copied.

    data : cf.Data, optional
        Provide the new instance with an N-dimensional data array. The
        array is deep copied.

    attributes : dict, optional
        Provide the new instance with attributes from the dictionary's
        key/value pairs. Values are deep copied.

    bounds : cf.Data, optional
        Provide the new instance with cell bounds.

    copy : bool, optional
        If False then do not copy arguments prior to
        initialization. By default arguments are deep copied.

'''         
        self._isbounded = False
        '''

True if and only if there are cell bounds.

**Examples**

>>> print hasattr(c, 'bounds')
True
>>> print c._isbounded
True
>>> del c.bounds
>>> print c._isbounded
False

'''
        # Initialize the new field with attributes and CF properties
        # and data
        super(Coordinate, self).__init__(properties=properties,
                                         attributes=attributes,
                                         data=data,
                                         copy=copy)

        # Bounds
        if bounds is not None:
            self.insert_bounds(bounds, copy=copy)

        # Set default standard names based on units
    #--- End: def 

    def _binary_operation(self, other, method):
        '''
'''
        self_bounds = getattr(self, 'bounds', None)

        if isinstance(other, self.__class__):
            other_bounds = getattr(other, 'bounds', None)
            if other_bounds:
                if not self_bounds:
                    raise TypeError("asdfsdfdfds 0000")
            elif self_bounds:
                raise TypeError("asdfsdfdfds 00001")

        elif isinstance(other, (float, int, long)):
            other_bounds = other

        elif isinstance(other, numpy_ndarray):
            if self_bounds:
                if other.size > 1:
                    raise TypeError("4444444")
                other_bounds = other
        #-- End: if

        new = super(Coordinate, self)._binary_operation(other, method)

        if self_bounds:
            bounds = self_bounds._binary_operation(other_bounds, method)

        inplace = method[2] == 'i'

        if not inplace:
            new.bounds = bounds
            return new
        else: 
            self.bounds = bounds
            return self
    #--- End: def        

    def asauxiliary(self, copy=True):
        '''

Return the coordinate recast as an auxiliary coordinate.

:Parameters:

    copy : bool, optional
        If False then the returned auxiliary coordinate is not
        independent. By default the returned auxiliary coordinate is
        independent.

:Returns:

    out : cf.AuxiliaryCoordinate
        The coordinate recast as an auxiliary coordinate.

**Examples**

>>> d = c.asauxiliary()
>>> d = c.asauxiliary(copy=False)

'''
        return AuxiliaryCoordinate(attributes=self.attributes,
                                   properties=self.properties,
                                   data=getattr(self, 'Data', None),
                                   bounds=getattr(self, 'bounds', None),
                                   copy=copy)
    #--- End: def

    def asdimension(self, copy=True):
        '''

Return the coordinate recast as a dimension coordinate.

:Parameters:

    copy : bool, optional
        If False then the returned dimension coordinate is not
        independent. By default the returned dimension coordinate is
        independent.

:Returns:

    out : cf.DimensionCoordinate
        The coordinate recast as a dimension coordinate.

**Examples**

>>> d = c.asdimension()
>>> d = c.asdimension(copy=False)

'''        
        if self._hasData:
            if self.ndim > 1:
                raise ValueError(
                    "Error: Dimension coordinate must be 1-d (not %d-d)" %
                    self.ndim)
        elif self._isbounded:
            if self.bounds.ndim > 2:
                raise ValueError(
                    "Error: Dimension coordinate must be 1-d (not %d-d)" %
                    self.ndim)

        return DimensionCoordinate(attributes=self.attributes,
                                   properties=self.properties,
                                   data=getattr(self, 'Data', None),
                                   bounds=getattr(self, 'bounds', None),
                                   copy=copy)
    #--- End: def

#    def _infer_direction(self):
#        '''
#    
#Return True if a coordinate is increasing, otherwise return False.
#
#A coordinate is considered to be increasing if its *raw* data array
#values are increasing in index space or if it has no data not bounds
#data.
#
#If the direction can not be inferred from the coordinate's data, then
#the `positive` CF property is used, if present, or the coordinate's
#units.
#
#The direction is inferred from the coordinate's data array values or
#its from coordinates. It is not taken directly from its `cf.Data`
#object.
#
#:Returns:
#
#    out : bool
#        Whether or not the coordinate is increasing.
#        
#**Examples**
#
#>>> c.array
#array([  0  30  60])
#>>> c._get_direction()
#True
#>>> c.array
#array([15])
#>>> c.bounds.array
#array([  30  0])
#>>> c._get_direction()
#False
#
#'''
#        # Then infer the direction from the dimension
#        # coordinate's array, if it has one.
#        if self._hasData:
#            c = self.Data
#            if c._size > 1:
#                c = c[0:2]._array
#                return c.item(0) < c.item(1)
#        #--- End: if
#
#        # Still here? Then infer the direction from the dimension
#        # coordinate's bounds, if it has any.
#        if self._isbounded:
#            b = self.bounds
#            if b._hasData:
#                b = b.Data
#                b = b[(0,)*(b.ndim-1)]._array
#                return b.item(0) < b.item(1)
#        #--- End: if
#
#        # Still here? Then infer the direction from the dimension
#        # coordinate's positive CF property, if it has one.
#        if self.hasprop('positive'): #hasattr(self, 'positive'):
#            if self.positive.lower().startswith('d'): # d for down
#                return False
#        #--- End: if
#
#        # Still here? Then infer the direction from the units
#        return not self.Units.ispressure
#    #--- End: def
#
#    def set_direction(self, direction):
#        '''
#
#Set the coordinate's direction.
#
#:Parameters:
#
#    direction : bool, optional
#
#:Returns:
#
#    None
#
#**Examples**
#
#>>> c.set_direction(False)
#
#'''
#        # ------------------------------------------------------------
#        # This method sets the _direction_is_set attribute to True
#        # ------------------------------------------------------------
#
#        if self.isscalar:
#            self._override_directions(None, direction)
#        elif self._hasData:
#            self._override_directions({self.Data.dimensions[0]: direction}, None)
#        elif self.isbounded:
#            bounds = self.bounds
#            if bounds._hasData:
#                self._override_directions({bounds.Data.dimensions[0]: direction}, None)
#
#        self._direction_is_set = True
#    #--- End: def

    def _override_directions(self, directions, scalar_dir):
        '''

:Returns: 

    None

**Examples**

'''
        super(Coordinate, self)._override_directions(directions, scalar_dir)

        if self._isbounded:
            bounds = self.bounds
            if not bounds._hasData:
                return

            if self.isscalar:
                directions = {bounds.Data.dimensions[0]: scalar_dir}
            elif self._hasData:
                self_data = self.Data
                c_directions = self_data.directions
                b_dimensions = bounds.Data.dimensions

                directions = {}
                for c_dim, b_dim in izip(self_data.dimensions, b_dimensions):
                    directions[b_dim] = c_directions[c_dim]

                if self_data.ndim == 1:
                    directions[b_dimensions[1]] =  directions[b_dimensions[0]]
            #--- End: if

            bounds._override_directions(directions, None)
        #--- End: if
    #--- End: def

#    def _equivalent(self, other, transpose=None,
#                         rtol=None, atol=None):
#        '''
#
#'''        
#        identity0 = self.identity()
#        identity1 = other.identity()
#
#        if not (identity0 is not None and identity0 == identity1):
#            return False
#
#        if not super(Coordinate, self)._equivalent_data(other, 
#                                                        transpose=transpose,
#                                                        rtol=rtol, atol=atol):
#            return False
#        #--- End: if
#
#        # Compare the bounds
#        bounds       = getattr(self,  'bounds', None)
#        other_bounds = getattr(other, 'bounds', None)
#        if bounds is not None:
#            if other_bounds is None:
#                return False
#
#            if tranpose:
#               tranpose = tranpose.copy()
#               tranpose['bounds'] = 'bounds'
#            #--- End: if
#
#            return bounds._equivalent_data(other_bounds, tranpose=transpose,
#                                           rtol=rtol, atol=atol)
#
#        elif other_bounds is not None:
#            return False
#
#        # Still here? Then the data are equivalent.
#        return True
#    #--- End: def

    def _equivalent_data(self, other, transpose=None,
                         rtol=None, atol=None):
        '''

'''
        # Compare the data arrays
        if not super(Coordinate, self)._equivalent_data(other, 
                                                        transpose=transpose,
                                                        rtol=rtol, atol=atol):
            return False
        #--- End: if

        # Compare the bounds' data arrays
        bounds       = getattr(self,  'bounds', None)
        other_bounds = getattr(other, 'bounds', None)
        if bounds is not None:
            if other_bounds is None:
                # Only one coordinate has bounds
                return False

            # Both coordinates have bounds
            if transpose:
               transpose = transpose.copy()
               transpose[bounds.Data.dimensions[-1]] = bounds.Data.dimensions[-1]
            #--- End: if

            return bounds._equivalent_data(other_bounds, transpose=transpose,
                                           rtol=rtol, atol=atol)

        elif other_bounds is not None:
            # Only one coordinate has bounds
            return False

        # Still here? Then the data are equivalent.
        return True
    #--- End: def

#    def _set_Data_attributes(self, dimensions, directions):
#        '''
#        Set the metadata.    
#
#:Parameters:
#
#    dimensions : list
#
#    directions : dict
#
#:Returns:
#
#    None
#
#'''
#        super(Coordinate, self)._set_Data_attributes(dimensions, directions)
#
#        # ------------------------------------------------------------
#        # Set the metadata for its bounds' data
#        # ------------------------------------------------------------
#        if self._isbounded:
#
#            c = self.Data
#            b = self.bounds.Data
#
#            # Units. Give them the coordinate's units. Note that we
#            # don't want to change any units set on the bounds
#            b.Units = c.Units.copy()
#            if not b.Units:
#                b.partitions[0].Units = b.Units.copy()  # dch check
#                
#            # Order
##            dim_name_map = dict([(dim, new_dim) 
# #                                for dim, new_dim in izip(b.dimensions[:-1],
# #                                                         dimensions)])
##            dim_name_map[b.dimensions[-1]] = 'bounds'
#
#            dim_name_map = {b.dimensions[-1]: 'bounds'}
#            for dim, new_dim in izip(b.dimensions[:-1], dimensions):
#                dim_name_map[dim] = new_dim
#                                 
#            b.change_dimension_names(dim_name_map)
#     
#            # Direction
#            if c.isscalar:
#                b.directions = {'bounds': c.directions}
#
#            else:
#                b.directions = c.directions.copy()
#                if c.ndim == 1:
#                    # 1-d coordinate
#                    b.directions['bounds'] = c.directions[c.dimensions[0]]
#                else:
#                    # N-d coordinate
#                    b.directions['bounds'] = True
#            #--- End: if
#
#            b.partitions[0].directions = b.directions.copy()
#
##            for partition in b.partitions.flat:
##                partition.directions = b.directions.copy()
#        #--- End: if
#    #--- End: def
        
    # ----------------------------------------------------------------
    # Attribute: attributes (read only)
    # ----------------------------------------------------------------
    @property
    def attributes(self):
        '''

A dictionary of the attributes which are not CF properties.

**Examples**

>>> c.attributes
{}

>>> c.foo = 'bar'
{'foo': 'bar'}

'''
        attributes = super(Coordinate, self).attributes
        attributes.pop('_isbounded')
        return attributes
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: dtype
    # ----------------------------------------------------------------
    @property
    def dtype(self):
        '''

Numpy data-type of the data array.

**Examples**

>>> c.dtype
dtype('float64')
>>> import numpy
>>> c.dtype = numpy.dtype('float32')

'''
        if self._hasData:
            return self.Data.dtype

        if self._isbounded:
            return self.bounds.dtype

        raise AttributeError("%s doesn't have attribute 'dtype'" %
                             self.__class__.__name__)
    #--- End: def
    @dtype.setter
    def dtype(self, value):
        if self._hasData:
            self.Data.dtype = value

        if self._isbounded:
            self.bounds.dtype = value
    #--- End: def

    @property
    def subspace(self):
        '''

Return a new coordinate whose data and bounds are subspaced in a
consistent manner.

This attribute may be indexed to select a subspace from dimension
index values.

**Subspacing by indexing**

Subspacing by dimension indices uses an extended Python slicing
syntax, which is similar numpy array indexing. There are two
extensions to the numpy indexing functionality:

* Size 1 dimensions are never removed.

  An integer index i takes the i-th element but does not reduce the
  rank of the output array by one.

* When advanced indexing is used on more than one dimension, the
  advanced indices work independently.

  When more than one dimension's slice is a 1-d boolean array or 1-d
  sequence of integers, then these indices work independently along
  each dimension (similar to the way vector subscripts work in
  Fortran), rather than by their elements.

**Examples**

'''
        return SubspaceCoordinate(self)
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: Units
    # ----------------------------------------------------------------
    @property
    def Units(self):
        '''
The Units object containing the units of the data array.

'''
        return Variable.Units.fget(self)
    #--- End: def

    @Units.setter
    def Units(self, value):
        Variable.Units.fset(self, value)

        # Set the Units object on the bounds
        if self._isbounded:
            self.bounds.Units = value #value.copy()
    #--- End: def
    
    @Units.deleter
    def Units(self):
        Variable.Units.fdel(self)
        
        # Delete the Units object from the bounds
        if self._isbounded:
            del self.bounds.Units
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: units (a special attribute)
    # ----------------------------------------------------------------
    # DCH possible inconsistency when setting self.Units.units
    @property
    def units(self):
        '''

The units CF property.

This property is a mirror of the units stored in the `Units`
attribute.

**Examples**

>>> c.units = 'degrees_east'
>>> c.units
'degree_east'
>>> del c.units

>>> f.setprop('units', 'days since 2004-06-01')
>>> f.getprop('units')
'days since 2004-06-01'
>>> f.delprop('units')

'''
        return Variable.units.fget(self)
    #--- End: def

    @units.setter
    def units(self, value):
        Variable.units.fset(self, value)

        # Set the units on the bounds        
        if self._isbounded:
            self.bounds.units = value 
    #--- End: def
    
    @units.deleter
    def units(self):
        Variable.units.fdel(self)
        
        # Delete the units from the bounds
        if self._isbounded:
            del self.bounds.units
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: axis
    # ----------------------------------------------------------------
    @property
    def axis(self):
        '''

The axis CF property.

**Examples**

>>> c.axis = 'Y'
>>> c.axis
'Y'
>>> del c.axis

>>> f.setprop('axis', 'T')
>>> f.getprop('axis')
'T'
>>> f.delprop('axis')

'''
        return self.getprop('axis')
    #--- End: def
    @axis.setter
    def axis(self, value): self.setprop('axis', value)
    @axis.deleter
    def axis(self):        self.delprop('axis')

    # ----------------------------------------------------------------
    # CF property: positive (a simple attribute)
    # ----------------------------------------------------------------
    @property
    def positive(self):
        '''

The positive CF property.

**Examples**

>>> c.positive = 'up'
>>> c.positive
'up'
>>> del c.positive

>>> f.setprop('positive', 'down')
>>> f.getprop('positive')
'down'
>>> f.delprop('positive')

'''
        return self.getprop('positive')
    #--- End: def

    @positive.setter
    def positive(self, value): self.setprop('positive', value)
    @positive.deleter
    def positive(self):        self.delprop('positive')

    # ----------------------------------------------------------------
    # CF property: calendar (a special attribute)
    # ----------------------------------------------------------------
    @property
    def calendar(self):
        '''
The calendar CF property.

This property is a mirror of the calendar stored in the `Units`
attribute.

**Examples**

>>> c.calendar = 'noleap'
>>> c.calendar
'noleap'
>>> del c.calendar

>>> f.setprop('calendar', 'proleptic_gregorian')
>>> f.getprop('calendar')
'proleptic_gregorian'
>>> f.delprop('calendar')

'''
        return Variable.calendar.fget(self)
    #--- End: def

    @calendar.setter
    def calendar(self, value):
        Variable.calendar.fset(self, value)
        
        # Set the calendar on the bounds
        if self._isbounded:
            self.bounds.calendar = value
    #--- End: def

    @calendar.deleter
    def calendar(self):
        Variable.calendar.fdel(self)
        
        # Delete the calendar from the bounds
        if self._isbounded:
            del self.bounds.calendar
    #--- End: def
   
    # ----------------------------------------------------------------
    # Attribute: bounds (a special attribute)
    # ----------------------------------------------------------------
    @property
    def bounds(self):
        '''

The `cf.CoordinateBounds` object containing the data array's cell
bounds.

**Examples**

>>> c.bounds
<CF CoordinateBounds: >

'''
        return self._get_special_attr('bounds')
    #--- End: def
    @bounds.setter
    def bounds(self, value): 
        self._set_special_attr('bounds', value)        
        self._isbounded = True
    #--- End: def
    @bounds.deleter
    def bounds(self):  
        self._del_special_attr('bounds')
        self._isbounded = False
    #--- End: def

#    # ----------------------------------------------------------------
#    # CF property: climatology (a special attribute)
#    # ----------------------------------------------------------------
#    @property
#    def climatology(self):
#        '''
#
#Indicator for the coordinate array's bounds representing
#climatological time intervals.
#
#If not set then bounds are assumed to not represent climatological
#time intervals.
#
#**Examples**
#
#>>> c.climatology = True
#>>> c.climatology
#True
#>>> del c.climatology
#
#'''
#        value = self._get_special_attr('climatology', None)
#        if value is None:
#            self._del_special_attr('climatology')
#            raise AttributeError("%s doesn't have attribute 'climatology'" %
#                                 self.__class__.__name__)
#        return value
#    #--- End: def
#    @climatology.setter
#    def climatology(self, value): self._set_special_attr('climatology', value)
#    @climatology.deleter
#    def climatology(self):        self._del_special_attr('climatology')
   
    # ----------------------------------------------------------------
    # Attribute: islatitude
    # ----------------------------------------------------------------
    @property
    def islatitude(self):
        '''
True if and only if the coordinate represents latitude.

This is the case only if any of the following are true:

* The `units` attribute is one of ``'degrees_north'``,
  ``'degree_north'``, ``'degree_N'``, ``'degrees_N'``, ``'degreeN'``,
  and ``'degreesN'`` (returns False if ``'degrees'``).

* The `standard_name` property is ``'latitude'`` (returns False if
  ``'grid_latitude'``).

**Examples**

**Examples**

>>> if c.islatitude:
...    print c

'''
        return (self.identity() == 'latitude' or self.Units.islatitude)
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: islongitude
    # ----------------------------------------------------------------
    @property
    def islongitude(self):
        '''

True if and only if the coordinate represents longitude.

This is the case only if any of the following are true:

* The `units` attribute is one of ``'degrees_east'``,
  ``'degree_east'``, ``'degree_E'``, ``'degrees_E'``, ``'degreeE'``,
  and ``'degreesE'`` (returns False if ``'degrees'``).

* The `standard_name` property is ``'longitude'`` (returns False if
  ``'grid_longitude'``).

**Examples**

>>> if c.islongitude
...    print c

'''
        return (self.identity() == 'longitude' or self.Units.islongitude)
    #--- End: def

    @property
    def isscalar(self):
        '''


'''
        if self._hasData:
            return self.Data.isscalar
        
        if self.isbounded:
            bounds = self.bounds
            if bounds._hasData:
                return bounds.Data._ndim == 1
        #--- End: if

        return False
    #--- End: if

#    # ----------------------------------------------------------------
#    # Attribute: transforms
#    # ----------------------------------------------------------------
#    @property
#    def transforms(self):
#        '''
#
#A list containing pointers to the coordinate's transforms.
#
#**Examples**
#
#>>> c.transforms
#['trans0']
#>>> del c.transforms
#>>> c.transforms = ['trans2', 'trans0']
#
#'''
#        return self._getter('transforms')
#    #--- End: for
#    @transforms.setter
#    def transforms(self, value): self._setter('transforms', value)
#    @transforms.deleter
#    def transforms(self):        self._deleter('transforms')

    # ----------------------------------------------------------------
    # Attribute: mask (read only)
    # ----------------------------------------------------------------
    @property
    def mask(self):
        '''

A variable containing the mask of the data array.

The bounds data array is ignored, if present.

**Examples**

>>> c
<CF Coordinate: latitude(23) degrees_N>
>>> m = c.mask
>>> m
<CF Variable: long_name:mask(23) >
>>> m.Data
<CF Data: [True, ..., False] >

'''
        return Variable(properties={'long_name': 'mask'},
                        attributes={'id': 'mask'}, 
                        data=self.Data.mask)
    #--- End: def
    @mask.setter
    def mask(self, value):
        Variable.mask.fset(self, value)
    @mask.deleter
    def mask(self): 
        Variable.mask.fdel(self)

    @property
    def varray(self):
        '''

Return a numpy view of the data.

Making changes to elements of the returned view changes the underlying
data. Refer to :obj:`numpy.ndarray.view`.
    
**Examples**

>>> a = c.varray
>>> type(a)
<type 'numpy.ndarray'>
>>> a
array([0, 1, 2, 3, 4])
>>> a[0] = 999
>>> c.varray
array([999, 1, 2, 3, 4])

'''
#        if self._isbounded:
#            self.bounds.varray
       
        return super(Coordinate, self).varray
    #--- End: if

    def _change_dimension_names(self, dim_name_map):
        '''

Change the dimension names.

Warning: dim_name_map may be changed in place

:Parameters:

    dim_name_map : dict

:Returns:

    None

**Examples**

'''
        # Change the dimension names of the data array
        super(Coordinate, self)._change_dimension_names(dim_name_map)

        if self._isbounded:
            bounds = self.bounds
            if bounds._hasData:
                b_dimensions = bounds.Data.dimensions
                if self._hasData:
                    # Change the dimension names of the bounds
                    # array. Note that it is assumed that the bounds
                    # array dimensions are in the same order as the
                    # coordinate's data array dimensions. It is not
                    # required that the set of original bounds
                    # dimension names (bar the trailing dimension)
                    # equals the set of original coordinate data array
                    # dimension names. The bounds array dimension
                    # names will be changed to match the updated
                    # coordinate data array dimension names.
                    dim_name_map = {b_dimensions[-1]: 'bounds'}
                    for c_dim, b_dim in izip(self.Data.dimensions, b_dimensions):
                         dim_name_map[b_dim] = c_dim
                else:
                    dim_name_map[b_dimensions[-1]] = 'bounds'
                #--- End: if

                bounds._change_dimension_names(dim_name_map)
    #--- End: def

    def chunk(self, chunksize=None):
        '''
'''         
        if not chunksize:
            # Set the default chunk size
            chunksize = CHUNKSIZE()
            
        # Partition the coordinate's data
        super(Coordinate, self).chunk(chunksize)

        # Partition the data of the bounds, if they exist.
        if self._isbounded:
            self.bounds.chunk(chunksize)
    #--- End: def

    def clip(self, a_min, a_max, units=None):
        '''

Clip (limit) the values in the data array and its bounds in place.

Given an interval, values outside the interval are clipped to the
interval edges.

Parameters :
 
    a_min : scalar

    a_max : scalar

    units : str or Units

:Returns: 

    None

**Examples**

'''
        self.Data.clip(a_min, a_max, units=units)
        
        if self._isbounded:
            self.bounds.Data.clip(a_min, a_max, units=units)
    #--- End: def
  
    def close(self):
        '''

Close all files referenced by the coordinate.

Note that a closed file will be automatically reopened if its contents
are subsequently required.

:Returns:

    None

**Examples**

>>> c.close()

'''
        new = super(Coordinate, self).close()
        
        if self._isbounded:
            self.bounds.close()
    #--- End: def

    def contiguous(self, overlap=True):
        '''

Return True if a coordinate is contiguous.

A coordinate is contiguous if its cell boundaries match up, or
overlap, with the boundaries of adjacent cells.

In general, it is only possible for 1 or 0 dimensional coordinates
with bounds to be contiguous, but size 1 coordinates with any number
of dimensions are always contiguous.

An exception occurs if the coordinate is multdimensional and has more
than one element.

:Parameters:

    overlap : bool, optional    
        If False then overlapping cell boundaries are not considered
        contiguous. By default cell boundaries are considered
        contiguous.

:Returns:

    out : bool
        Whether or not the coordinate is contiguous.

:Raises:

    ValueError :
        If the coordinate has more than one dimension.

**Examples**

>>> c._isbounded
False
>>> c.contiguous()
False

>>> print c.bounds[:, 0]
[  0.5   1.5   2.5   3.5 ]
>>> print c.bounds[:, 1]
[  1.5   2.5   3.5   4.5 ]
>>> c.contiuous()
True

>>> print c.bounds[:, 0]
[  0.5   1.5   2.5   3.5 ]
>>> print c.bounds[:, 1]
[  2.5   3.5   4.5   5.5 ]
>>> c.contiuous()
True
>>> c.contiuous(overlap=False)
False

'''
        if self.size == 1:
            return True

        if self.ndim > 1:
            raise ValueError(
                "Can't tell if a multidimensional coordinate is contiguous")

        if not self._isbounded:
            return False    

        bounds = self.bounds.Data
        bounds.to_memory()

        if overlap:
            return (bounds[1:, 0] <= bounds[:-1, 1]).all()
        else:
            return bounds[1:, 0].equals(bounds[:-1, 1])
    #--- End: def

    def cos(self):
        '''

Take the trigonometric cosine of the data array and bounds in place.

Units are accounted for in the calcualtion, so that the the cosine of
90 degrees_east is 0.0, as is the sine of 1.57079632 radians. If the
units are not equivalent to radians (such as Kelvin) then they are
treated as if they were radians.

The Units are changed to '1' (nondimensionsal).

:Returns: 

    None

**Examples**

>>> c.Units
<CF Units: degrees_east>
>>> print c.array
[[-90 0 90 --]]
>>> c.cos()
>>> c.Units
<CF Units: 1>
>>> print c.array
[[0.0 1.0 0.0 --]]


>>> c.Units
<CF Units: m s-1>
>>> print c.array
[[1 2 3 --]]
>>> c.cos()
>>> c.Units
<CF Units: 1>
>>> print c.array
[[0.540302305868 -0.416146836547 -0.9899924966 --]]

'''
        super(Coordinate, self).cos()

        if self._isbounded:
            self.bounds.cos()
    #--- End: def

    def copy(self, _omit_Data=False, _only_Data=False):
        '''
        
Return a deep copy.

Equivalent to ``copy.deepcopy(c)``.

:Returns:

    out :
        The deep copy.

**Examples**

>>> d = c.copy()

'''
        new = super(Coordinate, self).copy(_omit_Data=_omit_Data,
                                           _only_Data=_only_Data,
                                           _omit_special=('bounds',))

        if self._isbounded:
            new.bounds = self.bounds.copy(_omit_Data=_omit_Data,
                                          _only_Data=_only_Data)

        return new
    #--- End: def

    def dump(self, omit=(), domain=None, key=None, level=0): 
        '''

Return a string containing a full description of the coordinate.

:Parameters:

    omit : sequence of strs
        Omit the given CF properties from the description.

:Returns:

    out : str
        A string containing the description.

**Examples**

>>> x = c.dump()
>>> print c.dump()
>>> print c.dump(id='time_coord')
>>> print c.dump(omit=('long_name',))

'''
        indent0 = '    ' * level
        indent1 = '    ' * (level+1)

        string = []

        if domain:
            x = ['%s(%d)' % (domain.dimension_name(dim),
                             domain.dimension_sizes[dim])
                 for dim in domain.dimensions[key]]
            string.append('%sData(%s) = %s' % (indent0, ', '.join(x),
                                               str(self.Data)))
            
            if self._isbounded:
                x.append(str(self.bounds.shape[-1]))
                string.append('%sBounds(%s) = %s' % (indent0, ', '.join(x),
                                                     str(self.bounds.Data)))
        else:
            x = [str(s) for s in self.shape]
            string.append('%sData(%s) = %s' % (indent0, ', '.join(x),
                                               str(self.Data)))
            
            if self._isbounded:
                x.append(str(self.bounds.shape[-1]))
                string.append('%sBounds(%s) = %s' % (indent0, ', '.join(x),
                                                     str(self.bounds.Data)))
        #--- End: if

        if self._simple_properties():
            string.append(self.dump_simple_properties(level=level))
          
        if hasattr(self, 'transforms'):
#            string.append('')
#            if domain:
#                string.append('%sTransform: %s' % (indent0, repr(domain.transforms[self.tran#sforms.values()[0]])))
#            else:
            string.append('%sTransform: %s' % (indent0, self.transforms.keys()[0]))
            
        return '\n'.join(string)
    #--- End: def

    def _insert_data(self, other, extend, reversed,
                     PDim, PDim_direction, dim_name_map):
        '''
'''
        # Insert the coordinate's data
        super(Coordinate, self)._insert_data(other, extend, reversed,
                                             PDim, PDim_direction,
                                             dim_name_map)
        
        # Insert the coordinate's bounds, if it has any
        if self._isbounded:
            self.bounds._insert_data(other.bounds, extend, reversed,
                                     PDim, PDim_direction, dim_name_map)
        #--- End: if

    #--- End: def

    def expand_dims(self, axis, dim, direction):
        '''
axis is an integer
dim is a string
direction is a boolean

'''
        # Expand the requested dimensions in the coordinate's data
        super(Coordinate, self).expand_dims(axis, dim, direction)

        # Expand the requested dimensions in the coordinate's bounds,
        # if it has any
        if self._isbounded:
            if axis < 0:
                axis += self.ndims

            self.bounds.expand_dims(axis, dim, direction)
    #--- End: def

    def flip(self, axes=None):
        '''

Flip dimensions of the data array and bounds in place.

The trailing dimension of the bounds is flipped if and only if the
coordinate is 1 or 0 dimensional.

:Parameters:

    axes : int or sequence of ints
        Flip the dimensions whose positions are given. By default all
        dimensions are flipped.

:Returns:

    out : list of ints
        The axes which were flipped, in arbitrary order.

**Examples**

>>> c.flip()
>>> c.flip(1)

>>> d = c.subspace[::-1, :, ::-1, :]
>>> c.flip([2, 0]).equals(d)
True

'''  
        # Flip the requested dimensions in the coordinate's data
#        axes = super(Coordinate, self).flip(axes)
        axes = self.Data.flip(axes)

        # ------------------------------------------------------------
        # Flip the requested dimensions in the coordinate's bounds, if
        # it has any.
        #
        # As per section 7.1 in the CF conventions: i) if the
        # coordinate is 0 or 1 dimensional then flip all dimensions
        # (including the the trailing size 2 dimension); ii) if the
        # coordinate has 2 or more dimensions then do not flip the
        # trailing dimension.
        # ------------------------------------------------------------
        if self._isbounded:
            if self.bounds.ndim <= 2:
                axes = None   # None means flip all dimensions
            self.bounds.flip(axes)
    #--- End: def

    def insert_bounds(self, bounds, copy=True, fix_bounds_units=False):
        '''

Insert cell bounds into the coordinate in-place.

:Parameters:

    bounds : cf.Data or cf.CoordinateBounds

    copy : bool, optional

    fix_bounds_units : bool, optional

:Returns:

    None

'''
        # Check dimensionality
        if bounds.ndim != self.ndim + 1:
            raise ValueError(
"Can't set coordinate bounds: Incorrect number of dimemsions: %d (expected %d)" % 
(bounds.ndim, self.ndim+1))

        # Check shape
        if bounds.shape[:-1] != self.shape:
            raise ValueError(
"Can't set coordinate bounds: Incorrect shape: %s (expected %s)" % 
(bounds.shape, self.shape+(bounds.shape[-1],)))

        # Check units
        units = bounds.Units
        self_units = self.Units
        if units and not units.equivalent(self_units):
            if not fix_bounds_units:
                raise ValueError(
"Can't set coordinate bounds: Incompatible units: %s (not equivalent to %s)" %
(repr(bounds.Units), repr(self.Units)))
        
            print("WARNING: Overriding incompatible bounds units: %s (%s)" %
                  (repr(bounds.Units), repr(self.Units)))
        #--- End: if

        if copy:            
            bounds = bounds.copy()

        bounds.Units = self_units
        
        if isinstance(bounds, CoordinateBounds):
            self.bounds = bounds
        else:
            self.bounds = CoordinateBounds(data=bounds, copy=False)  
    #--- End: def

    def insert_data(self, data, bounds=None, copy=True,
                    fix_bounds_units=False):
        '''

Insert a new data array into the coordinate in place.

A coordinate bounds data array may also inserted if given with the
*bounds* keyword. Coordinate bounds may also be inserted independently
with the `insert_bounds` method.

:Parameters:

    data : cf.Data

    bounds : cf.Data, optional

    copy : bool, optional

:Returns:

    None

'''
        if data is not None:
            super(Coordinate, self).insert_data(data, copy=copy)
        
        if bounds is not None:
            self.insert_bounds(bounds, copy=copy,
                               fix_bounds_units=fix_bounds_units)
    #--- End: def

    def insert_transform(self, transform, transform_id):
        '''

:Parameters:

:Returns:

    None

**Examples**

>>> c.insert_transform(transform, 'trans1')

'''
        name = transform.name
        
#        transforms = getattr(self, 'transforms', None)
        
#        if not transforms:
        self.transforms = {name: transform_id}
#        else:
        
#        if name in transforms:
#            self.transforms[name].append(transform_id)
#        else:
#            self.transforms[name] = transform_id
    #--- End: def

    def sin(self):
        '''

Take the trigonometric sine of the data array and bounds in place.

Units are accounted for in the calculation. For example, the the sine
of 90 degrees_east is 1.0, as is the sine of 1.57079632 radians. If
the units are not equivalent to radians (such as Kelvin) then they are
treated as if they were radians.

The Units are changed to '1' (nondimensionsal).

:Returns:

    None

**Examples**

>>> c.Units
<CF Units: degrees_north>
>>> print c.array
[[-90 0 90 --]]
>>> c.sin()
>>> c.Units
<CF Units: 1>
>>> print c.array
[[-1.0 0.0 1.0 --]]

>>> c.Units
<CF Units: m s-1>
>>> print c.array
[[1 2 3 --]]
>>> c.sin()
>>> c.Units
<CF Units: 1>
>>> print c.array
[[0.841470984808 0.909297426826 0.14112000806 --]]

'''
        super(Coordinate, self).sin()

        if self._isbounded:
            self.bounds.sin()
    #--- End: def

    def squeeze(self, axes=None):
        '''

Remove size 1 dimensions from the data array and bounds in place.

:Parameters:

    axes : int or sequence of ints, optional
        The size 1 axes to remove. By default, all size 1 axes are
        removed. Size 1 axes for removal may be identified by the
        integer positions of dimensions in the data array.

:Returns:

    None

**Examples**

>>> c.squeeze()
>>> c.squeeze(1)
>>> c.squeeze([1, 2])

'''
        # Squeeze the coordinate's data array
        axes = self.Data.squeeze(axes)

        # Squeeze the coordinate's bounds, if it has any
        if axes and self._isbounded:
            self.bounds.squeeze(axes)
    #--- End: def

    def transpose(self, axes=None):
        '''

Permute the dimensions of the data array and bounds in place.

:Parameters:

    axes : sequence of ints, optional
        The new order of the data array. By default, reverse the
        dimensions' order, otherwise the axes are permuted according
        to the values given. The values of the sequence comprise the
        integer positions of the dimensions in the data array in the
        desired order.

:Returns:

    None

**Examples**

>>> c.ndim
3
>>> c.transpose()
>>> c.transpose([1, 2, 0])

'''
        # Transpose the coordinate's data array
#        super(Coordinate, self).transpose(axes=axes)
        self.Data.transpose(axes)

        # Transpose the coordinate's bounds, if it has any.
        if self._isbounded:
            axes = list(axes)
            axes.append(self.ndim)
            self.bounds.transpose(axes)
    #--- End: def

#--- End: class


# ====================================================================
#
# SubspaceCoordinate object
#
# ====================================================================

class SubspaceCoordinate(SubspaceVariable):

    __slots__ = []

    def __getitem__(self, indices):
        '''
x.__getitem__(index) <==> x[index]

'''
        coord = self.variable

        if indices is Ellipsis:
            return coord.copy()

        # Copy the coordinate
        new = coord.copy(_omit_Data=True)

        # Parse the index (so that it's ok for appending the bounds
        # index if required)
        indices = parse_indices(coord, indices)
    
        coord_data = coord.Data

        new.Data = coord_data[tuple(indices)]

        # Subspace the bounds, if there are any
        if not new._isbounded:
            bounds = None
        else:
            bounds = coord.bounds
            if bounds._hasData:
                if coord_data.ndim <= 1:
                    index = indices[0]
                    if isinstance(index, slice):
                        if index.step < 0:
                            # This scalar or 1-d coordinate has been
                            # reversed so reverse its bounds (as per
                            # 7.1 of the conventions)
                            indices.append(slice(None, None, -1))
                    elif coord_data.size > 1 and index[-1] < index[0]:
                        # This 1-d coordinate has been reversed so
                        # reverse its bounds (as per 7.1 of the
                        # conventions)
                        indices.append(slice(None, None, -1))
                    
                    #if coord.size == 1:
                    #    if isinstance(index, slice) and index.step < 0:
                    #        # This 0- or 1-d, size 1 coordinate has
                    #        # been reversed so reverse its bounds (as
                    #        # per 7.1 of the conventions).
                    #        indices.append(slice(None, None, -1))
                    #        
                    #elif isinstance(index, slice):
                    #    if index.step < 0:
                    #        # This 1-d, size > 1 coordinate has been
                    #        # reversed, so reverse its bounds (as per
                    #        # 7.1 of the conventions).
                    #        indices.append(slice(None, None, -1))
                    #elif index[-1] < index[0]:
                    #    # This 1-d, size > 1 coordinate has been
                    #    # reversed, so reverse its bounds (as per 7.1
                    #    # of the conventions).
                    #    indices.append(slice(None, None, -1))
                #--- End: if
                new.bounds.Data = bounds.Data[tuple(indices)]
        #--- End: if

        # Return the new coordinate
        return new
    #--- End: def

#--- End: class

# ====================================================================
#
# DimensionCoordinate object
#
# ====================================================================

class DimensionCoordinate(Coordinate):
    '''

A CF dimension coordinate construct.

The coordinate array's bounds (if present) are stored in the `bounds`
attribute. The `climatology` CF property indicates if the bounds are
intervals of climatological time.

If the coordinate is connected to one or more transform constructs
then pointers to the transformations are stored in the `transforms`
attribute.

'''

    def __init__(self, properties={}, data=None, attributes={}, bounds=None,
                 copy=True):
        '''

**Initialization**

:Parameters:

    properties : dict, optional
        Initialize a new instance with CF properties from the
        dictionary's key/value pairs. Values are deep copied.

    data : cf.Data, optional
        Provide the new instance with an N-dimensional data array. The
        array is deep copied.

    attributes : dict, optional
        Provide the new instance with attributes from the dictionary's
        key/value pairs. Values are deep copied.

    bounds : cf.Data, optional
        Provide the new instance with cell bounds.

    copy : bool, optional
        If False then do not copy arguments prior to
        initialization. By default arguments are deep copied.

'''
        self._direction = None

        super(DimensionCoordinate, self).__init__(
            properties=properties, data=data, attributes=attributes,
            bounds=bounds, copy=copy)
    #--- End: def 

    def __getitem__(self, indices):
        '''
x.__getitem__(indices) <==> x[indices]

'''
        new = super(DimensionCoordinate, self).__getitem__(indices)

        new._direction = None

        return new
    #--- End: def

    def _infer_direction(self):
        '''
    
Return True if a coordinate is increasing, otherwise return False.

A coordinate is considered to be increasing if its *raw* data array
values are increasing in index space or if it has no data not bounds
data.

If the direction can not be inferred from the coordinate's data, then
the `positive` CF property is used, if present, or the coordinate's
units.

The direction is inferred from the coordinate's data array values or
its from coordinates. It is not taken directly from its `cf.Data`
object.

:Returns:

    out : bool
        Whether or not the coordinate is increasing.
        
**Examples**

>>> c.array
array([  0  30  60])
>>> c._get_direction()
True
>>> c.array
array([15])
>>> c.bounds.array
array([  30  0])
>>> c._get_direction()
False

'''
        # Then infer the direction from the dimension
        # coordinate's array, if it has one.
        if self._hasData:
            c = self.Data
            if c._size > 1:
                c = c[0:2].unsafe_array
                return c.item(0) < c.item(1)
        #--- End: if

        # Still here? Then infer the direction from the dimension
        # coordinate's bounds, if it has any.
        if self._isbounded:
            b = self.bounds
            if b._hasData:
                b = b.Data
                b = b[(0,)*(b.ndim-1)].unsafe_array
                return b.item(0) < b.item(1)
        #--- End: if

        # Still here? Then infer the direction from the dimension
        # coordinate's positive CF property, if it has one.
        positive = self.getprop('positive', None)
        if positive is not None and positive[0] in ('d', 'D'):
            return False

        # Still here? Then infer the direction from the units
        return not self.Units.ispressure
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: Units
    # ----------------------------------------------------------------
    @property
    def Units(self):
        '''
The Units object containing the units of the data array.

'''
        return Coordinate.Units.fget(self)
    #--- End: def

    @Units.setter
    def Units(self, value):
        Coordinate.Units.fset(self, value)
        self._direction = None
    @Units.deleter
    def Units(self):
        Coordinate.Units.fdel(self)
        self._direction = None
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: positive
    # ----------------------------------------------------------------
    @property
    def positive(self):
        '''

The positive CF property.

**Examples**

>>> c.positive = 'up'
>>> c.positive
'up'
>>> del c.positive

>>> c.setprop('positive', 'down')
>>> c.getprop('positive')
'down'
>>> c.delprop('positive')

'''
        return Coordinate.positive.fget(self)
    #--- End: def
    @positive.setter
    def positive(self, value):
        Coordinate.positive.fset(self, value)
        self._direction = None
    @positive.deleter
    def positive(self):
        Coordinate.positive.fdel(self)
        self._direction = None
    #--- End: def
    
    # ----------------------------------------------------------------
    # CF property: units (a special attribute)
    # ----------------------------------------------------------------
    # DCH possible inconsistency when setting self.Units.units
    @property
    def units(self):
        '''

The units CF property.

This property is a mirror of the units stored in the `Units`
attribute.

**Examples**

>>> c.units = 'degrees_east'
>>> c.units
'degree_east'
>>> del c.units

>>> c.setprop('units', 'days since 2004-06-01')
>>> c.getprop('units')
'days since 2004-06-01'
>>> c.delprop('units')

'''
        return Coordinate.units.fget(self)
    #--- End: def

    @units.setter
    def units(self, value):
        Coordinate.units.fset(self, value)
        self._direction = None
    #--- End: def
    
    @units.deleter
    def units(self):
        Coordinate.units.fdel(self)
        self._direction = None
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: attributes (read only)
    # ----------------------------------------------------------------
    @property
    def attributes(self):
        '''

A dictionary of the attributes which are not CF properties.

**Examples**

>>> c.attributes
{}

>>> c.foo = 'bar'
{'foo': 'bar'}

'''
        attributes = super(DimensionCoordinate, self).attributes
        attributes.pop('_direction')
        return attributes
    #--- End: def

    def asdimension(self, copy=True):
        '''

Return the dimension coordinate.

:Parameters:

    copy : bool, optional
        If False then the returned dimension coordinate is not
        independent. By default the returned dimension coordinate is
        independent.

:Returns:

    out : cf.DimensionCoordinate
        The dimension coordinate.

**Examples**

>>> d = c.asdimension()
>>> print d is c
True

>>> d = c.asdimension(copy=False)
>>> print d == c
True
>>> print d is c
False

'''
        if copy:
            return self.copy()
        
        return self
    #--- End: def

    def _override_directions(self, directions, scalar_dir):
        '''

:Parameters:

    directions : dict or None
        Ignored.

    scalar_dir : bool
    
:Returns: 

    None

**Examples**

'''
        if self.isscalar:            
            super(Coordinate, self)._override_directions(None, scalar_dir)
        elif self._hasData:
            super(Coordinate, self)._override_directions(
                {self.Data.dimensions[0]: scalar_dir}, None)

        if self._isbounded:
            bounds = self.bounds
            if not bounds._hasData:
                return

            directions = {}
            for dim in bounds.Data.dimensions:
                directions[dim] = scalar_dir
                
            bounds._override_directions(directions, None)
        #--- End: if
    #--- End: def

    def direction(self):
        '''
    
Return True if a coordinate is increasing, otherwise return False.

A coordinate is increasing if its coordinate values are increasing in
index space.

The direction is inferred from one of, in order of precedence:

* The data array
* The bounds data array
* The `positive` property
* The `units` property (True if pressure, False otherwise or if
  missing)


:Returns:

    out : bool
        Whether or not the coordinate is increasing.
        
**Examples**

>>> c.array
array([  0  30  60])
>>> c._get_direction()
True
>>> c.array
array([15])
>>> c.bounds.array
array([  30  0])
>>> c._get_direction()
False

''' 
        coord_dir = self._direction

        if coord_dir is not None:
            return coord_dir

        coord_dir = self._infer_direction()

        self._direction = coord_dir

        self._override_directions(None, coord_dir)

        return coord_dir
    #--- End: def

    def find_bounds(self, create=False, insert=False, bound=None):
        '''
    
Find the dimension coordinate's bounds.
    
Either return its existing bounds or, if there are none, optionally
create bounds based on the coordinate's array values.
    
:Parameters:

    create : bool, optional
        If True then create bounds if and only if the the dimension
        coordinate does not already have them. Voronoi bounds are
        created unless *bound* is set. By default an exception is
        raised if the coordinate does not have bounds.

    insert : bool, optional 
        If True then insert the created bounds into the coordinate in
        place. By default the created bounds are not inserted. Ignored
        if *create* is not True.

    bound : number, optional
        If set to an upper or lower bound for the whole array, then
        create bounds for which include this value and for which each
        array element is in the centre of its bounds. By default
        Voronoi bounds are created. Ignored if *create* is not True.

:Returns:

    out : cf.CoordinateBounds
        The existing or created bounds.

**Examples**

>>> c.find_bounds()
>>> c.find_bounds(create=True)
>>> c.find_bounds(create=True, bound=0)
>>> c.find_bounds(create=True, insert=True)

'''
        if self._isbounded:
            return self.bounds
         
        if not create:
            raise ValueError("sdf6671nsdff0283  &&&&&_____________")

        array = self.array
        size = array.size

        bounds = numpy_empty((size, 2), dtype=array.dtype)
            
        if bound is not None:
            direction = self.direction()

            if not direction:
                array = array[::-1]

            bounds_1d = [bound]
            if bound <= array.item(0):
                for i in xrange(size):
                    bound = 2*array.item(i) - bound
                    bounds_1d.append(bound)
            elif bound >= array.item(-1):
                for i in xrange(size-1, -1, -1):
                    bound = 2*array.item(i) - bound
                    bounds_1d.append(bound)

                bounds_1d = bounds_1d[::-1]
            else:
                raise ValueError("bad bound value")

            if not direction:               
                bounds_1d = bounds_1d[::-1]
        else:
            # Create voronoi bounds
            bounds_1d = [array.item(0)*1.5 - array.item(1)*0.5]
            bounds_1d.extend((array[0:-1] + array[1:])*0.5)
            bounds_1d.append(array.item(-1)*1.5 - array.item(-2)*0.5)
        #--- End: if

        bounds[:,0] = bounds_1d[0:-1]
        bounds[:,1] = bounds_1d[1:]

        bounds = Data(bounds, self.Units)

        # Clip out-of-bounds latitude values
        if self.identity() in ('latitude', 'grid_latitude'):           
            bounds.clip(-90.0, 90.0, 'degrees')

        bounds = CoordinateBounds(data=bounds)
                           
        if insert:
            self.bounds = bounds
            if size == 1:
                self._direction = None
                self.direction()
        #--- End: if

        return bounds            
    #--- End: def

    def flip(self, axes=None):
        '''

Flip dimensions of the data array and bounds in place.

The trailing dimension of the bounds is flipped if and only if the
coordinate is 1 or 0 dimensional.

:Parameters:

    axes : int or sequence of ints
        Flip the dimensions whose positions are given. By default all
        dimensions are flipped.

:Returns:

    out : list of ints
        The axes which were flipped, in arbitrary order.

**Examples**

>>> c.flip()
>>> c.flip(0)

'''  
        super(DimensionCoordinate, self).flip(axes)
        
        direction = self._direction
        if direction is not None:
            self._direction = not direction
    #--- End: if

    def insert_bounds(self, bounds, copy=True, fix_bounds_units=False):
        '''

Insert a new bounds data array into the coordinate in-place.

:Parameters:

    bounds : cf.Data

    copy : bool, optional

:Returns:

    None

'''
        super(DimensionCoordinate, self).insert_bounds(
            bounds,
            copy=copy,
            fix_bounds_units=fix_bounds_units)

        if not self._hasData or self.Data.size == 1:
            self._direction = None
            self.direction()
    #--- End: def

    def insert_data(self, data, bounds=None, copy=True, fix_bounds_units=False):
        '''

Insert a new data array into the coordinate in place.

A coordinate bounds data array may also inserted if given with the
*bounds* keyword. Coordinate bounds may also be inserted independently
with the `insert_bounds` method.

:Parameters:

    data : cf.Data or None

    bounds : cf.Data or cf.CoordinateBounds, optional

    copy : bool, optional

    fix_bounds_units : bool, optional

:Returns:

    None

'''
        self._direction = None
        
        super(DimensionCoordinate, self).insert_data(
            data,
            bounds=bounds, 
            copy=copy,
            fix_bounds_units=fix_bounds_units)
        
        if not self._direction:
            self.direction()
    #--- End: def

#    def set_direction(self, direction):
#        '''
#
#Set the coordinate's direction.
#
#:Parameters:
#
#    direction : bool, optional
#
#:Returns:
#
#    None
#
#**Examples**
#
#>>> c.set_direction(False)
#
#'''
#        # ------------------------------------------------------------
#        # This method sets the _direction_is_set attribute to True
#        # ------------------------------------------------------------
#
#        if self.isscalar:
#            self._override_directions(None, direction)
#        elif self._hasData:
#            self._override_directions({self.Data.dimensions[0]: direction}, None)
#        elif self.isbounded:
#            bounds = self.bounds
#            if bounds._hasData:
#                dimensions = bounds.Data.dimensions
#                self._override_directions({dimensions[0]: direction,
#                                           dimensions[1]: direction}, None)
#
##        self._direction_is_set = True
#        self._direction = direction
#    #--- End: def

#--- End: class


# ====================================================================
#
# AuxiliaryCoordinate object
#
# ====================================================================

class AuxiliaryCoordinate(Coordinate):
    '''

A CF auxiliary coordinate construct.

The coordinate array's bounds (if present) are stored in the `bounds`
attribute. The `climatology` CF property indicates if the bounds are
intervals of climatological time.

If the coordinate is connected to one or more transform constructs
then pointers to the transformations are stored in the `transforms`
attribute.

'''

    def asauxiliary(self, copy=True):
        '''

Return the auxiliary coordinate.

:Parameters:

    copy : bool, optional   

        If False then the returned auxiliary coordinate is not
        independent. By default the returned auxiliary coordinate is
        independent. 

:Returns:

    out : cf.AuxiliaryCoordinate
        The auxiliary coordinate.

**Examples**

>>> d = c.asauxiliary()     
>>> print d is c
True

>>> d = c.asauxiliary(copy=False)
>>> print d == c
True
>>> print d is c
False

'''
        if copy:
            return self.copy()
        
        return self
    #--- End: def

#--- End: class

