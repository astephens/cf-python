from .variablelist   import VariableList, SubspaceVariableList
from .coordinatelist import CoordinateList
from .functions      import CHUNKSIZE

# ====================================================================
#
# FieldList object
#
# ====================================================================

class FieldList(VariableList):
    '''

An ordered sequence of fields stored in a list-like object.

In some contexts, whether an object is a field or a field list is not
known and does not matter. So to avoid ungainly type testing, some
aspects of the FieldList interface are shared by a field and vice
versa.

Any attribute or method belonging to a field may be used on a field
list and will be applied independently to each element.

Just as it is straight forward to iterate over the fields in a field
list, a field will behave like a single element field list in
iterative and indexing contexts.

'''

    @property
    def subspace(self):
        '''

Slice each field in the list, returning a new list of fields. Slicing
by indices or by coordinate values are both allowed.

**Examples**

>>> fl
[<CF Variable: air_temperature(73, 96)>,
 <CF Variable: air_temperature(73, 96)>]
>>> fl.subspace[0,0]
[<CF Variable: air_temperature(1,1)>,
 <CF Variable: air_temperature(1,1)>]
>>> fl.slice(longitude=0, latitude=0)
[<CF Variable: air_temperature(1,1)>,
 <CF Variable: air_temperature(1,1)>]

'''
        return SubspaceFieldList(self)
    #--- End: def

    def coord(self, *args, **kwargs):
        '''

Apply the `~Field.coord` method to each field of the list.

Note that a coordinate list is returned (as opposed to a field list).

:Parameters:

    args, kwargs :
        As for a field's `~Field.coord` method.

:Returns:

    out : CoordinateList

'''
        return CoordinateList([v.coord(*args, **kwargs) for v in self._list])
    #--- End: def

    def finalize(self):
        '''
'''
        for f in self._list:
            f.finalize()
    #--- End: def
            
    def squeeze(self, *args, **kwargs):
        '''

Apply the `~Field.squeeze` method to each field of the list.

:Parameters:

    args, kwargs :
        As for a field's `~Field.squeeze` method.

:Returns:

    out : FieldList

'''
        for f in self._list:
            f.squeeze(*args, **kwargs)
    #--- End: def

    def unsqueeze(self, *args, **kwargs):
        '''

Apply the `~Field.unsqueeze` method to each field of the list.

:Parameters:

    args, kwargs :
        As for a field's `~Field.unsqueeze` method.

:Returns:

    out : FieldList

'''
        for f in self._list:
            f.unsqueeze(*args, **kwargs)
    #--- End: def

#--- End: class

# ====================================================================
#
# List of Fields slice object
#
# ====================================================================

class SubspaceFieldList(SubspaceVariableList):
    '''

'''
    __slots__ = []

    def __call__(self, *arg, **kwargs):
        '''

Slice by fields by coordinate value.

'''
        fieldlist = self.variablelist
        return type(fieldlist)([f.subspace(*arg, **kwargs) for f in fieldlist])
    #--- End: def

#--- End: class
