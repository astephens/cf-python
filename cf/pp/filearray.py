import numpy

from numpy import array       as numpy_array
from numpy import fromfile    as numpy_fromfile
from numpy import memmap      as numpy_memmap
from numpy import result_type as numpy_result_type
from numpy import where       as numpy_where

from numpy.ma import array        as numpy_ma_array
from numpy.ma import column_stack as numpy_ma_column_stack
from numpy.ma import where        as numpy_ma_where

from ..constants import _file_to_fh
from ..functions import (open_files_threshold_exceeded,
                         close_one_file,
                         parse_indices,
                         subspace_array)

_filename_to_file = _file_to_fh.setdefault('PP', {})

# ====================================================================
#
# PPFileArray object
#
# ====================================================================

class PPFileArray(object):
    ''' 
    
A sub-array stored in a PP file.
    
'''
    def __init__(self, _FillValue=None, _lbpack=0, _binary_mask=None,
                 _scale_factor=1, _add_offset=0, dtype=None, shape=None,
                 size=None, ndim=None,
                 file=None, file_offset=None):
        '''

**Initialization**

:Parameters:

    file : str
        The PP file name in normalized, absolute form.

    file_offset : int
        The start position in the file of the data array.

    dtype : numpy.dtype
        The numpy data type of the data array.

    ndim : int
        Number of dimensions in the data array.

    shape : tuple
        The data array's dimension sizes.

    size : int
        Number of elements in the data array.

    _add_offset : optional 
         The numeric additive offset (in the CF sense) of the
         sub-array. By default the array is assumed to have no offset.
  
    _lbpack : int, optional
         The integer PP packing code of the sub-array. By default the
         array is assumed to be unpacked.

    _scale_factor : optional 
         The numeric scale factor (in the CF sense) of the
         sub-array. By default the array is assumed to have no scale
         factor.

    _binary_mask : bool, optional

    _FillValue : optional 
        The missing data value for the sub-array.

**Examples**

>>> ppfile
<open file 'file.pp', mode 'rb' at 0xc45e00>
>>> a = PPFileArray(file=ppfile.name, file_offset=ppfile.tell(),
                    dtype=numpy.dtype('float32'), shape=(73, 96), size=7008,
                    ndim=2)

'''
        self._FillValue    = _FillValue
        self._binary_mask  = _binary_mask
        self._lbpack       = _lbpack
        self._scale_factor = _scale_factor
        self._add_offset   = _add_offset
        self.dtype         = dtype
        self.shape         = shape
        self.size          = size
        self.ndim          = ndim
        self.file          = file
        self.file_offset   = file_offset
    #--- End: def
 
    def __deepcopy__(self, memo):
        '''
Used if copy.deepcopy is called on the variable.

'''
        return self.copy()
    #--- End: def

    def __getitem__(self, indices):
        '''
x.__getitem__(indices) <==> x[indices]

Returns a numpy array.

''' 
        pp = self.open()

        _lbpack = self._lbpack

        if indices is Ellipsis:
            gg=1            
            if not _lbpack:
                pp.seek(self.file_offset, 0)
                array = numpy_fromfile(pp, dtype=self.dtype, count=self.size)
                array.resize(self.shape)
            else:
                raise ValueError(
"PP data array is packed (lbpack=%d) and so can not be accessed (yet ...)" %
_lbpack)     

        else:
            gg=0
            # ------------------------------------------------------------
            # Read the array from the PP file
            # ------------------------------------------------------------
            if not _lbpack:
                # Unpacked data array
                mm_array = numpy_memmap(pp, mode = 'r',
                                        offset = self.file_offset,
                                        dtype  = self.dtype,
                                        shape  = self.shape)
            else:
                # Packed data array (see e-mail of Fri, 4 Jan 2013 for how
                # to do this, when I get round to it)
                raise ValueError(
"PP data array is packed (lbpack=%d) and so can not be accessed (yet ...)" %
_lbpack)     
            
                mm_array = numpy_memmap(pp, mode = 'r',
                                        offset = self.file_offset,
                                        dtype  = self.dtype,
                                        shape  = (self.size,))
            
                # Now unpack the data array ...
            #--- End: If
            
            indices = parse_indices(mm_array, indices)
            
            array = subspace_array(mm_array, indices)
            
            array = array.copy()
        #--- End: if

        # ------------------------------------------------------------
        # Convert to a masked array
        # ------------------------------------------------------------
#        if hasattr(self, '_FillValue'):
        fill_value = self._FillValue
        print fill_value
        print array
        if fill_value is not None:
            # _FillValue is set so mask any missing values
#            fill_value = self._FillValue
            mask = (array == fill_value)
            if mask.any():                
                array = numpy_ma_array(array, mask=mask, fill_value=fill_value,
                                       copy=False)
                masked = True
            else:
                masked = False
#                array = array.copy()
                
        else:
            # _FillValue is not set, so there are no missing values
            array = numpy_array(array, copy=True)
            masked = False

        # Close the file
        if not gg:
            del mm_array

        # ------------------------------------------------------------
        # Unpack the array using the scale_factor and add_offset, if
        # either is available
        # ------------------------------------------------------------
        if self._scale_factor != 1:
            array *= self._scale_factor

        if self._add_offset:
            array += self._add_offset

        if self._binary_mask:
            if not masked:
                array = numpy_where(array!=0, 1, 0)
            else:
                array = numpy_ma_where(array!=0, 1, 0)

        # ------------------------------------------------------------
        # Return the array
        # ------------------------------------------------------------
        return array
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''
        return '%s%s' % (self.__class__.__name__, self.shape)
    #--- End: def

    def close(self):
        '''

Close the file containing the data array.

If the file is not open then no action is taken.

:Returns:

    None

**Examples**

>>> f.close()

'''
        pp = _filename_to_file.pop(self.file, None)
        if pp is not None:
            pp.close()
    #--- End: def
   
    def copy(self):
        '''

Return a deep copy.

Equivalent to ``copy.deepcopy(a)``.

:Returns:

    out :
        A deep copy.
    
**Examples**

>>> b = a.copy()

'''  
        return type(self)(**self.__dict__)
    #--- End: def
    
    def open(self):
        '''

Return the 

:Returns:

    out : file

**Examples**

>>> f.open()

'''    
        filename = self.file

        pp = _filename_to_file.get(filename, None)
        
        if pp is not None:
            # File is already open
            return pp
      
        if open_files_threshold_exceeded():
            # Close an arbitrary data file to make way for this one
            close_one_file()
            
        pp = open(filename, 'rb')

        # Update _file_to_Dataset dictionary
        _filename_to_file[filename] = pp

        return pp
    #--- End: def

#--- End: class


# ====================================================================
#
# PPFileArrayBounds object
#
# ====================================================================

class PPFileArrayBounds(object):
    '''  
'''
    __slots__ = ('_lower'    ,
                 '_upper'    ,
                 'dtype'     ,
                 'shape'     ,
                 'size'      ,
                 'ndim'      ,
                 )

    def __init__(self, lower, upper):
        '''
'''
        self._lower = lower
        self._upper = upper

        self.dtype = numpy_result_type(lower.dtype, upper.dtype)
        self.shape = lower.shape + (2,)       
        self.size  = lower.size * 2
        self.ndim  = lower.ndim + 1
    #--- End: def
   
    def __getitem__(self, indices):
        '''
x.__getitem__(indices) <==> x[indices]

Returns a numpy array.

'''
        # ------------------------------------------------------------
        # Read the upper and lower bounds from the PP file and stick
        # them together
        # ------------------------------------------------------------
        array = numpy_ma_column_stack((self._lower[...],
                                       self._upper[...],))

        indices = parse_indices(array, indices)

        return subspace_array(array, indices)
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''
        return '%s%s' % (self.__class__.__name__, self.shape)
    #--- End: def
   
    def close(self):
        '''

Close the file containing the data array.

If the file is not open then no action is taken.

:Returns:

    None

**Examples**

>>> a.close()

'''
        # An instance references no open files
        pass
    #--- End: def
   
#--- End: class
