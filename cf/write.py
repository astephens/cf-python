import netCDF4
import random
import string
import json
from os import remove
from os.path import isfile

from numpy import bool_       as numpy_bool_
from numpy import dtype       as numpy_dtype
from numpy import ndenumerate as numpy_ndenumerate
from numpy import integer     as numpy_integer
from numpy import intersect1d as numpy_intersect1d
from numpy import floating    as numpy_floating

from numpy.ma import empty as numpy_ma_empty
from numpy.ma import isMA  as numpy_ma_isMA

from .                 import __Conventions__
from .variable         import Variable
from .coordinate       import Coordinate
from .coordinatebounds import CoordinateBounds
from .functions        import equals, flat, relpath
from .functions        import pickle as cf_pickle
from .fieldlist        import FieldList

from .data.partition   import Partition
from .pp.filearray     import PPFileArray
from .netcdf.filearray import NetCDFFileArray

def write(fields, filename, format='NETCDF3_CLASSIC', overwrite=True,
          verbose=False, cfa_options=None, mode='w'):
    '''
    
Write fields to a CF-netCDF or CFA-netCDF file.
    
NetCDF dimension and variable names will be taken from variables'
`~Variable.ncvar` attributes and the domain attribute
`~Domain.nc_dimensions` if present, otherwise they are inferred from
standard names or set to defaults. NetCDF names may be automatically
given a numerical suffix to avoid duplication.

Output netCDF file global properties are those which occur in the set
of CF global properties and non-standard data variable properties and
which have equal values across all input fields.

Logically identical field components are only written to the file
once, apart from when they need to fulfil both dimension coordinate
and auxiliary coordinate roles for different data variables.

:Parameters:

    fields : (arbitrarily nested sequence of) Field or FieldList
        The field or fields to write to the file.

    filename : str
        The netCDF file to write fields to.

    format : str, optional
        The format of the output file. One of:

           =====================  ================================================
           format                 Description
           =====================  ================================================
           ``'NETCDF3_CLASSIC'``  Output to a CF-netCDF3 classic format file     
           ``'NETCDF3_64BIT'``    Output to a CF-netCDF3 64-bit offset format file 
           ``'NETCDF4_CLASSIC'``  Output to a CF-netCDF4 classic format file      
           ``'NETCDF4'``          Output to a CF-netCDF4 format file              
           ``'CFA'``              Output to an CFA-netCDF(3_CLASSIC) format file 
           =====================  ================================================

        By default the format is ``'NETCDF3_CLASSIC'``. Note that the
        NETCDF3 and NETCDF4_CLASSIC formats may be slower than any of
        the other options.

    overwrite: bool, optional
        If False then raise an exception if the output file
        pre-exists. By default a pre-existing output file is over
        written.

    verbose : bool, optional
        If True then print one-line summaries of each field written.

    cfa_options : dict, optional
        A dictionary giving parameters for configuring the output
        CFA-netCDF file:

           ==========  ===============================================
           Key         Value
           ==========  ===============================================
           ``'base'``  * If ``None`` (the default) then file names
                         within CFA-netCDF files are stored with
                         absolute paths.

                       * If set to an empty string then file names
                         within CFA-netCDF files are given relative to
                         the directory or URL base containing the
                         output CFA-netCDF file.

                       * If set to a string then file names within
                         CFA-netCDF files are given relative to the
                         directory or URL base described by the
                         value. For example: ``'../archive'``.
           ==========  ===============================================        

        By default no parameters are specified.
    
    mode : str, optional
        Specify the mode of write access for the output file. One of:

           =======  =====================================================
           mode     Description
           =======  =====================================================
           ``'w'``  Open a new file for writing to. If it exists and
                    *overwrite* is True then the file is deleted proir to
                    being recreated.
           =======  =====================================================
       
        By default the file is opened with write access mode ``'w'``.

:Returns:

    None

:Raises:

    IOError :
        If *overwrite* is False and the output file pre-exists.

    ValueError :
        If a field does not have information required to write certain
        aspects of a CF-netCDF file.

**Examples**

>>> f
[<CF Field: air_pressure(30, 24)>,
 <CF Field: u_compnt_of_wind(19, 29, 24)>,
 <CF Field: v_compnt_of_wind(19, 29, 24)>,
 <CF Field: potential_temperature(19, 30, 24)>]
>>> write(f, 'file')

>>> type(f)
<class 'cf.field.FieldList'>
>>> type(g)
<class 'cf.field.Field'>
>>> cf.write([f, g], 'file.nc', verbose=True)
[<CF Field: air_pressure(30, 24)>,
 <CF Field: u_compnt_of_wind(19, 29, 24)>,
 <CF Field: v_compnt_of_wind(19, 29, 24)>,
 <CF Field: potential_temperature(19, 30, 24)>]

'''      
    # ----------------------------------------------------------------
    # Initialize dictionary of useful global variables
    # ----------------------------------------------------------------
    g = {'netcdf'            : None,    # - netCDF4.Dataset instance
                                        #-----------------------------
         'nc'                : {},      # - Dictionary of
                                        #   netCDF4.Variable instances
                                        #   keyed by their netCDF
                                        #   variable names.
         'dim_to_ncdim'      : {},      # - Dictionary of netCDF
                                        #   dimension names keyed by
                                        #   their field dimension
                                        #   names.
         'dim_to_ncscalar'   : {},      # - Dictionary of netCDF
                                        #   scalar coordinate names
                                        #   keyed by their field
                                        #   dimension names.
         'ncdim_to_size'     : {},      # - Dictionary of dimension
                                        #   sizes keyed by netCDF
                                        #   dimension names.
         'ncpdim_to_size'    : {},      # - Dictionary of PARTITION
                                        #   dimension sizes keyed by
                                        #   netCDF dimension names.
         'seen'              : {},      # - Dictionary netCDF variable
                                        #   names and netCDF
                                        #   dimensions keyed by
                                        #   elements of the field
                                        #   (such as a coordinate or a
                                        #   transform).
                                        #-----------------------------
         'ncvar_names'       : set(()), # - set of all netCDF
                                        #   dimension and netCDF
                                        #   variable names.
         'global_properties' : set(()), # - Set of global or
                                        #   non-standard CF properties
                                        #   which have identical
                                        #   values across all input
                                        #   fields.         
                                        #-----------------------------
         'dimN'              : 1,       # - Counter
         'auxN'              : 1,       # - Counter
         'scalarN'           : 1,       # - Counter
         'cmN'               : 1,       # - Counter
         'dataN'             : 1,       # - Counter
         'gmN'               : 1,       # - Counter
         'bndN'              : 1,       # - Counter
         'bnddimN'           : 1,       # - Counter
         'strlenN'           : 1,       # - Counter
         'partition_arrayN'  : 1,       # - Counter
         'partitionN'        : 1,       # - Counter
                                        #=============================
                                        # CFA parameters
                                        #=============================
         'cfa'               : False,   # - flag to use the CFA
                                        #   convention, or not.
         'cfa_options'       : {},      # - 
         'CFA_ncdims'        : set(()), # - set of all private CFA
                                        #   netCDF dimension names.
         'CFAdimN'           : 1,       # - Counter
         }

    if format == 'CFA':
        g['cfa'] = True
        format = 'NETCDF3_CLASSIC'
        if cfa_options:
            g['cfa_options'] = cfa_options
    #--- End: if
           
    # ---------------------------------------------------------------
    # Flatten the sequence of intput fields
    # ---------------------------------------------------------------
    fields = FieldList(flat(fields))

    # ---------------------------------------------------------------
    # Still here? Open the output netCDF file.
    # ---------------------------------------------------------------
    if mode != 'w':
        raise ValueError("Can only set mode='w' at the moment")

    if overwrite:
        if isfile(filename):
            remove(filename)
    elif isfile(filename):
        raise IOError(
            "Can't write to an existing file unless overwrite=True")

    g['netcdf'] = netCDF4.Dataset(filename, mode, format=format)

    # ---------------------------------------------------------------
    # Set the fill mode for a Dataset open for writing to off. This
    # will prevent the data from being pre-filled with fill values,
    # which may result in some performance improvements.
    # ---------------------------------------------------------------
    g['netcdf'].set_fill_off()

    # ---------------------------------------------------------------
    # Write global properties to the file first. This is important as
    # doing it later could slow things down enormously. This function
    # also creates the g['global_properties'] set, which is used in
    # the _write_a_field function.
    # ---------------------------------------------------------------
    _create_global_properties(fields, g=g)

    # ---------------------------------------------------------------
    # ---------------------------------------------------------------
    for f in fields:
        _write_a_field(f, g=g)
        if verbose:
            for e in f:            
                print repr(e)
    #-- End: for

    # ---------------------------------------------------------------
    # Write all of the buffered data to disk
    # ---------------------------------------------------------------
    g['netcdf'].close()
#--- End: def

def _check_name(base, counter, g=None, dimsize=None, cfa=False):
    '''

:Parameters:

    base : str

    counter : int

    g : dict

    dimsize : int, optional

    cfa : bool, optional

:Returns:

    ncvar : str

    counter : int

'''
    ncvar_names = g['ncvar_names']

    if dimsize is not None:
        if not cfa:
            if base in ncvar_names and dimsize == g['ncdim_to_size'][base]:
                # Return the name of an existing netCDF dimension with
                # this size
                return base, counter
            
        elif base in g['CFA_ncdims']:
            # Return the name of an existing private CFA-netCDF
            # dimension with this size
            return base, counter
    #--- End: if

    if base in ncvar_names:
        ncvar = '%(base)s_%(counter)d' % locals()
        while ncvar in ncvar_names:
            counter += 1
            ncvar = '%(base)s_%(counter)d' % locals()
    else:
        ncvar = base

    ncvar_names.add(ncvar)

    return ncvar, counter
#--- End: def

def _write_attributes(netcdf_var, netcdf_attrs):
    '''

:Parameters:

    netcdf_var : netCDF4.Variable

    netcdf_attrs : dict

:Returns:

    None

**Examples**


'''
    if hasattr(netcdf_var, 'setncatts'):
        # Use the faster setncatts
        netcdf_var.setncatts(netcdf_attrs)
    else:
        # Otherwise use the slower setncattr
        for attr, value in netcdf_attrs.iteritems():
            netcdf_var.setncattr(attr, value)
#--- End: def

def _character_array(array):
    '''

Convert a numpy string array to a numpy character array wih an extra
trailing dimension

:Parameters:

    array : numpy.ndarray

:Returns:

    out : numpy.ndarray

**Examples**

>>> print a, a.shape, a.dtype.itemsize
['fu' 'bar'] (2,) 3
>>> b = _character_array(a)
>>> print b, b.shape, b.dtype.itemsize
[['f' 'u' ' ']
 ['b' 'a' 'r']] (2, 3) 1

'''
    strlen = array.dtype.itemsize

    new = numpy_ma_empty(array.shape + (strlen,), dtype='S1')
    
    for index, value in numpy_ndenumerate(array): #iterindices(array):
        #new[index] = list(array[index].ljust(strlen, ' ')) 
        new[index] = list(value.ljust(strlen, ' ')) 

    return new
#--- End: def

def _datatype(variable):
    '''

Return the netCDF4.createVariable datatype corresponding to the
datatype of the array of the input variable. E.g. if variable.dtype is
'float32', then 'f4' will be returned.

Numpy string data types will return 'S1' regardless of the numpy
string length. This means that the required conversion of
multi-character datatype numpy arrays into single-character datatype
numpy arrays (with an extra trailing dimension) is expected to be done
elsewhere (currently in def _create_netcdf_variable).

If the input variable has no `~Variable.dtype` attribute (or it is
None) then 'S1' is returned.

'''
    if (not hasattr(variable, 'dtype') or
        variable.dtype.char == 'S'     or
        variable.dtype is None):
        return 'S1'            

    elif variable.dtype is numpy_dtype(bool):
        # Boolean arrays will be written as 8-bit signed integers (0
        # or 1)
        return 'i1'
    
    # Still here?
    return '%s%s' % (variable.dtype.kind, variable.dtype.itemsize)
#--- End: def

def _string_length_dimension(size, g=None):
    # ----------------------------------------------------------------
    # Create a new dimension for the maximum string length
    # ----------------------------------------------------------------
    ncdim, g['strlenN'] = _check_name('strlen%d' % size, g['strlenN'],
                                      dimsize=size, g=g)
    
    if ncdim not in g['ncdim_to_size']:
        # This string length dimension needs creating
        g['ncdim_to_size'][ncdim] = size
        g['netcdf'].createDimension(ncdim, size)
    #--- End: if

    return ncdim
#--- End: def

def _random_hex_string(size=10):
    '''

Return a random hexadecimal string with the given number of
characters.

:Parameters:

    size : int, optional
        The number of characters in the generated string.

:Returns:

    out : str
        The hexadecimal string.

**Examples**

>>> _random_hex_string()
'C3eECbBBcf'
>>> _random_hex_string(6)
'7a4acc'

'''                        
    return ''.join(random.choice(string.hexdigits) for i in xrange(size))
#--- End: def
    
def _cfa_dimension(size, g=None):
    '''

Write an private CFA dimension to the netCDF file, unless one for the
given size already exists. In either case returns the netCDF dimension
name.

Updates g['CFA_ncdims'], g['ncvar_names'], g['netcdf']

:Parameters:

  
    size : int
        The size of the private CFA dimension.

    g : dict

:Returns:

     out : str
         The netCDF dimension name.

**Examples**

>>> _cfa_dimension(10, g=g)
'cfa10'

'''        
    ncdim, g['CFAdimN'] = _check_name('cfa%d' % size, g['CFAdimN'], g=g, 
                                      dimsize=size, cfa=True)

    if ncdim not in g['CFA_ncdims']:
        g['CFA_ncdims'].add(ncdim)
        g['netcdf'].createDimension(ncdim, size)
    #--- End: if

    return ncdim
#--- End: def

def _write_cfa_variable(ncvar, ncdimensions, netcdf_attrs, data,
                        fill_value=None, g=None):
    '''

Write a CFA variable to the file.

Any CFA private variables required will be autmatically created and
written to the file.

:Parameters:

    ncvar : str
        The netCDF name for the variable.

    ncdimensions : sequence of str

    netcdf_attrs : dict

    data : cf.Data
        
    g : dict

:Returns:

    None

**Examples**

'''
    fill_value = getattr(data, '_FillValue', None)
    g['nc'][ncvar] = g['netcdf'].createVariable(ncvar, _datatype(data), (),
                                                fill_value=fill_value)

    netcdf_attrs['cf_role']        = 'cfa_variable'
    netcdf_attrs['cfa_dimensions'] = ' '.join(ncdimensions)

    # Create a dictionary representation of the data
    cfa_array = data.dumpd(dimensions=ncdimensions)

    # Modify the dictionary so that it is suitable for JSON
    # serialization
    del cfa_array['_FillValue']
    del cfa_array['shape']
    master_dimensions = cfa_array.pop('dimensions')
    master_units      = cfa_array.pop('Units')
    master_dtype      = cfa_array.pop('dtype')

    if not cfa_array['pmshape']:
        del cfa_array['pmshape']
    if not cfa_array['pmdimensions']:
        del cfa_array['pmdimensions']

    conform_args = data.conform_args(revert_to_file=True,
                                     dimensions=master_dimensions,
                                     directions=cfa_array['directions'])
 
    if 'base' in g['cfa_options'] and g['cfa_options']['base'] is not None:
        cfa_array['base'] = g['cfa_options']['base']

    for attrs in cfa_array['Partitions']:
        format = attrs.get('format', None)

        if format is None:
            # --------------------------------------------------------
            # This partition has an internal sub-array
            # --------------------------------------------------------
            # Create the data array for this partition.
            dimensions = attrs.pop('dimensions', master_dimensions)

            array = Partition(location=attrs['location'],
                              part=attrs.pop('part', []),
                              _directions=attrs.pop('directions', cfa_array['directions']),
                              _dimensions=dimensions,
                              Units=attrs.pop('Units', master_units),
                              subarray=attrs['subarray']).dataarray(**conform_args)

            # Boolean arrays are written as 8-bit signed integers (0
            # or 1)
            if array.dtype is numpy_dtype(bool):
                array = array.astype('int32')
                
            shape = array.shape        
            ncdim_strlen = []
            if array.dtype.kind == 'S':
                strlen = array.dtype.itemsize    
                if strlen > 1:
                    # Convert the string array to a character array
                    array = _character_array(array)
                     # Get the netCDF dimension for the string length
                    ncdim_strlen = [_string_length_dimension(strlen, g=None)]
            #--- End: if

            # Create a name for the netCDF variable to contain the array
            p_ncvar = 'cfa_'+_random_hex_string()
            while p_ncvar in g['ncvar_names']:
                p_ncvar = 'cfa_'+_random_hex_string()
            #--- End: while
            g['ncvar_names'].add(p_ncvar)
          
            # Get the private CFA netCDF dimensions for the array.
            cfa_dimensions = [_cfa_dimension(n, g=g) for n in array.shape]
            
            # Create the private CFA variable and write the array to it
            v = g['netcdf'].createVariable(p_ncvar, _datatype(array),
                                           cfa_dimensions + ncdim_strlen,
                                           fill_value=fill_value)
            
            _write_attributes(v, {'cf_role': 'cfa_private'})
            v[...] = array

            # Update the attrs dictionary.
            #
            # Note that we don't need to set 'part', 'dtype', 'units',
            # 'calendar', 'dimensions' and 'directions' since a
            # partition's in-memory data array always matches up with
            # the master data array.
            attrs['subarray'] = {'shape' : shape,
                                 'ncvar' : p_ncvar}

        else:
            # --------------------------------------------------------
            # This partition has an external sub-array
            # --------------------------------------------------------
            # punits, pcalendar: Change from Units object to netCDF
            #                    string(s)
            units = attrs.pop('Units', None)
            if units is not None:
                attrs['punits'] = units.units
                if hasattr(units, 'calendar'):
                    attrs['pcalendar'] = units.calendar

            # pdimensions: 
            pdimensions = attrs.pop('dimensions', None)
            if pdimensions is not None:
                attrs['pdimensions'] = pdimensions

            # pdirections: 
            pdirections = attrs.pop('directions', None)
            if pdirections is not None:
                attrs['pdirections'] = pdirections

            # dtype: Change from numpy.dtype object to netCDF string
            dtype = attrs['subarray'].pop('dtype', None)
            if dtype is not None:
                if dtype.kind != 'S':
                    attrs['subarray']['dtype'] = _convert_to_netCDF_datatype(dtype)

            # format: 
            sformat = attrs.pop('format', None)
            if sformat is not None:
                attrs['subarray']['format'] = sformat
        #--- End: if
 
        # location: Change from python to CFA indexing (i.e. range
        #           includes the final index)
        attrs['location'] = [(x[0], x[1]-1) for x in attrs['location']]
        
        # part: Change from python to to CFA indexing (i.e. slice
        #       range includes the final index)
        part = attrs.get('part', None)
        if part:
            p = []
            for x, size in zip(part, attrs['subarray']['shape']):
                if isinstance(x, slice):
                    x = x.indices(size)
                    if x[2] > 0:
                        p.append((x[0], x[1]-1, x[2]))
                    elif x[1] == -1:
                        p.append((x[0], 0, x[2]))
                    else:
                        p.append((x[0], x[1]+1, x[2]))
                else:
                    p.append(x)
            #--- End: for
            attrs['part'] = str(p)
        #--- End: if
                
        # Make file name relative to base, if required.
        if 'base' in cfa_array and 'file' in attrs['subarray']:
            attrs['subarray']['file'] = relpath(attrs['subarray']['file'],
                                                cfa_array['base'])            
    #--- End: for

    # Add the description (as a JSON string) of the partition array to
    # the netcdf attributes.
    netcdf_attrs['cfa_array'] = json.dumps(cfa_array,
                                           default=_convert_to_builtin_type)

    # Write the netCDF attributes to the file
    _write_attributes(g['nc'][ncvar], netcdf_attrs)
#--- End: def

def _convert_to_netCDF_datatype(dtype):
    '''

Convert a numpy.dtype object to a netCDF data type string.

:Parameters:

    dtype : numpy.dtype

:Returns:

    out : str

**Examples**

>>> _convert_to_netCDF_datatype(numpy.dtype('float32'))
'float'
>>> _convert_to_netCDF_datatype(numpy.dtype('float64'))
'double'
>>> _convert_to_netCDF_datatype(numpy.dtype('int8'))
'byte'

'''    
    if dtype.char is 'f':
        return 'float'
    if dtype.char is 'd':
        return 'double'
    if dtype.kind is 'i':  # long int??
        return 'int'
    if dtype.char is 'S':
        return 'char'
    if dtype.char is 'b':
        return 'byte'
    if dtype.char is 'h':
        return 'short'

    raise TypeError("Ho hum de hum")
#--- End: def

def _convert_to_builtin_type(x):
    '''

Convert a non-JSON-encodable object to a JSON-encodable built-in type.

Possible conversions are:

==============  =============  ======================================
Input object    Output object  numpy data types covered
==============  =============  ======================================
numpy.bool_     bool           bool
numpy.integer   int            int, int8, int16, int32, int64, uint8,
                               uint16, uint32, uint64
numpy.floating  float          float, float16, float32, float64
==============  =============  ======================================

:Parameters:

    x : 
        
:Returns: 

    out :

:Raises:

    TypeError :
        If *x* can't be converted to a JSON serializableis type.

**Examples**

>>> type(_convert_to_netCDF_datatype(numpy.bool_(True)))
bool
>>> type(_convert_to_netCDF_datatype(numpy.array([1.0])[0]))
double
>>> type(_convert_to_netCDF_datatype(numpy.array([2])[0]))
int

''' 
    if isinstance(x, numpy_bool_):
        return bool(x)

    if isinstance(x, numpy_integer):
        return int(x)
     
    if isinstance(x, numpy_floating):
        return float(x)

    raise TypeError("%s object is not JSON serializable: %s" %
                    (type(x), repr(x)))
#--- End: def


def _grid_ncdimensions(f, key, g=None):
    '''

Return a tuple of the netCDF names for the dimensions of a coordinate
or cell measures variable.

:Parameters:

    f : Field

    key : str

    g : dict

:Returns:

    out : tuple

'''
    domain = f.domain
    
    if domain[key].ndim == 0:
        return ()
    else:
        return tuple(g['dim_to_ncdim'][dim] 
                     for dim in domain.dimensions[key])
#--- End: def
    
def _grid_ncvar(f, key, default, counter, g=None):
    '''
    
:Returns:

    f : Field

    key : str
        A key of the field's domain.
       
    default : str

    counter : int

    g : dict

'''
    variable = f.domain[key]
    ncvar = getattr(variable, 'ncvar', variable.identity(default=default))
    
    return _check_name(ncvar, counter, g=g)        
#--- End: def
    

def _write_dimension(ncdim, f, dim, g=None):
    '''

Write a dimension to the netCDF file.

Updates g['dim_to_ncdim'].

:Parameters:

    ncdim : str
        The netCDF name for the dimension.

    f : Field
   
    dim : str
        The field's domain's identifier for the dimension.

    g : dict

:Returns:

     None

'''        
    ncdim_to_size = g['ncdim_to_size']
    
    ncdim_to_size[ncdim] = f.domain.dimension_sizes[dim]
    g['dim_to_ncdim'][dim] = ncdim
    
    g['netcdf'].createDimension(ncdim, ncdim_to_size[ncdim])
#--- End: def

def _write_dimension_coordinate(f, key, g=None):
    '''

Write a dimension coordinate and bounds to the netCDF file.

This also writes a new netCDF dimension to the file and, if required,
a new netCDF bounds dimension.

Updates g['dimN'] and g['dim_to_ncdim'], g['seen'].

:Parameters:

    f : Field
   
    key : str

    g : dict

:Returns:

    out : str
        The netCDF name of the dimension coordinate.

'''       
    seen  = g['seen']
    
    domain = f.domain
    coord = domain[key]

    create = False
    if not _seen(coord, g=g):
        create = True
    elif seen[id(coord)]['ncdims'] != ():
        if seen[id(coord)]['ncvar'] != seen[id(coord)]['ncdims'][0]:
            # Already seen this coordinate but it was an auxiliary
            # coordinate, so it needs to be created as a dimension
            # coordinate.
            create = True
    #--- End: if

    if create:
        ncdim, g['dimN'] = _grid_ncvar(f, key, 'dim', g['dimN'], g=g)

        # Create a new dimension, if it is not a scalar coordinate
        if coord.ndim > 0:
            _write_dimension(ncdim, f, key, g=g)

        ncdimensions = _grid_ncdimensions(f, key, g=g)
        
        # If this dimension coordinate has bounds then create the
        # bounds netCDF variable and add the bounds or climatology
        # attribute to the dictionary of extra attributes
        extra = _write_coordinate_bounds(coord, ncdimensions, ncdim, g=g)

        # Create a new dimension coordinate variable
        _create_netcdf_variable(ncdim, ncdimensions, coord, 
                                extra=extra, g=g)
    else:
        ncdim = seen[id(coord)]['ncvar']
    #--- End: if

    try:    ### ????? why not always do this dch??
        g['dim_to_ncdim'][key] = ncdim
    except KeyError:
        pass

    return ncdim
#--- End: def

def _seen(variable, ncdims=None, g=None):
    '''

Return True if a variable is logically equal any variable in the
g['seen'] dictionary.

If this is the case then the variable has already been written to the
output netCDF file and so we don't need to do it again.

If 'ncdims' is set then a extra condition for equality is applied,
namely that of 'ncdims' being equal to the netCDF dimensions (names
and order) to that of a variable in the g['seen'] dictionary.

When True is returned, the input variable is added to the g['seen']
dictionary.

Updates g['seen'].

:Parameters:

    variable : 

    ncdims : tuple, optional

    g : dict

:Returns:

    out : bool
        True if the variable has already been written to the file,
        False otherwise.

'''
    seen = g['seen']

    for key, value in seen.iteritems():
        if ncdims is not None and ncdims != value['ncdims']:
            # The netCDF dimensions (names and order) of the input
            # variable are different to those of this variable in
            # the 'seen' dictionary
            continue

        # Still here?
        if variable.equals(value['variable']):
            seen[id(variable)] = {'variable': variable,
                                  'ncvar'   : value['ncvar'],
                                  'ncdims'  : value['ncdims']}
            return True
    #--- End: for

    return
#--- End: def

def _write_coordinate_bounds(coord, coord_ncdimensions, coord_ncvar, g=None):
    '''

Create a coordinate's bounds netCDF variable, creating a new bounds
netCDF dimension if required. Return the bounds variable's netCDF
variable name.

Updates g['bnddimN'], g['bndN'], g['netcdf'].

:Parameters:

    coord : Coordinate

    coord_ncdimensions : tuple
        The ordered netCDF names of the coordinate's dimensions (which
        do not include the bounds dimension).

     coord_ncvar : str
        The netCDF variable of the coordinate.
    g : dict

:Returns:

    out : dict

**Examples**

>>> extra = _write_coordinate_bounds(c, ('dim2',), g=g)

'''
    extra = {}

    if not (coord._isbounded and coord.bounds._hasData):
        return extra

    # Still here? Then this coordinate has a bounds attribute
    # which contains data.
    bounds = coord.bounds

    size = bounds.shape[-1]

    ncdim, g['bnddimN'] = _check_name('bounds%(size)d' % locals(),
                                      g['bnddimN'], dimsize=size, g=g)

    # Check if this bounds variable has not been previously
    # created.
    ncdimensions = coord_ncdimensions +(ncdim,)        
    if _seen(bounds, ncdimensions, g=g):
        # This bounds variable has been previously created, so no
        # need to do so again.
        ncvar = g['seen'][id(bounds)]['ncvar']

    else:

        # This bounds variable has not been previously created, so
        # create it now.
        ncdim_to_size = g['ncdim_to_size']
        if ncdim not in ncdim_to_size:
            ncdim_to_size[ncdim] = size
            g['netcdf'].createDimension(ncdim, size) #ncdim_to_size[ncdim])
        #--- End: if
        
        ncvar = getattr(bounds, 'ncvar', '%s_bounds' % coord_ncvar)
        
        ncvar, g['bndN'] = _check_name(ncvar, g['bndN'], g=g)
        
        # Note that, in a field, bounds always have equal units to
        # their parent coordinate
        
        # Create the bounds netCDF variable
        _create_netcdf_variable(ncvar, ncdimensions, bounds,
                                omit=('units', 'calendar'), g=g)
    #--- End: if

    if getattr(coord, 'climatology', None):
        extra['climatology'] = ncvar
    else:
        extra['bounds'] = ncvar
        
    return extra
#--- End: def
        
def _write_scalar_coordinate(f, dim, coordinates, g=None):
    '''

Write a scalar coordinate and bounds to the netCDF file.

It is assumed that the input coordinate is has size 1, but this is not
checked.

If an equal scalar coordinate has already been written to the file
then the input coordinate is not written.

Updates g['scalarN'], g['dim_to_ncscalar'].

:Parameters:

    f : Field
   
    dim : str

    coordinates : list

    g : dict

:Returns:

    coordinates : list
        The updated list of netCDF auxiliary coordinate names.

'''
    coord = f.domain[dim]
    coord.squeeze()

    if not _seen(coord, (), g=g):
        ncvar, g['scalarN'] = _grid_ncvar(f, dim, 'scalar', 
                                          g['scalarN'], g=g)
        
        # If this scalar coordinate has bounds then create the
        # bounds netCDF variable and add the bounds or climatology
        # attribute to the dictionary of extra attributes
        extra = _write_coordinate_bounds(coord, (), ncvar, g=g)

        # Create a new auxiliary coordinate variable
        _create_netcdf_variable(ncvar, (), coord, extra=extra, g=g)

        g['dim_to_ncscalar'][dim] = ncvar

    else:
        # This scalar coordinate has already been written to the
        # file
        ncvar = g['seen'][id(coord)]['ncvar']
    #--- End: if

    coordinates.append(ncvar)

    return coordinates
#--- End: def

def _write_auxiliary_coordinate(f, key, coordinates, g=None):
    '''

Write an auxiliary coordinate and its bounds to the netCDF file.

If an equal auxiliary coordinate has already been written to the file
then the input coordinate is not written.

Updates g['auxN'].

:Parameters:

    f : Field
   
    key : str

    coordinates : list

    g : dict

:Returns:

    coordinates : list
        The list of netCDF auxiliary coordinate names updated in
        place.

**Examples**

>>> coordinates = _write_auxiliary_coordinate(f, 'aux2', coordinates, g=g)

'''
    coord = f.domain[key]
    
    ncdimensions = _grid_ncdimensions(f, key, g=g)

    if _seen(coord, ncdimensions, g=g):
        ncvar = g['seen'][id(coord)]['ncvar']
    
    else:
        ncvar, g['auxN'] = _grid_ncvar(f, key, 'aux', g['auxN'], g=g)
        
        # If this auxiliary coordinate has bounds then create the
        # bounds netCDF variable and add the bounds or climatology
        # attribute to the dictionary of extra attributes
        extra = _write_coordinate_bounds(coord, ncdimensions, ncvar, g=g)

        # Create a new auxiliary coordinate variable
        _create_netcdf_variable(ncvar, ncdimensions, coord,
                                extra=extra, g=g)
    #--- End: if

    coordinates.append(ncvar)

    return coordinates
#--- End: def
  
def _write_cell_measure(f, key, cell_measures, g=None):
    '''

Write an auxiliary coordinate and bounds to the netCDF file.

If an equal cell measure has already been written to the file then the
input coordinate is not written.

Updates g['cmN'].

:Parameters:

    f : Field
        The field containing the cell measure.

    key : str
        The domain identifier of the cell measure (e.g. 'cmo').

    cell_measures : list

    g : dict

:Returns:

    cell_measures : list
        The updated list of netCDF cell_measures 'measure: name'
        pairings.

**Examples**

'''
    cell_measure = f.domain[key]

    ncdimensions = _grid_ncdimensions(f, key, g=g)

    if _seen(cell_measure, ncdimensions, g=g):
        ncvar = g['seen'][id(cell_measure)]['ncvar']
    else:
        ncvar, g['cmN'] = _grid_ncvar(f, key, 'cm', g['cmN'], g=g)
        
        if not hasattr(cell_measure, 'measure'):
            raise ValueError(
                "Can't write a cell measure without a 'measure' attribute")

        _create_netcdf_variable(ncvar, ncdimensions, cell_measure, g=g)
    #--- End: if
            
    # Update the cell_measures list
    cell_measures.append('%s: %s' % (cell_measure.measure, ncvar))

    return cell_measures
#--- End: def
  

def _write_grid_mapping(transform, grid_mapping, g=None):
    '''

Write a grid mapping transform to the netCDF file.

Updates g['seen'].

:Parameters:

    transform : Transform
        The grid mapping transform to write to the file.

    grid_mapping : list

    g : dict

:Returns:

    grid_mapping : list
        The updated list of netCDF grid_mapping names.

**Examples**

'''
    if _seen(transform, g=g):
        # Use existing grid_mapping
        ncvar = g['seen'][id(transform)]['ncvar']

    else:
        # Create a new grid mapping
        ncvar = getattr(transform, 'ncvar', transform.name)
    
        ncvar, g['gmN'] = _check_name(ncvar, g['gmN'], g=g) 
                        
        g['nc'][ncvar] = g['netcdf'].createVariable(ncvar, 'S1', ())
        
        # Add properties from key/value pairs
        if hasattr(g['nc'][ncvar], 'setncatts'):
            # Use the faster setncatts
            g['nc'][ncvar].setncatts(transform)
        else:
            # Otherwise use the slower setncattr
            for attr, value in transform.iteritems():
                g['nc'][ncvar].setncattr(attr, value)
        #--- End: if
        
        # Update the 'seen' dictionary. Note that grid mappings have
        # no dimensions.
        g['seen'][id(transform)] = {'variable': transform, 
                                    'ncvar'   : ncvar, 
                                    'ncdims'  : ()}
    #--- End: if

    grid_mapping.append(ncvar)

    return grid_mapping
#--- End: def

def _create_netcdf_variable(ncvar, dimensions, cfvar, omit=(), extra={},
                            g=None):
    '''

Create a netCDF variable fron 'cfvar' with name 'ncvar' and dimensions
'ncdimensions'. The netCDF variable's properties are given by
cfvar.properties(), less any given by the 'omit' argument. If a new
string-length netCDF dimension is required then it will also be
created. The 'seen' dictionary is updated for 'cfvar'.

Updates g['strlenN'], g['ncdim_to_size'], g['netcdf'], g['nc'],
        g['seen'].

:Parameters:

    ncvar : str
        The netCDF name of the variable.

    dimensions : tuple
        The netCDF dimension names of the variable

    cfvar : Variable
        The variable (coordinate, cell measure or field) to write to
        the file.

    omit : sequence, optional

    extra : dict, optional

    g : dict

:Returns:

    None

'''
    # Set the netCDF4.createVariable datatype
    datatype = _datatype(cfvar)

    ncdimensions = dimensions
    
    if not cfvar._hasData:
        data = None
    else:
        data = cfvar.Data            
        conform_args = data.conform_args()

        if datatype == 'S1':

            # Convert a multi-character data type numpy array into
            # a single-character data type numpy array with an
            # extra trailing dimension.
            strlen = data.dtype.itemsize
            if strlen > 1:
                ncdim = _string_length_dimension(strlen, g=g)
                    
                ncdimensions = dimensions + (ncdim,)
                 
                data = data.copy()

                for partition in data.partitions.flat:
                    
                    array = partition.dataarray(**conform_args)
                    
                    # Convert the string array to a character
                    # array
                    partition.subarray = _character_array(array)

                    partition.dimensions.append(ncdim)
                    partition.shape.append(strlen)
                    partition.location.append((0, strlen))
                    partition.directions[ncdim] = True

                    partition.close()
                #--- End: for

                data._shape += (strlen,)
                data.dimensions.append(ncdim)
                data.directions[ncdim] = True

                conform_args['dimensions'] = data.dimensions
                conform_args['directions'] = data.directions
                conform_args['save']       = False
        #--- End: if
    #--- End: if

    # Find the fill value (note that this is set in the call to
    # netCDF4.createVariable, rather than with setncattr).
    fill_value = cfvar._fill_value

    # Add simple properties (and units and calendar) to the netCDF
    # variable
    netcdf_attrs = cfvar.properties
    for attr in ('units', 'calendar'):
        value = getattr(cfvar, attr, None)
        if value is not None:
            netcdf_attrs[attr] = value
    #--- End: for

    netcdf_attrs.update(extra)

    for attr in omit:
        netcdf_attrs.pop(attr, None) 

    is1d_coord = (isinstance(cfvar, Coordinate)       and cfvar.ndim <= 1 or
                  isinstance(cfvar, CoordinateBounds) and cfvar.ndim <= 2)

    if not g['cfa'] or data.in_memory or is1d_coord:
        #---------------------------------------------------------
        # Write a normal netCDF variable 
        #---------------------------------------------------------

        # ------------------------------------------------------------
        # Create a new netCDF variable and set the _FillValue
        # ------------------------------------------------------------ 
        try:
            g['nc'][ncvar] = g['netcdf'].createVariable(ncvar, datatype, 
                                                        ncdimensions,
                                                        fill_value=fill_value)
        except RuntimeError:
            raise RuntimeError("Can't write data type %s to a %s file %s" %
                               (datatype, g['netcdf'].file_format, repr(cfvar)))

        _write_attributes(g['nc'][ncvar], netcdf_attrs)

        #-------------------------------------------------------------
        # Add data to the netCDF variable
        #
        # Note that we don't need to worry about scale_factor and
        # add_offset, since if a partition's data array is *not* a
        # numpy array, then it will have its own scale_factor and
        # add_offset parameters which will be applied when the array
        # is realised, and the python netCDF4 package will deal with
        # the case when scale_factor or add_offset are set as
        # properties on the variable.
        # -------------------------------------------------------------
        if data is not None:  

            if not fill_value:
                missing_data = None
            else:
                _FillValue    = getattr(cfvar, '_FillValue', None) 
                missing_value = getattr(cfvar, 'missing_value', None)            
                missing_data = [value for value in (_FillValue, missing_value)
                                if value is not None]
            #--- End: if

            conform_args['revert_to_file'] = True
            for partition in data.partitions.flat:
                array = partition.dataarray(**conform_args)   

                if array.dtype is numpy_dtype(bool):
                    array = array.astype('int32')                    

                # Check that the array doesn't contain any elements
                # which are equal to the missing data values
                if missing_data:
                    if numpy_ma_isMA(array):
                        temp_array = array.compressed()
                    else:
                        temp_array = array

                    if numpy_intersect1d(missing_data, temp_array):
                        raise ValueError(
"ERROR: Array has _FillValue or missing_value at unmasked point: %s" %
repr(cfvar))
                #--- End: if

                g['nc'][ncvar][partition.indices] = array        

                partition.close()
            #--- End: for                  
        #--- End: if

        # Update the 'seen' dictionary
        g['seen'][id(cfvar)] = {'variable': cfvar,
                                'ncvar'   : ncvar,
                                'ncdims'  : dimensions}

        return

    elif data is not None:
        #---------------------------------------------------------
        # Write a CFA variable 
        #---------------------------------------------------------
        _write_cfa_variable(ncvar, ncdimensions, netcdf_attrs, data, 
                            fill_value=fill_value, g=g)

        return
   #--- End: if
#--- End: def
 
def _write_a_field(f, save_dim_to_ncdim=False, add_to_seen=False, g=None):
    '''

:Parameters:

    f : Field

    save_dim_to_ncdim : bool, optional

    add_to_seen : bool, optional

    g : dict

:Returns:

    None

'''
    seen = g['seen']

    if save_dim_to_ncdim:
        saved_dim_to_ncdim = g['dim_to_ncdim'].copy()
        
    if add_to_seen:
        id_f = id(f)
        org_f = f
        
    f = f.copy()

    domain     = f.domain
    dimensions = domain.dimensions

    g['dim_to_ncdim'] = {}

    # Initialize the list of the field's auxiliary coordinates
    coordinates = []

    for dim in sorted(domain.dimension_sizes):
        if dim in domain:
            # ----------------------------------------------------
            # A dimension coordinate exists for this dimension, so
            # create a netCDF dimension and a netCDF coordinate
            # variable.
            # ----------------------------------------------------
            if dimensions[dim][0] in dimensions['data']:
                # The field's data array spans this dimension, so
                # write it to the file as a 1-d dimension
                # coordinate.
                ncdim = _write_dimension_coordinate(f, dim, g=g)

            else:
                # The field's data array does not span this
                # dimension.
                write_as_scalar_coordinate = True
                for key in domain.get_keys('^aux|^cm'):
                    if dim in dimensions[key]:
                        write_as_scalar_coordinate = False
                        break
                #--- End: for

                if write_as_scalar_coordinate:
                    # There are NO auxiliary coordinates/cell
                    # measures which span this dimension, so write
                    # to the file as a scalar coordinate.
                    coordinates = _write_scalar_coordinate(f, dim,
                                                           coordinates,
                                                           g=g)
                else:
                    # There ARE auxiliary coordinates/cell
                    # measures which span this dimension, so it
                    # needs to be written to the file as a 1-d
                    # dimension coordinate and the field's data
                    # array needs to be expanded to include the
                    # dimension.
                    ncdim = _write_dimension_coordinate(f, dim, g=g)

                    f.expand_dims(dim)
                #--- End: if
            
        else:
            # ----------------------------------------------------
            # There is no dimension coordinate for this dimension,
            # so just create a netCDF dimension if it spans the
            # field's data array.
            # ----------------------------------------------------
            if dim not in dimensions['data']:
                # If there are auxiliary coordinates/cell measures
                # which span this dimension, then expand the
                # field's data array to include it.
                for key in domain.get_keys('^aux|^cm'):
                    if dim in dimensions[key]:
                        f.expand_dims(dim)
                        break
            #--- End: if

            # Create the new dimension if the field's data array
            # spans it
            if dim in dimensions['data']:
                try:
                    ncdim = domain.nc_dimensions[dim]
                except (AttributeError, KeyError):
                    ncdim = 'dim'
                    
                ncdim, g['dimN'] = _check_name(ncdim, g['dimN'], g=g)

                _write_dimension(ncdim, f, dim, g=g)
             #--- End: if
        #--- End: if

    #--- End: for

    # ------------------------------------------------------------
    # Create auxiliary coordinate variables, except those which
    # might be completely specified elsewhere by a transformation.
    # ------------------------------------------------------------
    # Initialize the list of 'coordinates' attribute variable
    # values (each of the form 'name')
    for key in sorted(domain.get_keys('^aux')):
        coordinates = _write_auxiliary_coordinate(f, key,
                                                  coordinates,
                                                  g=g)
    #--- End: for

    # ------------------------------------------------------------
    # Create cell measures variables
    # ------------------------------------------------------------
    # Initialize the list of 'cell_measures' attribute values
    # (each of the form 'measure: name')
    cell_measures = []
    for key in domain.cell_measures(): #sorted(domain.get_keys('^cm')):            
        cell_measures = _write_cell_measure(f, key, cell_measures, g=g)

    # ------------------------------------------------------------
    # Create grid mapping and formula terms variables
    # ------------------------------------------------------------
    # Initialize the list of 'grid_mapping' attribute variable
    # values (each of the form 'name')
    grid_mapping  = []
    formula_terms = []
    for transform in domain.transforms.values():

        if transform.isgrid_mapping: 
            # ------------------------------------------------
            # This transform is a grid mapping
            # ------------------------------------------------
            grid_mapping = _write_grid_mapping(transform, grid_mapping, g=g)

        else:
            # ------------------------------------------------
            # This transform is a formula_terms
            # ------------------------------------------------

            key = domain.coord(transform.name, exact=True, key=True)
            if key is None:
                # This formula_terms doesn't match up with an existing
                # coordinate
                continue
            
            # Still here? Then create the formula_terms attribute
            # string
            for term, variable in transform.iteritems():
                
                if variable is None:
                    # Do not create broken formula_terms
                    formula_terms = None
                    break
            
                if isinstance(variable, Variable):
                    if not _seen(variable, g=g):
                        # var is a variable which hasn't been written
                        # yet (set save=True so we don't lose the
                        # current the dim_to_ncdim dictionary).
                        _write_a_field(variable, save_dim_to_ncdim=True,
                                       add_to_seen=True, g=g)
                elif variable in domain:
                    # var is a key of the domain, such as 'dim1'
                    variable = domain[variable]
                else:
                    # Do not create broken formula_terms
                    formula_terms = None
                    break
                #--- End: if
               
                # Record the netCDF variable name for the
                # formula_terms string
                ncvar = seen[id(variable)]['ncvar']
                formula_terms.append('%(term)s: %(ncvar)s' % locals())
            #--- End: for
    
            # Add the formula_terms attribute to the output
            # variable
            if formula_terms:
                ncvar = seen[id(domain[key])]['ncvar']
                g['nc'][ncvar].setncattr('formula_terms', ' '.join(formula_terms))
        #--- End: if

    #--- End: for

    # ------------------------------------------------------------
    # Create ancillary variables
    # ------------------------------------------------------------
    # Initialize the list of 'ancillary_variables' attribute
    # values
    ancillary_variables = []
    if hasattr(f, 'ancillary_variables'):
        for av in f.ancillary_variables:
            _write_a_field(av, save_dim_to_ncdim=True, add_to_seen=True,
                           g=g)
            ancillary_variables.append(seen[id(av)]['ncvar'])
    #--- End: if

    # ------------------------------------------------------------
    # Create the data variable
    # ------------------------------------------------------------
    ncvar = getattr(f, 'ncvar', f.identity(default='data'))

    ncvar, g['dataN'] = _check_name(ncvar, g['dataN'], g=g)

    dim_to_ncdim    = g['dim_to_ncdim']
    dim_to_ncscalar = g['dim_to_ncscalar']

    ncdimensions = tuple(dim_to_ncdim[dim] 
                         for dim in domain.dimensions['data'])

    extra = {}

    # Cell measures
    if cell_measures:
        extra['cell_measures'] = ' '.join(cell_measures)           

    # Auxiliary/scalar coordinates
    if coordinates:
        extra['coordinates'] = ' '.join(coordinates)

    # Grid mapping
    if grid_mapping: 
        extra['grid_mapping']  = ' '.join(grid_mapping)

    # Ancillary variables
    if ancillary_variables:
        extra['ancillary_variables'] = ' '.join(ancillary_variables)
        
    # Flag values
    if hasattr(f, 'flag_values'):
        extra['flag_values'] = f.flag_values

    # Flag masks
    if hasattr(f, 'flag_masks'):
        extra['flag_masks'] = f.flag_masks

    # Flag meanings
    if hasattr(f, 'flag_meanings'):
        extra['flag_meanings'] = ' '.join(f.flag_meanings)

    # Cell methods
    if hasattr(f, 'cell_methods'):
        cell_methods = f.cell_methods.copy()

        for cm in cell_methods:
            if cm['name'] == ['area']:
                continue

            # Still here?
            for i, dim in enumerate(cm['dim']):
                if dim in dim_to_ncdim:
                    cm['name'][i] = dim_to_ncdim[dim]
                elif dim in dim_to_ncscalar:
                    cm['name'][i] = dim_to_ncscalar[dim]
        #--- End: for

        extra['cell_methods'] = ' '.join(cell_methods.strings())
    #--- End: if

    # Create a new data variable
    _create_netcdf_variable(ncvar, ncdimensions, f,
                            omit  = g['global_properties'],
                            extra = extra,
                            g=g)
    
    # ------------------------------------------------------------
    # If we saved the original dim_to_ncdim then reinstate it
    # ------------------------------------------------------------
    if save_dim_to_ncdim:
        g['dim_to_ncdim'] = saved_dim_to_ncdim

    # Update the 'seen' dictionary, if required
    if add_to_seen:
        g['seen'][id_f] = {'variable': org_f,
                           'ncvar'   : ncvar,
                           'ncdims'  : ncdimensions}
#--- End: def

def _create_global_properties(fields, g=None):
    '''

Find the netCDF global properties from all of the input fields and
write them to the netCDF4.Dataset.

Updates g['global_properties'].

:Parameters:

    fields : FieldList

    g : dict

:Returns:

    None

'''
    # Data variable properties, as defined in Appendix A, without
    # those which are not simple.
    data_properties = set(('add_offset',
                           'cell_methods',
                           '_FillValue',
                           'flag_masks',
                           'flag_meanings',
                           'flag_values',
                           'long_name',
                           'missing_value',
                           'scale_factor',
                           'standard_error_multiplier',
                           'standard_name',
                           'units',
                           'valid_max',
                           'valid_min',
                           'valid_range',
                           ))

    # Global properties, as defined in Appendix A
    global_properties = set(('comment',
                             'Conventions',
                             'history',
                             'institution',
                             'references',
                             'source',
                             'title',
                             ))

    # Put all non-standard CF properties (i.e. those not in the
    # data_properties set) into the global_properties set.
    for f in fields:
        for attr in set(f._simple_properties()) - global_properties:
            if attr not in data_properties:
                global_properties.add(attr)
    #--- End: for

    # Remove properties from the new global_properties set which
    # have different values in different fields
    f0 = fields[0]
    for prop in tuple(global_properties):
        if not f0.hasprop(prop):
            global_properties.remove(prop)
            continue
            
        prop0 = f0.getprop(prop)

        if len(fields) > 1:
            for f in fields[1:]:
                if (not f.hasprop(prop) or 
                    not equals(f.getprop(prop), prop0, traceback=False)):
                    global_properties.remove(prop)
                    break
    #--- End: if

    # Write the global properties to the file
    Conventions = __Conventions__
    if g['cfa']:
        Conventions += ' CFA'

    g['netcdf'].setncattr('Conventions', Conventions)
    
    for attr in global_properties - set(('Conventions',)):
        g['netcdf'].setncattr(attr, f0.getprop(attr)) 

    g['global_properties'] = global_properties
#--- End: def
