import numpy

from numpy.ma import masked as numpy_ma_masked

from copy      import deepcopy
from itertools import izip

from .ancillaryvariables import AncillaryVariables
from .cellmethods        import CellMethods
from .comparison         import Comparison
from .flags              import Flags
from .domain             import Domain
from .functions          import parse_indices, CHUNKSIZE
from .variable           import Variable, SubspaceVariable

from .data.data import Data

# ====================================================================
#
# Field object
#
# ====================================================================

class Field(Variable):
    '''

A field construct according to the CF data model.

A field is a container for a data array and metadata comprising
properties to describe the physical nature of the data and a
coordinate system (called a domain) which describes the positions of
each element of the data array.

The field's domain may contain coordinates and cell measures (which
themselves contain data arrays and properties to describe them) and
transforms to further describe existing coordinates or to describe how
new coordinates may be computed.

All components of a field are optional.

**Miscellaneous**

Field objects are picklable.

'''

    _special_properties = Variable._special_properties.union(        
        ('ancillary_variables',
         'cell_methods',
         'flag_values',
         'flag_masks',
         'flag_meanings')
         )
    
    def __init__(self, properties={}, domain=None, data=None, attributes={},
                 flags=None, ancillary_variables=None, dimensions=None, 
                 finalize=True, copy=True):
        '''

**Initialization**

:Parameters:

    properties : dict, optional
        Provide the new field with CF properties from the dictionary's
        key/value pairs. Values are deep copied.

    data : cf.Data, optional
        Provide the new field with an N-dimensional data array in a
        `cf.Data` object. The array is deep copied.

    domain : cf.Domain, optional
        Provide the new field with a coordinate system in a
        `cf.Domain` object. The domain is deep copied. By default an
        empty domain is created.

    attributes : dict, optional
        Provide the new field with attributes from the dictionary's
        key/value pairs. Values are deep copied.

    flags : cf.Flags, optional
        Provide the new field with self-describing flag values in a
        `cf.Flags` object. The flags are deep copied.

    ancillary_variables : cf.AncillaryVariables, optional
        Provide the new field with ancillary variable fields in a
        `cf.AncillaryVariables` object. The ancillary variables are
        deep copied.

    dimensions : sequence of str, optional
        A list of dimension identifiers (``'dimN'``), stating the
        dimensions, in order, of field's data array. By default these
        dimension identifiers will be the sequence of consecutive
        dimension identifiers ``'dim0'`` up to ``'dimM'``, where ``M``
        is the number of dimensions of the data array, or an empty
        sequence if the data array is a scalar.

        If the field's domain defines dimensions, then their
        identifiers and sizes must correspond to those of the field's
        data array. Otherwise, new dimensions will be created in the
        field's domain.

        If the field's domain has any of the data array's dimensions,
        then their sizes must correspond to those of the field's data
        array.

    finalize : bool, optional
        If False then do not finalize the field with its `finalize`
        method. By default the field is finalized.

'''
        # Initialize the new field with attributes and CF properties
        super(Field, self).__init__(properties=properties,
                                    attributes=attributes, copy=copy)   
    
        # Domain
        if domain is not None:
            if not copy:
                self.domain = domain
            else:
                self.domain = domain.copy()
        else:
            # A field always has a domain
            self.domain = Domain(finalize=False)

        # Data array
        if data is not None:
            if dimensions is None:
                dimensions = self.domain.dimensions.get('data', None)
                
            self.insert_data(data, dimensions=dimensions, copy=copy,
                             finalize=False)
        #--- End: if
           
        # Flags
        if flags is not None:
            self.Flags = Flags.copy()
                
        # Ancillary variables
        if ancillary_variables is not None:
            self.ancillary_variables = ancillary_variables.copy()

        # Finalize
        if finalize:
            self.finalize()
    #--- End: def

    def _binary_operation(self, other, method):
        '''

Implement binary arithmetic and comparison operations on the master
data array with metadata-aware broadcasting.

It is intended to be called by the binary arithmetic and comparison
methods, such as `__sub__`, `__imul__`, `__rdiv__`, `__lt__`, etc.

:Parameters:

    other : standard Python scalar object or cf.Field or cf.Comaparison or cf.Data

    method : str
        The binary arithmetic or comparison method name (such as
        ``'__rdiv__'`` or ``'__ge__'``).

:Returns:

    out : cf.Field
        The new Field, or the same Field if the operation was an
        in-place augmented arithmetic assignment.

**Examples**

>>> h = f._binary_operation(g, '__add__')
>>> h = f._binary_operation(g, '__ge__')
>>> f._binary_operation(g, '__isub__')
>>> f._binary_operation(g, '__rdiv__')

'''
        if (isinstance(other, (float, int, long, bool, basestring)) or
            other is self):
            # ========================================================
            # Combine the field with one of the following:
            #
            #   * A python scalar
            #   * Itself
            #
            # These cases are special because they don't involve any
            # changes to the field's domain and so can use the
            # metadata-UNaware Variable._binary_operation method.
            # ========================================================
            return super(Field, self)._binary_operation(other, method)
        #--- End: if

        if isinstance(other, Data) and other.size == 1:
            # ========================================================
            # Combine the field with a size 1 Data object
            #
            # This case is special because it doesn't involve any
            # changes to the field's domain and so can use the
            # metadata-UNaware Variable._binary_operation method.
            # ========================================================
            other = other.copy()
            other.squeeze()
            return super(Field, self)._binary_operation(other, method)
        #--- End: if

        if isinstance(other, Comparison):
            # ========================================================
            # Combine the field with a Comparison object
            # ========================================================
            return NotImplemented
        #--- End: if

        if not isinstance(other, self.__class__):
            raise ValueError(
                "Can't combine '%s' with '%s'" %
                (self.__class__.__name__, other.__class__.__name__))
        #--- End: if

        # ============================================================
        # Still here? Then combine the field with another field
        # ============================================================

        # ------------------------------------------------------------
        # Analyse each domain
        # ------------------------------------------------------------
        s = self.domain.analyse()
        v = other.domain.analyse()

        if s['warnings'] or v['warnings']:
            raise ValueError("Can't combine fields: %s" % 
                             (s['warnings'] or v['warnings']))

        # Check that at most one field has undefined dimensions
        if s['undefined_dims'] and v['undefined_dims']:
            raise ValueError(
"Can't combine fields: Both fields have undefined dimensions")

        # Find the dimension names which are present in both fields
        matching_ids = set(s['id_to_dim']).intersection(v['id_to_dim'])
        
        # Check that any matching dimensions defined by an auxiliary
        # coordinate are done so in both fields.
        for identity in set(s['id_to_aux']).symmetric_difference(v['id_to_aux']):
            if identity in matching_ids:
                raise ValueError(
"Can't combine fields: '%s' dimension defined by auxiliary in only 1 field" %
standard_name)
        #--- End: for

        # ------------------------------------------------------------
        # For matching dimension coordinates check that they have
        # consistent transforms and that one of the following is
        # true:
        #
        # 1) They have equal size > 1 and their data arrays are
        #    equivalent
        #
        # 2) They have unequal sizes and one of them has size 1
        #
        # 3) They have equal size = 1. In this case, if the data
        #    arrays are not equivalent then the dimension will be
        #    omitted from the result field's domain.
        #-------------------------------------------------------------

        # List of size 1 dimensions to be completely removed from the
        # result field. Such a dimension's size 1 defining coordinates
        # have unequivalent data arrays.
        #
        # For example:
        # >>> remove_size1_dims
        # ['dim2']
        remove_size1_dims = []

        # List of matching dimensions with equivalent defining
        # dimension coordinate data arrays. 
        #
        # Note that we don't need to include matching dimensions with
        # equivalent defining *auxiliary* coordinate data arrays.
        #
        # For example:
        # >>> equivalent_data_matching_dimensions
        # [('dim2', 'dim0')]
        matching_dimensions_with_equivalent_data = []

        # For each field, list those of its matching dimensions which
        # need to be broadcast against the other field. I.e. those
        # dimensions which are size 1 but size > 1 in the other field.
        #
        # For example:
        # >>> s['broadcast_dims']
        # ['dim1']
        s['broadcast_dims'] = []
        v['broadcast_dims'] = []

        for identity in matching_ids:
            coord0 = s['id_to_coord'][identity]
            coord1 = v['id_to_coord'][identity]

            # Check the sizes of the defining coordinates
            size0 = coord0.size
            size1 = coord1.size
            if size0 != size1:
                # Defining coordinates have different sizes
                if size0 == 1:
                    # Can broadcast
                    s['broadcast_dims'].append(s['id_to_dim'][identity])
                elif size1 == 1:
                    # Can broadcast
                    v['broadcast_dims'].append(v['id_to_dim'][identity])
                else:
                    # Can't broadcast
                    raise ValueError(
"Can't combine fields: Can't broadcast '%s' dimensions with sizes %d and %d" %
(identity, size0, size1))

                continue
            #--- End: if

            # Still here? Then check that the defining coordinates'
            # data arrays are compatible. (Note that we know that
            # these defining coordinates have the same size.)
            dim1_to_dim0 = {v['id_to_dim'][identity]: s['id_to_dim'][identity]}
            
            if coord0._equivalent_data(coord1, transpose=dim1_to_dim0):
                # We defining coordinates with equivalent data arrays.

                dim0 = s['id_to_dim'][identity]
                dim1 = v['id_to_dim'][identity]

                # If the defining coordinates have metadata transforms
                # check that they have at least one in common
                incompatible_transforms = False
                if hasattr(coord0, 'transforms'):
                    found_match = False
                    for name in set(coord0.transforms).intersection(
                        getattr(coord1, 'transforms', ())):
                        transform0 = self.domain.transforms[coord0.transforms[name]]
                        transform1 = self.domain.transforms[coord1.transforms[name]]
                        if transform0.equivalent(transform1): # dch DCHDCHDCHDCHDCDCD domain?
                            found_match = True
                            break
                    #--- End: for
                    if not found_match:
                        incompatible_transforms = True
                elif hasattr(coord1, 'transforms'):
                    incompatible_transforms = True

                if incompatible_transforms:
                    if coord0.size > 1:
                        # We have two size > 1 defining coordinates
                        # with equivalent data arrays but incompatible
                        # metadata transforms.
                        raise ValueError(
"Can't combine fields: Incompatible '%s' coordinate transforms" % identity)
                    
                    # We have two size 1 defining coordinates with
                    # equivalent data arrays but incompatible metadata
                    # transforms, so flag this dimension to be omitted
                    # from the result field.
                    remove_size1_dims.append(dim0)

                elif identity not in s['id_to_aux']:
                    # The two defining coordinates are i) both
                    # dimension coordinates, ii) have equivalent data
                    # arrays and iii) have compatible metadata
                    # transforms (if any).
                    matching_dimensions_with_equivalent_data.append((dim0, dim1))

            else:
                if coord0.size > 1:
                    # We have two size > 1 defining coordinates with
                    # unequivalent data arrays
                    raise ValueError(
"Can't combine fields: Incompatible '%s' coordinate data arrays" % identity)
                
                # We have two size 1 defining coordinates with
                # unequivalent data arrays. Therefore flag this
                # dimension to be omitted from the result field.
                dim0 = s['id_to_dim'][identity]
                remove_size1_dims.append(dim0)
        #--- End: for

        # --------------------------------------------------------
        # Still here? Then the two fields are combinable!
        # --------------------------------------------------------

        # ------------------------------------------------------------
        # 2.1 Create copies of the two fields, unless it is an in
        # place combination, in which case we don't want to copy self)
        # ------------------------------------------------------------
        field1 = other.copy()

        inplace = method[2] == 'i'
        if not inplace:
            field0 = self.copy()
        else:
            field0 = self

        # Aliases for the field's domain and data array
        domain0 = field0.domain
        domain1 = field1.domain
        data0   = field0.Data
        data1   = field1.Data

        # ------------------------------------------------------------
        # 2.2 Map the dimension names of domain1 to the dimension
        # names of domain0.
        #
        # Where this is not possible (because domain1 has a dimension
        # which domain0 hasn't) then a new domain0 dimension name is
        # created.dim1_to
        # ------------------------------------------------------------
 
        # Map dimensions in field1 to dimensions in field0
        #
        # For example:
        # >>> dim1_to_dim0
        # {'dim1': 'dim0', 'dim2': 'dim1', 'dim0': 'dim2'}
        dim1_to_dim0 = {}

        s['new_dims'] = []
            
        for dim1 in domain1.dimension_sizes:
            if dim1 in v['dim_to_id']:
                identity = v['dim_to_id'][dim1]
                if identity in matching_ids:
                    dim1_to_dim0[dim1] = s['id_to_dim'][identity]
                    continue
            #--- End: if

            # Still here? Then dim1 has no match in domain0, so create
            # a new domain0 dimension name.
            dim0 = domain0.new_dimension_identifier()
            dim1_to_dim0[dim1] = dim0

            # Include this new dimension as a broadcast dimension,
            # regardless of the size of the dimension in domain1. This
            # is a sneaky way of, a bit later, getting the coordinates
            # from domain1 copied over to domain0.
            s['new_dims'].append(dim0)
        #--- End: for
            
        # Map dimensions in field0 to dimensions in field1
        #
        # For example:
        # >>> dim0_to_dim1
        # {'dim0': 'dim1', 'dim1': 'dim2', 'dim2': 'dim0'}
#        dim0_to_dim1 = dict([(V, k) for k, V in dim1_to_dim0.iteritems()])
        dim0_to_dim1 = {}
        for k, V in dim1_to_dim0.iteritems():
            dim0_to_dim1[V] = k

        # ------------------------------------------------------------
        # Permute the dimensions of data0 so that:
        #
        # * All of the matching dimensions are the inner (fastest
        #   varying) dimensions
        # * All of the undefined dimensions are the outer (slowest
        #   varying) dimensions        
        # * All of the defined but unmatched dimensions are in the
        #   middle
        # ------------------------------------------------------------
        axes_unD = []  # Undefined axes
        axes_unM = []  # Defined but unmatched axes
        axes_M   = []  # Defined and matched axes
        for i, dim0 in enumerate(data0.dimensions):
            if dim0 in s['undefined_dims']:
                axes_unD.append(i)
            elif s['dim_to_id'][dim0] in matching_ids:
                axes_M.append(i)
            else:
                axes_unM.append(i)
        #--- End: for
        axes                = axes_unD + axes_unM + axes_M
        start_of_unmatched0 = len(axes_unD)
        start_of_matched0   = start_of_unmatched0 + len(axes_unM)
        unmatched_indices0  = slice(start_of_unmatched0, start_of_matched0)
        field0.transpose(axes)

        # ------------------------------------------------------------
        # Permute the dimensions of data1 so that:
        #
        # * All of the matching dimensions are the inner (fastest
        #   varying) dimensions and in corresponding positions to
        #   data0
        # * All of the undefined dimensions are the outer (slowest
        #   varying) dimensions
        # * All of the defined but unmatched dimensions are in the
        #   middle
        # ------------------------------------------------------------
        axes_unD = []
        axes_unM = []
        axes_M   = []
        for i, dim1 in enumerate(data1.dimensions):
            if dim1 in v['undefined_dims']:
                axes_unD.append(i)
            elif v['dim_to_id'][dim1] in matching_ids:
                continue
            else:
                axes_unM.append(i)
        #--- End: for
        for dim0 in data0.dimensions[start_of_matched0:]:
            axes_M.append(data1.dimensions.index(dim0_to_dim1[dim0]))
        #--- End: for
        axes                = axes_unD + axes_unM + axes_M
        start_of_unmatched1 = len(axes_unD)
        start_of_matched1   = start_of_unmatched1 + len(axes_unM)
        undefined_indices1  = slice(None, start_of_unmatched1)
        unmatched_indices1  = slice(start_of_unmatched1, start_of_matched1)
        matched_indices1    = slice(start_of_matched1, None)
        field1.transpose(axes)

        # ----------------------------------------------------------------
        # Make sure that matching dimensions have the same directions
        # ----------------------------------------------------------------
        for identity in matching_ids:
            dim1 = v['id_to_dim'][identity]
            if domain1.dimension_sizes[dim1] > 1:
                dim0 = s['id_to_dim'][identity]
                if field1.Data.directions[dim1] != field0.Data.directions[dim0]:
                    field1.flip(dim1)
        #--- End: for

        # ------------------------------------------------------------
        # 2f. Insert size 1 dimensions into data0 to correspond to
        # defined but unmatched dimensions in data1
        #
        # For example, if   data0 is          T Y X
        #              and  data1 is        P Z Y X
        #              then data0 becomes P Z T y X
        # ------------------------------------------------------------
        original_start_of_unmatched0 = start_of_unmatched0
        for dim1 in data1.dimensions[unmatched_indices1]:
            data0.expand_dims(start_of_unmatched0,
                              dim1_to_dim0[dim1],
                              data1.directions[dim1])
            start_of_unmatched0 += 1
            start_of_matched0   += 1
        #--- End: for

        # ------------------------------------------------------------
        # Insert size 1 dimensions into data1 to correspond to defined
        # but unmatched dimensions in data0
        #
        # For example, if   data0 is      P Z T Y X
        #              and  data1 is        P Z Y X
        #              then data1 becomes P Z T Y X
        # ------------------------------------------------------------
        for dim0 in data0.dimensions[start_of_unmatched0:start_of_matched0]:
            data1.expand_dims(start_of_unmatched1,
                              start_of_unmatched1,  # Arbitrary, but unique, name
                              data0.directions[dim0])
            start_of_unmatched1 += 1
        #--- End: for

        # ------------------------------------------------------------
        # Insert size 1 dimensions into data0 to correspond to
        # undefined dimensions (of any size) in data1.
        #
        # For example, if   data0 is          P Z T Y X
        #              and  data1 is      4 1 P Z T Y X
        #              then data0 becomes 1 1 P Z T Y X
        # ------------------------------------------------------------
        start_of_unmatched0 = original_start_of_unmatched0
        for dim1 in data1.dimensions[undefined_indices1]:
            dim0 = dim1_to_dim0[dim1]
            data0.expand_dims(start_of_unmatched0,
                              dim0,
                              data1.directions[dim1])
            start_of_unmatched0 += 1
        #--- End: for

        # ============================================================
        # 3. Combine the data objects
        #
        # By now, data0.ndim >= data1.ndim and all of the dimension
        # names in data0 are correct for domain0. Therefore, the
        # dimension names of the combined data will all be ok.
        # ============================================================
        data0                      = getattr(data0, method)(data1)
        field0.Data                = data0
        domain0.dimensions['data'] = data0.dimensions[:]

        # ============================================================
        # 4. Adjust the domain of field0 to accommodate its new data
        # ============================================================

        # ------------------------------------------------------------
        # 4a. Remove any size 1 dimensions which are matching
        # dimensions but with different coordinate data array values
        # ------------------------------------------------------------
        field0.squeeze(remove_size1_dims, domain=True)

        # ------------------------------------------------------------
        # 4b. If broadcasting has grown any size 1 dimensions in
        # domain0 then replace their size 1 coordinates with the
        # corresponding size > 1 coordinates from domain1.
        # ------------------------------------------------------------
        for dim0 in s['broadcast_dims'] + s['new_dims']:

            dim1                         = dim0_to_dim1[dim0]
            domain0.dimension_sizes[dim0] = domain1.dimension_sizes[dim1]

            # If it exists, copy the domain1 dimension coordinate to
            # domain0
            if dim1 in domain1:
                domain0.insert_dim_coordinate(domain1[dim1], space=domain1, # dch
                                              key=dim0)
            #--- End: if

            # Remove from domain0 any 1-d auxiliary coordinates for
            # this dimension
            if dim0 in s['aux_coords']:
                for aux0 in s['aux_coords'][dim0]['1-d'].keys():
                    del s['aux_coords'][dim0]['1-d'][aux0]
                    domain0.remove_coordinate(aux0)
            #--- End: if

            # Copy to domain0 any domain1 1-d auxiliary coordinates for
            # this dimension
            if dim1 in v['aux_coords']:
                for coord1 in v['aux_coords'][dim1]['1-d'].itervalues():
                    domain0.insert_aux_coordinate(coord1, dimensions=[dim0],
                                                 space=domain1) # dch
        #--- End: for

        # ------------------------------------------------------------
        # Consolidate any 1-d auxiliary coordinates for matching
        # dimensions whose defining dimension coordinates have
        # equivalent data arrays.
        #
        # A domain0 1-d auxiliary coordinate is retained if there is a
        # corresponding domain1 1-d auxiliary with the same standard
        # name and equivalent data array.
        # ------------------------------------------------------------
        for dim0, dim1 in matching_dimensions_with_equivalent_data:

            for aux0, coord0 in s['aux_coords'][dim0]['1-d'].iteritems():
                # Remove this domain0 1-d auxiliary coordinate if it
                # has no identity
                if coord0.identity() is None:
                    domain0.remove_coordinate(aux0)
                    continue

                # Still here?
                aux0_has_equivalent_pair = False

                for aux1, coord1 in v['aux_coords'][dim1]['1-d'].items():
                    if coord1.identity() is None:
                        continue
                    
                    if coord0._equivalent_data(coord1, transpose=dim1_to_dim0):
                        del v['aux_coords'][dim1]['1-d'][aux1]
                        aux0_has_equivalent_pair = True
                        break
                #--- End: for

                # Remove this domain0 1-d auxiliary coordinate if it
                # has no equivalent in domain1
                if not aux0_has_equivalent_pair:
                    domain0.remove_coordinate(aux0)
        #--- End: for

        # ------------------------------------------------------------
        # Consolidate N-d auxiliary coordinates for matching
        # dimensions which have the same size
        # ------------------------------------------------------------
        # Remove any N-d auxiliary coordinates which span broadcasted
        # dimensions
        for broadcast_dims, aux_coords, domain in izip((s['broadcast_dims'], v['broadcast_dims']),
                                                       (s['aux_coords']    , v['aux_coords']),
                                                       (domain0            , domain1)):
            for dim in broadcast_dims:
                if dim not in aux_coords:
                    continue

                for aux in aux_coords[dim]['N-d']:
                    del aux_coords['N-d'][aux]
                    domain.remove_coordinate(aux)
        #--- End: for

        # Remove any N-d auxiliary coordinates which span a mixture of
        # matching and non-matching dimensions
        for aux_coords, domain, dim_to_id in izip((s['aux_coords'], v['aux_coords']),
                                                  (domain0         , domain1         ),
                                                  (s['dim_to_id'] , v['dim_to_id'] )):
            for aux in aux_coords['N-d'].keys():
                # Count how many of this N-d auxiliary coordinate's
                # dimensions are matching dimensions
                n_matching_dims = len([True for dim in domain.dimensions[aux]
                                       if dim_to_id[dim] in matching_ids])
                
                if 1 <= n_matching_dims < len(domain.dimensions[aux]):
                    # At least one dimension is a matching dimension
                    # and at least one dimension isn't, so remove this
                    # auxiliary coordinate
                    del aux_coords['N-d'][aux]
                    if domain is domain0:
                        domain.remove_coordinate(aux)
            #--- End: for
        #--- End: for

        # Forget about
        for aux0 in s['aux_coords']['N-d'].keys():
             n_matching_dims = len(s['aux_coords']['N-d'][aux0])
             if n_matching_dims == 0:
                 del s['aux_coords']['N-d'][aux0]
        #--- End: for

        # Copy to domain0 any domain1 N-d auxiliary coordinates which do
        # not span any matching dimensions
        for aux1, coord1 in v['aux_coords']['N-d'].items():
             n_matching_dims = len(v['aux_coords']['N-d'][aux1])
             if n_matching_dims == 0:
                 del v['aux_coords']['N-d'][aux1]
                 dims = [dim1_to_dim0[dim1] for dim1 in domain1.dimensions[aux1]]
                 domain0.insert_aux_coordinate(coord1, dimensions=dims,
                                               space=domain1) # dch
        #--- End: for

        # By now, aux_coords0['N-d'] contains only those N-d auxiliary
        # coordinates which span equal sized matching dimensions.
 
        # Remove from domain0 any N-d auxiliary coordinates which span
        # same-size matching dimensions and do not have an equivalent
        # N-d auxiliary coordinate in domain1 (i.e. one which spans the
        # same dimensions, has the same standard name and has
        # equivalent data) 
        for aux0, coord0 in s['aux_coords']['N-d'].iteritems():

            # Remove domain0 N-d auxiliary coordinate if it has no
            # standard name
            if coord0.identity() is None:
                domain0.remove_coordinate(aux0)
                continue

            # Still here?
            aux0_has_equivalent_pair = False
            for aux1, coord1 in v['aux_coords']['N-d'].items():
                if coord1.identity() is None:
                    continue

                if coord0._equivalent_data(coord1, transpose=dim1_to_dim0):
                    del v['aux_coords']['N-d'][aux1]
                    aux0_has_equivalent_pair = True
                    break
            #--- End: for

            # Remove domain0 N-d auxiliary coordinate if it has no
            # equivalent in domain1
            if not aux0_has_equivalent_pair:
                domain0.remove_coordinate(aux0)
        #--- End: for

        return field0
    #--- End: def
    
    def equivalent(self, other, rtol=None, atol=None, traceback=False):
        '''

True if and only if two fields are logically equivalent.

Equivalence is defined as:

* Both fields must have the same dentity as returned by the `identity`
  methods.

* Both fields' data arrays being the same after accounting for
  different but equivalent:

    * units

    * size one dimensions (if *squeeze* is True), 
    
    * dimension directions (if *use_directions* is True) 
    
    * dimension orders (if *transpose* is set to a dictionary).

* Both fields' domains must have the same dimensionality and where a
  dimension in one field has an identity inferred a 1-d coordinate,
  the other field has a matching dimension whose identity inferred is
  inferred from a 1-d coordinate with an equivalent data array.

:Parameters:

    other :
        The object to compare for equivalence.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        objects differ.

:Returns: 

    out : bool
        Whether or not the two objects are equivalent.
      


'''
        if not self.equivalent_domain(other, rtol=rtol, atol=atol,
                                      traceback=traceback):
            return False # dch add traceback

        if not self.equivalent_data(other, rtol=rtol, atol=atol,
                                    traceback=False):
            return False # dch add traceback
                
        return True
    #--- End_def

    def equivalent_domain(self, other, rtol=None, atol=None, traceback=False):
        '''
'''
        return self.domain.equivalent(other.domain, rtol=rtol, atol=atol,
                                      traceback=traceback)
    #--- End_def

    def equivalent_data(self, other, rtol=None, atol=None, traceback=False):
        '''

Equivelence is defined as both fields having the same data arrays
after accounting for different but equivalent units, size one
dimensions, different dimension directions and different dimension
orders.

:Parameters:

    other : cf.Field

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `cf.ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `cf.RTOL` function is used.

:Returns:

    out : bool
        Whether or not the two fields' data arrays are equivalent.

**Examples**

>>> f.equivalent_data(g)

'''
        if self._hasData != other._hasData:
            if traceback:
                print("%s: Only one field has data: %s, %s" %
                      (self.__class__.__name__, self._hasData, other._hasData))
            return False
        
        if not self._hasData:
            return True

        if self.size != other.size:
            if traceback:
                print("%s: Different sizes: %d, %d" %
                      (self.__class__.__name__, self.size, other.size))
            return False

        s = self.domain.analyse()
        t = other.domain.analyse()

#        transpose = dict([(dim1, s['id_to_dim'][identity])
#                          for dim1, identity in t['dim_to_id'].iteritems()
#                          if identity in s['id_to_dim']
#                          ])

        transpose = {}
        for dim1, identity in t['dim_to_id'].iteritems():
            s_identity = s['id_to_dim'].get(identity, None)
            if s_identity is not None:
                transpose[dim1] = s_identity
        #--- End: for

#        if set(transpose) != set(other.domain.dimensions['data']):
#            return False

        return self.Data.equivalent(other.Data,
                                    rtol=rtol, atol=atol,
                                    transpose=transpose,
                                    squeeze=True,
                                    use_directions=True)
    #--- End: def
 
    def _parse_axes(self, axes, method, default=(), dims=False, ignore=False):
        '''

:Parameters:

    axes : (sequence of) str or int

    method : str

    default : sequence of str or int, optional

    dims : bool, optional

    ignore : bool, optional
        If True then ignore any dimensions which do not span the
        field's data array. By default an exception is raised if a
        specified dimension does not span the field's data array.

:Returns:
    
    out : list of ints (, list of strs)
        Return a list of integer positions of the field's data
        array. If *dims* is True then also return the domain dimension
        identifiers corresponding to the integer positions.

**Examples**

>>> f._parse_axes()

'''
        domain          = self.domain
        dimensions      = domain.dimensions
        data_dimensions = dimensions['data']

        if axes is None: #not axes and axes is not 0:
            axes = default

        # Convert axes to a list of integer positions
        if isinstance(axes, (str, int, long)):
            axes = [axes]
        else:
            axes = list(axes)

        axes2 = []
        for axis in axes:
            if axis in data_dimensions:
                axes2.append(data_dimensions.index(axis))
            elif isinstance(axis, (int, long)):
                if axis < 0:
                    axes2.append(axis + self.ndim)
                else:
                    axes2.append(axis)
            else:
                key = self.coord(axis, key=True, exact=True)
        
                if key is None:
                    raise ValueError(
                        "Can't %s: Can't determine %s dimension from '%s'" %
                        (method, axis))

                axis = dimensions[key][0] 
                if axis in data_dimensions:
                    axes2.append(data_dimensions.index(axis))
                elif not ignore:
                    # Do not ignore dimensions which don't span the
                    # data array
                    raise ValueError(
                        "Can't %s: '%s' dimension not spanned by data array" %
                        axis, method)
        #--- End: for

        if axes2:
            # Check for duplicate axes
            if len(axes2) != len(set(axes2)):
                raise ValueError("Can't %s: Repeated axis: %s" %
                                 (method, repr(axes2)))
            
            # Check for out of range axes
            if max(axes2) >= self.ndim:
                raise ValueError("Can't %s: Invalid axis for this array: %d" %
                             (method, max(axes2)))
        #--- End: if

        if dims:
            return axes2, [data_dimensions[i] for i in axes2]
        else:
            return axes2
    #--- End: def

    def __repr__(self):
        '''
x.__repr__() <==> repr(x)

'''
        if self._hasData:
#            if hasattr(self, 'domain'):
            domain = self.domain
            x = ['%s(%d)' % (domain.dimension_name(dim),
                             domain.dimension_sizes[dim])
                 for dim in domain.dimensions['data']]
            dim_names = '(%s)' % ', '.join(x)
        else:
            dim_names = ''
        #--- End: if
            
        # Field units
        units = getattr(self, 'units', '')
        calendar = getattr(self, 'calendar', None)
        if calendar:
            units += '%s calendar' % calendar

        return '<CF Field: %s%s %s>' % (self.name(default=''), dim_names, units)
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''
        string = ["%s field summary" % self.name(default='')]
        string.append(''.ljust(len(string[0]), '-'))

        # Units
        units = getattr(self, 'units', '')
        calendar = getattr(self, 'calendar', None)
        if calendar:
            units += ' %s calendar' % calendar

        domain = self.domain

        # Data
        if self._hasData:
            x = ['%s(%d)' % (domain.dimension_name(dim),
                             domain.dimension_sizes[dim])
                 for dim in domain.dimensions['data']]
            
            string.append('Data            : %s(%s) %s' % (self.name(default=''),
                                                           ', '.join(x), units))
        elif units:
            string.append('Data            : %s' % units)

        # Cell methods
        cell_methods = getattr(self, 'cell_methods', None)
        if cell_methods is not None:
            string.append('Cell methods    : %s' % str(cell_methods))

        # Domain
        if domain:
            string.append(str(domain))
            
        # Ancillary variables
        ancillary_variables = getattr(self, 'ancillary_variables', None)
        if ancillary_variables is not None:
            y = ['Ancillary vars  : ']
            y.append('\n                : '.join(
                    [repr(a) for a in ancillary_variables]))
            string.append(''.join(y))

        string.append('')

        return '\n'.join(string)
    #--- End def

#    # ----------------------------------------------------------------
#    # Attribute: Data
#    # ----------------------------------------------------------------
#    @property
#    def Data(self):
#        '''
#
#The `cf.Data` object containing the data array.
#
#It is preferrable to use the `get_data` method to retrieve the
#`cf.Data` object.
#
#**Examples**
#
#>>> print repr(f.Data)
#<CF Data: [[273.15, ..., 278.56]] K>
#
#'''
#        return Variable.Data.fget(self)
#    #--- End: def
#    @Data.setter
#    def Data(self, value):
#        private = self._private
#        private['Data'] = value
#
#        self._hasData = True
#
#        # Delete from the variable, if they had been set
#        special_properties = private['special_attributes']
#        special_properties.pop('Units', None)
##        special_properties.pop('_FillValue', None)
#
#        domain_data_dimensions = self.domain.dimensions.get('data', None)
#        value_dimensions       = value.dimensions
#        if (domain_data_dimensions and
#            value_dimensions != domain_data_dimensions):
#            dim_name_map = {}
#            for k, v in izip(value_dimensions, domain_data_dimensions):
#                dim_name_map[k] = v
#
#            self.Data.change_dimension_names(dim_name_map)
#        #--- End: if
#
##        domain_dimensions = self.domain.dimensions
##        if ('data' in domain_dimensions and 
##            value.dimensions != domain_dimensions['data']):
##            dim_name_map = {}
##            for k, v in izip(value.dimensions, domain_dimensions['data']):
##                dim_name_map[k] = v
##
###            dim_name_map = dict(
###                izip(value.dimensions, domain_dimensions['data']))
##
##            self.Data.change_dimension_names(dim_name_map)
#    #--- End: def
#    @Data.deleter
#    def Data(self):
#        Variable.Data.fdel(self)

    # ----------------------------------------------------------------
    # Attribute: ancillary_variables
    # ----------------------------------------------------------------
    @property
    def ancillary_variables(self):
        '''

A `cf.AncillaryVariables` object containing CF ancillary data.

**Examples**

>>> f.ancillary_variables
[<CF Field: >]

'''
        return self._get_special_attr('ancillary_variables')
    #--- End: def
    @ancillary_variables.setter
    def ancillary_variables(self, value):
        self._set_special_attr('ancillary_variables', value)
    @ancillary_variables.deleter
    def ancillary_variables(self):
        self._del_special_attr('ancillary_variables')

    # ----------------------------------------------------------------
    # Attribute: Flags
    # ----------------------------------------------------------------
    @property
    def Flags(self):
        '''

A Flags object containing self-describing CF flag values.

Stores the flag_values, flag_meanings and flag_masks CF properties in
an internally consistent manner.

**Examples**

>>> f.Flags
<CF Flags: flag_values=[0 1 2], flag_masks=[0 2 2], flag_meanings=['low' 'medium' 'high']>

'''
        return self._get_special_attr('Flags')
    @Flags.setter
    def Flags(self, value):
        self._set_special_attr('Flags', value)
    @Flags.deleter
    def Flags(self):
        self._del_special_attr('Flags')

    # ----------------------------------------------------------------
    # Attribute: mask (read only)
    # ----------------------------------------------------------------
    @property
    def mask(self):
        '''

A field containing the mask of the data array.

**Examples**

>>> f
<CF Field: air_temperature(time(12), latitude(73), longitude(96)) K>
>>> m = f.mask
>>> m
<CF Field: mask(time(12), latitude(73), longitude(96)) >
>>> m.Data
<CF Data: [[[True, ..., False]]] >

'''
        return type(self)(properties={'long_name': 'mask'},
                          attributes={'id': 'mask'}, 
                          domain=self.domain,
                          data=self.Data.mask)
    #--- End: def
    @mask.setter
    def mask(self, value):
        Variable.mask.fset(self, value)
    @mask.deleter
    def mask(self): 
        Variable.mask.fdel(self)

    # ----------------------------------------------------------------
    # CF property: flag_values
    # ----------------------------------------------------------------
    @property
    def flag_values(self):
        '''

The flag_values CF property.

Stored as a 1-d numpy array but may be set as any array-like object.

**Examples**

>>> f.flag_values = ['a', 'b', 'c']
>>> f.flag_values
array(['a', 'b', 'c'], dtype='|S1')
>>> f.flag_values = numpy.arange(4)
>>> f.flag_values
array([1, 2, 3, 4])
>>> del f.flag_values

>>> f.setprop('flag_values', 1)
>>> f.getprop('flag_values')
array([1])
>>> f.delprop('flag_values')

'''
        try:
            return self.Flags.flag_values
        except AttributeError:
            raise AttributeError(
                "%s doen't have CF property 'flag_values'" %
                self.__class__.__name__)
    #--- End: def
    @flag_values.setter
    def flag_values(self, value):
        try:
            flags = self.Flags
        except AttributeError:
            self.Flags = Flags(flag_values=value)
        else:
            flags.flag_values = value
    #--- End: def
    @flag_values.deleter
    def flag_values(self):
        try:
            del self.Flags.flag_values
        except AttributeError:
            raise AttributeError(
                "%s doen't have CF property 'flag_values'" %
                self.__class__.__name__)
        else:
            if not self.Flags:
                del self.Flags
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: flag_masks
    # ----------------------------------------------------------------
    @property
    def flag_masks(self):
        '''
The flag_masks CF property.

Stored as a 1-d numpy array but may be set as array-like object.

**Examples**

>>> f.flag_masks = numpy.array([1, 2, 4], dtype='int8')
>>> f.flag_masks
array([1, 2, 4], dtype=int8)
>>> f.flag_masks = (1, 2, 4, 8)
>>> f.flag_masks
array([1, 2, 4, 8], dtype=int8)
>>> del f.flag_masks

>>> f.setprop('flag_masks', 1)
>>> f.getprop('flag_masks')
array([1])
>>> f.delprop('flag_masks')

'''
        try:
            return self.Flags.flag_masks
        except AttributeError:
            raise AttributeError(
                "%s doen't have CF property 'flag_masks'" %
                self.__class__.__name__)
    #--- End: def
    @flag_masks.setter
    def flag_masks(self, value):
        try:
            flags = self.Flags
        except AttributeError:
            self.Flags = Flags(flag_masks=value)
        else:
            flags.flag_masks = value
    #--- End: def
    @flag_masks.deleter
    def flag_masks(self):
        try:
            del self.Flags.flag_masks
        except AttributeError:
            raise AttributeError(
                "%s doen't have CF property 'flag_masks'" %
                self.__class__.__name__)
        else:
            if not self.Flags:
                del self.Flags
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: flag_meanings
    # ----------------------------------------------------------------
    @property
    def flag_meanings(self):
        '''

The flag_meanings CF property.

Stored as a 1-d numpy string array but may be set as a space delimited
string or any array-like object.

**Examples**

>>> f.flag_meanings = 'low medium      high'
>>> f.flag_meanings
array(['low', 'medium', 'high'],
      dtype='|S6')
>>> del flag_meanings

>>> f.flag_meanings = ['left', 'right']
>>> f.flag_meanings
array(['left', 'right'],
      dtype='|S5')

>>> f.flag_meanings = 'ok'
>>> f.flag_meanings
array(['ok'],
      dtype='|S2')

>>> f.setprop('flag_meanings', numpy.array(['a', 'b'])
>>> f.getprop('flag_meanings')
array(['a', 'b'],
      dtype='|S1')
>>> f.delprop('flag_meanings')

'''
        try:
            return self.Flags.flag_meanings
        except AttributeError:
            raise AttributeError(
                "%s doen't have CF property 'flag_meanings'" %
                self.__class__.__name__)
    #--- End: def
    @flag_meanings.setter
    def flag_meanings(self, value): 
        try:
            flags = self.Flags
        except AttributeError:
            self.Flags = Flags(flag_meanings=value)
        else:
            flags.flag_meanings = value
    #--- End: def
    @flag_meanings.deleter
    def flag_meanings(self):
        try:
            del self.Flags.flag_meanings
        except AttributeError:
            raise AttributeError(
                "%s doen't have CF property 'flag_meanings'" %
                self.__class__.__name__)
        else:
            if not self.Flags:
                del self.Flags
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: cell_methods
    # ----------------------------------------------------------------
    @property
    def cell_methods(self):
        '''

The CellMethods object containing the CF cell methods of the data array.

**Examples**

>>> f.cell_methods
<CF CellMethods: time: mean (interval: 1.0 month)>

'''
        return self._get_special_attr('cell_methods')
    #--- End: def
    @cell_methods.setter
    def cell_methods(self, value):
        self._set_special_attr('cell_methods', value)
    @cell_methods.deleter
    def cell_methods(self):
        self._del_special_attr('cell_methods')

    # ----------------------------------------------------------------
    # CF property: Conventions	
    # ----------------------------------------------------------------
    @property
    def Conventions(self):
        '''

The Conventions CF property.

**Examples**

>>> f.Conventions = 'CF-1.5'
>>> f.Conventions
'CF-1.5'
>>> del f.Conventions

>>> f.setprop('Conventions', 'CF-1.5')
>>> f.getprop('Conventions')
'CF-1.5'
>>> f.delprop('Conventions')

'''
        return self.getprop('Conventions')
    #--- End: def

    @Conventions.setter
    def Conventions(self, value): self.setprop('Conventions', value)
    @Conventions.deleter
    def Conventions(self):        self.delprop('Conventions')

    # ----------------------------------------------------------------
    # CF property: institution (a simple attribute)
    # ----------------------------------------------------------------
    @property
    def institution(self):
        '''

The institution CF property.

**Examples**

>>> f.institution = 'University of Reading'
>>> f.institution
'University of Reading'
>>> del f.institution

>>> f.setprop('institution', 'University of Reading')
>>> f.getprop('institution')
'University of Reading'
>>> f.delprop('institution')

'''
        return self.getprop('institution')
    #--- End: def
    @institution.setter
    def institution(self, value): self.setprop('institution', value)
    @institution.deleter
    def institution(self):        self.delprop('institution')

    # ----------------------------------------------------------------
    # CF property: references (a simple attribute)
    # ----------------------------------------------------------------
    @property
    def references(self):
        '''

The references CF property.

**Examples**

>>> f.references = 'some references'
>>> f.references
'some references'
>>> del f.references

>>> f.setprop('references', 'some references')
>>> f.getprop('references')
'some references'
>>> f.delprop('references')

'''
        return self.getprop('references')
    #--- End: def
    @references.setter
    def references(self, value): self.setprop('references', value)
    @references.deleter
    def references(self):        self.delprop('references')

    # ----------------------------------------------------------------
    # CF property: standard_error_multiplier	
    # ----------------------------------------------------------------
    @property
    def standard_error_multiplier(self):
        '''

The standard_error_multiplier CF property.

**Examples**

>>> f.standard_error_multiplier = 2.0
>>> f.standard_error_multiplier
2.0
>>> del f.standard_error_multiplier

>>> f.setprop('standard_error_multiplier', 2.0)
>>> f.getprop('standard_error_multiplier')
2.0
>>> f.delprop('standard_error_multiplier')

'''
        return self.getprop('standard_error_multiplier')
    #--- End: def

    @standard_error_multiplier.setter
    def standard_error_multiplier(self, value):
        self.setprop('standard_error_multiplier', value)
    @standard_error_multiplier.deleter
    def standard_error_multiplier(self):
        self.delprop('standard_error_multiplier')

    # ----------------------------------------------------------------
    # CF property: source	
    # ----------------------------------------------------------------
    @property
    def source(self):
        '''

The source CF property.

**Examples**

>>> f.source = 'radiosonde'
>>> f.source
'radiosonde'
>>> del f.source

>>> f.setprop('source', 'surface observation')
>>> f.getprop('source')
'surface observation'
>>> f.delprop('source')

'''
        return self.getprop('source')
    #--- End: def

    @source.setter
    def source(self, value): self.setprop('source', value)
    @source.deleter
    def source(self):        self.delprop('source')

    # ----------------------------------------------------------------
    # CF property: title	
    # ----------------------------------------------------------------
    @property
    def title(self):
        '''

The title CF property.

**Examples**

>>> f.title = 'model data'
>>> f.title
'model data'
>>> del f.title

>>> f.setprop('title', 'model data')
>>> f.getprop('title')
'model data'
>>> f.delprop('title')

'''
        return self.getprop('title')
    #--- End: def

    @title.setter
    def title(self, value): self.setprop('title', value)
    @title.deleter
    def title(self):        self.delprop('title')

    # ----------------------------------------------------------------
    # Attribute: domain
    # ----------------------------------------------------------------
    @property
    def domain(self):
        '''

The Domain object containing the coordinate system (domain).

**Examples**

>>> f.domain
<CF Domain: (12, 19, 73, 96)>

'''
        return self._get_special_attr('domain')
    #--- End: def
    @domain.setter
    def domain(self, value):
        self._set_special_attr('domain', value)
    @domain.deleter
    def domain(self):
        self._del_special_attr('domain')

    # ----------------------------------------------------------------
    # Attribute: subspace (read only)
    # ----------------------------------------------------------------
    @property
    def subspace(self):
        '''

Return a new object which in turn may return a subspace of the field.

The `subspace` attribute may be indexed to select a subspace by
dimension index values (``f.subspace[indices]``) or called to select a
subspace by dimension coordinate array values
(``f.subpace(**coordinate_values)``).

**Subspacing by indexing**

Subspacing by dimension indices uses an extended Python slicing
syntax, which is similar :ref:`numpy array indexing
<numpy:arrays.indexing>`

There are two important extensions to the numpy indexing
functionality:

* Size 1 dimensions are never removed.

  An integer index i takes the i-th element but does not reduce the
  rank of the output array by one:

* When advanced indexing is used on more than one dimension, the
  advanced indices work independently.

  When more than one dimension's slice is a 1-d boolean sequence
  or 1-d sequence of integers, then these indices work
  independently along each dimension (similar to the way vector
  subscripts work in Fortran), rather than by their elements:

**Subspacing by coordinate values**

Subspacing by values of 1-d coordinates allows a subspaced field to be
defined via coordinate values of its domain.

Coordinate values are provided as keyword arguments to a call to the
`subspace` attribute. Coordinates are identified by their
`~Coordinate.identity` or their dimension's identifier in the field's
domain. Note that:

* The dimensions to be subspaced may be given in any order.

* Dimensions for which no subspacing is required need not be
  specified.

* Size 1 dimensions of the domain which are not spanned by the data
  array may be specified.

**Examples**

>>> print f
Data            : air_temperature(time(12), latitude(73), longitude(96)) K
Cell methods    : time: mean
Dimensions      : time(12) = [15, ..., 345] days since 1860-1-1
                : latitude(73) = [-90, ..., 90] degrees_north
                : longitude(96) = [0, ..., 356.25] degrees_east
                : height(1) = [2] m

>>> f.shape
(12, 73, 96)
>>> f.subspace[...].shape
(12, 73, 96)
>>> f.subspace[slice(0, 12), :, 10:0:-2].shape
(12, 73, 5)
>>> lon = f.coord('longitude').array
>>> f.subspace[..., lon<180]

>>> f.shape
(12, 73, 96)
>>> f.subspace[0, ...].shape
(1, 73, 96)
>>> f.subspace[3, slice(10, 0, -2), 95].shape
(1, 5, 1)

>>> f.shape
(12, 73, 96)
>>> f.subspace[:, [0, 72], [5, 4, 3]].shape
(12, 2, 3)

>>> f.subspace().shape
(12, 73, 96)
>>> f.subspace(latitude=0).shape
(12, 1, 96)
>>> f.subspace(latitude=cf.wi(-30, 30)).shape
(12, 25, 96)
>>> f.subspace(long=cf.ge(270, 'degrees_east'), lat=cf.set([0, 2.5, 10])).shape
(12, 3, 24)
>>> f.subspace(latitude=cf.lt(0, 'degrees_north'))
(12, 36, 96)
>>> f.subspace(latitude=[cf.lt(0, 'degrees_north'), 90])
(12, 37, 96)
>>> import math
>>> f.subspace(longitude=cf.lt(math.pi, 'radian'), height=2)
(12, 73, 48)
>>> f.subspace(height=cf.gt(3))
IndexError: No indices found for 'height' values gt 3

>>> f.subspace(dim2=3.75).shape
(12, 1, 96)

'''
        return SubspaceField(self)
    #--- End: def

#    def _change_dimension_names(self, data_only=False):
#        '''
#
#Change the dimension names of the field's Data, coordinate and cell
#measure objects. 
#
#The dimension identifiers of the domain are not changed.
#
#:Returns:
#
#    None
#
#**Examples**
#
#>>> f._change_dimension_names()
#
#'''
#        domain     = self.domain
#        dimensions = domain.dimensions
#        
#        # 
#        domain_data_dimensions = dimensions.get('data', None)
#
#        if domain_data_dimensions is not None:
#            dim_name_map = {}
#            for k, v in izip(self.Data.dimensions, domain_data_dimensions):
#                dim_name_map[k] = v
#                
#            super(Field, self)._change_dimension_names(dim_name_map)
#        #--- End: if
#
#        if data_only:
#            return
#
#        for key, var in domain.iteritems():
#            dim_name_map = {}
#            for k, v in izip(var.Data.dimensions, dimensions[key]):
#                dim_name_map[k] = v
#
#            var._change_dimension_names(dim_name_map)
#    #--- End: def
        
#    def chunk(self, chunksize=None):
#        '''
#
#Partition the field in place for LAMA functionality.
#
#:Parameters:
#
#    chunksize : int, optional
#
#:Returns:
#
#    None
#
#'''
#        if chunksize is None:
#            # Set the default chunk size
#            chunksize = CHUNKSIZE()
#
#        # Partition the field's data
#        if self._hasData:
#            self.Data.chunk(chunksize)
#
#        # Partition the data of the field's domain components
#        for variable in self.domain.itervalues():
#            variable.chunk(chunksize)
#    #--- End: def

    def close(self):
        '''

Close all files referenced by the field.

Note that a closed file will be automatically reopened if its contents
are subsequently required.

:Returns:

    None

**Examples**

>>> f.close()

'''
        new = super(Field, self).close()

        self.domain.close()

        ancillary_variables = getattr(self, 'ancillary_variables', None)
        if ancillary_variables is not None:
            ancillary_variables.close()
    #--- End: def

    def finalize(self):
        '''

Finalize a newly created field.

It is essential that this is carried out on every new field to ensure
that methods and functions of fields work correctly. Finalization
entails:

* Expanding scalar coordinate and cell measures to 1-d.

* Setting the direction of the domain's dimensions.

* Conforming the domain's internal dimension names.

* Partitioning the data arrays for LAMA functionality.

:Returns:

    None

**Examples**

>>> f.finalize()

'''
        domain = self.domain
        
        directions = domain.finalize()
        
        if self._hasData:
            self._finalize_data(directions)
#            return
#
#        data = self.Data
#
#        data_dimensions        = data.dimensions
#        domain_data_dimensions = domain.dimensions.get('data', [])
#
#        if data_dimensions != domain_data_dimensions:
#            dim_name_map = {}
#            for k, v in izip(data_dimensions, domain_data_dimensions):
#                dim_name_map[k] = v
#            
#            data.change_dimension_names(dim_name_map)
#        #--- End: if
#
#        data.override_directions(directions, True)
    #--- End: def

    def _finalize_data(self, directions):
        
        data   = self.Data
        domain = self.domain

        data_dimensions        = data.dimensions
        domain_data_dimensions = domain.dimensions.get('data', [])

        if data_dimensions != domain_data_dimensions:
            dim_name_map = {}
            for k, v in izip(data_dimensions, domain_data_dimensions):
                dim_name_map[k] = v
            
            data.change_dimension_names(dim_name_map)
        #--- End: if

        data.override_directions(directions, True)
    #--- End: def

    def coord(self, arg,
              role=None, key=False, exact=False, dimensions=False, 
              one_d=False, maximal_match=True):
        '''

Return a coordinate of the domain.

Note that the returned coordinate is an object identity (not a copy)
to the coordinate stored in the domain so, for example, a coordinate's
properties may be changed in-place:

>>> f.coord('height').long_name
AttributeError: Coordinate has no CF property 'long_name'
>>> f.coord('height').long_name = 'height'
>>> f.coord('height').long_name
'height'

:Parameters:

    arg : str or dict or int
        The identify of the coordinate. One of:

        * A string containing an unambiguous abbreviation (see the
          *exact* parameter) of its identity as returned by the
          coordinate's `~Coordinate.identity`.

        * A string containing a 1-d coordinate's dimension identifier
          in the domain (see the `cf.Domain.coordinates` method).

        * A dictionary containing one or more CF property names and
          their unambiguous abbreviations (see the *exact* parameter)
          values as its key/value pairs (such as ``{'long_name':
          'temperature'}``). See the *maximal_match* parameter for
          when two or more properties are specified in this manner.

        * An integer giving the position of a 1-d coordinate's
          dimension in the field's data array (negative integers count
          from the slowest moving domension).

    exact : bool, optional
        If True then do not string abbreviations given by the *arg*
        parameter will not match, nor will different but equivalent
        units. By default unambiguous string abbreviations will match,
        as will different but equivalent units.

    role : str, optional
        Restrict the search to coordinates of the given role. Valid
        values are ``'dim'`` and ``'aux'`` for dimension and auxiliary
        coordinate repectively. By default both types are considered.

    one_d : bool, optional
        Restrict the search to one dimensionsal coordinates. By
        default coordinates with any number of dimensions are
        considered.

    key : bool, optional
        If True then return the domain's identifier for the
        coordinate. By default the unique coordinate itself is
        returned.

    dimensions : bool, optional
        If True then return a list of the unique coordinate's
        dimensions described by their domain identifiers. By default
        the unique coordinate itself is returned.

    maximal_match : bool, optional
        If False and two or more properties are specified by the *arg*
        parameter then a unique coordinate will satisfy at least one
        of the properties' criteria. By default a unique coordinate
        will satisfy all of the properties' criteria.

:Returns:

    out : cf.Coordinate or str or list or None
        The unique coordinate or, if *key* is True, the unique
        coordinate's identifier in the domain  or, if *dimensions* is
        True, the unique coordinate's dimensions described by their
        domain identifiers, if no unique coordinate could be found,
        None.

**Examples**

>>> print f
Data            : air_temperature(time(12), latitude(73), longitude(96)) K
Cell methods    : time: mean
Dimensions      : time(12) = [15, ..., 345] days since 1860-1-1
                : latitude(73) = [-90, ..., 90] degrees_north
                : longitude(96) = [0, ..., 356.25] degrees_east
                : height(1) = [2] m
>>> f.domain['dim2'].properties
{'_FillValue': None,
 'axis': 'X',
 'long_name': 'longitude',
 'standard_name': 'longitude',
 'units': 'degrees_east'}
>>> f.domain['dim2'].shape
(360,)

>>> f.coord('longitude')
<CF Coordinate: longitude(96)>
>>> f.coord('long')
<CF Coordinate: longitude(96)>
>>> f.coord('long', key=True)
'dim2'
>>> f.coord('long', dimensions=True)
['dim2']
>>> f.coord('long', oned_d=True)
<CF Coordinate: longitude(96)>

>>> f.coord('lon', exact=True)
None
>>> f.coord('longitude', exact=True)
<CF Coordinate: longitude(96)>

>>> f.coord({'standard_name': 'long', 'axis': X})
<CF Coordinate: longitude(96)>
>>> f.coord({'standard_name': 'long', 'axis': 'X'}, maximal_match=False)
<CF Coordinate: longitude(96)>
>>> f.coord({'standard_name': 'long', 'axis': 'Y'})
None
>>> f.coord({'standard_name': 'long', 'axis': 'Y'}, maximal_match=False)
<CF Coordinate: longitude(96)>

>>> f.coord('long', role='dim')
<CF Coordinate: longitude(96)>
>>> f.coord('long', role='aux')
None

'''       
#        if not hasattr(self, 'domain'):
#            return None
          
        return self.domain.coord(arg,
                                 role=role, key=key, exact=exact,
                                 dimensions=dimensions, one_d=one_d,
                                 maximal_match=maximal_match)
    #--- End: def

#    def copy(self, _omit_Data=False):
#        '''
#    
#Return a deep copy.
#
#Equivalent to ``copy.deepcopy(f)``.
#
#:Returns:
#
#    out :
#        The deep copy.
#
#**Examples**
#
#>>> g = f.copy()
#
#'''
#        new = super(Field, self).copy(_omit_Data=_omit_Data) #,
##                                      _omit_special=('domain',))
#        
##        if hasattr(self, 'domain'):
##        new.domain = self.domain.copy()
#            
#        return new
#    #--- End: def

    def dump(self, level=0, complete=False, title='Field', q='='):
        '''

Return a string containing a description of the field.

:Parameters:

    level : int, optional

    complete : bool, optional

    title : str, optional
    
    q : str, optional
    
:Returns:

    out : str
        A string containing the description.

**Examples**

>>> x = f.dump()
>>> print f.dump(id='pressure_field')

'''       
        indent = '    '      
        indent0 = indent * level
        indent1 = indent0 + indent

        domain = self.domain

        title = '%s%s: %s' % (indent0, title, self.name(default=''))
        line  = '%s%s'     % (indent0, ''.ljust(len(title)-level*4, q))

        # Title
        string = [line, title, line]
#            title, #'%s%s: %s' % (indent0, title, self.name(default='')),
#            '%s%s'     % (indent0, ''.ljust(len(titlestring[-1])-level*4, q))
#            ]
#        string.insert(0, string[-1])

        # Dimensions
        dimension_sizes = domain.dimension_sizes
        if dimension_sizes:
            string.extend((domain.dump_dimensions(level=level), ''))

        # Data
        if self._hasData:
            x = ['%s(%d)' % (domain.dimension_name(dim),
                             dimension_sizes[dim])
                 for dim in domain.dimensions['data']]
            string.append('%sData(%s) = %s' % (indent0, ', '.join(x), str(self.Data)))

        # Cell methods
        cell_methods = getattr(self, 'cell_methods', None)
        if cell_methods is not None:            
            string.append('%scell_methods = %s' % (indent0, str(cell_methods)))

        # Simple properties
        if self._simple_properties():
            string.extend(('',
                           super(Field, self).dump_simple_properties(
                        level=level,
                        omit=('Conventions',))))
            
        # Flags
        flags = getattr(self, 'Flags', None)
        if flags is not None:            
            string.extend(('', flags.dump(level=level)))

        # Domain
        string.append(domain.dump_components(level=level, complete=complete))

        # Ancillary variables
        ancillary_variables = getattr(self, 'ancillary_variables', None)
        if ancillary_variables is not None:
            string.extend(('', '%sAncillary variables' % indent0))
            if not complete:
                x = ['%s%s' % (indent1, repr(f)) for f in ancillary_variables]
                string.extend(x)
            else:
                for f in ancillary_variables:
                    string.append(f.dump(level=level+1, complete=False,
                                         title='Ancilliary field', q='-'))
        #--- End: if

        string.append('')

        return '\n'.join(string)
    #--- End: def

    def equals(self, other, rtol=None, atol=None, traceback=False):
        '''

True if two fields are logically equal, False otherwise.

:Parameters:

    other :
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `cf.ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `cf.RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns: 

    out : bool
        Whether or not the two instances are equal.

**Examples**

>>> f.Conventions
'CF-1.0'
>>> g = f.copy()
>>> g.Conventions = 'CF-1.5'
>>> f.equals(g)
True

In the following example, two fields differ only by the long name of
their time coordinates. The traceback shows that they differ in their
domains, that they differ in their time coordinates and that the long
name could not be matched.

>>> g = f.copy()
>>> g.coord('time').long_name = 'something_else'
>>> f.equals(g, traceback=True)
Coordinate: Different long_name: 'time', 'something else'
Coordinate: Different long_name: 'time', 'latitude in rotated pole grid'
Coordinate: Different long_name: 'time', 'longitude in rotated pole grid'
Domain: Different coordinate: <CF Coordinate: time(12)>
Field: Different 'domain': <CF Domain: (73, 96, 12)>, <CF Domain: (73, 96, 12)>
False

'''
        return super(Field, self).equals(other, rtol=rtol, atol=atol, 
                                         traceback=traceback,
                                         ignore=('Conventions',))
    #---End: def

    def expand_dims(self, arg=None, axis=0):
        '''

Expand the shape of the data array in place.

Insert a new size 1 axis, corresponding to a given position in the
data array shape.

:Parameters:

    arg : str, optional
        The dimension to insert. if specified, the dimension should
        exist in the field's domain and is identified by its standard
        name or by the domain's internal dimension name. By default,
        insert a new dimension which doesn't yet exist in the field's
        domain.

    axis : int, optional
        Position (amongst axes) where new axis is to be inserted. By
        default, insert at position 0.

:Returns:

    None

**Examples**

>>> f.expand_dims()
>>> f.expand_dims(axis=1)
>>> f.expand_dims('height')
>>> f.expand_dims('height', 3)
>>> f.expand_dims('dim1', 3)

'''
        domain          = self.domain
        dimension_sizes = domain.dimension_sizes
        dimensions      = domain.dimensions
        data_dimensions = dimensions['data']

        if arg is None:
            dim = domain.new_dimension_identifier()

        elif arg in dimension_sizes:
            if dimension_sizes[arg] != 1:
                raise ValueError(
                    "Can't insert a dimension with size > 1 into %s: '%s' (size %d)" %
                    (self.__class__.__name__, arg, dimension_sizes[arg]))

            if arg in data_dimensions:
                raise ValueError(
                    "Can't insert a duplicate dimension into %s: '%s'" %
                    (self.__class__.__name__, arg))

            dim = arg
           
        else:
            key = self.coord(arg, key=True, exact=True)

            if key is None or len(dimensions[key]) > 1:
                raise ValueError("9889asdasdasdaaaaaas 98123 gto dch axis")

            dim = dimensions[key][0]

            if dimension_sizes[dim] > 1:
                raise ValueError(
                    "Can't insert into %s Data a dimension with size > 1: '%s' ('%s') (size %d)" %
                    (self.__class__.__name__, dim, arg, dimension_sizes[dim]))

            if dim in data_dimensions:
                raise ValueError(
                    "Can't insert into %s Data a duplicate dimension: '%s' ('%s')" %
                    (self.__class__.__name__, dim, arg))
        #--- End: if

        super(Field, self).expand_dims(axis, dim, domain.direction(dim))

        data_dimensions.insert(axis, dim)

        # Make sure that dimension_sizes is up to date
        if dim not in dimension_sizes:
            dimension_sizes[dim] = 1

        return dim
    #--- End: def

    def indices(self, **kwargs):
        '''
Return the indices to the data array which correspond to coordinate
values.

Any index for a dimension of the data array for which no coordinates
are specified is returned as a full slice (i.e. ``slice(None)``).

Size 1 coordinates for dimensions of the domain which are not spanned
by the data array may be specified, but no corresponding index will be
returned.

:Parameters:

    kwargs : optional
        Keyword names identify coordinates; and keyword values specify
        the coordinate values which are to be reinterpreted as indices
        to the field's data array. By default, indices equivalent to
        `Ellipsis` are returned.

        Coordinates are identified by their exact
        `~Coordinate.identity` or by their dimension's identifier in
        the field's domain.

        A keyword value is a condition which is evaluated by checking
        where the coordinate's data array is equals to it. The
        locations where the conditions are satisfied are interpreted
        as indices to the field's data array. If a condition is a
        scalar ``x`` then this is equivalent to the `cf.Comparison`
        object ``cf.eq(x)``.

:Returns:

    out : tuple
        
**Examples**

>>> f.indices(lat=0.0, lon=0.0)
(slice(0, 1, 1), slice(0, 1, 1))
>>> f.indices(lon=cf.lt(0.0), lon=cf.set([0, 3.75]))
(slice(0, 32, 1), slice(0, 2, 1))
>>> f.indices(lon=cf.lt(0.0), lon=cf.set([0, 356.25]))
(slice(0, 32, 1), slice(0, 96, 96))
>>> f.indices(lon=cf.lt(0.0), lon=cf.set([0, 3.75, 356.25]))
(slice(0, 32, 1), [0, 1, 95])

'''
        if not kwargs:
            return (slice(None),) * self.ndim
        
        domain = self.domain

        seen_dimensions = []

        # Initialize indices
        indices = [slice(None)] * self.ndim

        # Loop round slice criteria
        for identity, value in kwargs.iteritems():

            key = self.coord(identity, key=True, one_d=True, exact=True)

            if key is None:
                raise ValueError(
                    "Can't find indices: Unknown coordinate indentity: '%s'" %
                    identity)

            if key.startswith('dim'):
                # Dimension coordinate
                dim = key
            else:
                # Auxiliary coordinate
                dim = domain.dimensions[key][0]

            if dim in seen_dimensions:
                raise ValueError(
                    "Can't find indices: Two coordinates share the same dimension")

            seen_dimensions.append(dim)

            if dim not in domain.dimensions['data']:
                continue

            coord = domain[key]

            coord_match = (coord.Data == value)

            if not coord_match.any():
                raise IndexError(
                    "Can't find indices: No indices found for '%s' values: %s" %
                    (identity, value))

            # Add the index to the right place in the list of indices.
            pos          = domain.dimensions['data'].index(dim)
            indices[pos] = coord_match.array
        #--- End: for

        return tuple(parse_indices(self, tuple(indices)))
    #--- End: def

    def insert_data(self, data, dimensions=None, copy=True, replace=True,
                    finalize=True):
        '''

Insert a new data array into the field in place, preserving internal
consistency.

Note that the data array's missing data value, if it has one, is not
transferred to the field.

:Parameters:

    data : cf.Data
        The new data array.

    dimensions : sequence of str, optional
        A list of dimension identifiers (``'dimN'``), stating the
        dimensions, in order, of the data array.

        The ``N`` part of each identifier should be replaced by an
        integer greater then or equal to zero such that either a) each
        dimension identifier is the same as one in the field's domain,
        or b) if the domain has no dimensions, arbitrary integers
        greater then or equal to zero may be used, the only
        restriction being that the resulting identifiers are unique.

        By default these dimension identifiers will be the sequence of
        consecutive dimension identifiers ``'dim0'`` up to ``'dimM'``,
        where ``M`` is the number of dimensions of the data array, or
        an empty sequence if the data array is a scalar.

        If the field's domain has any of the data array's dimensions,
        then their sizes must correspond to those of the field's data
        array.

    copy : bool, optional
        If False then the new data array is not deep copied prior to
        insertion. By default the new data array is deep copied.

    replace : bool, optional
        If False then do not replace an existing data array. By
        default an data array is replaced with *data*.
   
:Returns:

    None

**Examples**

>>> f.dimension_sizes
{'dim0': 1, 'dim1': 3}
>>> f.insert_data(cf.Data([[0, 1, 2]]))

>>> f.dimension_sizes
{'dim0': 1, 'dim1': 3}
>>> f.insert_data(cf.Data([[0, 1, 2]]), dimensions['dim0', 'dim1'])

>>> f.dimension_sizes
{}
>>> f.insert_data(cf.Data([[0, 1], [2, 3, 4]]))
>>> f.dimension_sizes
{'dim0': 2, 'dim1': 3}

>>> f.insert_data(cf.Data(4))

>>> f.insert_data(cf.Data(4), dimensions=[])

>>> f.dimension_sizes
{'dim0': 3, 'dim1': 2}
>>> data = cf.Data([[0, 1], [2, 3, 4]])
>>> f.insert_data(data, dimensions=['dim1', 'dim0'], copy=False)

>>> f.insert_data(cf.Data([0, 1, 2]))
>>> f.insert_data(cf.Data([3, 4, 5]), replace=False)
ValueError: Can't initialize data: Data already exists
>>> f.insert_data(cf.Data([3, 4, 5]))

'''
        if self._hasData and not replace:
            raise ValueError(
                "Can't set data: Data already exists and replace=%s" %
                replace)

        domain = self.domain

        if data.isscalar:
            if dimensions: 
                raise ValueError(
"Can't set data: Wrong number of dimensions for data array: %s" %
dimensions)

            dimensions = []

        elif dimensions is not None: 
            if len(set(dimensions)) != data.ndim:
                raise ValueError(
"Can't set data: Wrong number of dimensions for data array: %s" %
dimensions)
            
            for dim, size in izip(dimensions, data.shape):
                try:
                    domain.insert_dimension(size, dim, replace=False)
                except ValueError:
                    raise ValueError(
"Can't set data: Incompatible domain size for dimension '%s' (%d)" %
(dim, size))                

        else:
            # dimensions is None
            dimensions = domain.dimensions.get('data', None)
            
            if dimensions is not None:
                if len(set(dimensions)) != data.ndim:
                    raise ValueError(
"Can't set data: Wrong number of dimensions for data array: %s" %
dimensions)
            else:
                dimensions = ['dim%d' % N for N in range(data.ndim)]
            #--- End: if

            for dim, size in izip(dimensions, data.shape):
                try:
                    domain.insert_dimension(size, dim, replace=False)
                except ValueError:
                    raise ValueError(
"Can't set data: Incompatible domain size for dimension '%s' (%d)" %
(dim, size))                
        #--- End: if

        domain.dimensions['data'] = list(dimensions)

        if copy:
            data = data.copy()

        self.Data = data

        if finalize:
            self._finalize_data(domain.directions())
#            data_dimensions        = data.dimensions
#            domain_data_dimensions = domain.dimensions['data']
#            if data_dimensions != domain_data_dimensions:
#                dim_name_map = {}
#                for k, v in izip(data_dimensions, domain_data_dimensions):
#                    dim_name_map[k] = v
#
#                data.change_dimension_names(dim_name_map)
#            #--- End: if
#
#            data.override_directions(domain.directions(), True)
#        #--- End: if
    #--- End: def

    def match(self, prop={}, attr={}, coord={}, cellsize={}):
        '''

Determine whether or not a field matches the given conditions.

Conditions may be specified on the field's:

* CF properties
* Attributes
* Coordinate values
* Coordinate cell sizes.

A match occurs if all of the given conditions are satified.

:Parameters:

    prop : dict, optional
        Dictionary for which each key/value pair is a CF property name
        and a condition for the property to be tested against. A match
        occurs if the CF property value equals the condition. The
        condition may be a scalar or a `cf.Comparison` object.

        In general, a scalar condition ``x`` is equivalent to the
        `cf.Comparison` object ``cf.eq(x)``, unless ``x`` is a
        string. In this case, ``x`` is automatically interpreted as
        ``^x$`` and is treated as a regular expression pattern as
        recognised by the :py:obj:`re` module. For example, the string
        ``'.*wind'`` would be equivalent to ``'^.*wind$'``. To prevent
        interpretation as a regular expression, replace the string
        with a comparison object, as in the general case. For example,
        ``cf.eq('.*wind')`` would only match a property whose value
        was a string with those same six characters.

    attr : dict, optional
        Dictionary for which each key/value pair is an attribute name
        and a condition for the attribute to be tested against. A
        match occurs if the attribute value equals the condition. See
        the *prop* parameter for details on how to specify the
        condition. In addition, for the `cf.Units` attribute the
        condition is passed if the attribute is equivalent (rather
        than equal) to the condition.

    coord : dict, optional
        Dictionary for which each key/value pair is a coordinate's
        identity and a condition for the coordinate's data array to be
        tested against. A match occurs if at least one element of the
        coordinate's data array equals the condition. The condition
        may be a scalar or a `cf.Comparison` object. A scalar
        condition ``x`` is equivalent to the `cf.Comparison` object
        ``cf.eq(x)``.

    cellsize : dict, optional
        Dictionary for which each key/value pair is a coordinate's
        indentity and a condition for the coordinate's cell sizes to
        be tested against. The condition may be a scalar or a
        `cf.Comparison` object. A scalar condition ``x`` is equivalent
        to the `cf.Comparison` object ``cf.eq(x)``. Whether or not a
        match occurs also depends on the *coord* parameter:

           * If the coordinate has not also been specified with the
             *coord* parameter then a match occurs if all of the
             coordinate's cell sizes equal the condition.

           * If the coordinate has also been specified with the
             *coord* parameter then a match occurs if all of the cells
             meeting the *coord* condition equal the cell size
             condition.

:Returns:

    out : bool
        True if the field satisfies the given criteria, False
        otherwise.

**Examples**

>>> print f
Data            : air_temperature(time, latitude, longitude)
Cell methods    : time: mean
Dimensions      : time(12) = [15, ..., 345] days since 1860-1-1
                : latitude(73) = [-90, ..., 90] degrees_north
                : longitude(96) = [0, ..., 356.25] degrees_east
                : height(1) = [2] m

>>> f.match(prop={'standard_name': 'air_temperature'})
True        
>>> f.match(prop={'standard_name': ['air_temperature']})
True        
>>> f.match(prop={'standard_name': cf.set(['air_temperature', 'air_pressure'])})
True        
>>> f.match(prop={'standard_name': '.*temperature.*'})
True        
>>> f.match(prop={'standard_name': cf.set(['.*temperature.*', 'air_pressure'])})
True        
>>> f.match(prop={'standard_name': '.*pressure.*'})
False       
            
>>> f.match(prop={'Units': 'K'})
True        
>>> f.match(prop={'Units': cf.Units('1.8 K @ 459.67')})
True        
>>> f.match(prop={'Units': cf.set([cf.Units('Pa'), 'K'])})
True        
>>> f.match(prop={'Units': cf.Units('Pa')})
False       
            
>>> f.match(prop={'cell_methods': 'time: mean'})
True        
>>> f.match(prop={'cell_methods': cf.CellMethods('time: mean')})
True
>>> f.match(attr={'cell_methods': ['time: mean', 'time: max']})
True
>>> f.match(prop={'cell_methods': cf.CellMethods('time: max')})
False
>>> f.match(prop={'cell_methods': 'time: mean time: min')})
False

>>> f.match(coord={'latitude': 0})
False
>>> f.match(coord={'latitude': cf.set([0, cf.gt(30)]})
True

>>> f.match(cellsize={'time': cf.wi(28, 31, 'days')})
True

'''
        prop = prop.copy()

        # ------------------------------------------------------------
        # Try to match other cell methods
        # ------------------------------------------------------------
        cell_methods = prop.pop('cell_methods', None)

        if cell_methods is not None:
            if not hasattr(self, 'cell_methods'):
                return False

            if not isinstance(cell_methods, (tuple, list)):
                cell_methods = (cell_methods,)

            match = False
            for v in cell_methods:
                if isinstance(v, basestring):
                    v = CellMethods(v)

                if self.cell_methods.has_cellmethods(v):
                    match = True
                    break
                else:
                    continue
            #--- End: for
            if not match:
                return False
        #--- End: if

        # ------------------------------------------------------------
        # Try to match other properties and attributes
        # ------------------------------------------------------------
        if not super(Field, self).match(prop=prop, attr=attr):
            return False

        # Still here? Then the properties and attributes have matched.

        # ------------------------------------------------------------
        # Try to match coordinate values
        # ------------------------------------------------------------
        
        # Keep a note of found coordinates to save us possibly having
        # to look them up again if there are cellsize conditions
        coords = {}

        if coord:
            # Some coordinate conditions have been been specified

            # Loop round coordinate criteria
            for identity, condition in coord.iteritems():
                if condition is None:
                    continue

                c = self.coord(identity, exact=True)
                if not c:
                    # The field does not have a coordinate with this
                    # identity, so it does not match.
                    return False

                coord_match = (c.Data == condition)

                if not coord_match.any():
                    return False

                coords[identity] = (c, coord_match)
            #--- End: for
        #--- End: if

        # ------------------------------------------------------------
        # Try to match coordinate cell sizes
        # ------------------------------------------------------------
        if cellsize:
            # Some coordinate conditions have been been specified

            for identity, condition in cellsize.iteritems():
                if condition is None:
                    continue

                if identity in coords:
                    c, coord_match = coords[identity]
                else:
                    c = self.coord(identity, exact=True)
                    if not c:
                        # The field does not have a coordinate with
                        # this name, so it does not match.
                        return False            
                #--- End: if

                if not c._isbounded:
                    if condition == 0: # dch this needs work - what if condition is Comparison?
                        continue
                    else:
                        # The coordinate does not have any bounds and
                        # the specified cell size is not zero, so it
                        # does not match.
                        return False
                #--- End: if

                b = c.bounds.Data
                cell_sizes = abs(b[...,1] - b[...,0])
                cell_sizes.squeeze('bounds')

                bounds_match = (cell_sizes == condition)

                if identity in coords: 
                    # Coordinate conditions have also been specified,
                    # so return False if any of the matching
                    # coordinate cells do not match the cell size
                    # condition.
                    if not ((bounds_match & coord_match) == coord_match).all():
                        return False

                else:
                    # No coordinate conditions have been specified, so
                    # return False if any coordinate cells do not
                    # match the cell size condition.
                    if not bounds_match.all():
                        return False

            #--- End: for
        #--- End: if

        return True
    #--- End: def

    def flip(self, axes=None):
        '''

Flip dimensions of the data array and domain in place.

:Parameters:

    axes : sequence, optional
        The dimensions to be flipped. By default all dimensions are
        flipped. Dimensions to be flipped may be identified with one
        of, or a sequence of any combination of:

            * A dimension's standard name.
            * The integer position of a dimension in the data array.
            * The field's domain's internal name of a dimension

:Returns:

    None

**Examples**

>>> f.flip()
>>> f.flip('time')
>>> f.flip(1)
>>> f.flip('dim2')
>>> f.flip(['time', 1, 'dim2'])

'''
        if axes is not None:
            axes = self._parse_axes(axes, 'flip', ignore=True)

        # Flip the requested dimensions in the field's data array
        axes = self.Data.flip(axes)

        # Flip any coordinate and cell measures which span the flipped
        # dimensions
        domain     = self.domain
        dimensions = domain.dimensions['data']
        flip_dims  = [dimensions[i] for i in axes]

        dimensions = domain.dimensions

        for key, variable in domain.iteritems():
            axes = [i
                    for i, dim in enumerate(dimensions[key])
                    if dim in flip_dims]
            if axes:
                variable.flip(axes)
    #--- End: def

    def remove_data(self):
        '''

Remove the fields's data array in-place, preserving internal
consistency.

The field's `units` and `calendar` properties are preserved.

Note that this will never remove a dimension from the field's domain.

:Returns: 

    out : cf.Data
        The removed data array.

**Examples**

>>> f.Data
<CF Data: [1, ..., 9] m>
>>> f.remove_data()
>>> f._hasData
False

'''
        if not self._hasData:
            raise ValueError("Can't remove data array: Doesn't exist")

        self.domain.dimensions.pop('data', None)

        data = self.get_data()
        del self.Data

        return data
    #--- End: def

    def subset(self, *args, **kwargs):
        '''
'subset' is obselete. Please use 'select' instead.
'''
        raise NotImplementedError("'subset' is obselete. Please use 'select' instead")
    #--- End: def

    def select(self, *args, **kwargs):
        '''

Return the field if it matches the given conditions.

:Parameters:

    args, kwargs :
        As for the `match` method.

:Returns:

    out : cf.Field
        The field as an object identity, if it matches the given
        conditions.

:Raises:

    ValueError :
        If the field does not match the conditions.

'''
        if self.match(*args, **kwargs):
            return self
        else:
            raise ValueError("%s does not match the conditions" %
                             self.__class__.__name__)
    #--- End: def

    def setitem(self, value, indices=Ellipsis, condition=None, masked=None,
                ref_mask=None, hardmask=None):
        '''

Set selected elements of the data array in place.

The value to which the selected elements of the data array will be set
may be any object which is broadcastable across the selected elements,
but some options insist that the value is logically scalar.

A subspace of elements may be selected by indices of the data array or
by locations where coordinates satisfy given conditions. A further
selection method may also be included (such as'only set elements whose
data array value is less than zero'), which applies only to the
previously selected subspace.

If the data array's mask is to be treated as a hard mask then it will
remain unchanged after the assignment with the one exception of the
assignment value being `cf.masked`, in which case selected unmasked
elements will always be masked.

If and only if the value to be assigned is `cf.masked` then the
selected elements of data array's *mask* will be set to True
(masked). This is consistent with the behaviour of numpy masked
arrays.

Note the following equivalences and alternative means of assignment:

======================================  =================================================== 
setitem method                          Alternative method
======================================  =================================================== 
``f.setitem(value)``                    ``f.subspace[...]=value``

``f.setitem(value, indices)``           ``f.subspace[indices]=value``

``f.setitem(value, coord_conditions)``  ``f.subspace[f.indices(**coord_conditions)]=value``

``f.setitem(cf.masked, indices)``       ``f.setmask(True, indices)``
======================================  =================================================== 

:Parameters:

    value : array-like
        The value to which the selected elements of the data array
        will be set. Must be an object which is broadcastable across
        the selected elements. The special case of *value* being a
        field is equivalent to providing the field's `cf.Data` object,
        or its `array` attribute. Some element selection options
        insist that *value* is logically scalar.
        
    indices : optional
        Select elements of the data array to be set as indices or
        coordinate conditions as would be accepted by the field's
        `subspace` attribute. By default, the entire data array is
        considered.
 
        Only elements of the data array described by *indices*, and
        where other conditions (if any) are met, are set to
        *value*. How masked elements of the data array are treated
        depends on the *hardmask* parameter. The *value* must be any
        object broadcastable across the shape implied by *indices*.

    condition : scalar or Comparison, optional
        A condition applied to each element of the data array
        specified by *indices* which determines whether or not that
        element is set to the logically scalar *value*. The condition
        is evaluated by checking if each element equals the condition,
        and if it does then that element is set to *value*. How masked
        elements of the data array are treated depends on the
        *hardmask* parameter. If *condition* is a scalar then this is
        equivalent to the `cf.Comparison` object
        ``cf.eq(condition)``. By default there is no selection by data
        array condition.

    masked : bool, optional
        If False then each element of the data array specified by
        *indices* which is unmasked is set to the logically scalar
        *value*. If True then each element of the data array specified
        by *indices* which is masked is unmasked and set to the
        logically scalar *value*, regardless of the *hardmask* is
        parameter. By default no there is no selection by data array
        mask.

    ref_mask : array-like, optional
        Each element of the data array specified by *indices* and
        which corresponds to an element which evaluates to False from
        the reference mask *ref_mask* is set to the logically scalar
        *value*. *ref_mask* may be any object which is broadcastable
        across the shape implied by *indices*. How masked elements of
        the data array are treated depends on the *hardmask*
        parameter. By default there is no seelction by reference mask.

    hardmask : bool, optional
        If True then any selected elements of the data array which are
        masked *will not* be unmasked and assigned to. If False then
        any selected elements of the data array which are masked will
        be unmasked and assigned to. By default, the value of the
        instance's *hardmask* attribute is used.

:Returns:

    None

**Examples**

>>> f.setitem(273.15)
>>> f.setitem(273.15, dict(longitude=cf.wi(210, 270, 'degrees_east'),
                           latitude=cf.wi(-5, 5, 'degrees_north'))
>>> f.setitem(1, masked=False)
>>> f.setitem(2, ref_mask=g)

Mask each data array element which equals -999.0:

>>> f.setitem(cf.masked, condition=-999.0)

'''
        if isinstance(indices, dict):
            indices = self.indices(indices)

        if not isinstance(value, Variable):
            if value is not numpy_ma_masked:
                if not isinstance(value, Data):
                    value = Data(value, units=self.Units)
                    
                if value.size > 1:
                    raise ValueError("Can't assign size %d array to Field" %
                                     value.size)
            #--- End: if

        elif isinstance(value, self.__class__):
           value = _conform_for_assignment(self, value, 'setmask')
           value = value.Data

        elif value.size > 1:
            raise ValueError("8787----6766666667") 
        #--- End: if

        # ------------------------------------------------------------
        # Set elements of self.Data
        # ------------------------------------------------------------
        super(Field, self).setitem(value,
                                   indices=indices,
                                   condition=condition,
                                   masked=masked,
                                   ref_mask=ref_mask,
                                   hardmask=hardmask)  
    #--- End: def

    def setmask(self, value, indices=Ellipsis):
	'''

Set selected elements of the data array's mask in place.

Missing entries in the data array are signified by True elements of
the data array's mask.

The value to which the selected elements of the mask will be set may
be any object which is broadcastable across the selected elements. The
value may be of any data type but will be evaluated as boolean.

The mask may be effectively removed by setting every element to False::

   >>> f.setmask(False)

Unmasked elements are set to the field's fill value.

Note that if and only if the value to be assigned is logically scalar
and evaluates to True then ``f.setmask(value, indices)`` is equivalent
to ``f.setitem(cf.masked, indices)``. This is consistent with the
behaviour of numpy masked arrays.

:Parameters:

    value : array-like
        The value to which the selected elements of the mask will be
        set. Must be an object which is broadcastable across the
        selected elements. *value* may be of any data type but will be
        evaluated as boolean. The special case of *value* being a
        field is equivalent to providing the field's `cf.Data` object,
        or its `array` attribute.

    indices : optional
        Select elements of the mask to be set as indices or coordinate
        conditions as would be accepted by the field's `subspace`
        attribute. By default the entire mask is set.
 
        Only elements of the mask described by *indices* are set with
        *value*.

:Returns:

    None

**Examples**

>>> f.shape
(2, 3, 4)
>>> f.setmask(False)
>>> f.setmask(False, indices=dict(longitude=cf.wo(-30, 30, 'degrees')))
>>> f.setmask([1, 0, 0, 1])
>>> f.setmask([[[False]], [[True]]], indices=(slice(None), 0, 0))

>>> g.shape
(3, 4)
>>> f.setmask(g.mask)
>>> f.setmask(g.mask.subspace[0:2, :], indices=1)

The mask may be inverted as follows:

>>> f.setmask(~f.mask)

Use the `setitem` method to mask where a condition is met:

>>> f.setitem(cf.masked, condition=cf.gt(15))

'''
        if not isinstance(value, Variable):
            if not isinstance(value, Data):
                value = Data(value, units=self.Units)
                
            if value.size > 1:
                raise ValueError("2323")

        elif isinstance(value, self.__class__):
           value = _conform_for_assignment(self, value, 'setmask')
           value = value.Data

        elif value.size > 1:
            raise ValueError("2323----6767") 
        #--- End: if

        if isinstance(indices, dict):
            indices = self.indices(indices)

        # ------------------------------------------------------------
        # Set elements of the data array mask
        # ------------------------------------------------------------
        super(Field, self).setmask(value, indices=indices)
    #--- End: def

    def spans(self, identities, spans_all=False):
        '''
'''
        s = self.domain.analyse()
        
        if not spans_all:
            for identity in identities:
                if identity in s['id_to_dim']:
                    return True

            return False
        else:
            for identity in identities:
                if identity not in s['id_to_dim']:
                    return False

            return True
    #--- End: def

    def squeeze(self, axes=None, domain=False):
        '''

Remove size 1 dimensions from the field's data array in place.

:Parameters:

    axes : (sequence of) str or int, optional

        The size 1 axes to be removed. By default, all size 1 axes are
        removed. Size 1 axes for removal may be identified with one
        of, or a sequence of any combination of zero or more of:

            * A dimension's standard name.
            * The integer position of a dimension in the data array.
            * The field's domain's internal name of a dimension

        If an empty sequence is given then no dimensions are removed.

    domain : bool, optional
        If True then also remove the dimensions from the field's
        domain.


:Returns:

    None

**Examples**

>>> f.squeeze()
>>> f.squeeze('time')
>>> f.squeeze(1)
>>> f.squeeze('dim2')
>>> f.squeeze([1, 'time', 'dim2'])
>>> f.squeeze('height', domain=True)
>>> f.squeeze([])

'''
        if axes is not None:
            axes = self._parse_axes(axes, 'squeeze', ignore=True)

        # Squeeze the field's data array
        axes = self.Data.squeeze(axes)

        # Remove the squeezed dimensions from the domain's list of
        # data dimensions
        if axes:            
            data_dimensions = self.domain.dimensions['data']

            if domain:
                dims = [data_dimensions[i] for i in axes]
                self.domain.squeeze(dims)
            #--- End: if

            data_dimensions[:] = [dim
                                  for i, dim in enumerate(data_dimensions)
                                  if i not in axes]
    #--- End: def

    def transpose(self, axes=None):
        '''

Permute the dimensions of the data array in place.

:Parameters:

    axes : sequence, optional
        The new order of the data array. By default, reverse the
        dimensions' order, otherwise the axes are permuted according
        to the values given. The values of the sequence may be any
        combination of:

            * A dimension's standard name.
            * The integer position of a dimension in the data array.
            * The field's domain's internal name of a dimension.

:Returns:

    None

**Examples**

>>> f.ndim
3
>>> f.transpose()
>>> f.transpose(['latitude', 'time', 'longitude'])
>>> f.transpose([1, 0, 2])
>>> f.transpose((1, 'time', 'dim2'))

'''
        if axes is not None:
            axes = self._parse_axes(axes, 'transpose')

        # Transpose the field's data array
        axes = self.Data.transpose(axes)

        # Reorder the list of dimensions in the domain
        dimensions    = self.domain.dimensions['data']
        dimensions[:] = [dimensions[i] for i in axes]
    #--- End: def

    def unsqueeze(self):
        '''

Insert size 1 dimensions from the field's domain into its data array in
place.

All size 1 dimensions which are not already spanned by the field's
data array are inserted.

:Returns:

    None

**Examples**

>>> print f
Data            : air_temperature(time, latitude, longitude)
Cell methods    : time: mean
Dimensions      : time(1) = [15] days since 1860-1-1
                : latitude(73) = [-90, ..., 90] degrees_north
                : longitude(96) = [0, ..., 356.25] degrees_east
                : height(1) = [2] m
Auxiliary coords:
>>> f.unsqueeze()
>>> print f
Data            : air_temperature(height, time, latitude, longitude)
Cell methods    : time: mean
Dimensions      : time(1) = [15] days since 1860-1-1
                : latitude(73) = [-90, ..., 90] degrees_north
                : longitude(96) = [0, ..., 356.25] degrees_east
                : height(1) = [2] m
Auxiliary coords:

'''
        domain          = self.domain
        dimensions      = domain.dimensions
        data_dimensions = dimensions['data']

        # --------------------------------------------------------
        # Unsqueeze everything
        # --------------------------------------------------------
        # Expand any the data
        for dim, size in domain.dimension_sizes.iteritems():
            if size == 1 and dim not in data_dimensions:
                self.expand_dims(dim)
        #--- End: for

        return
    #--- End: def

#--- End: class


# ====================================================================
#
# SubspaceField object
#
# ====================================================================

class SubspaceField(SubspaceVariable):
    '''

'''
    __slots__ = []

    def __getitem__(self, indices):
        '''
x.__getitem__(indices) <==> x[indices]

'''
        field = self.variable

        if indices is Ellipsis:
            return field.copy()

        # Parse the index
        indices = parse_indices(field, indices)
        
        # Initialise the output field
        new = field.copy(_omit_Data=True)

        ## Work out if the indices are equivalent to Ellipsis and
        ## return if they are.
        #ellipsis = True
        #for index, size in izip(indices, field.shape):
        #    if index.step != 1 or index.stop-index.start != size:
        #        ellipsis = False
        #        break
        ##--- End: for
        #if ellipsis:
        #    return new

        # ------------------------------------------------------------
        # Subspace the field's data
        # ------------------------------------------------------------
        new.Data = field.Data[tuple(indices)]

        # ------------------------------------------------------------
        # Subspace ancillary variables.
        # 
        # If this is not possible for a particular ancillary variable
        # then it will be discarded from the output field.
        # ------------------------------------------------------------
        s = None
        if hasattr(field, 'ancillary_variables'):
            new.ancillary_variables = AncillaryVariables()

#            s = field.domain.analyse()

            for anc in field.ancillary_variables:
                dim_map = anc.domain.map_dims(field.domain)
                anc_indices = []
                flip_dims = []

                for adim in anc.domain.dimensions['data']:
                    if anc.domain.dimension_sizes[adim] == 1:
                        # Size 1 dimensions are always ok
                        anc_indices.append(slice(None))
                        continue

                    if adim not in dim_map:
                        # Unmatched size > 1 dimensions are not ok
                        anc_indices = None
                        break

                    dim = dim_map[adim]
                    if dim in field.domain.dimensions['data']:
                        # Matched dimensions spanning the data arrays
                        # are ok
                        i = field.domain.dimensions['data'].index(dim)
                        anc_indices.append(indices[i])
                        if anc.Data.directions[adim] != new.Data.directions[dim]:
                            flip_dims.append(adim)
                    else:
                        anc_indices = None
                        break                      
                #--- End: for

                if anc_indices is not None:
                    # We have successfully matched up each dimension
                    # of the ancillary variable's data array with a
                    # unique dimension in the parent field's data
                    # array, so we can keep a subspace of this
                    # ancillary field      
                    if flip_dims:
                        anc = anc.copy()
                        anc.flip(flip_dims)
                    #--- End: if   
                    new.ancillary_variables.append(anc.subspace[tuple(anc_indices)])
            #--- End: for

            if not new.ancillary_variables:
                del new.ancillary_variables
        #--- End: if

        # -----------------------------------------------------------
        # Subspace the domain
        # -----------------------------------------------------------
        domain = new.domain
        for i, dim in enumerate(domain.dimensions['data'][:]):
            # Continue if no slicing to be done on this dimension
            if indices[i] == slice(None, None, None): # dch ??
                continue

            for key in domain:

                if dim not in domain.dimensions[key]:
                    # This domain component does not span the current
                    # dimension so continue
                    continue

                dice        = [slice(None)] * domain[key].ndim
                pos         = domain.dimensions[key].index(dim)
                dice[pos]   = indices[i]
                domain[key] = domain[key].subspace[tuple(dice)]

                # Update the domain's dimension size
                domain.dimension_sizes[dim] = domain[key].shape[pos]
            #--- End: for
        #--- End: for

        if domain.transforms:
            if s is None:
                s = domain.analyse()        
                
            broken = []

            for key, transform in domain.transforms.iteritems():
                if transform.isgrid_mapping:
                    # Don't need to subspace grid mapping transforms
                    # since they contain no fields
                    continue

                # Still here? Then try to subspace a formula_terms
                # transform.
                for term, variable in transform.iteritems():
                    if not isinstance(variable, Field):
                        continue

                    # Still here? Then try to subspace a formula_terms
                    # field.
                    dim_map = variable.domain.map_dims(domain)
                    v_indices = []
                    flip_dims = []

                    for vdim in variable.domain.dimensions['data']:
                        if variable.domain.dimension_sizes[vdim] == 1:
                            # We can always index a size 1 dimension
                            # of the data array
                            v_indices.append(slice(None))
                            continue
                        
                        if vdim not in dim_map:
                            # Unmatched size > 1 dimensions are not ok
                            v_indices = None
                            break

                        dim = dim_map[vdim]
                        if dim in domain.dimensions['data']:
                            # We can index a matched dimension which
                            # spans the data array
                            i = domain.dimensions['data'].index(dim)
                            v_indices.append(indices[i])
                            if variable.Data.directions[vdim] != new.Data.directions[dim]:
                                flip_dims.append(vdim)
                        else:
                            v_indices = None
                            break                      
                    #--- End: for

                    if v_indices is not None:
                        # This formula terms field is subspaceable
                        if flip_dims:
                            variable = variable.copy()
                            variable.flip(flip_dims)
                        #--- End: if
                        transform[term] = variable.subspace[tuple(v_indices)]
                    else:
                        # This formula terms is broken
                        broken.append(key)
                        break
                #--- End: for
            #--- End: for
                
            # Get rid of broken formula terms
            for key in broken:
                domain.remove_transform(key)
#                del domain.transforms[key]                
#                for coord in new.domain.itercoordinates():
#                    if key in getattr(coord, 'transforms', []):
#                        coord.transforms.remove(key)
#            #--- End: for
#
#            for coord in new.domain.itercoordinates():
#                if hasattr(coord, 'transforms') and not coord.transforms:
#                    del coord.transforms
#            #--- End: for
        #--- End: if

        return new
    #--- End: def

    def __call__(self, **kwargs):
        '''

Return a subspace of the field defined by coordinate values.

:Parameters:

    kwargs : optional
        Keyword names identify coordinates; and keyword values specify
        the coordinate values which are to be reinterpreted as indices
        to the field's data array.

        Coordinates are identified by their exact identity or by their
        dimension's identifier in the field's domain.

        A keyword value is a condition, or sequence of conditions,
        which is evaluated by finding where the coordinate's data
        array equals each condition. The locations where the
        conditions are satisfied are interpreted as indices to the
        field's data array. If a condition is a scalar ``x`` then this
        is equivalent to the `cf.Comparison` object ``cf.eq(x)``.

:Returns:

    out : cf.Field

**Examples**

>>> f.indices(lat=0.0, lon=0.0)
>>> f.indices(lon=cf.lt(0.0), lon=cf.set([0, 3.75]))
>>> f.indices(lon=cf.lt(0.0), lon=cf.set([0, 356.25]))
>>> f.indices(lon=cf.lt(0.0), lon=cf.set([0, 3.75, 356.25]))

'''
        field = self.variable

        if not kwargs:
            return field.copy()

        return field.subspace[field.indices(**kwargs)]
    #--- End: def

#--- End: class


def _conform_for_assignment(ref, other, method):
    '''

Conform `other` so that it is ready for metadata-unaware assignment
broadcasting across `ref`.

Called by the setitem and setmask methods.

`other` is not changed in place.

:Parameters:

    ref : cf.Field
        The field that `other` is conformed against.

    other : cf.Field
        The field to conform.

    method : str
        The name of the method calling this function. Used for
        meaningful error messages.

:Returns:

    out : cf.Field
        The conformed version of `other`.

**Examples**

>>> new = _conform_for_assignment(f, value, 'setitem')

'''
    # Analyse each domain
    s = ref.domain.analyse()
    v = other.domain.analyse()

    if s['warnings'] or v['warnings']:
        raise ValueError("Can't setitem: %s" % (s['warnings'] or v['warnings']))

    # Find the set of matching dimensions
    matching_ids = set(s['id_to_dim']).intersection(v['id_to_dim'])
    if not matching_ids:
         raise ValueError("Can't %s: No matching dimensions" % method)

    # ------------------------------------------------------------
    # Check that any matching dimensions defined by auxiliary
    # coordinates are done so in both fields.
    # ------------------------------------------------------------
    for identity in matching_ids:
        if (identity in s['id_to_aux']) + (identity in v['id_to_aux']) == 1:
            raise ValueError(
"Can't %s: '%s' dimension defined by auxiliary in only 1 field" %
(method, identity))
    #--- End: for

    # Copy other, as we could be about to change it.
    other = other.copy()

    # ------------------------------------------------------------
    # Check that all undefined dimensions in value have size 1,
    # and squeeze any such dimensions.
    # ------------------------------------------------------------
    for dim1 in v['undefined_dims']:
        if other.domain.dimension_sizes[dim1] == 1:
            other.squeeze(dim1, domain=True)
        else:
            raise ValueError(
                "Can't %s: Can't broadcast size %d undefined dimension" %
                (method, other.domain.dimension_sizes[dim1]))
    #--- End: for

    # ------------------------------------------------------------
    # Check that all of value's unmatched but defined dimensions
    # have size 1, and squeeze any such dimensions.
    # ------------------------------------------------------------
    for identity in set(v['id_to_dim']).difference(matching_ids):
        dim1 = v['id_to_dim'][identity]
        if other.domain.dimension_sizes[dim1] == 1:
            other.squeeze(dim1, domain=True)
        else:
            raise ValueError(
                "Can't %s: Can't broadcast size %d '%s' dimension" %
                (method, other.domain.dimension_sizes[dim1], identity))
    #--- End: for

    # ------------------------------------------------------------
    # Permute the axes of other.Data so that they are in the same
    # orer as their matching counterparts in ref.Data
    #
    # For example, if   ref.Data is        P T Z Y X A
    #              and  other.Data is            Y X T
    #              then other.Data becomes       T Y X
    # ------------------------------------------------------------
    axes = []       
    for dim0 in ref.domain.dimensions['data']:            
        identity = s['dim_to_id'][dim0]
        if identity in matching_ids:
            dim1 = v['id_to_dim'][identity]                
            if dim1 in other.domain.dimensions['data']:
                axes.append(dim1)
    #--- End: for
    other.transpose(axes)

    # ------------------------------------------------------------
    # Insert size 1 dimensions into other.Data to match dimensions
    # in ref.Data which other.Data doesn't have.
    #
    # For example, if   ref.Data is        P T Z Y X A
    #              and  other.Data is            T Y X
    #              then other.Data becomes 1 T 1 Y X 1
    # ------------------------------------------------------------
    for i, dim0 in enumerate(ref.domain.dimensions['data']):
        identity = s['dim_to_id'][dim0]
        if identity in matching_ids:
            dim1 = v['id_to_dim'][identity]
            if dim1 not in other.domain.dimensions['data']:
                other.expand_dims(axis=i)
        else:
             other.expand_dims(axis=i)
    #--- End: for

    # ----------------------------------------------------------------
    # Make sure that matching dimensions have the same directions
    # ----------------------------------------------------------------
    for identity in matching_ids:
        dim1 = v['id_to_dim'][identity]
        if other.domain.dimension_sizes[dim1] > 1:
            dim0 = s['id_to_dim'][identity]
            if other.Data.directions[dim1] != ref.Data.directions[dim0]:
                other.flip(dim1)
    #--- End: for

    return other
#--- End: def
