from numpy import argsort as numpy_argsort
from numpy import dtype   as numpy_dtype

from operator  import attrgetter, itemgetter
from itertools import izip

from .ancillaryvariables import AncillaryVariables
from .coordinate         import AuxiliaryCoordinate
from .fieldlist          import FieldList
from .transform          import Transform
from .units              import Units
from .functions          import (flat, RTOL, ATOL, equals, SET_FREE_MEMORY,
                                 hash_array)

from .data.data import Data

dtype_float = numpy_dtype(float)
dtype_str   = numpy_dtype(str)

# --------------------------------------------------------------------
# Global properties, as defined in Appendix A of the CF conventions.
# --------------------------------------------------------------------
_global_properties = set(('comment',
                          'Conventions',
                          'history',
                          'institution',
                          'references',
                          'source',
                          'title',
                          ))

# --------------------------------------------------------------------
# Data variable properties, as defined in Appendix A of the CF
# conventions, without those which are not simple. And less
# 'long_name'.
# --------------------------------------------------------------------
_data_properties = set(('add_offset',
                        'cell_methods',
                        '_FillValue',
                        'flag_masks',
                        'flag_meanings',
                        'flag_values',
                        'missing_value',
                        'scale_factor',
                        'standard_error_multiplier',
                        'standard_name',
                        'units',
                        'valid_max',
                        'valid_min',
                        'valid_range',
                        ))

_no_units = Units()

class _Meta(object):
    '''

A summary of a field's metadata.

'''
    _canonical_units = {}
    '''
doof
'''

    _canonical_cell_methods = []
    '''
woof
'''

    def __init__(self, f, rtol=None, atol=None, messages=False,
                 no_strict_units=False, no_strict_identity=False,
                 equal_all=False, equal_ignore=(), equal=(),
                 exist_all=False, exist_ignore=(), exist=(),
                 dimension=()):
        '''

**initialization**

:Parameters:

    f : Field

    messages : bool, optional
        See the `aggregate` function for details.

    no_strict_units : bool, optional
        See the `aggregate` function for details.

    no_strict_identity : bool, optional
        See the `aggregate` function for details.

    rtol : float, optional
        See the `aggregate` function for details.

    atol : float, optional
        See the `aggregate` function for details.
   
    dimension : (sequence of) str, optional
        See the `aggregate` function for details.

**Examples**

'''
        self._nonzero = False

        self.messages = messages
        
        # ------------------------------------------------------------
        # Field
        # ------------------------------------------------------------
        self.field    = f
        self._hasData = f._hasData
        self.identity = f.identity()

        if self.identity is None:
            if not no_strict_identity and self._hasData:
                if messages:
                    print(
"Field has data but no identity and no_strict_identity=%s: %s" %
(no_strict_identity, repr(f)))
                return
        else:
            if not self._hasData:
                if messages:
                    print(
"Field has identity but no data and no_strict_identity=%s: %s" %
(no_strict_identity, repr(f)))
                return
        #--- End: if

        domain = f.domain
 
        # ------------------------------------------------------------
        # Promote properties to 1-d, size 1 auxiliary coordinates
        # ------------------------------------------------------------
        for prop in dimension:
            value = f.getprop(prop, None)
            if value is None:
                continue

            coord = AuxiliaryCoordinate(properties={'long_name': prop},
                                        attributes={'id': prop},
                                        data=Data([value], units=''),
                                        copy=False)
            dim = domain.new_dimension_identifier()
            domain.insert_aux_coordinate(coord, dimensions=[dim], copy=False)
            f.delprop(prop)
        #--- End: for

        self.units = self.canonical_units(f, self.identity,
                                          no_strict_units=no_strict_units)

        self.cell_methods = self.canonical_cell_methods(rtol=rtol, atol=atol)

        # ------------------------------------------------------------
        # Formula_terms
        # ------------------------------------------------------------
        self.formula_terms = {}
        self.transform_ids = {}
        for transform_id, transform in domain.transforms.iteritems():
            if not transform.isgrid_mapping:
                name = transform.name
                self.formula_terms[name] = transform
                self.transform_ids[name] = transform_id
        #--- End: for

        # ------------------------------------------------------------
        # Ancillary variables
        # ------------------------------------------------------------
        self.ancillary_variables = self.find_ancillary_variables()
        if self.ancillary_variables is None:            
            return
           
        # ------------------------------------------------------------
        # Coordinate and cell measure arrays
        # ------------------------------------------------------------
        self.hash_values  = {}
        self.first_values = {}
        self.last_values  = {}
        self.first_bounds = {}
        self.last_bounds  = {}

        self.id_to_dim = {}
        self.dim_to_id = {}

        aux_coords_1d = {}
        aux_coords_Nd = {}
        for aux, coord in domain.aux_coords().iteritems():
            if coord.ndim <= 1:
                aux_coords_1d[aux] = coord
            else:
                aux_coords_Nd[aux] = coord
        #--- End: for
            
        # ----------------------------------------------------------------
        # 1-d dimension and auxiliary coordinates
        # ----------------------------------------------------------------
        self.all_identities = set()
        self.dim = {}
        self.sort_indices = {}
        self.sort_keys    = {}
    
        for dim in domain.dimension_sizes:
    
            # List some information about each 1-d coordinate which spans
            # this dimension. The order of elements in this is arbitrary,
            # as ultimately it will get sorted by the 'name' key values.
            #
            # For example:
            # >>> info
            # [{'name': 'time', 'key': 'dim0', 'units': <CF Units: ...>},
            #  {'name': 'forecast_ref_time', 'key': 'aux0', 'units': <CF Units: ...>}]
            info = []
    
            if dim in domain: 
                # This dimension of the domain has a dimension coordinate
                coord = domain[dim]

                identity = self.coord_has_identity_and_data(coord)
                if identity is None:
                    return

                # Find the canonical units for this dimension
                # coordinate
                units = self.canonical_units(coord, identity,
                                             no_strict_units=no_strict_units)
    
                info.append({'identity'  : identity,
                             'key'       : dim,
                             'units'     : units,
                             'isbounded' : coord._isbounded,
                             'transforms': self.find_transforms(coord)})

                dim_coord_id = identity
    
            else:
                # This dimension of the domain does not have a dimension
                # coordinate
                dim_coord_id = None
            #--- End: if
    
            # Find the 1-d auxiliary coordinates which span this
            # dimension
            aux_coords = {}
            for aux in aux_coords_1d.keys():
                if dim in domain.dimensions[aux]:
                    aux_coords[aux] = aux_coords_1d.pop(aux)
            #--- End: for
    
            for aux, coord in aux_coords.iteritems():
                
                identity = self.coord_has_identity_and_data(coord)
                if identity is None:
                    return
    
                # Find the canonical units for this 1-d auxiliary
                # coordinate
                units = self.canonical_units(coord, identity,
                                             no_strict_units=no_strict_units)

                info.append({'identity'  : identity,
                             'key'       : aux,
                             'units'     : units,
                             'isbounded' : coord._isbounded,
                             'transforms': self.find_transforms(coord)})
            #--- End: for
    
            if not info:
                if messages:
                    print(
"Can't aggregate %s: A dimension has no 1-d coordinates" % repr(f))
                return
            #--- End: if
    
            # Sort the info by identity
            info.sort(key=itemgetter('identity'))
    
            # Find the canonical identity for this dimension
            identity = info[0]['identity']

            self.dim[identity] = \
                {'ids'       : tuple([i['identity']   for i in info]),
                 'keys'      : tuple([i['key']        for i in info]),
                 'units'     : tuple([i['units']      for i in info]),
                 'isbounded' : tuple([i['isbounded']  for i in info]),
                 'transforms': tuple([i['transforms'] for i in info])}
            
            d = self.dim[identity]
            if dim_coord_id is not None:
                d['dim_coord_index'] = d['ids'].index(dim_coord_id)
            else:
                d['dim_coord_index'] = None
    
            # Map the domain's dimensions to their canonical names
            self.id_to_dim[identity] = dim
            self.dim_to_id[dim]      = identity
        #--- End: for
    
        # Create a sorted list of the canonical dimension names
        self.dim_ids = sorted(self.dim)
    
        self.calculate_hash_values = set(self.dim_ids)

        # ------------------------------------------------------------
        # N-d auxiliary coordinates
        # ------------------------------------------------------------
        self.aux = {}
        for aux, coord in aux_coords_Nd.iteritems():
           
            identity = self.coord_has_identity_and_data(coord)
            if identity is None:
                return

            # Find this N-d auxiliary coordinate canonical units
            units = self.canonical_units(coord, identity,
                                         no_strict_units=no_strict_units)
            
            # Find this N-d auxiliary coordinate's canonical dimension
            # names
            dim_ids = [self.dim_to_id[dim] for dim in domain.dimensions[aux]]
            dim_ids = tuple(sorted(dim_ids))

            self.aux[identity] = {'key'       : aux,
                                  'units'     : units,
                                  'dim_ids'   : dim_ids,
                                  'isbounded' : coord._isbounded,
                                  'transforms': self.find_transforms(coord)}
        #--- End: for
    
        # ------------------------------------------------------------
        # Cell measures
        # ------------------------------------------------------------
        self.cm = {}
        info    = {}
        for key, cm in domain.cell_measures().iteritems():
            
            if not self.cell_measure_has_data_and_units(cm):
                return

            # Find the canonical units for this cell measure
            units = self.canonical_units(cm, cm.identity(),
                                         no_strict_units=no_strict_units)
            
            # Find this cell measure's canonical dimension names
            dim_ids = [self.dim_to_id[dim] for dim in domain.dimensions[key]]
            dim_ids = tuple(sorted(dim_ids))
            
            if units in info:
                # Check for ambiguous cell measures, i.e. those which have
                # the same units and span the same dimensions
                for value in info[units]:
                    if dim_ids == value['dim_ids']:
                        if messages:
                           print(
"Can't aggregate %s: Ambiguous '%s' cell measures" % (repr(f), repr(units)))
                        return
            else:
                info[units] = []
            #--- End: if
    
            info[units].append({'key'    : key,
                                'dim_ids': dim_ids})
        #--- End: for
    
        # For each cell measure's canonical units, sort the info by
        # dimension names
        for units, value in info.iteritems():
            value.sort(key=itemgetter('dim_ids'))        
            self.cm[units] = {'keys'   : tuple([i['key']     for i in value]),
                              'dim_ids': tuple([i['dim_ids'] for i in value]),
                              'hash_values': (None,) * len(value)}
        #--- End: for

        # ------------------------------------------------------------
        # Properties and attributes
        # ------------------------------------------------------------
        properties = set(f.properties)
        properties.difference_update(_data_properties)

        # Properties which must exist in both fields and are equal
        x = []

        if equal:            
            p = dict([(prop, f.getprop(prop)) for prop in equal
                      if prop not in equal_ignore and prop in properties])
            properties.difference_update(p)
            x.append(tuple(sorted(p.items())))
        else:
            x.append(None)

        if equal_all:        
            p = dict([(prop, f.getprop(prop))
                      for prop in (properties.difference(('long_name',)) - 
                                   _global_properties.union(exist_ignore))])
            properties.difference_update(p)
            x.append(tuple(sorted(p.items())))
        else:
            x.append(None)

        self.equal_properties = tuple(x)

        # Properties which might not exist in either field and are not
        # necessarily equal
        self.properties = properties        

        # Properties which must exist in both fields but are not
        # necessarily equal
        if exist_all:
            exist_properties = properties.difference(('long_name',))
            exist_properties.difference_update(_global_properties)
        else:
            exist_properties = set()

        if exist:
            exist_properties.update(exist)

        exist_properties.difference_update(exist_ignore)

        self.exist_properties = tuple(sorted(exist_properties))

        # Attributes
        self.attributes = set(('file',))

        # ----------------------------------------------------------------
        # Still here? Then create the structural signature.
        # ----------------------------------------------------------------
        self.structural_signature()

        # Initialize the flag which indicates whether or not this
        # field has already been aggregated
        self.aggregated_field = False

        self._nonzero = True
    #--- End: def

    def __nonzero__(self):
        '''
x.__nonzero__() <==> bool(x)

'''
        return self._nonzero
    #--- End: if

    def __repr__(self):
        '''
x.__repr__() <==> repr(x)

'''
        return '<CF %s: %s>' % (self.__class__.__name__,
                                repr(getattr(self, 'field', None)))
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''
        strings = []
        for attr in sorted(self.__dict__):
            strings.append('%s.%s = %s' % (self.__class__.__name__, attr,
                                           repr(getattr(self, attr))))
            
        return '\n'.join(strings)
    #--- End: def

    def canonical_units(self, variable, identity, no_strict_units=False):
        '''

Updates the `_canonical_units` attribute.

:Parameters:

    variable : cf.Variable

    identity : str

    no_strict_units : bool 
        See the `aggregate` function for details.

:Returns:

    out : Units or None

**Examples**

'''
#        print  identity, '__', variable.Units
        var_units = variable.Units

        _canonical_units = self._canonical_units

        if identity in _canonical_units:
            if var_units:
                for u in _canonical_units[identity]:
                    if var_units.equivalent(u):
                        return u
                #--- End: for
    
                # Still here?
                _canonical_units[identity].append(var_units)

            elif no_strict_units or variable.dtype.kind == 'S':
                var_units = _no_units
        else:
            if var_units:
                _canonical_units[identity] = [var_units]                
            elif no_strict_units or variable.dtype.kind == 'S':
                var_units = _no_units
        #--- End: if

        # Still here?
        return var_units
    #--- End: def

    def canonical_cell_methods(self, rtol=None, atol=None):
        '''

Updates the `_canonical_cell_methods` attribute.

:Parameters:

    atol : float

    rtol : float

:Returns:

    out : CellMethods or None

**Examples**

'''
        cell_methods = getattr(self.field, 'cell_methods', None)

        if cell_methods is None:
            return None

        _canonical_cell_methods = self._canonical_cell_methods

        for cm in _canonical_cell_methods:
            if cell_methods.equivalent(cm, rtol=rtol, atol=atol):
                return cm
        #--- End: for
               
        # Still here?
        _canonical_cell_methods.append(cell_methods)

        return cell_methods
    #--- End: def

    def cell_measure_has_data_and_units(self, cm):
        '''

:Parameters:

    cm : cf.CellMethods

:Returns:

    out : bool

**Examples**

'''
        if cm.Units and cm._hasData:
            return True
        
        if self.messages:
            print("Cell measure has no units or no data: %s" %
                  repr(self.field))
    #--- End: def

    def coord_has_identity_and_data(self, coord):
        '''

:Parameters:

    coord : Coordinate

:Returns:

    out : str or None

**Examples**

'''
        identity = coord.identity()


        if identity is not None:
            all_identities = self.all_identities

            if identity in all_identities:
                if self.messages:
                    print("Field has duplicate '%s' coordinates: %s" %
                          (identity, repr(self.field)))
                return None
            #--- End: if

            if coord._hasData or (coord._isbounded and coord.bounds._hasData):
                all_identities.add(identity)
                return identity
        #--- End: if

        if self.messages:
            print("Coordinate has no identity or no data: %s" %
                  repr(self.field))
            
        # Return None
        return None
    #--- End: def

    def find_ancillary_variables(self):
        '''

:Returns:

    out : dict or None

**Examples**

'''
        field = self.field

        if not hasattr(field, 'ancillary_variables'):
            return {}

        ancillary_variables = {}
        for f in field.ancillary_variables:
            identity = f.identity()
            if identity in ancillary_variables:
                if self.messages:
                    print("Field has duplicate '%s' ancillary variables: %s" %
                          (identity, repr(self.field)))

                return None
            #--- End: if
            ancillary_variables[identity] = f
        #--- End: for

        return ancillary_variables
    #--- End: def

    def structural_signature(self, global_properties=None,
                             ignore_properties=None):
        '''

:Returns:

    out : tuple

**Examples**

'''
        f = self.field    
      
        # Initialize the structual signature the presence of a data
        # array, the identity, the canonical units and the canonical
        # cell methods
#        print  _Meta._canonical_units
        signature = [self._hasData, self.identity, self.units, self.cell_methods]
#        print '================================================================='
#        print signature, hash(signature[2])
#        print hash(tuple(signature))

        # Add properties
        signature_append = signature.append

        signature_append(self.equal_properties)
        
        signature_append(self.exist_properties)

        # Add Flags
        signature_append(getattr(f, 'Flags', None))
        
        # Add valid_min, valid_max, valid_range 
        signature.extend((getattr(f, 'valid_min'  , None),
                          getattr(f, 'valid_max'  , None),
                          getattr(f, 'valid_range', None)))

        # Add standard_error_multiplier
        signature_append(getattr(f, 'standard_error_multiplier', None))
        
        # Add ancillary variables to the structural signature
        if self.ancillary_variables:
            signature_append(tuple(sorted(self.ancillary_variables)))
        else:
            signature_append(None)
        
        # Add 1-d dimensions and 1-d auxiliary coordinates
        x = [(self.dim[identity]['ids'],
              self.dim[identity]['units'],
              self.dim[identity]['isbounded'],
              self.dim[identity]['transforms']) for identity in self.dim_ids]
        signature_append(tuple(x))
        
        x = [False if self.dim[identity]['dim_coord_index'] is None else True
             for identity in self.dim_ids]
        signature_append(tuple(x))
        
        # Add N-d auxiliary coordinates
        x = [(identity,
              self.aux[identity]['units'],
              self.aux[identity]['dim_ids'],
              self.aux[identity]['isbounded'],
              self.aux[identity]['transforms']) for identity in sorted(self.aux)]
        signature_append(tuple(x))
        
        # Add cell measures
        x = [(units,
              self.cm[units]['dim_ids']) for units in sorted(self.cm)]
        signature_append(tuple(x))
        
        self.signature = tuple(signature)
    #--- End: def

    def find_transforms(self, coord):
        '''

:Parameters:

    coord : Coordinate

:Returns:

    out : tuple or None

**Examples**

'''
        if not hasattr(coord, 'transforms'):
            return None
        
        transforms = self.field.domain.transforms
        transforms = [transforms[t] for t in coord.transforms.values()]

        # Formula terms        
        formula_terms = [t for t in transforms if not t.isgrid_mapping]
        if formula_terms:
            formula_terms = tuple(sorted([t.name for t in formula_terms]))
        else:
            formula_terms = ()

        # Grid mappings
        grid_mappings = [t for t in transforms if t.isgrid_mapping]
        if grid_mappings:
            grid_mappings.sort(key=attrgetter('name'))
            grid_mappings = tuple([tuple(sorted(t.items())) for t in grid_mappings])
            return formula_terms + grid_mappings
        #--- End: if

        return formula_terms
    #--- End: def

#--- End: class

def aggregate(fields, messages=False, no_strict_units=False, no_overlap=False,
              contiguous=False, _delete_meta=True, no_strict_identity=False,
              equal_all=False, equal_ignore=(), equal=(),
              exist_all=False, exist_ignore=(), exist=(),
              dimension=(), concatenate=True, copy=True, debug=0):
    '''

Aggregate fields into as few fields as possible.

The aggregation of fields may be thought of as the combination fields
into each other to create a new field that occupies a larger domain.

Using the CF aggregation rules, input fields are separated into
aggregatable groups and each group (which may contain just one field)
is then aggregated to a single field. These aggregated fields are
returned in a field list.

:Parameters:

    fields : (sequence of) Field or FieldList
        The field or fields to aggregated.

    messages : bool, optional
        If True then print messages giving reasons why particular
        fields have not been aggregated.

    no_overlap : bool, optional
        If True then require that aggregated fields have adjacent
        dimension coordinate cells which do not overlap (but they may
        share common boundary values). Ignored if the dimension
        coordinates do not have bounds. See the *contiguous*
        parameter.

    contiguous : bool, optional
        If True then require that aggregated fields have adjacent
        dimension coordinate cells which partially overlap or share
        common boundary values. Ignored if the dimension coordinates
        do not have bounds. See the *no_overlap* parameter.

    no_strict_units : bool, optional
        If True then assume that fields or their components (such as
        coordinates) with the same identity but missing units all have
        equivalent (but unspecified) units, so that aggregation may
        occur. By default such fields are not aggregatable.

    no_strict_identity : bool, optional
        If True then treat fields with data arrays but no identities
        as having equal (but unspecified) identities, so that
        aggregation may occur. By default such fields are not
        aggregatable.

    equal_all : bool, optional
        If True then require that aggregated fields have the same set
        of non-standard CF properties (including long_name), with the
        same values. See the *concatenate* parameter.

    equal_ignore : (sequence of) str, optional
        Specify CF properties to omit from any properties specified by
        or implied by the *equal_all* and *equal* parameters.

    equal : (sequence of) str, optional
        Specify CF properties for which it is required that aggregated
        fields all contain the properties, with the same values. See
        the *concatenate* parameter.

    exist_all : bool, optional
        If True then require that aggregated fields have the same set
        of non-standard CF properties (including long_name), but not
        requiring the values to be the same. See the *concatenate*
        parameter.

    exist_ignore : (sequence of) str, optional
        Specify CF properties to omit from the properties specified by
        or implied by the *exist_all* and *exist* parameters.

    exist : (sequence of) str, optional
        Specify CF properties for which it is required that aggregated
        fields all contain the properties, but not requiring the
        values to be the same. See the *concatenate* parameter.

    dimension : (sequence of) str, optional
        Create new dimensions for each input field which has one or
        more of the given properties. For each CF property name
        specified, if an input field has the property then, prior to
        aggregation, a new dimension is created with an auxiliary
        coordinate whose datum is the property's value and the
        property itself is deleted from that field.

    concatenate : bool, optional
        If False then a CF property is omitted from an aggregated
        field if the property has unequal values across constituent
        fields or is missing from at least one constituent field. By
        default a CF property in an aggregated field is the
        concatenated collection of the distinct values from the
        constituent fields, delimited with the string
        ``' :AGGREGATED: '``.

    copy : bool, optional
        If False then do not copy fields prior to
        aggregation. **WARNING:** Setting this option to False may
        change input fields or field lists in place, and the returned
        fields will not be independent of the input fields. However,
        if the input variable is overwritten by the output field list
        (``f = cf.aggregate(f)``) then setting this option to False
        can improve performance.

    debug : int, optional
        Print debugging information for each input field. If 1 then
        print each field's structural signature. If 2 the print the
        entire metadata summary for each field, which includes the
        structural signature. By default no debugging information is
        printed.

:Returns:

    out : FieldList
        The aggregated fields.
    
**Examples**

The following six fields comprise eastward wind at two different times
and for three different atmospheric heights for each time:

>>> f
[<CF Field: eastward_wind(latitude(73), longitude(96)>,
 <CF Field: eastward_wind(latitude(73), longitude(96)>,
 <CF Field: eastward_wind(latitude(73), longitude(96)>,
 <CF Field: eastward_wind(latitude(73), longitude(96)>,
 <CF Field: eastward_wind(latitude(73), longitude(96)>,
 <CF Field: eastward_wind(latitude(73), longitude(96)>]
>>> g = cf.aggregate(f)
>>> g
[<CF Field: eastward_wind(height(3), time(2), latitude(73), longitude(96)>]
>>> g[0].source
'Model A'
>>> g = cf.aggregate(f, dimension=('source',))
[<CF Field: eastward_wind(source(1), height(3), time(2), latitude(73), longitude(96)>]
>>> g[0].source
AttributeError: 'Field' object has no attribute 'source'

'''

    SET_FREE_MEMORY()

    output_fields = FieldList()

    output_fields_append = output_fields.append

    atol = ATOL()
    rtol = RTOL()

    # Parse parameters
    properties = {'equal'       : equal,
                  'exist'       : exist, 
                  'equal_ignore': equal_ignore,
                  'exist_ignore': exist_ignore}

    for key, value in properties.iteritems():
        if not value:
            continue

        if isinstance(equal, basestring):
            # If it is a string then convert to a single element
            # sequence
            properties[key] = (value,)
        else:
            try:
                value[0]
            except TypeError:
                raise TypeError("Bad type of '%s' parameter: %s" % (key, value))
    #--- End: for

    equal        = properties['equal']
    exist        = properties['exist']
    equal_ignore = properties['equal_ignore']
    exist_ignore = properties['exist_ignore']

    # ----------------------------------------------------------------
    # Group fields with the same structural signature
    # ----------------------------------------------------------------
    signatures = {}
    for f in flat(fields):

        # Create the metadata summary, including the structural
        # signature
        meta = _Meta(f, messages=messages, rtol=rtol, atol=atol,
                     no_strict_units=no_strict_units, 
                     no_strict_identity=no_strict_identity,
                     equal_all=equal_all,
                     equal_ignore=equal_ignore,
                     equal=equal,
                     exist_all=exist_all,
                     exist_ignore=exist_ignore,
                     exist=exist,
                     dimension=dimension)

        if not meta:
            # Can't find a structural signature for this field, so it
            # can't be aggregated. Put it straight into the output
            # list and move on to the next input field.
            if not copy:
                output_fields_append(f)
            else:
                output_fields_append(f.copy())

            continue
        #--- End: if

        # Append this field to the list of fields with the same
        # structural signature
        signatures.setdefault(meta.signature, []).append(meta)

#        signature = meta.signature
#        if signature not in signatures:
#            signatures[signature] = []
#        signatures[signature].append(meta)

        if debug:
            if hasattr(meta.field, 'file'):
                print 'FILE:', meta.field.file
            else:
                print 'NO FILE'

            if debug == 1:
                print 'Structural signature:', meta.signature, '\n'
            elif debug == 2:
                print meta, '\n'
        #--- End: if
    #--- End: for    

    # ----------------------------------------------------------------
    # Within each group of fields with the same structural signature,
    # aggregate as many fields as possible. Sort the signatures so
    # that independent aggregations of the same set of input fields
    # return fields in the same order.
    # ----------------------------------------------------------------
    for signature in sorted(signatures):
#        print 'SIGNATURE@', signature
        meta = signatures[signature]

        if len(meta) == 1:
            # There's only one field with this signature, so we can
            # add it straight to the output list and move on to the
            # next signature.
            if not copy:       
                output_fields_append(meta[0].field) 
            else:
                output_fields_append(meta[0].field.copy()) 

            if messages:
                print("Field has unique structural signature: %s" %
                      repr(meta[0].field))

            continue
        #--- End: if

        # ------------------------------------------------------------
        # Still here? Then there are 2 or more fields with this
        # signature which may be aggregatable. These fields need to be
        # passed through until no more aggregations are possible. With
        # each pass, the number of fields in the group will reduce by
        # one for each aggregation that occurs. Each pass represents
        # an aggregation in another dimension
        # ------------------------------------------------------------
        while 1:
            number_of_fields = len(meta)
            if number_of_fields == 1:
                break

            # --------------------------------------------------------
            # For each dimension's 1-d coordinates, create the
            # canonical hash value and first (and last) datum.
            # --------------------------------------------------------
            _create_hash_and_first_values(meta)

            # --------------------------------------------------------
            # Separate the fields with the same structural signature
            # into groups such that either within each group the
            # fields' domains differ in only one dimension or each
            # group contains only one field. Note that the
            # 'a_identity' attribute is set in the _group_fields
            # function.
            # --------------------------------------------------------
            grouped_meta = _group_fields(meta)

            # --------------------------------------------------------
            # Within each group, aggregate as many fields as possible.
            # --------------------------------------------------------
            for m in grouped_meta:

                if len(m) == 1:
                    continue

                # ----------------------------------------------------
                # Sort the fields in place by the canonical first
                # values of their 1-d coordinates.
                # ----------------------------------------------------
                _sorted_by_first_values(m)

                # ----------------------------------------------------
                # Check that the aggregating dimension's 1-d
                # coordinates don't overlap, and don't aggregate
                # anything in this group if any do.
                # ----------------------------------------------------
                if not _ok_coordinate_arrays(m, no_overlap, contiguous):
                    continue

                # ----------------------------------------------------
                # Still here? Then pass through the fields
                # ----------------------------------------------------
                while len(m) >= 2:
                    new = _aggregate_2_fields(m[0], m[1],
                                              rtol=rtol, atol=atol,
                                              contiguous=contiguous,
                                              no_overlap=no_overlap,
                                              no_strict_units=no_strict_units,
                                              messages=messages,
                                              concatenate=concatenate,
                                              copy=copy)

                    if new:
                        # Successfully aggregated these two fields
                        m[0:2] = [new] 
                    else:
                        # Couldn't aggregate these two fields, so move the
                        # first one onto the list of output fields
                        output_fields_append(m.pop(0).field)
                #--- End: while

            #--- End: for

            # Re-assemble the fields as a single list
            meta = [m for gm in grouped_meta for m in gm]

            if len(meta) == number_of_fields:
                # ----------------------------------------------------
                # No aggregation took place on this pass, so we're
                # done and can add these fields to the output list.
                # ----------------------------------------------------
                break            
        #--- End: while

        output_fields.extend((m.field for m in meta))
    #--- End: for

    SET_FREE_MEMORY(None)

    return output_fields
#--- End: def

def _create_hash_and_first_values(meta):
    '''

Updates each field's _Meta object.

:Parameters:

    meta : list of _Meta

:Returns:

    None

'''
    for m in meta:
        calculate_hash_values = m.calculate_hash_values
        if not calculate_hash_values:
            return

        domain = m.field.domain
        domain_dimensions = domain.dimensions

        m_sort_keys    = m.sort_keys
        m_sort_indices = m.sort_indices

        m_hash_values  = m.hash_values
        m_first_values = m.first_values
        m_last_values  = m.last_values

        m_id_to_dim = m.id_to_dim
        # --------------------------------------------------------
        # Create a hash value for each metadata array
        # --------------------------------------------------------
        
        # --------------------------------------------------------
        # 1-d coordinates
        # --------------------------------------------------------
        for identity in calculate_hash_values:

            m_dim_identity = m.dim[identity]

            # Find the sort indices for this dimension ...
            dim = m_id_to_dim[identity]
            if dim in domain:
                # ... which has a dimension coordinate
                m_sort_keys[dim] = dim
                if not domain[dim].Data.directions[dim]:
                    # Dimension is decreasing
                    sort_indices = slice(None, None, -1)
                    null_sort = False
                else:
                    # Dimension is increasing
                    sort_indices = slice(None)
                    null_sort = True
             
            else:
                # ... which doesn't have a dimension coordinate but
                #     does have one or more 1-d auxiliary coordinates
                aux = m_dim_identity['keys'][0]
                sort_indices = numpy_argsort(domain[aux].unsafe_array)
                m_sort_keys[dim] = aux 
                null_sort = False
            #-- End: if
            m_sort_indices[dim] = sort_indices

            hash_values  = []
            first_values = []
            last_values  = []
            first_bounds = []
            last_bounds  = []

            for key, canonical_units in izip(m_dim_identity['keys'],
                                             m_dim_identity['units']):

                coord = domain[key]
                coord_units = coord.Units

                # Change the coordinate data type if required
                if coord.dtype not in (dtype_float, dtype_str):
                    coord = coord.copy(_only_Data=True)
                    coord.dtype = dtype_float

                # Change the coordinate's units to the canonical ones
                coord.Units = canonical_units

                # Get the coordinate's data array
                array = coord.Data.unsafe_array

                if not null_sort:
                    array = array[sort_indices]

                hash_value = hash_array(array)

                first_values.append(array[0])
                last_values.append(array[-1])

                if coord._isbounded:
                    array = coord.bounds.Data.unsafe_array
                    if not null_sort:
                        array = array[sort_indices, ...]

                    hash_value = (hash_value, hash_array(array))

                    if key[:3] == 'dim':
                        # Record the bounds of the first and last
                        # (sorted) cells of a dimension coordinate
                        # (don't need to do this for an auxiliary
                        # coordinate).
                        array0 = array[0, ...]
                        array0.sort()
                        m.first_bounds[identity] = array0

                        array0 = array[-1, ...]
                        array0.sort()
                        m.last_bounds[identity] = array0
                #--- End: if
                    
                hash_values.append(hash_value)

                # Reinstate the coordinate's original units
                coord.Units = coord_units
            #--- End: for
                
            m_hash_values[identity]  = hash_values
            m_first_values[identity] = first_values
            m_last_values[identity]  = last_values
        #--- End: for

        # ------------------------------------------------------------
        # N-d auxiliary coordinates
        # ------------------------------------------------------------
        for aux in m.aux.itervalues():
#        for identity in m.aux:
 #           aux = m.aux[identity]
#            dim_ids = aux['dim_ids']
#            if not calculate_hash_values.intersection(dim_ids):
#                continue

            key = aux['key']
            
            sort_indices = [m_sort_indices[dim] 
                            for dim in domain_dimensions[key]]
            
            coord = domain[key]
            coord_units = coord.Units
            
            # Change the coordinate data type if required
            if coord.dtype not in (dtype_float, dtype_str):
                coord = coord.copy(_only_Data=True)
                coord.dtype = dtype_float
                copied = True
            else:
                copied = False
                
            # Change the coordinate's units to the canonical ones
            coord.Units = aux['units'] #canonical_units
            
            axes = [m_id_to_dim[identity] for identity in aux['dim_ids']] #dim_ids]
            if axes != coord.Data.dimensions:
                if not copied:
                    coord = coord.copy(_only_Data=True)

                coord.transpose(axes)
            #--- End: if

            # Get the coordinate's data array
            array = coord.Data.unsafe_array[tuple(sort_indices)]
            
            hash_value = hash_array(array)

            if coord._isbounded:
                sort_indices.append(Ellipsis)
                array = coord.bounds.Data.unsafe_array[tuple(sort_indices)]
                hash_value = (hash_value, hash_array(array))
                
            aux['hash_value'] = hash_value
            
            # Reinstate the coordinate's original units
            coord.Units = coord_units
        #--- End: for
            
        # ------------------------------------------------------------
        # Cell measures
        # ------------------------------------------------------------
        for units, cm in m.cm.iteritems():
#            cm = m.cm[units]

            hash_values = []
            for key, dim_ids, hash_value in izip(cm['keys'],
                                                 cm['dim_ids'],
                                                 cm['hash_values']):
#                if not calculate_hash_values.intersection(dim_ids):
#                    hash_values.append(hash_value)
#                    continue

                sort_indices = [m_sort_indices[dim]
                                for dim in domain_dimensions[key]]

                coord = domain[key]
                coord_units = coord.Units
                
                # Change the coordinate data type if required
                if coord.dtype not in (dtype_float, dtype_str):
                    coord = coord.copy(_only_Data=True)
                    coord.dtype = dtype_float
                    copied = True
                else:
                    copied = False
                    
                # Change the coordinate's units to the canonical ones
                coord.Units = units
                
                axes = [m_id_to_dim[identity] for identity in dim_ids]
                if axes != coord.Data.dimensions:
                    if not copied:
                        coord = coord.copy(_only_Data=True)
                        
                    coord.transpose(axes)
                #--- End: if
                
                array = coord.Data.unsafe_array[tuple(sort_indices)]
            
                hash_values.append(hash_array(array)) 

                # Reinstate the coordinate's original units
                coord.Units = coord_units  
            #--- End: for

            cm['hash_values'] = hash_values
        #--- End: for

        m.calculate_hash_values = set()
    #--- End: for
#--- End: def

#def _create_hash_and_first_values(meta):
#    '''
#
#Updates each field's _Meta object.
#
#:Parameters:
#
#    meta : list of _Meta
#
#:Returns:
#
#    None
#
#'''
#    for m in meta:
#        calculate_hash_values = m.calculate_hash_values
#        if not calculate_hash_values:
#            return
#
#        domain = m.domain
#
#        # --------------------------------------------------------
#        # Create a hash value for each metadata array
#        # --------------------------------------------------------
#        
#        # --------------------------------------------------------
#        # 1-d coordinates
#        # --------------------------------------------------------
#        for identity in calculate_hash_values:
#
#            m_dim_identity = m.dim[identity]
#
#            # Find the sort indices for this dimension ...
#            dim = m.id_to_dim[identity]
#            if dim in domain:
#                # ... which has a dimension coordinate
#                has_dimension_coord = True
#                m.sort_keys[dim] = dim
#                if not domain[dim].Data.directions[dim]:
#                    # Dimension is decreasing
#                    sort_indices = slice(None, None, -1)
#                else:
#                    # Dimension is increasing
#                    sort_indices = slice(None)
#            else:
#                # ... which doesn't have a dimension coordinate but
#                #     does have one or more 1-d auxiliary coordinates
#                has_dimension_coord = False
#                aux = m_dim_identity['keys'][0]
#                sort_indices = numpy.argsort(domain[aux].array)
#                m.sort_keys[dim] = aux
#            #-- End: if
#            m.sort_indices[dim] = sort_indices
#
#            hash_values  = []
#            first_values = []
#            last_values  = []
#            first_bounds = []
#            last_bounds  = []
#
#            for key, canonical_units in izip(m_dim_identity['keys'],
#                                             m_dim_identity['units']):
#
#                if has_dimension_coord:
#                    # There is a dimension coordinate => the sort
#                    # indices are a strictly monotonic sequence.
#                    if sort_indices == slice(None):
#                        coord = domain[key].copy() 
#                    else:
#                        coord = domain[key].subspace[sort_indices]
#                else:
#                    # There is no dimension coordinate but there at
#                    # least on auxiliary coordinate => the sort
#                    # indices may not be a strictly monotonic
#                    # sequence.
#                    coord = domain[key].copy()
#                    varray = coord.varray
#                    varray[...] = varray[sort_indices]
#                #--- End: if
#            
#                coord.Units = canonical_units
#
#                if coord.dtype.kind != 'S':
#                    coord.dtype = numpy.dtype(float)
#
#                hash_value = hash(coord.Data)
#
##                first_values.append(coord.Data[0])
##                last_values.append(coord.Data[-1])
#                first_values.append(coord.first_datum)
#                last_values.append(coord.first_datum)
#
#                if coord._isbounded:
#                    hash_value = (hash_value, hash(coord.bounds.Data))
#
#                    if key[:3] == 'dim':
#                        # Record the bounds of the first and last
#                        # (sorted) cells of a dimension coordinate.
#                        array = coord.bounds.subspace[0, :].array
#                        array.sort()
#                        m.first_bounds[identity] = array.squeeze() #Data(array.squeeze(), coord.Units)
#                        array = coord.bounds.subspace[-1, :].array
#                        array.sort()
#                        m.last_bounds[identity] = array.squeeze() #Data(array.squeeze(), coord.Units)
#                #--- End: if
#                    
#                hash_values.append(hash_value)
#            #--- End: for
#                
#            m.hash_values[identity]  = hash_values
#            m.first_values[identity] = first_values
#            m.last_values[identity]  = last_values
#        #--- End: for
#
#        # ------------------------------------------------------------
#        # N-d auxiliary coordinates
#        # ------------------------------------------------------------
#        for identity in m.aux:
#            aux = m.aux[identity]
#            dim_ids = aux['dim_ids']
#            if not calculate_hash_values.intersection(dim_ids):
#                continue
#
#            key = aux['key']
#            
#            sort_indices = tuple(
#                [m.sort_indices[dim] for dim in domain.dimensions[key]])
#            try:
#                coord = domain[key].subspace[sort_indices]
#            except ValueError:
#                # At least one dimension's sort indices is not a
#                # strictly monotonic sequence.
#                coord = domain[key].copy()
#                varray = coord.varray
#                varray[...] = varray[sort_indices]
#            #--- End: try
#                
#            coord.Units = aux['units']
#
#            axes = [m.id_to_dim[identity] for identity in dim_ids]
#            coord.transpose(axes)
#
#            if coord.dtype.kind != 'S':
#                coord.dtype = numpy.dtype(float)
#
#            hash_value = hash(coord.Data)
#            if coord._isbounded:
#                hash_value = (hash_value, hash(coord.bounds.Data))
#                
#            aux['hash_value'] = hash_value
#        #--- End: for
#            
#        # ------------------------------------------------------------
#        # Cell measures
#        # ------------------------------------------------------------
#        for units in m.cm:
#            cm = m.cm[units]
#
#            hash_values = []
#            for key, dim_ids, hash_value in izip(cm['keys'],
#                                                 cm['dim_ids'],
#                                                 cm['hash_values']):
#                if not calculate_hash_values.intersection(dim_ids):
#                    hash_values.append(hash_value)
#                    continue
#
#                sort_indices = tuple(
#                    [m.sort_indices[dim] for dim in domain.dimensions[key]])
#                try:
#                    cmv = domain[key].subspace[sort_indices]
#                except ValueError:
#                    # At least one dimensions sort indices is not a
#                    # strictly monotonic sequence.
#                    cmv = domain[key].copy()
#                    varray = coord.varray
#                    varray[...] = varray[sort_indices]
#                #--- End: try
#
#                cmv.Units = units
#
#                axes = [m.id_to_dim[identity] for identity in dim_ids]
#                cmv.transpose(axes)
#
#                if cmv.dtype.kind != 'S':
#                    cmv.dtype = numpy.dtype(float)
#
#                hash_values.append(hash(cmv.Data))
#            #--- End: for
#
#            cm['hash_values'] = hash_values
#        #--- End: for
#
#        m.calculate_hash_values = set()
#    #--- End: for
##--- End: def

def _group_fields(meta):
    '''
:Parameters:

    meta : list of _Meta

:Returns:

    out : list of FieldLists

'''
    if meta[0].dim_ids:
        sort_by_dim_ids = itemgetter(*meta[0].dim_ids)
        def _hash_values(m):
            return sort_by_dim_ids(m.hash_values)

        meta.sort(key=_hash_values)
    #--- End: if

    # Create a new group of potentially aggregatable fields (which
    # contains the first field in the sorted list)
    groups_of_fields = [[meta[0]]]

    for m0, m1 in izip(meta[:-1], meta[1:]):

        #-------------------------------------------------------------
        # Count the number of dimensions which are different between
        # the two fields
        # -------------------------------------------------------------
        count = 0
        hash0 = m0.hash_values
        hash1 = m1.hash_values
        for identity in m0.dim_ids:
            if hash0[identity] != hash1[identity]:
                count += 1
                a_identity = identity                
        #--- End: for

        ok = True
        if count == 1:
            # --------------------------------------------------------
            # Exactly one dimension has different 1-d coordinate
            # values
            # --------------------------------------------------------
            # Check N-d auxiliary coordinates
            for identity in m0.aux:
                if a_identity in m0.aux[identity]['dim_ids']:
                    # This matching pair of auxiliary coordinates span
                    # the aggregating dimension
                    continue

                if m0.aux[identity]['hash_value'] != m1.aux[identity]['hash_value']:
                    # This matching pair of N-d auxiliary coordinates
                    # does not span the aggregating dimension and they
                    # have different data array values
                    ok = False
                    break
            #--- End: for

            if not ok:
                break

            # Check cell measures
            for units in m0.cm:
                
                if all([a_identity in dim_ids
                        for dim_ids in m0.cm[units]['dim_ids']]):  
                    # All matching pairs of cell measures with these
                    # units span the aggregating dimension.
                    continue

#                if a_identity in m0.cm[units]['dim_ids']:
#                    continue

                if m0.cm[units]['hash_values'] != m1.cm[units]['hash_values']:
                    # There is at least one matching pair of cell
                    # measures with these units which do not span the
                    # aggregating dimension and have different data
                    # array values
                    ok = False
                    break
            #--- End: for

            if not ok:
                break  

            m0.a_identity = a_identity
            m1.a_identity = a_identity


        elif not count:   
            # --------------------------------------------------------
            # No dimensions have different 1-d coordinate values
            # --------------------------------------------------------
            ok = False
            if m0.messages:
                print("'%s' fields have identical 1-d coordinate values" %
                      m0.identity)

        else:   
            # --------------------------------------------------------
            # 2 or more dimensions have different 1-d coordinate
            # values
            # --------------------------------------------------------
            ok = False
        #--- End: if

        if ok:
            # Append field1 to this group of potentially aggregatable
            # fields
            groups_of_fields[-1].append(m1)
        else:
            # Create a new group of potentially aggregatable fields
            # (which contains field1)
            groups_of_fields.append([m1])
    #--- End: for

    return groups_of_fields
#--- End: def

def _sorted_by_first_values(meta):
    '''

Sort fields inplace

:Parameters:

    meta : list of _Meta

:Returns:

    None

''' 
    sort_by_dim_ids = itemgetter(*meta[0].dim_ids)
    def _first_values(m):
        return sort_by_dim_ids(m.first_values)

    meta.sort(key=_first_values)
#--- End: def

def _ok_coordinate_arrays(meta, no_overlap, contiguous):
    '''

Return True if the aggregating dimension's 1-d coordinates are all
aggregatable.

It is assumed that the input metadata objects have already been sorted
by the canonical first values of their 1-d coordinates.

:Parameters:

    meta : list of _Meta

    no_overlap : bool
        See the `aggregate` function for details.

    contiguous : bool
        See the `aggregate` function for details.

:Returns:

    out : bool

**Examples**

>>> if not _ok_coordinate_arrays(meta, True, False)
...     print "Don't aggregate"

'''
    m = meta[0]

    # Find the canonical identity of the aggregating dimension
    a_identity = m.a_identity

    dim_coord_index = m.dim[a_identity]['dim_coord_index']

    if dim_coord_index is not None:
        # ------------------------------------------------------------
        # The aggregating dimension has a dimension coordinate
        # ------------------------------------------------------------
        # Check for overlapping dimension coordinate cell centres
        dim_coord_index0 = dim_coord_index

        for m0, m1 in izip(meta[:-1], meta[1:]):
            dim_coord_index1 = m1.dim[a_identity]['dim_coord_index']
            if (m0.last_values[a_identity][dim_coord_index0] >=
                m1.first_values[a_identity][dim_coord_index1]):
                # Found overlap
                if m.messages:
                    print(
"Won't aggregate group containing %s: Overlapping '%s' coordinate centres" %
(repr(m.field), m.dim[a_identity]['ids'][dim_coord_index]))
                return

            dim_coord_index0 = dim_coord_index1        
        #--- End: for

        if a_identity in m.first_bounds:
            # --------------------------------------------------------
            # The dimension coordinates have bounds
            # --------------------------------------------------------
            if no_overlap:
                for m0, m1 in izip(meta[:-1], meta[1:]):
                    if (m1.first_bounds[a_identity][0] <
                        m0.last_bounds[a_identity][1]):
                        # Do not aggregate anything in this group
                        # because overlapping has been disallowed and
                        # the first cell from field1 overlaps with the
                        # last cell from field0.
                        if m.messages:
                            print(
"Won't aggregate group containing %s: no_overlap=True and overlapping '%s' coordinate bounds" %
(repr(m.field), m.dim[a_identity]['ids'][dim_coord_index]))
                        return
                #--- End: for

            else:
                for m0, m1 in izip(meta[:-1], meta[1:]):
                    m0_last_bounds  = m0.last_bounds[a_identity]        
                    m1_first_bounds = m1.first_bounds[a_identity]
                    if (m1_first_bounds[0] <= m0_last_bounds[0] or
                        m1_first_bounds[1] <= m0_last_bounds[1]):
                        # Do not aggregate anything in this group
                        # because, even though overlapping has been
                        # allowed, the first cell from field1 overlaps
                        # in an unreasonable way with the last cell
                        # from field0.
                        if m.messages:
                            print(
"Can't aggregate group containing %s: Unreasonably overlapping '%s' coordinate bounds" %
(repr(m.field), m.dim[a_identity]['ids'][dim_coord_index]))
                        return
                #--- End: for
            #--- End: if

            if contiguous:
                for m0, m1 in izip(meta[:-1], meta[1:]):
                    if (m0.last_bounds[a_identity][1] <
                        m1.first_bounds[a_identity][0]):
                        # Do not aggregate anything in this group
                        # because contiguous coordinates have been
                        # specified and the first cell from field1 is
                        # not contiguous with the last cell from
                        # field0.
                        if m.messages:
                            print(
"Won't aggregate group containing %s: contiguous=True and noncontiguous '%s' coordinate bounds" %
(repr(m.field), m.dim[a_identity]['ids'][dim_coord_index]))
                        return
                #--- End: for

#        elif contiguous:
#            # --------------------------------------------------------
#            # The dimension coordinates have no bounds and contiguous
#            # coordinates have been specified, so do not aggregate
#            # anything in this group.
#            # --------------------------------------------------------
#            if m.messages:
##                print(
#"Won't aggregate %s: contiguous=True and group has noncontiguous '%s' coordinates" %
#(repr(m.field), m.dim[a_identity]['ids'][dim_coord_index]))
#            return
        #--- End: if

    else:
        # ------------------------------------------------------------
        # The aggregating dimension does not have a dimension
        # coordinate, but it does have at least one 1-d auxiliary
        # coordinate.
        # ------------------------------------------------------------
        # Check for duplicate auxiliary coordinate values
        for i, identity in enumerate(meta[0].dim[a_identity]['ids']):
            set_of_1d_aux_coord_values    = set()
            number_of_1d_aux_coord_values = 0
            for m in meta:
                key = m.dim[a_identity]['keys'][i]
                array = m.field.domain[key].array
                set_of_1d_aux_coord_values.update(array)
                number_of_1d_aux_coord_values += array.size
                if len(set_of_1d_aux_coord_values) != number_of_1d_aux_coord_values:
                    # Found duplicate auxiliary coordinate values
                    if m.messages:
                        print(
"Won't aggregate group containing %s: Duplicate '%s' auxiliary coordinate values" %
(repr(m.field), identity))
                        return
            #--- End: for

        #--- End: for
    #--- End: if
 
    # Still here? Then the aggregating dimension does not overlap
    # between any of the fields.
    return True
#--- End: def

def _aggregate_2_fields(m0, m1, rtol=None, atol=None, messages=False,    
                        no_strict_units=False, no_overlap=False, 
                        contiguous=False, concatenate=True, copy=True):
    '''

:Parameters:

    m0 : _Meta

    m1 : _Meta

    contiguous : bool, optional
        See the `aggregate` function for details.
   
    rtol : float, optional
        See the `aggregate` function for details.

    atol : float, optional
        See the `aggregate` function for details.
   
    messages : bool, optional
        See the `aggregate` function for details.
   
    no_overlap : bool, optional
        See the `aggregate` function for details.
  
    no_strict_units : bool, optional
        See the `aggregate` function for details.
  
''' 
    if copy and not m0.aggregated_field:
        m0.field = m0.field.copy()

    a_identity = m0.a_identity
    
    # ----------------------------------------------------------------
    # Aggregate transforms
    # ----------------------------------------------------------------
    if m0.formula_terms:
        t = _aggregate_transforms(m0, m1, rtol=rtol, atol=atol,
                                  no_strict_units=no_strict_units,
                                  no_overlap=no_overlap, messages=messages,
                                  contiguous=contiguous)
        if not t:
            return
    else:
        t = None
  
    # ----------------------------------------------------------------
    # Aggregate ancillary variables
    # ----------------------------------------------------------------
    if m0.ancillary_variables:
        av = _aggregate_ancillary_variables(m0, m1, rtol=rtol,
                                            atol=atol,
                                            no_strict_units=no_strict_units,
                                            no_overlap=no_overlap,
                                            messages=messages,
                                            contiguous=contiguous)
        if not av:
            return
    else:
        av = None
 
    # Still here?
    field0 = m0.field
    field1 = m1.field

    domain0 = field0.domain
    domain1 = field1.domain

    if t:
        domain0.transforms.update(t)
#        m0.domain.transforms.update(t)

    if av:
        field1.ancillary_variables = av

#    domain0 = m0.domain
#    domain1 = m1.domain

    # ----------------------------------------------------------------
    # Map the dimensions of field1 to those of field0
    # ----------------------------------------------------------------
    dim1_name_map = {'bounds': 'bounds'}
    for identity in m0.dim_ids:
        dim1_name_map[m1.id_to_dim[identity]] = m0.id_to_dim[identity]

    # ----------------------------------------------------------------
    # In each field, find the identifier of the aggregating dimension
    # and its direction
    # ----------------------------------------------------------------
    adim0 = m0.id_to_dim[a_identity]
    adim1 = m1.id_to_dim[a_identity]

    adim_directions0 = domain0.direction(adim0)
    adim_directions1 = domain1.direction(adim1)

    # ----------------------------------------------------------------
    # Find the insertion indices into field0 for each aggregating
    # dimension partition of field1
    # ----------------------------------------------------------------
    pmsize1 = domain1[m1.sort_keys[adim1]].Data.pmsize
    if adim_directions0:
        # The aggregating dimension in field0 is increasing
        pmsize0 = domain0[m0.sort_keys[adim0]].Data.pmsize
        extend = True
        if not adim_directions1:
            # The aggregating dimension in field1 is also increasing
            reversed = True
        else:
            # The aggregating dimension in field1 is decreasing
            reversed = False
    else:
        # The aggregating dimension in field0 is decreasing
        extend = False
        if not adim_directions1:
            # The aggregating dimension in field1 is also decreasing
            reversed = False
        else:
            # The aggregating dimension in field1 is increasing
            reversed = True
    #--- End: if
            
    # ----------------------------------------------------------------
    # Find matching pairs of coordinate and cell measure variables
    # which span the aggregating dimension
    # ----------------------------------------------------------------
    # 1-d dimension and auxiliary coordinates
    spanning_variables = [(domain0[key0], domain1[key1])
                          for key0, key1 in izip(m0.dim[a_identity]['keys'],
                                                 m1.dim[a_identity]['keys'])] 
   
    hash_values0 = m0.hash_values[a_identity]
    hash_values1 = m1.hash_values[a_identity]
    for i, (hash0, hash1) in enumerate(izip(hash_values0, hash_values1)):
        try:
            hash_values0[i].append(hash_values1[i])
        except AttributeError:
            hash_values0[i] = [hash_values0[i], hash_values1[i]]
    #--- End: for

    # N-d auxiliary coordinates
    for identity in m0.aux:
        aux0 = m0.aux[identity]
        aux1 = m1.aux[identity]
        if a_identity in aux0['dim_ids']:
            spanning_variables.append((domain0[aux0['key']],
                                       domain1[aux1['key']]))

            hash_value0 = aux0['hash_value']
            hash_value1 = aux1['hash_value']
            try:
#                aux0['hash_value'].append(aux1['hash_value'])
                hash_value0.append(hash_value1)
            except AttributeError:
#                aux0['hash_value'] = [aux0['hash_value'], aux1['hash_value']]
                aux0['hash_value'] = [hash_value0, hash_value1]
    #--- End: for
    
    # Cell measures                
    for units in m0.cm:
        hash_values0 = m0.cm[units]['hash_values']
        hash_values1 = m1.cm[units]['hash_values']
        for i, (dim_ids, key0, key1) in enumerate(izip(m0.cm[units]['dim_ids'],
                                                       m0.cm[units]['keys'],
                                                       m1.cm[units]['keys'])):
            if a_identity in dim_ids:
                spanning_variables.append((domain0[key0], domain1[key1]))

                try:
                    hash_values0[i].append(hash_values1[i])
                except AttributeError:
                    hash_values0[i] = [hash_values0[i], hash_values1[i]]
    #--- End: for

    # ----------------------------------------------------------------
    # For each matching pair of coordinates and cell measures which
    # span the aggregating dimension, insert the one from field1 into
    # the one from field0
    # ----------------------------------------------------------------
    for variable0, variable1 in spanning_variables:
        variable0._insert_data(variable1, extend, reversed,
                               adim0, adim_directions0, dim1_name_map)
    #--- End: for        
        
    # ----------------------------------------------------------------
    # Insert the data array from field1 into the data array of field0
    # ----------------------------------------------------------------
    if m0._hasData:
        field0._insert_data(field1, extend, reversed,
                            adim0, adim_directions0, dim1_name_map)
        
        # Update the data dimensions in field0        
        domain0.dimensions['data'] = field0.Data.dimensions[:]        
    #--- End: if

    # Update the Aggregating Dimension size in field0
    domain0.dimension_sizes[adim0] += domain1.dimension_sizes[adim1]

    # Make sure that field0 has a standard_name, if possible.
    if getattr(field0, 'id', None) is not None:
        standard_name = getattr(field1, 'standard_name', None)
        if standard_name is not None:
            field0.standard_name = standard_name
            del field0.id
    #--- End: if

    # Update the properties in field0
    for prop in set(field0._simple_properties()) | set(field1._simple_properties()):
        value0 = field0.getprop(prop, None)
        value1 = field1.getprop(prop, None)
        if equals(value0, value1):
            continue

        if concatenate:
            if value1 is not None:
                if value0 is not None:
                    field0.setprop(prop, '%s :AGGREGATED: %s' % (value0, value1))
                else:
                    field0.setprop(prop, ' :AGGREGATED: %s' % value1)
        else:
            m0.properties.discard(prop)
            if value0 is not None:
                field0.delprop(prop)
    #--- End: for

    # Update the attributes in field0
    for attr in m0.attributes | m1.attributes:
        value0 = getattr(field0, attr, None)
        value1 = getattr(field1, attr, None)
        if equals(value0, value1):
            continue

        if concatenate:
            if value1 is not None:
                if value0 is not None:
                    setattr(field0, attr, '%s :AGGREGATED: %s' % (value0, value1))
                else:
                    setattr(field0, attr, ' :AGGREGATED: %s' % value1)
        else:
            m0.attributes.discard(attr)
            if value0 is not None:
                delattr(field0, attr)
    #--- End: for

    # Note that the field in this _Meta object has already been
    # aggregated
    m0.aggregated_field = True

    # ----------------------------------------------------------------
    # Return the _Meta object containing the aggregated field
    # ----------------------------------------------------------------
    return m0
#--- End: def

def _aggregate_transforms(m0, m1, rtol=None, atol=None, 
                          no_strict_units=False, no_overlap=False,
                          messages=False, contiguous=False):
    '''
aggregate 2 fields transforms

:Parameters:

    m0 : _Meta

    m1 : _Meta

    no_overlap : bool, optional
        See the `aggregate` function for details.

    contiguous : bool, optional
        See the `aggregate` function for details.
   
    rtol : float, optional
        See the `aggregate` function for details.

    atol : float, optional
        See the `aggregate` function for details.
   
    messages : bool, optional
        See the `aggregate` function for details.
   
    no_strict_units : bool, optional
        See the `aggregate` function for details.

:Returns:

    out : bool

'''
    a_identity = m0.a_identity

    # Check formula_terms fields
    for name, transform0 in m0.formula_terms.iteritems():
        transform1 = m1.formula_terms[name]

        new_transform = Transform()
        new_transform.name = transform0.name
          
        for term, value1 in transform1.iteritems():
            if (term not in transform0 and
                not (value1.isscalar and value1.first_datum == 0.0)):
                if messages:
                    print("Won't aggregate %s: Incompatible '%s' transform" %
                          (repr(m0.field), name))
                return
        #--- End: for

        for term, value0 in transform0.iteritems():

            if (term not in transform1 and
                not (value0.isscalar and value0.first_datum == 0.0)):
                if messages:
                    print(
"Won't aggregate %s: Incompatible '%s' transform term '%s'" %
(repr(m0.field), name, term))
                return
            #--- End: if

            value1 = transform1[term]
  
            if (isinstance(value0, basestring) and
                isinstance(value1, basestring)):
                # ----------------------------------------------------
                # Both transform values are pointers to coordinates in
                # their respective fields
                # ----------------------------------------------------
                new_transform[term] = value0
                continue
            #--- End: if
           
            if isinstance(value0, basestring) or isinstance(value1, basestring):
                # One transform value is a pointer to a coordinate and
                # the other is not
                if messages:
                    print(
"Won't aggregate %s: Incompatible '%s' transform term '%s'" %
(repr(m0.field), name, term))
                return
            #--- End: if
                        
            # --------------------------------------------------------
            # Both transform values are fields
            # --------------------------------------------------------
            x0 = _Meta(value0, messages=messages, no_strict_units=no_strict_units,
                       no_strict_identity=True)
            x1 = _Meta(value1, messages=messages, no_strict_units=no_strict_units,
                       no_strict_identity=True)

            if not (x0 and x1):
                # at least one field doesn't have a structual
                # signature.
                if messages:
                    print(
"Won't aggregate %s: Incompatible '%s' transform term '%s'" %
(repr(m0.field), name, term))
                return
            #--- End: if

            if a_identity not in x0.dim and a_identity not in x1.dim:
                # Neither field spans the aggregating dimension ...
                if value0.equivalent_data(value1, rtol=rtol, atol=atol):
                    # ... and the fields have equivalent data
                    new_transform[term] = value0
                    continue
                else:
                    # ... and the fields do not have equivalent data
                    if messages:
                        print(
"Won't aggregate %s: Incompatible '%s' transform term '%s'" %
(repr(m0.field), name, term))
                    return
            #--- End: if

            if not (a_identity in x0.dim and a_identity in x1.dim):
                # Only one of the fields spans the aggregating
                # dimension
                if messages:
                    print(
"Won't aggregate %s: Incompatible '%s' transform term '%s'" %
(repr(m0.field), name, term))
                return
            #--- End: if
            
            # Both fields span the aggregating dimension. So let's
            # aggregate them.
            new_value = aggregate((value0, value1), messages=messages,
                                  no_overlap=no_overlap, contiguous=contiguous,
                                  no_strict_units=no_strict_units,
                                  no_strict_identity=True)
            
            if len(new_value) == 2:
                # Couldn't aggregate the two formula_terms fields (because we
                # got two back instead of one).
                if messages:
                    print(
" X Won't aggregate %s: Incompatible '%s' transform term '%s'" %
(repr(m0.field), name, term))
                return
            #--- End: if

            # Successfully aggregated the transform fields
            transform0[term] = new_value[0]
            new_transform[term] = new_value[0]
        #---End: for

    #---End: for

    return {m0.transform_ids[name]: new_transform} #True
#--- End: def

def _aggregate_ancillary_variables(m0, m1, rtol=None, atol=None,
                                   no_strict_units=False, no_overlap=False,
                                   messages=False, contiguous=False):
    '''
aggregate 2 fields' ancillary variables

:Parameters:

    m0 : _Meta

    m1 : _Meta

    no_overlap : bool, optional
        See the `aggregate` function for details.

    contiguous : bool, optional
        See the `aggregate` function for details.
   
    rtol : float, optional
        See the `aggregate` function for details.

    atol : float, optional
        See the `aggregate` function for details.
   
    messages : bool, optional
        See the `aggregate` function for details.
   
    no_strict_units : bool, optional
        See the `aggregate` function for details.

:Returns:

    out : bool

'''
    a_identity = m0.a_identity

    new_ancillary_variables = AncillaryVariables()

    # Check formula_terms fields
    for identity, field0 in m0.ancillary_variables.iteritems():
        field1 = m1.ancillary_variables[identity]

        x0 = _Meta(field0, messages=messages, no_strict_units=no_strict_units,
                   no_strict_identity=False)
        x1 = _Meta(field1, messages=messages, no_strict_units=no_strict_units,
                   no_strict_identity=False)
        
        if not (x0 and x1):
            # At least one field doesn't have a structual signature.
            if messages:
                print(
                    "Won't aggregate %s: Incompatible '%s' ancillary variable" %
                    (repr(m0.field), identity))
            return
        #--- End: if
            
        if a_identity not in x0.dim and a_identity not in x1.dim:
            # Neither field spans the aggregating dimension ...
            if field0.equivalent_data(field1, rtol=rtol, atol=atol, traceback=True):
                # ... and the fields are equivalent
                new_ancillary_variables.append(field0)
                continue

            # ... and the fields are not equivalent
            if messages:
                print(
"A matching pair of ancillary variables which don't span the aggregating dimension have different values")
                print(
                    "Won't aggregate %s: Incompatible '%s' ancillary variable" %
                    (repr(m0.field), identity))
                return
        #--- End: if

        if not (a_identity in x0.dim and a_identity in x1.dim):
            if messages:
                print(
"Only one of a matching pair of ancillary variables spans the aggregating dimension")
                print(
                    "Won't aggregate %s: Incompatible '%s' ancillary variable" %
                    (repr(m0.field), identity))
            return
        #--- End: if
            
        # Both fields span the aggregating dimension. So let's
        # aggregate them.
        new_value = aggregate((field0, field1), messages=messages, 
                              no_overlap=no_overlap, contiguous=contiguous,
                              no_strict_units=no_strict_units,
                              no_strict_identity=True)
        
        if len(new_value) == 2:
            # Couldn't aggregate the two formula_terms fields (because we
            # got two back instead of one).
            if messages:
                print(
                    "Won't aggregate %s: Incompatible '%s' ancillary variable" %
                    (repr(m0.field), identity))
            return
        #--- End: if

        m0.ancillary_variables[identity] = new_value[0]
        new_ancillary_variables.append(new_value[0])
    #---End: for

    return new_ancillary_variables
#--- End: def
